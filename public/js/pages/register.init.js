$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.fn.steps.setStep = function (step)
    {

        var options = getOptions(this),
            state = getState(this);

        return _goToStep(this, options, state, step);

    };

    $.fn.serializefiles = function() {
        var obj = $(this);
        /* ADD FILE TO PARAM AJAX */
        var formData = new FormData();
        $.each($(obj).find("input[type='file']"), function(i, tag) {
            $.each($(tag)[0].files, function(i, file) {
                formData.append(tag.name, file);
            });
        });
        var params = $(obj).serializeArray();
        $.each(params, function (i, val) {
            formData.append(val.name, val.value);
        });
        return formData;
    };

    $.ajax({
        type : 'get',
        url: 'freelances',
        dataType: "json",
        success: function (data) { 
            $("#wintType1").select2({
                placeholder: "กรุณาเลือกอาชีพอิสระ",
                data: data.datas
            });
        }
    });


    var form = $(".validation-wizard").show();

    $(".validation-wizard").steps({
        headerTag: "h6",
        enableCancelButton: true,
        autoFocus: true,
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            next: "ถัดไป",
            previous: "ก่อนหน้า",
            finish: "บันทึก",
            cancel: "ยกเลิก"
        },
        onCanceled: function (event) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'mr-2 btn btn-danger'
                },
                buttonsStyling: false,
            });

            swalWithBootstrapButtons.fire({
                title: 'คุณต้องการยกเลิกใช่หรือไม่ ?',
                text: "หากยืนยันการยกเลิก ข้อมูลจะไม่ถูกบันทึก ?",
                type: 'info',
                showCancelButton: true,
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ปิดหน้าต่าง',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    //window.history.back();
                    window.location = '/';
                }
            });

        },
        onStepChanging: function (event, currentIndex, newIndex) {
            var valid = currentIndex > newIndex || (currentIndex < newIndex && (form.validate().settings.ignore = ":disabled,:hidden", form.valid()));

            /*$.fn.steps.setStep = function (step) {
                $(this).steps('next');
            };*/
            //return true;
           
            $("#wPromtpayNumber").removeClass("highlight-border");
            
            if (currentIndex == 0 && valid) {
                var channelId = $('#channelId').val();
                var isWarning = $('#isWarning').val();
                
                if (isWarning == 0 && channelId == 1) {
                    const swalButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'mr-2 btn btn-danger'
                        },
                        buttonsStyling: false,
                    });
                   
                    swalButtons.fire({
                        title: 'หมายเลขบัญชีพร้อมเพย์ที่ระบุไว้ ถูกต้องใช่หรือไม่?',
                        text: "หากไม่ถูกต้อง อาจส่งผลให้การพิจารณาจ่ายผลประโยชน์ทดแทนกรณีว่างงานของท่านเกิดความล่าช้าได้",
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonText: 'ยืนยัน',
                        cancelButtonText: 'ปิด',
                        reverseButtons: true
                    }).then((result) => {
                        if (result.value) {
                            $('#isWarning').val("1");
                            $(this).steps('next');
                            /**/
                            return true;
                        } else {
                            $("#wPromtpayNumber").addClass("highlight-border");
                            return false;
                        }
                    });
                } else {
                    return true;
                }
                
                /*var requestResult = $.ajax({
                    type: 'POST',
                    url: "cnt_job_apply",
                    async: false,
                    contentType: "application/json",
                    dataType: 'json'
                });

                console.log("Cnt = ",requestResult.responseJSON.cnt);

                return requestResult.responseJSON.cnt;*/

            } else if (newIndex == 2 && valid) {
                var isHidden = $('.form-check-inline').css('display') == 'none';
                console.log("isHidden = ",isHidden);
                console.log("haveFreelance = ",$("#haveFreelance"));
                if (!isHidden) {
                    //
                    $("#haveFreelance").prop('checked',true);
                    $("#freelance-area").show();
                    $('#freelance-table').show();
                } else {
                    var x = $("select[name='wintType[]']")
                    .map(function(){
                        if ($(this).val() !== "")
                        {
                            return $(this).val()
                        }
                    }).get();
                    console.log("x = ",x);
                    if (x.length > 0) {
                        $("#haveFreelance").prop('checked',true);
                        $("#freelance-area").show();
                        $('#freelance-table').show();
                    } else {
                        $("#haveFreelance").prop('checked',false);
                        $("#freelance-area").hide();
                        $('#freelance-table').hide();
                    }
                }
                return valid;
            } else {
                return valid;
            }

        },
        onFinishing: function (event, currentIndex) {

            if (form.validate().settings.ignore = ":disabled,:hidden", form.valid()) {
                return true;
            } else {
                Swal.fire({
                    title: "บันทึกข้อมูลไม่สำเร็จ",
                    text: "กรุณาเพิ่มอาชีพอิสระของคุณ",
                    type: "error",
                    button: "ปิด",
                });
                return false;
            }
            //return form.validate().settings.ignore = ":disabled,:hidden", form.valid();
            
        },
        onFinished: function (event, currentIndex) {
            //swal("Form Submitted!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.");

            var goAhead = true;
            
            if ($('#haveFreelance').is(':checked')) {
                $('#freelance-table').find("select[name^='wintType']").each(function(index, element){
                    //$(element).removeClass("is-invalid");
                    var value = element.value;
                    if (value == "") {
                        $('.select2-selection').each(function (i, element) {
                            var n = element.getAttribute('aria-labelledby').indexOf("wintType");
                            
                            if (n >= 0) {
                                var id = $(element).attr('data-id');
                                console.log("id = ",id);
                            }
                            
                        });
                        goAhead = false;
                        return false;
                    } else {
                        var other = $(element).attr('data-other');
                        var remark = $('#freelance-table').find("input[name='wint[]']")[index];
                        if (other && remark.value == "") {
                            goAhead = false;
                            return false;
                        }
                    }
                });
            }

            console.log("goAhead = ",goAhead);
            var x = $("select[name='wintType[]']")
            .map(function(){
                if ($(this).val() !== "")
                {
                    return $(this).val()
                }
            }).get();

            var isHidden = $('.form-check-inline').css('display') == 'none';
            if ((!isHidden && $('#ignoreJobId').is(':checked')) && x.length <= 0) {
                Swal.fire({
                    title: "บันทึกข้อมูลไม่สำเร็จ",
                    text: "หากไม่ประสงค์จะสมัครงาน กรุณาระบุข้อมูลประกอบอาชีพอิสระ อย่างน้อย 1 รายการ",
                    type: "error",
                    button: "ตกลง",
                });
            } else {
                if (goAhead) {

                    var valid = true;

                    if (valid) {

                        const swalWithBootstrapButtons = Swal.mixin({
                            customClass: {
                                confirmButton: 'btn btn-success',
                                cancelButton: 'mr-2 btn btn-danger'
                            },
                            buttonsStyling: false,
                        });
    
                        var formDatas = form.serializefiles();

                        if ($('select[name="province"] option:selected').val()) {
                            formDatas.append("province", $('select[name="province"] option:selected').val());
                        }
                        if ($('select[name="district"] option:selected').val()) {
                            formDatas.append("district", $('select[name="district"] option:selected').val());
                        }
                        if ($('select[name="subDistrict"] option:selected').val()) {
                            formDatas.append("subDistrict", $('select[name="subDistrict"] option:selected').val());
                        }
                        
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
    
                        swalWithBootstrapButtons.fire({
                            title: 'คุณต้องการบันทึก ?',
                            text: "ข้อมูลการขึ้นทะเบียนผู้ประกันตน คนว่างงานหรือไม่ ?",
                            type: 'info',
                            showCancelButton: true,
                            confirmButtonText: 'ตกลง',
                            cancelButtonText: 'ยกเลิก',
                            reverseButtons: true
                        }).then((result) => {
                            if (result.value) {
    
                                let timerInterval;
                                Swal.fire({
                                    title: 'กำลังบันทึกข้อมูล',
                                    html: '',
                                    timer: 10000,
                                    allowOutsideClick: false,
                                    onBeforeOpen: () => {
                                        const content = Swal.getContent();
                                        const $ = content.querySelector.bind(content);
    
                                        const stop = $('#stop');
                                        const resume = $('#resume');
                                        const toggle = $('#toggle');
                                        const increase = $('#increase');
    
                                        Swal.showLoading();
    
                                        timerInterval = setInterval(() => {
                                            1000
                                        }, 100)
    
                                        Swal.stopTimer();
                                    },
                                    onClose: () => {
                                        clearInterval(timerInterval);
                                    }
    
                                });

                                $.ajax({
                                    type: 'post',
                                    url: 'register',
                                    dataType: "json",
                                    data: formDatas,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function (data) {
                                        console.log("data === > ",data);
                                        clearInterval(timerInterval);
                                        Swal.close();
                                        if (data.success) {
    
                                            var msg = "ลงทะเบียนสำเร็จ";
                                            if (data.auto == 1) {
                                                msg = "ลงทะเบียน และรายงานตัวครั้งที่ 1 สำเร็จ กรุณาตรวจสอบผลการพิจารณาได้ที่ เมนูข้อมูลขอรับประโยชน์ทดแทนกรณีว่างงาน";
                                            }
    
                                            Swal.fire({
                                                title: "บันทึกข้อมูลสำเร็จ",
                                                text: msg,
                                                type: "success",
                                                button: "ตกลง",
                                                onClose: () => {
                                                    window.location = '/register/user-index';
                                                }
                                            });
    
                                        } else {
                                            Swal.fire({
                                                title: "บันทึกข้อมูลไม่สำเร็จ",
                                                text: data.message ? data.message : "กรุณาลองใหม่อีกครั้ง",
                                                type: "error",
                                                button: "ตกลง",
                                            });
    
                                        }
                                    }
                                });
    
                            }
                        });
                    }

                    
                } else {
                    Swal.fire({
                        title: "บันทึกข้อมูลไม่สำเร็จ",
                        text: "กรุณาเพิ่มอาชีพอิสระของคุณ",
                        type: "error",
                        button: "ปิด",
                    });
                }
            }

        },
        saveState: true
    }), $(".validation-wizard").validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger",
        successClass: "text-success",
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass)
            var id = element.id;
            if (id == "ignoreJobId") {
                var scrollBottom = $(window).scrollTop() + $(window).height();
                $("html, body").animate({ scrollTop: scrollBottom }, "slow");
            }
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass)
            $(element).removeClass("is-invalid");
        },
        errorPlacement: function (error, element) {
            //error.insertAfter(element)
           
            $(element).addClass("is-invalid");
        }
    });

    $(document).on('change', '#wDistrict', function() {
        var id = $(this).val();
    
        var op = "";
        $.ajax({
            type : 'get',
            contentType: "application/json; charset=utf-8",
            url : 'findTambon',
            data : { 'id' : id},
            dataType: "json",
            success : function(datas) {
                op += '<option value="">กรุณาเลือก</option>';
                for (var i=0; i< datas.tambons.length; i++){
                    
                    op += '<option value="'+datas.tambons[i].TambonID+'">'+datas.tambons[i].TambonName+'</option>';
                }
                $('#wSubDistrict').html("");
                $('#wSubDistrict').append(op);
            },
            error : function(err) {
                console.log("In error ",err);
            }
        });
    });    

    //$listJob = $('.listing-job');
    /*$('.listing-job').click(function () {

        //console.log($('#applyJobsTmp').val());
        var obj = [];

        if ($('#applyJobsTmp').val()) {
            obj = JSON.parse($('#applyJobsTmp').val());
        }

        var jobDesc = $(this).attr('data-job-desc');
        var id = $(this).attr('id');
        $('#jobId').val(id);

        //datas = datas.replace(/Array\(/g, '{').replace(/\)/g, '}').replace(/=>/g, ':');
        //var jDatas = JSON.parse('{ [EmployerName] : บริษัท พรีโม ฟู้ด แอนด์ เบฟเวอเรจ จำกัด (สนง.ใหญ่}  }');
        var parentDiv = $(this).parents('.outerDiv');

        $('.listing-job').find('.job-short-area').removeClass('active');
        $(this).find('.job-short-area').addClass('active');

        parentDiv.find('#details-box').css('display', 'block');
        var jobPosition = $(this).find('.job-position').text();
        var jobAnnouceDate = $(this).find('.job-annouce-date').text();
        var jobExpireDate = $(this).find('.job-annouce-date').attr('data-exprie');
        var jobEmployer = $(this).find('.job-employer').text();
        var jobSalary = $(this).find('.job-salary').text();
        var jobApply = $(this).attr('data-job-apply');

        const found = obj.find(element => element == id);
        //console.log("found = ",found);

        if (jobApply || found) {
            parentDiv.find('.rightDiv .bnt-apply-job').hide();
            parentDiv.find('.rightDiv .p-apply-job').show();


        } else {

           
            parentDiv.find('.rightDiv .bnt-apply-job').show();
            parentDiv.find('.rightDiv .p-apply-job').hide();

        }

        parentDiv.find('.rightDiv .present-job-position').html(jobPosition);
        parentDiv.find('.rightDiv .present-job-annouce-date').html(jobAnnouceDate);
        parentDiv.find('.rightDiv .present-job-expire-date').html(jobExpireDate);
        parentDiv.find('.rightDiv .present-job-employer').html(jobEmployer);
        parentDiv.find('.rightDiv .present-job-detail').html(jobDesc);

    });

    $('#jobmaching').click(function () {
        var category = $('#category').val();
        var categoryValue = $('#categoryValue').val();
        var employmentType = $('#EmploymentType').val();
        var jobFieldID = $('#JobFieldID').val();
        var businessTypeID = $('#ParentBusinessTypeID').val();
        var degreeID = $('#DegreeID').val();
        var ageMin = $('#Age_Min').val();
        var ageMax = $('#Age_Max').val();
        var wageMin = $('#Wage_Min').val();
        var wageMax = $('#Wage_Max').val();
        var experience = $('#Experience').val();


        //console.log("jobFieldID -> ",jobFieldID);

        $.ajax({
            type: 'get',
            url: 'searchJobs',
            data: {
                'category': category,
                'categoryValue': categoryValue,
                'employmentType': employmentType,
                'businessTypeID': businessTypeID,
                'jobFieldID': jobFieldID,
                'degreeID': degreeID,
                'ageMin': ageMin,
                'ageMax': ageMax,
                'wageMin': wageMin,
                'wageMax': wageMax,
                'experience': experience
            },
            //dataType: "html",
            success: function (data) {

                //$('#cntFreelance').attr('id', "wintType"+$id)

                $('.jobs-annouced').html(data.html);

            }
        });
    });*/

    
    init();

    $.datetimepicker.setLocale('th'); // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
         
        // กรณีใช้แบบ inline
      /*  $("#testdate4").datetimepicker({
            timepicker:false,
            format:'d-m-Y',  // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000            
            lang:'th',  // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
            inline:true  
        });    */   
         
         
    // กรณีใช้แบบ input
    $(".custom-date-picker").datetimepicker({
        timepicker:false,
        format:'d/m/Y',  // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000            
        lang:'th',  // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
        onSelectDate:function(dp,$input){
            var yearT=new Date(dp).getFullYear();  
            var yearTH=yearT+543;
            var fulldate=$input.val();
            var fulldateTH=fulldate.replace(yearT,yearTH);
            $input.val(fulldateTH);
        },
    }); 

});

function manageResignOther(obj) {
    var select = obj.target;
    var val = JSON.parse(obj.value);
    var text = obj.options[obj.selectedIndex].text;

    if (val) {
        $('#label-resign-case').show();
    } else {
        $('#label-resign-case').hide();
    }

    if (val.other == "" || val.other == "0") {

        $('#label-resign-case').text("สาเหตุการ" + text);
        $('#other-resign-case-area').hide();

        var url = 'get_resign_case';
        $.ajax({
            type: 'get',
            url: url,
            data: val,
            dataType: "json",
            beforeSend: function () {
                $('#resign-case-area').show();
                var loading = ` <div class="spinner-border text-info" role="status">
                <span class="sr-only">Loading...</span>
              </div>`;
                $('#resign-case-area').html(loading);
            },
            success: function (data) {
                $('#resign-case-area').show();
                $('#wLayoffCaseID-option').show();
                $('#resign-case-area').html(data.html);
            }
        });
    } else {
        $('#label-resign-case').text("ระบุสาเหตุ");
        $('#other-resign-case-area').show();
        $('#resign-case-area').hide();


    }
}

function showOrHide(action) {
    if (action === 'show') {
        $("#empInfomationId").show();
    } else {
        $("#empInfomationId").hide();
    }
}

//dbd_employer
$(document).on('click', '#btnDBD', function () {
    var employerCode = $("#EmployerCode").val();

    $("#wTypeOfBusiness").prop("readonly", false);
    $("#wEstablishment").prop("readonly", false);
    $("#employerAddress").prop("readonly", false);

    $("#wTypeOfBusiness").val("");
    $("#wEstablishment").val("");
    $("#employerAddress").val("");


    $('#wProvince > option[selected="selected"]').removeAttr('selected');
    $('#wProvince').removeAttr('disabled');
    
    $('#wDistrict').html("");
    $('#wDistrict').removeAttr('disabled');

    $('#wSubDistrict').html("");
    $('#wSubDistrict').removeAttr('disabled');
  
    if (employerCode.length != "") {
        let timerInterval;
        $.ajax({
            type: 'post',
            url: 'dbd_employer',
            data: {
                'EmployerCode': employerCode
            },
            //dataType: "html",
            beforeSend: function () {

                Swal.fire({
                    title: 'กำลังตรวจสอบข้อมูลนายจ้าง',
                    html: '',
                    timer: 10000,
                    onBeforeOpen: () => {
                        const content = Swal.getContent();
                        const $ = content.querySelector.bind(content);

                        const stop = $('#stop');
                        const resume = $('#resume');

                        Swal.showLoading();

                        timerInterval = setInterval(() => {
                            1000
                        }, 100)

                        Swal.stopTimer();
                    },
                    onClose: () => {
                        clearInterval(timerInterval);
                    }

                });
            },
            success: function (result) {
                clearInterval(timerInterval);
                Swal.close();
                //V1
                var data = result.data;
                //V2
                data = data.ResultList;
                //$('#cntFreelance').attr('id', "wintType"+$id)
                if (data == null || data.length == 0 || data.Code) {
                    $("#EmployerCode").val("");
                    Swal.fire({
                        title: "ไม่พบข้อมูลนายจ้าง",
                        text: data ? data.Message : "",
                        type: "error",
                        button: "ปิด",
                    });
                } else {
                     //V2
                    data = data[0];
                    console.log("data = ",data);
                    console.log("data = ",data[0]);
                    console.log("data = ",data.AddressInformations[0]);
                    if (data.StandardObjectives && data.StandardObjectives[0]) {
                        $("#wTypeOfBusiness").val(data.StandardObjectives[0].ObjectiveDescription)
                        if ($("#wTypeOfBusiness").val().length > 0) {
                            $("#wTypeOfBusiness").prop("readonly", true);
                        }
                    }

                    if (data.JuristicName_TH) {
                        $("#wEstablishment").val(data.JuristicName_TH)
                        $("#wEstablishment").prop("readonly", true);
                    }

                    if (data.AddressInformations[0]) {
                        if (data.AddressInformations[0].FullAddress) {
                            $("#employerAddress").val(data.AddressInformations[0].FullAddress)
                            $("#employerAddress").prop("readonly", true);
                        }
                        var pName= data.AddressInformations[0].Province;
                        if (pName) {
                            
                            var province = (function get_province_data() {
                                var province_data;
                                $.ajax({
                                    url: "find_province_id",
                                    async: false, 
                                    data : { 'name' : pName},
                                    //very important: else php_data will be returned even before we get Json from the url
                                    dataType: 'json',
                                    success: function (json) {
                                        province_data = json;
                                    }
                                });
                                return province_data;
                            })();

                            if (province.success) {
                                //$("#wProvince").val(province.id).change();
                                
                                $("#wProvince option[value="+province.id+"]").attr('selected','selected');
                                //$("#wProvince option[value="+province.id+"]").text(pName);
                                $('#wProvince').val(province.id);

                                $('#wProvince').attr('disabled', 'disabled');
                            }

                            var dName = data.AddressInformations[0].Ampur;
                            if (dName && province.success) {
                               
                                var district = (function get_district_data() {
                                    var district_data;
                                    $.ajax({
                                        url: "find_district_id",
                                        async: false, 
                                        data : { 'name' : dName , 'province_id' : province.id},
                                        //very important: else php_data will be returned even before we get Json from the url
                                        dataType: 'json',
                                        success: function (json) {
                                            district_data = json;
                                        },
                                        error :function (err) {
                                            console.log("err = ",err);
                                        }
                                    });
                                    return district_data;
                                })();
                                
                                if (district.success) {
                                    $("#wDistrict").attr("disable", true);
                                    var op = '<option value="'+district.id+'" selected>'+dName+'</option>';
                                   
                                    $('#wDistrict').append(op);
                                    $('#wDistrict').val(district.id);
                                    $('#wDistrict').attr('disabled', 'disabled');
                                } else {
                                    $("#wProvince").val(province.id).change();
                                }

                                var tName = data.AddressInformations[0].Tumbol;
                                if (tName && district.success) {
                                    var tumbon = (function get_tumbon_data() {
                                        var tumbon_data;
                                        $.ajax({
                                            url: "find_tumbon_id",
                                            async: false, 
                                            data : { 'name' : tName , 'district_id' : district.id},
                                            //very important: else php_data will be returned even before we get Json from the url
                                            dataType: 'json',
                                            success: function (json) {
                                                tumbon_data = json;
                                            },
                                            error :function (err) {
                                                console.log("err = ",err);
                                            }
                                        });
                                        return tumbon_data;
                                    })();
                                    if (tumbon.success) {
                                        $("#wSubDistrict").attr("disable", true);
                                        var op = '<option value="'+tumbon.id+'" selected>'+tName+'</option>';

                                        $('#wSubDistrict').append(op);
                                        $('#wSubDistrict').val(tumbon.id);
                                        $('#wSubDistrict').attr('disabled', 'disabled');
                                    } else {
                                        $("#wDistrict").val(district.id).change();
                                    }
                                }
                            }

                        }

                        var phone= data.AddressInformations[0].Phone;
                        if (phone) {
                            $("#wPhoneNumber").val(phone)
                            $("#wPhoneNumber").prop("readonly", true);
                        }

                        var email= data.AddressInformations[0].Email;
                        if (email) {
                            $("#wContactPerson").val(email)
                            $("#wContactPerson").prop("readonly", true);
                        }
                    }

                }
            }
        });
    } else {
        $("#EmployerCode").val("");
        Swal.fire({
            title: "กรุณาใส่เลขนิติบุคคลของบริษัท 13 หลัก",
            text: "",
            type: "error",
            button: "ปิด",
        });
    }
});