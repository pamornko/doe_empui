

function cancel(registerId) {
    console.log("registerId = ",registerId);
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'mr-2 btn btn-danger'
        },
        buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
        title: 'คุณต้องการยกเลิกข้อมูล ?',
        text: "ต้องการยกเลิกรายการขึ้นทะเบียนนี้หรือไม่ ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var $container = $('.table-responsive');
            
            /*$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type : 'post',
                url: '{!! URL::to('delete_register')!!}',
                data: 'RegisterID='+registerId,
                dataType: "json",
                success: function (data) { 
                    //console.log("Success -> ",data.success);
                    Swal.close();
                    if (data.success) {
                        Swal.fire({
                            title: "ยกเลิกรายการสำเร็จ",
                            text: "",
                            type: "success",
                            button: "ตกลง",
                        });
                        
                    } else {
                        Swal.fire({
                            title: "บันทึกข้อมูลไม่สำเร็จ",
                            text: "กรุณาลองใหม่อีกครั้ง",
                            type: "error",
                            button: "ตกลง",
                        });
                        
                    }
                }
            }); */
        }
    });

}

function exportExcel() {
    var RegisterNumber = $("#RegisterNumber").val();
    var Organization = $("#wOrganization").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();

    var path = '{!! URL::route('export_excel_register')!!}';
    var url = path+'?RegisterNumber='+ RegisterNumber+"&Organization="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate ;
    window.location=url;
}

function exportPDF() {
    var RegisterNumber = $("#RegisterNumber").val();
    var Organization = $("#wOrganization").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();

    var path = '{!! URL::route('export_pdf_register')!!}';
    var url = path+'?RegisterNumber='+ RegisterNumber+"&Organization="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate ;
    window.location=url;
}

function print() {
    var RegisterNumber = $("#RegisterNumber").val();
    var Organization = $("#wOrganization").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();

    var path = '{!! URL::route('print_register')!!}';
    var url = path+'?RegisterNumber='+ RegisterNumber+"&Organization="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate ;
    //window.location=url;

    window.open(url, '_blank');
}