$(document).ready(function(){
    $("#report-search").submit(function(event){
        event.preventDefault();

        if ($('#OrganizationID').val() == "") {
            toastr.error('กรุณา เลือกสำนักงาน สำหรับการค้นหาข้อมูล.', 'คำเตือน');
            $("#OrganizationID").focus();
            return 
        }

        if ($('#StartDate').val() == "" || $('#EndDate').val() == "") {
            toastr.error('กรุณา เลือกวันที่ สำหรับการค้นหาข้อมูล.', 'คำเตือน');
            return 
        }
        
        showLoadingModal();
        var dataString = $(this).serialize();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type : 'post',
            url: 'report_summary_personal_search',
            data: dataString,
            //dataType: "html",
            success: function (data) { 
                $('.table-responsive').html(data.html);
                hideLoadingModal();
            },
            error : function (err) { 
                hideLoadingModal();
            }
        }); 

    });
});

function exportPDF() {
    var Organization = $("#OrganizationID").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();

    var path = 'export_pdf_summary_insured_personal';
    var url = path+"?OrganizationID="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate ;
    window.location=url;
}

function exportExcel() {
    var Organization = $("#OrganizationID").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();

    var path = 'export_excel_summary_insured_personal';
    var url = path+"?OrganizationID="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate ;
    window.location=url;
}

function print() {
    var Organization = $("#OrganizationID").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();

    var path = 'print_summary_insured_personal';
    var url = path+"?OrganizationID="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate ;
    window.open(url, '_blank');
}