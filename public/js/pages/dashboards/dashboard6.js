/*
Template Name: Admin Pro Admin
Author: Wrappixel
Email: niravjoshi87@gmail.com
File: js
*/
$(function() {
    "use strict";
    // ============================================================== 
    // Our Visitor
    // ============================================================== 
    var registerColor = ['#2F2FA2', '#F64C72', '#A239CA', '#5CDB95', '#3FEEE6'];
    var registerCss = ['text-purple-dark', 'text-pink-dark', 'text-purple', '#5CDB95', '#3FEEE6'];

    var registerRequireColor = ['#E7717D' ,'#AFD275', '#D79922' , '#8EE4AF', '#4CAF50'];
    var registerRequireCss = ['text-pink-soft', 'text-green-soft', 'text-orange-soft', 'text-green'];

    var reportingColor = ['#40c4ff', '#2961ff', '#ff821c', '#4CAF50'];
    var reportingCss = ['text-blue-soft', 'text-blue'];

    if (!config.isEmployee) {
        $.ajax({
            type: "GET",
            url: "get_register_dash",
            //data: "{}",
            contentType: "application/json",
            dataType: "json",
            async: "true",
            cache: "false",
            success: function (result) {
                OnRegisterSuccess(result);
            },
            error: function (xhr, status, error) {
                console.log("error ",error);
            }
        });

        $.ajax({
            type: "GET",
            url: "get_register_year_dash",
            //data: "{}",
            contentType: "application/json",
            dataType: "json",
            async: "true",
            cache: "false",
            success: function (result) {
                OnRegisterYearSuccess(result);
            },
            error: function (xhr, status, error) {
                console.log("error ",error);
            }
        });

        $.ajax({
            type: "GET",
            url: "get_reporting_dash",
            //data: "{}",
            contentType: "application/json",
            dataType: "json",
            async: "true",
            cache: "false",
            success: function (result) {
                OnReportingSuccess(result);
            },
            error: function (xhr, status, error) {
                console.log("error ",error);
            }
        });

        $.ajax({
            type: "GET",
            url: "get_reporting_year_dash",
            //data: "{}",
            contentType: "application/json",
            dataType: "json",
            async: "true",
            cache: "false",
            success: function (result) {
                OnReportingYearSuccess(result);
            },
            error: function (xhr, status, error) {
                console.log("error ",error);
            }
        });

        $.ajax({
            type: "GET",
            url: "get_job_year_dash",
            //data: "{}",
            contentType: "application/json",
            dataType: "json",
            async: "true",
            cache: "false",
            success: function (result) {
                OnJobSuccess(result);
            },
            error: function (xhr, status, error) {
                console.log("error ",error);
            }
        });
    }

    function OnRegisterSuccess(response) {
        var results = response.results;
        if (results.register) {
            $("#register").text(results.register.format()) ;
            $("#c-register-internet").text(results.place ? results.place.internet.format() : '') ;
            $("#c-register-office").text(results.place ? results.place.office.format() : '') ;

            var registers = [];
            var n = 0;
            var haveValue = false;
            $.each(results.reasonResign, function( index, value ) {
                registers.push([index, value]);
                if (!haveValue && value > 0){
                    haveValue = true;
                }
                $("#ul-c-register").append(`<li class="mt-3"><i class="fa fa-circle mr-1 font-12 ${registerCss[n]}"></i> ${index} จำนวน ${value.format()} คน</li>`);
                n += 1;
            });

            if (!haveValue) {
                $("#c-register").hide();
            }

            c3.generate({
                bindto: '#c-register',
                data: {
                    columns: registers,
        
                    type: 'pie',
                
                },
                donut: {
                    label: {
                        show: false
                    },
                    title: "Ans: A",
                    width: 15,
        
                },
        
                legend: {
                    hide: true
                    //or hide: 'data1'
                    //or hide: ['data1', 'data2']
                },
                color: {
                    pattern: registerColor 
                }
            });
        }
    }

    function OnRegisterYearSuccess(response) {
        
        var results = response.results;
        var registerYears = [];
        var n = 0;

        if (results.register) {
            $("#register-year").text(results.register.format()) ;
            $("#c-register-year-internet").text(results.place ? results.place.internet.format() : '') ;
            $("#c-register-year-office").text(results.place ? results.place.office.format() : '') ;
            $.each(results.reasonResign, function( index, value ) {
                registerYears.push([index, value]);
                $("#ul-c-register-year").append(`<li class="mt-3"><i class="fa fa-circle mr-1 font-12 ${registerCss[n]}"></i> ${index} จำนวน ${value.format()} คน</li>`);
                n += 1;
            });
        
            c3.generate({
                bindto: '#c-register-year',
                data: {
                    columns: registerYears,
        
                    type: 'pie',
                    onclick: function(d, i) { /*console.log("onclick", d, i);*/ },
                    onmouseover: function(d, i) { /*console.log("onmouseover", d, i);*/ },
                    onmouseout: function(d, i) { /*console.log("onmouseout", d, i);*/ }
                },
                donut: {
                    label: {
                        show: false
                    },
                    title: "Ans: A",
                    width: 15,
        
                },
        
                legend: {
                    hide: true
                    //or hide: 'data1'
                    //or hide: ['data1', 'data2']
                },
                color: {
                    pattern: registerColor 
                }
            });

            n = 0;
            var wishYears = [];
            $.each(results.purpose, function( index, value ) {
                var text = "";
                if (index == "requireJob") {
                    text = "หางาน";
                    wishYears.push([text, value]);
                }/* else if (index == "nworkfreelance") {
                    text = "ไม่ประสงค์หางานและประกอบอาชีพอิสระ";
                    wishYears.push([text, value]);
                }*/
                else if (index == "haveFreelance") {
                    text = "ประกอบอาชีพอิสระ";
                    wishYears.push([text, value]);
                }  
                else if (index == "unRequireJob") {
                
                } else if (index == "workfreelance") {
                    text = "หางานและประกอบอาชีพอิสระ";
                    wishYears.push([text, value]);
                }

                if (text != "") {
                    $("#ul-c-wish").append(`<li class="mt-3"><i class="fa fa-circle mr-1 font-12 ${registerRequireCss[n]}"></i> ${text} จำนวน ${value.format()} คน</li>`);
                    n += 1;
                }
                
            })
            
            c3.generate({
                bindto: '#c-wish',
                data: {
                    columns: wishYears,
                    type: 'pie'
                },
                donut: {
                    label: {
                        show: false
                    },
                    title: "Ans: A",
                    width: 15,
        
                },
        
                legend: {
                    hide: true
                    //or hide: 'data1'
                    //or hide: ['data1', 'data2']
                },
                color: {
                    pattern: registerRequireColor
                }
            });
        }

    }

    function OnReportingSuccess(response) {
        var results = response.results;
        //console.log("OnReportingSuccess = ", results);
        if (results.tracking) {
            $("#reporting").text(results.tracking) ;
            $("#c-reporting-internet").text(results.place ? results.place.internet.format() : '') ;
            $("#c-reporting-office").text(results.place ? results.place.office.format() : '') ;

            var reportings = [];
            reportings.push(['ว่างงาน', results.purpose ? results.purpose.nHaveWork : '']);
            reportings.push(['ได้งานใหม่', results.purpose ? results.purpose.haveWork : '']);
            var haveValue = false;

            $.each(reportings, function( index, reporting ) {
                if (!haveValue && reporting[1] > 0){
                    haveValue = true;
                }
                $("#ul-c-reporting").append(`<li class="mt-3"><i class="fa fa-circle mr-1 font-12 ${reportingCss[index]}"></i> ${reporting[0]} จำนวน ${reporting[1].format()} คน</li>`);
                //console.log("reportingCss ",reportingCss[index]);
            });

            if (!haveValue) {
                $("#c-reporting").hide();
            }
        
            c3.generate({
                bindto: '#c-reporting',
                data: {
                    columns: reportings,
        
                    type: 'pie',
                
                },
                donut: {
                    label: {
                        show: false
                    },
                    title: "Ans: A",
                    width: 15,
        
                },
        
                legend: {
                    hide: true
                    //or hide: 'data1'
                    //or hide: ['data1', 'data2']
                },
                color: {
                    pattern: reportingColor 
                }
            });
        }
    }


    function OnReportingYearSuccess(response) {
        var results = response.results;
        if (results.tracking) {
            $("#reporting-year").text(results.tracking.format()) ;
            $("#c-reporting-year-internet").text(results.place ? results.place.internet.format() : '') ;
            $("#c-reporting-year-office").text(results.place ? results.place.office.format() : '') ;

            var reportingYears = [];
            reportingYears.push(['ว่างงาน', results.purpose ? results.purpose.nHaveWork : '']);
            reportingYears.push(['ได้งาน', results.purpose ? results.purpose.haveWork : '']);
            

            $.each(reportingYears, function( index, reporting ) {
                $("#ul-c-reporting-year").append(`<li class="mt-3"><i class="fa fa-circle mr-1 font-12 ${reportingCss[index]}"></i> ${reporting[0]} จำนวน ${reporting[1].format()} คน</li>`);
            });
        
            c3.generate({
                bindto: '#c-reporting-year',
                data: {
                    columns: reportingYears,
        
                    type: 'pie'
                },
                donut: {
                    label: {
                        show: false
                    },
                    title: "Ans: A",
                    width: 15,
        
                },
        
                legend: {
                    hide: true
                    //or hide: 'data1'
                    //or hide: ['data1', 'data2']
                },
                color: {
                    pattern: reportingColor
                }
            });
        }
    }

    function OnJobSuccess(response) {
        var results = response.results;
        //console.log("OnJobSuccess ",results);
        if (results.purpose) {
            $("#have-work").text(results.purpose.haveWork) ;
        }
    }

    // ============================================================== 
    // Our Visitor
    // ============================================================== 
    var sparklineLogin = function() {
        $('#ravenue').sparkline([6, 10, 9, 11, 9, 10, 12, 10, 9, 11, 9, 10], {
            type: 'bar',
            height: '75',
            barWidth: '4',
            width: '100%',
            resize: true,
            barSpacing: '8',
            barColor: '#fff'
        });
        $('#active-users').sparkline([6, 10, 9, 11, 9, 10, 12, 10, 9, 11, 9, 10, 12, 10, 9, 11, 9], {
            type: 'bar',
            height: '60',
            barWidth: '4',
            width: '100%',
            resize: true,
            barSpacing: '8',
            barColor: '#4fc3f7'
        });
        $('#views').sparkline([6, 10, 9, 11, 9, 10, 12], {
            type: 'line',
            height: '45',
            lineColor: 'transparent',
            fillColor: 'rgba(255, 255, 255, 0.3)',
            width: '100%',
            resize: true,
        });

    };
    var sparkResize;

    $(window).resize(function(e) {
        clearTimeout(sparkResize);
        sparkResize = setTimeout(sparklineLogin, 500);
    });
    sparklineLogin();

    // ============================================================== 
    // world map
    // ==============================================================
    /*$('#usa').vectorMap({
        map: 'us_aea_en',
        backgroundColor: 'transparent',
        zoomOnScroll: false,
        regionStyle: {
            initial: {
                fill: '#4fc3f7'
            }
        },
        markerStyle: {
            initial: {
                r: 5,
                'fill': '#fff',
                'fill-opacity': 1,
                'stroke': '#fff',
                'stroke-width': 1,
                'stroke-opacity': 1
            },
        },
        enableZoom: true,
        hoverColor: '#fff',
        markers: [{
            latLng: [31.96, -99.90],
            name: 'Texas',
            style: { fill: '#fff' }
        }, {
            latLng: [43.07, -107.29],
            name: 'Wyoming',
            style: { fill: '#fff' }
        }, {
            latLng: [40.63, -89.39],
            name: 'Illinois',
            style: { fill: '#fff' }
        }],
    });*/

    
});

Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};