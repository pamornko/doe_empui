$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var form = $(".validation-wizard").show();
    $(".validation-wizard").steps({
        headerTag: "h6",
        enableCancelButton: true,
        bodyTag: "section",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        labels: {
            next : "ถัดไป",
            previous : "ก่อนหน้า",
            finish: "บันทึก",
            cancel : "ยกเลิก"
        },
        onCanceled: function (event)
        {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'mr-2 btn btn-danger'
                },
                buttonsStyling: false,
            });

            swalWithBootstrapButtons.fire({
                title: 'คุณต้องการยกเลิกใช่หรือไม่ ?',
                text: "หากยืนยันการยกเลิก ข้อมูลจะไม่ถูกบันทึก ?",
                type: 'info',
                showCancelButton: true,
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    window.history.back();
                }
            });
            
        },
        onStepChanged: function(event, currentIndex) {
            /*var work = $('input[name="workStatus"]:checked').val();
            if (work && currentIndex == 1) {
                $(".validation-wizard").steps("next", 2);
                //$(".validation-wizard").skip(2);
            }
            console.log('currentIndex = ',currentIndex);
            console.log('workStatus = ',work);*/
            
        },
        onStepChanging: function(event, currentIndex, newIndex) {
        
            //return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
            var valid = currentIndex > newIndex || (currentIndex < newIndex &&  (form.validate().settings.ignore = ":disabled,:hidden", form.valid()));

            if (newIndex == 2 && valid) {
                var workStatus =  $('input[name="workStatus"]:checked').val();
                console.log("workStatus = ",workStatus);
                var isHidden = $('.form-check-inline').css('display') == 'none';
                if (!isHidden && workStatus == "0") {
                    //
                    $("#haveFreelance").prop('checked',true);
                    $("#freelance-area").show();
                    $('#freelance-table').show();
                } else {
                    var x = $("select[name='wintType[]']")
                    .map(function(){
                        if ($(this).val() !== "")
                        {
                            return $(this).val()
                        }
                    }).get();
                    
                    if (x.length > 0) {
                        $("#haveFreelance").prop('checked',true);
                        $("#freelance-area").show();
                        $('#freelance-table').show();
                    } else {
                        $("#haveFreelance").prop('checked',false);
                        $("#freelance-area").hide();
                        $('#freelance-table').hide();
                    }
                }
                return valid;
            } else {
                return valid;
            }
        },
        onFinishing: function(event, currentIndex) {
            return form.validate().settings.ignore = ":disabled,:hidden", form.valid()
        },
        onFinished: function(event, currentIndex) {
            var goAhead = true;
            if ($('#haveFreelance').is(':checked')) {
                $('#freelance-table').find("select[name^='wintType']").each(function(index, element){
                    //$(element).removeClass("is-invalid");
                    var value = element.value;
                    if (value == "") {
                        $('.select2-selection').each(function (i, element) {
                            var n = element.getAttribute('aria-labelledby').indexOf("wintType");
                            
                            if (n >= 0) {
                                var id = $(element).attr('data-id');
                                console.log("id = ",id);
                            }
                            
                        });
                        goAhead = false;
                        return false;
                    } else {
                        var other = $(element).attr('data-other');
                        var remark = $('#freelance-table').find("input[name='wint[]']")[index];
                        if (other && remark.value == "") {
                            goAhead = false;
                            return false;
                        }
                    }
                });
            }

            /*if (goAhead) {
                $('select[name^="wintType"]').each(function() {
                    if ($(this).val() == "") {
                        goAhead = false;
                        return;
                    }
                });
            }*/
            var workStatus = $('input[name="workStatus"]:checked').val();
            var x = $("select[name='wintType[]']")
                    .map(function(){
                        if ($(this).val() !== "")
                        {
                            return $(this).val()
                        }
                    }).get();
           
            var isHidden = $('.form-check-inline').css('display') == 'none';
            if ((!isHidden && $('#ignoreJobId').is(':checked')) && x.length <= 0 && workStatus == "0") {
                //บังคับลงอาชีพอิสระ ถ้าไมสมัครงาน
                Swal.fire({
                    title: "บันทึกข้อมูลไม่สำเร็จ",
                    text: "หากไม่ประสงค์จะสมัครงาน กรุณาระบุข้อมูลประกอบอาชีพอิสระ อย่างน้อย 1 รายการ",
                    type: "error",
                    button: "ตกลง",
                });
            } else {
                if (goAhead) {

                    var valid = true;
    
                    if (valid) {
                        const swalWithBootstrapButtons = Swal.mixin({
                            customClass: {
                                confirmButton: 'btn btn-success',
                                cancelButton: 'mr-2 btn btn-danger'
                            },
                            buttonsStyling: false,
                        });
        
                        var dataString = form.serialize();
                        
                        if ($('select[name="province"] option:selected').val()) {
                            dataString += "&province="+$('select[name="province"] option:selected').val();
                        }
                        if ($('select[name="district"] option:selected').val()) {
                            dataString += "&district="+$('select[name="district"] option:selected').val();
                        }
                        if ($('select[name="subDistrict"] option:selected').val()) {
                            dataString += "&subDistrict="+$('select[name="subDistrict"] option:selected').val();
                        }
    
                        console.log("dataString = ", dataString);
        
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
        
                        swalWithBootstrapButtons.fire({
                            title: 'คุณต้องการบันทึก ?',
                            text: "การรายงานตัวหรือไม่ ?",
                            type: 'info',
                            showCancelButton: true,
                            confirmButtonText: 'ตกลง',
                            cancelButtonText: 'ยกเลิก',
                            reverseButtons: true
                        }).then((result) => {
                            if (result.value) {
                            
                                let timerInterval;
                                Swal.fire({
                                    title: 'กำลังบันทึกข้อมูล',
                                    html: '',
                                    timer: 10000,
                                    allowOutsideClick: false,
                                    onBeforeOpen: () => {
                                        const content = Swal.getContent();
                                        const $ = content.querySelector.bind(content);
        
                                        const stop = $('#stop');
                                        const resume = $('#resume');
                                        const toggle = $('#toggle');
                                        const increase = $('#increase');
        
                                        Swal.showLoading();
        
                                        timerInterval = setInterval(() => {
                                            1000
                                        }, 100)
        
                                        Swal.stopTimer();
                                    },
                                    onClose: () => {
                                        clearInterval(timerInterval);
                                    }
                                    
                                });
        
                                $.ajax({
                                    type : 'post',
                                    url: 'reporting_update',
                                    data: dataString,
                                    dataType: "json",
                                    success: function (data) { 
                                        //console.log("Success -> ",data.success);
                                        clearInterval(timerInterval);
                                        Swal.close();
        
                                        var path = 'reporting';
                                        var url = path+'?id='+ $('#RegisterID').val() ;
                                        
                                        if (data.success) {
                                            Swal.fire({
                                                title: "บันทึกข้อมูลสำเร็จ",
                                                text: "รายงานตัวสำเร็จ กรุณาตรวจสอบผลการพิจารณา ได้ที่เมนู ข้อมูลขอรับประโยชน์ทดแทนกรณีว่างงาน",
                                                type: "success",
                                                button: "ตกลง",
                                                onClose: () => {
                                                    //window.location.href = base+'?id='+data.id ;
                                                    window.location.href = url;
                                                }
                                            });
                                        
                                        } else {
                                            Swal.fire({
                                                title: "บันทึกข้อมูลไม่สำเร็จ",
                                                text: "กรุณาลองใหม่อีกครั้ง",
                                                type: "error",
                                                button: "ตกลง",
                                            });
                                        
                                        }
                                    }
                                }); 
        
        
                            }/* else if (
                                // Read more about handling dismissals
                                result.dismiss === Swal.DismissReason.cancel
                            ) {
                                swalWithBootstrapButtons.fire(
                                    'ยกเลิก',
                                    'ข้อมูลของคุณจะไม่ถูกบันทึก',
                                    'error'
                                )
                            }*/
                        });
                    }
                
                } else {
                    Swal.fire({
                        title: "บันทึกข้อมูลไม่สำเร็จ",
                        text: "กรุณาเพิ่มอาชีพอิสระของคุณ",
                        type: "error",
                        button: "ตกลง",
                    });
                }
            }

        }
    }), $(".validation-wizard").validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger",
        successClass: "text-success",
        highlight: function(element, errorClass) {
            var id = element.id;
            if (id == "ignoreJobId") {
                var scrollBottom = $(window).scrollTop() + $(window).height();
                $("html, body").animate({ scrollTop: scrollBottom }, "slow");
            }
            $(element).removeClass(errorClass)
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass)
            $(element).removeClass("is-invalid");
        },
        errorPlacement: function(error, element) {
            //console.log('errorPlacement element = ',element);
            //console.log('errorPlacement errorClass = ',error);
            $(element).addClass("is-invalid");
            //error.insertAfter(element)
        },
        rules: {
            email: {
                email: !0
            }
        }
    });

    init();

    $.datetimepicker.setLocale('th'); // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
         
        // กรณีใช้แบบ inline
      /*  $("#testdate4").datetimepicker({
            timepicker:false,
            format:'d-m-Y',  // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000            
            lang:'th',  // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
            inline:true  
        });    */   
         
         
    // กรณีใช้แบบ input
    $(".custom-date-picker").datetimepicker({
        timepicker:false,
        format:'d/m/Y',  // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000            
        lang:'th',  // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
        onSelectDate:function(dp,$input){
            var yearT=new Date(dp).getFullYear();  
            var yearTH=yearT+543;
            var fulldate=$input.val();
            var fulldateTH=fulldate.replace(yearT,yearTH);
            $input.val(fulldateTH);
        },
    }); 

});

$(document).on('change', '#wProvince', function() {
    var id = $(this).val();

    var op = "";
    $.ajax({
        type : 'get',
        contentType: "application/json; charset=utf-8",
        url : 'findDistrict',
        data : { 'id' : id},
        dataType: "json",
        success : function(datas) {
            op += '<option value="" selected disabled>เลือกอำเภอ / เขต</option>';
            for (var i=0; i< datas.districts.length; i++){
                
                op += '<option value="'+datas.districts[i].DistrictID+'">'+datas.districts[i].DistrictName+'</option>';
            }
            //console.log($('#wDistrict'));
            $('#wDistrict').html("");
            $('#wDistrict').append(op);
            //div.find('#wDistrict')
        },
        error : function(err) {
            console.log("In error ",err);
        }
    });
});

/*** Dropdown dynamic ***/

$(document).on('change', '#wDistrict', function() {
    var id = $(this).val();

    var op = "";
    $.ajax({
        type : 'get',
        contentType: "application/json; charset=utf-8",
        url : 'findTambon',
        data : { 'id' : id},
        dataType: "json",
        success : function(datas) {
            op += '<option value="">กรุณาเลือก</option>';
            for (var i=0; i< datas.tambons.length; i++){
                
                op += '<option value="'+datas.tambons[i].TambonID+'">'+datas.tambons[i].TambonName+'</option>';
            }
            $('#wSubDistrict').html("");
            $('#wSubDistrict').append(op);
        },
        error : function(err) {
            console.log("In error ",err);
        }
    });
});

function showOrHide(action) {
    if (action === 'show') {
        $("#empInfomationId").show();
    } else {
        $("#empInfomationId").hide();
    }
}

//dbd_employer
$(document).on('click', '#btnDBD', function () {
    var employerCode = $("#EmployerCode").val();

    $("#wOrganization").prop("readonly", false);
    $("#contactAddressId").prop("readonly", false);

    $('#wProvince > option[selected="selected"]').removeAttr('selected');
    $('#wProvince').attr('disabled',false);
    
    $('#wDistrict').html("");
    $('#wDistrict').removeAttr('disabled');
    $('#wSubDistrict').html("");
    $('#wSubDistrict').removeAttr('disabled');

    if (employerCode.length != "") {
        let timerInterval;
        $.ajax({
            type: 'post',
            url: 'dbd_employer',
            data: {
                'EmployerCode': employerCode
            },
            //dataType: "html",
            beforeSend: function () {

                Swal.fire({
                    title: 'กำลังตรวจสอบข้อมูลนายจ้าง',
                    html: '',
                    timer: 10000,
                    onBeforeOpen: () => {
                        const content = Swal.getContent();
                        const $ = content.querySelector.bind(content);

                        const stop = $('#stop');
                        const resume = $('#resume');

                        Swal.showLoading();

                        timerInterval = setInterval(() => {
                            1000
                        }, 100)

                        Swal.stopTimer();
                    },
                    onClose: () => {
                        clearInterval(timerInterval);
                    }

                });
            },
            success: function (result) {
                clearInterval(timerInterval);
                Swal.close();
                var data = result.data;
                //V2
                data = data.ResultList;
                //$('#cntFreelance').attr('id', "wintType"+$id)
                if (data == null || data.length == 0 || data.Code) {
                    $("#EmployerCode").val("");
                    Swal.fire({
                        title: "ไม่พบข้อมูลนายจ้าง",
                        text: data ? data.Message : "",
                        type: "error",
                        button: "ปิด",
                    });
                } else {
                    data = data[0];
                    console.log("data = ",data);
                    console.log("data = ",data[0]);
                    console.log("data = ",data.AddressInformations[0]);
                    if (data.JuristicName_TH) {
                        $("#wOrganization").val(data.JuristicName_TH)
                        $("#wOrganization").prop("readonly", true);
                    }

                    if (data.AddressInformations[0]) {
                        if (data.AddressInformations[0].FullAddress) {
                            $("#contactAddressId").val(data.AddressInformations[0].FullAddress)
                            $("#contactAddressId").prop("readonly", true);
                        }
                        var pName= data.AddressInformations[0].Province;
                        if (pName) {
                            
                            var province = (function get_province_data() {
                                var province_data;
                                $.ajax({
                                    url: "find_province_id",
                                    async: false, 
                                    data : { 'name' : pName},
                                    //very important: else php_data will be returned even before we get Json from the url
                                    dataType: 'json',
                                    success: function (json) {
                                        province_data = json;
                                    }
                                });
                                return province_data;
                            })();

                            if (province.success) {
                                //$("#wProvince").val(province.id).change();
                                $("#wProvince option[value="+province.id+"]").attr('selected','selected');
                                $('#wProvince').attr('disabled', 'disabled');
                            }

                            var dName = data.AddressInformations[0].Ampur;
                            if (dName && province.success) {
                               
                                var district = (function get_district_data() {
                                    var district_data;
                                    $.ajax({
                                        url: "find_district_id",
                                        async: false, 
                                        data : { 'name' : dName , 'province_id' : province.id},
                                        //very important: else php_data will be returned even before we get Json from the url
                                        dataType: 'json',
                                        success: function (json) {
                                            district_data = json;
                                        },
                                        error :function (err) {
                                            console.log("err = ",err);
                                        }
                                    });
                                    return district_data;
                                })();
                                if (district.success) {
                                    
                                    var op = '<option value="'+district.id+'" selected>'+dName+'</option>';
                                   
                                    $('#wDistrict').append(op);
                                    $('#wDistrict').attr('disabled', 'disabled');
                                }

                                var tName = data.AddressInformations[0].Tumbol;
                                if (tName && district.success) {

                                    var tumbon = (function get_tumbon_data() {
                                        var tumbon_data;
                                        $.ajax({
                                            url: "find_tumbon_id",
                                            async: false, 
                                            data : { 'name' : tName , 'district_id' : district.id},
                                            //very important: else php_data will be returned even before we get Json from the url
                                            dataType: 'json',
                                            success: function (json) {
                                                tumbon_data = json;
                                            },
                                            error :function (err) {
                                                console.log("err = ",err);
                                            }
                                        });
                                        return tumbon_data;
                                    })();
                                    if (tumbon.success) {
                                        
                                        var op = '<option value="'+tumbon.id+'" selected>'+tName+'</option>';

                                        $('#wSubDistrict').append(op);
                                        $('#wSubDistrict').attr('disabled', 'disabled');
                                       
                                    }
                                }
                            }

                        }

                        var phone= data.AddressInformations[0].Phone;
                        if (phone) {
                            $("#wPhoneNumber").val(phone)
                            $("#wPhoneNumber").prop("readonly", true);
                        }

                        var email= data.AddressInformations[0].Email;
                        if (email) {
                            $("#wContactPerson").val(email)
                            $("#wContactPerson").prop("readonly", true);
                        }
                    }
                }
            }
        });
    } else {
        $("#EmployerCode").val("");
        Swal.fire({
            title: "กรุณาใส่เลขนิติบุคคลของบริษัท 13 หลัก",
            text: "",
            type: "error",
            button: "ปิด",
        });
    }
});
