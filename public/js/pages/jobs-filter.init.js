$(document).ready(function () {
    // Denotes total number of rows 
    var rowIdx = 1;
    var selectIdx = 1;
    // jQuery button click event to add a row 
    $('#addBtn').on('click', function () {
        // Adding a row inside the tbody. 
        ++rowIdx;
        ++selectIdx;
        var html = `<select class="form-control custom-fillter-select" data-id="${selectIdx}" id="wintType${selectIdx}" name="wintType[]" style="width: 100%;height: 36px;" onchange="manageOther(this)" required></select>`;
       
        $('#tbody').append(`<tr id="R${rowIdx}"> 
                    <td class="row-index"> 
                        <div class="form-group">
                            ${html}
                        </div>
                    </td> 
                    <td class="text-center"> 
                        <div class="form-group" style="display:none" data-id="${selectIdx}" id="divwintT${selectIdx}">
                            <input type="text" class="form-control  required" name="wint[]"> 
                        </div>
                    </td>
                    <td class="text-center"> 
                        <a class="delete-multiple text-danger remove" style="cursor: pointer;"><i class="fas fa-trash font-16 font-medium"></i></a>
                        
                        </td> 
                </tr>`);

        $.ajax({
            type : 'get',
            url: 'freelances',
            dataType: "json",
            success: function (data) { 
                $("#wintType"+selectIdx).select2({
                    placeholder: "กรุณาเลือกอาชีพอิสระ",
                    data: data.datas
                });
            }
        });
        /*$.ajax({
            type : 'get',
            url: 'get_freelances',
            success: function (datas) { 
                
                $('#freelance-table').show();
                var op = '<option value="">กรุณาเลือกอาชีพอิสระ</option>';
                for (var i=0; i< datas.freelances.length; i++){
                    
                    op += '<option value="'+datas.freelances[i].FreelanceID+'" other="'+datas.freelances[i].Other+'">'+datas.freelances[i].FreelanceName+'</option>';
                }
               
                $('#tbody').append(`<tr id="R${++rowIdx}"> 
                    <td class="row-index text-center"> 
                        <div class="form-group">
                            <select class="form-control custom-select required" data-id="${rowIdx}" name="wintType[]" onchange="manageOther(this)" required>
                                ${op}
                            </select>
                        </div>
                    </td> 
                    <td class="text-center"> 
                        <div class="form-group" style="display:none" data-id="${rowIdx}" id="divwintT${rowIdx}">
                            <input type="text" class="form-control  required" name="wint[]"> 
                        </div>
                    </td>
                    <td class="text-center"> 
                        <a class="delete-multiple text-danger remove" style="cursor: pointer;"><i class="fas fa-trash font-16 font-medium"></i></a>
                        
                        </td> 
                </tr>`);

            }
        });*/
    });

    // jQuery button click event to remove a row. 
    $('#tbody').on('click', '.remove', function () {

        // Getting all the rows next to the row 
        // containing the clicked button 
        var child = $(this).closest('tr').nextAll();

        // Iterating across all the rows  
        // obtained to change the index 
        child.each(function () {

            // Getting <tr> id. 
            var id = $(this).attr('id');

            // Getting the <p> inside the .row-index class. 
            var idx = $(this).children('.row-index').children('p');

            // Gets the row number from <tr> id. 
            var dig = parseInt(id.substring(1));

            // Modifying row index. 
            idx.html(`Row ${dig - 1}`);

            // Modifying row id. 
            $(this).attr('id', `R${dig - 1}`);
        });

        // Removing the current row. 
        $(this).closest('tr').remove();
        

        // Decreasing total number of rows by 1. 
        rowIdx--;

        if (rowIdx == 0) {
            $("#haveFreelance").prop('checked',false);
        }
    });

});


function init() {
    //get jobs list
    $.ajax({
        type : 'get',
        url: 'job_private_list_ai',
        data : { 'order_by' : $('#orderBy').val()},
        success: function (data) { 
            $('.block-list-job').html(data.html);
        }
    });
    //get jobs field
    $.ajax({
        type : 'get',
        url: 'jobsFields',
        dataType: "json",
        success: function (data) { 
            $("#jobs-field").select2({
                placeholder: "ประเภทงาน",
                data: data.datas
            });
        }
    });

    reloadApplyJobs();
}

$(document).on('click', '.btn-close', function () {
    $('#job-detail-modal').trigger('click.dismiss.bs.modal')
});

$(document).on('change', '#salary, #emp-type, #degree, #province-filter, #amphoe-filter', function() {
    var val = $(this).val();
    if (val) {
        $(this).css('color', 'black');
    } else {
        $(this).css('color', 'gray');
    }
});

$(document).on('change', '#wProvince', function() {
    var id = $(this).val();

    var op = "";
    $.ajax({
        type : 'get',
        contentType: "application/json; charset=utf-8",
        url : 'findDistrict',
        data : { 'id' : id},
        dataType: "json",
        success : function(datas) {
            op += '<option value="" selected disabled>เลือกอำเภอ / เขต</option>';
            for (var i=0; i< datas.districts.length; i++){
                
                op += '<option value="'+datas.districts[i].DistrictID+'">'+datas.districts[i].DistrictName+'</option>';
            }
            //console.log($('#wDistrict'));
            $('#wDistrict').html("");
            $('#wDistrict').append(op);
            //div.find('#wDistrict')
        },
        error : function(err) {
            console.log("In error ",err);
        }
    });
});

let jobID ;
$(document).on('click', '.item-list-job', function() {
    jobID = $(this).attr('data-job-id');
    $('.modal-body').html('<div class="d-flex justify-content-center"><div class="spinner-border" style="width: 3rem; height: 3rem;" role="status"><span class="sr-only">Loading...</span></div></div>');
    $('#job-detail-modal').modal('show');
    //window.open("/job_details?ID="+jobID, "_blank");
    $.ajax({
        type : 'get',
        url : 'job_details',
        data : { 'ID' : jobID},
        success : function(datas) {
            $('.modal-body').html(datas.html);
        },
        error : function(err) {
            console.log("Job details error ",err);
        }
    });
   
});

$('#job-detail-modal').on('shown.bs.modal', function() { 
    
});

$(document).on('click', '.btn-close', function() {
    $('#job-detail-modal').trigger('click.dismiss.bs.modal')
});

$(document).on('click', '.fav-button', function(env) {
    env.stopPropagation();
    var id = $(this).attr('data-id');
    const swalWithBootstrapButtons = Swal.mixin({
        /*customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'mr-2 btn btn-danger'
        },
        buttonsStyling: false,*/
    });
    console.log("id = ",id);
   
    swalWithBootstrapButtons.fire({
        //title: 'คุณต้องการยกเลิกใช่หรือไม่ ?',
        text: "คุณยืนยันการเก็บงานนี้ ในประวัติงานนี้ หรือไม่ ?",
        type: 'info',
        showCancelButton: true,
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            var my =  $(this);
            $.ajax({
                type : 'post',
                url : 'job_fav',
                data : { 'ID' : id},
                success : function(datas) {
                    console.log("Fav job success ",datas);
                    my.addClass("fav-tomato");
                    my.prop( "disabled", true );
                },
                error : function(err) {
                    console.log("Fav job error ",err);
                }
            });
        }
    });
});

$(document).on('click', '.apply-job-button', function(env) {
    env.stopPropagation();
    var parent = $(this).parents('.bd-highlight');
    var btn = parent.find('.applying-job-button');
    var id = $(this).attr('data-id');
    console.log("id = ",id);

    Swal.fire({
        //title: 'คุณต้องการยกเลิกใช่หรือไม่ ?',
        text: "คุณยืนยัน การสมัครงานนี้ หรือไม่ ?",
        type: 'info',
        showCancelButton: true,
        confirmButtonText: 'สมัครเลย !',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            var my =  $(this);
            $.ajax({
                type : 'post',
                url : 'job_apply',
                data : { 'ID' : id},
                success : function(datas) {
                    reloadApplyJobs();
                    my.addClass("e-hide");
                    btn.removeClass("e-hide");
                },
                error : function(err) {
                    console.log("Apply job error ",err);
                }
            });
        }
    });
});

$(document).on('change', '#province-filter', function() {
    var id = $(this).val();
    console.log($('#amphoe-filter'));
    var op = "";
    $.ajax({
        type : 'get',
        contentType: "application/json; charset=utf-8",
        url : 'findDistrict',
        data : { 'id' : id},
        dataType: "json",
        success : function(datas) {
            op += '<option value="">กรุณาเลือก</option>';
            for (var i=0; i< datas.districts.length; i++){
                
                op += '<option value="'+datas.districts[i].DistrictID+'">'+datas.districts[i].DistrictName+'</option>';
            }
            //console.log($('#wDistrict'));
            $('#amphoe-filter').html("");
            $('#amphoe-filter').append(op);
            //div.find('#wDistrict')
        },
        error : function(err) {
            console.log("In error ",err);
        }
    });
});

function search(page) {
   
    var loading = `<div class="d-flex justify-content-center mt-4">
    <div class="spinner-border text-info" style="width: 5rem; height: 5rem;" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div>`;
    $('.block-list-job').html(loading);
    $.ajax({
        type : 'get',
        url : 'job_private_list_ai',
        data : { 
            'order_by' : $('#orderBy').val(),
            'position' : $('#position').val(),
            'salary' : $('#salary').val(),
            'emp_type' : $('#emp-type').val(),
            'jobs_field' : $('#jobs-field').val(),
            'degree' : $('#degree').val(),
            'province' : $('#province-filter').val(),
            'amphoe' : $('#amphoe-filter').val(),
            'page' : page
        },
        beforeSend: function(){
            //$("#loader").show();
            //document.getElementById("loader").style.display = "none";
        },
        success : function(data) {
            $('.block-list-job').html(data.html);
            $("html, body").animate({ scrollTop: 0 }, "slow");
            //$("#loader").hide();
        },
        error : function(err) {
            console.log("Apply list error ",err);
        }
    });
}

$(document).on('click', '.fillter-button', function() {
    search(0);
});

$(document).on('click', '.reset-fillter-button', function() {
    $('#orderBy').val('latest');
    $('#position').val('');
    $('#salary').val('');
    $('#emp-type').val('');
    $('#jobs-field').val('');

    /*$('#select2-jobs-field-container').html("");
    $("#jobs-field").select2({
        placeholder: "ประเภทงาน"
    });*/

    $('#jobs-field').val(null).trigger('change');

    $('#degree').val('');
    $('#province-filter').val('');
    $('#amphoe-filter').val('');


    $('#salary').css('color', 'gray');
    $('#emp-type').css('color', 'gray');
    $('#degree').css('color', 'gray');
    $('#province-filter').css('color', 'gray');
});

$(document).on('click', '.a-page', function() {
    var val = $(this).text();
    search(val);
});

$(document).on('click', '.first-page', function() {
    var cPage = parseInt($("#c-page").val(), 10);
    cPage -=1;
    search(cPage);
});

$(document).on('click', '.last-page', function() {
    //for test
    var cPage = parseInt($("#c-page").val(), 10);
    cPage +=1;
    search(cPage);
});

function reloadApplyJobs() {
    $.ajax({
        type : 'get',
        url : 'reloadApplyJobs',
       
        success : function(datas) {
          
            $('.applying-jobs-table').html(datas.html);
            if (datas.jobs.length > 0) {
                $(".form-check-inline").hide();
            }
        },
        error : function(err) {
            console.log("Apply job error ",err);
        }
    });
}

function remove(id) {

    const swalRemove = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'mr-2 btn btn-danger'
        },
        buttonsStyling: false,
    });

    swalRemove.fire({
        title: 'คุณต้องการลบข้อมูล ?',
        text: "การสมัครงานที่เลือกไว้ หรือไม่ ?",
        type: 'info',
        showCancelButton: true,
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type : 'post',
                url: 'removeApplyJob',
                data : { 
                    'id' : id ,
                },
                //dataType: "html",
                success: function (data) { 
                    //$('#cntFreelance').attr('id', "wintType"+$id)

                    $('#applyJobsTmp').val(JSON.stringify(data.jobs));
                    $('.table-responsive').html(data.html);

                    var key = '#btn-area-'+id;
                    var applyingBtn = $(key).find('.applying-job-button');
                    var applyBtn = $(key).find('.apply-job-button');

                    applyingBtn.addClass("e-hide");
                    applyBtn.removeClass("e-hide");

                    if (data.cnt <= 0) {
                        $(".form-check-inline").show();
                    }
                    //console.log(data.html);
                    //console.log(data.cnt);
                }
            });
        }
    });
}


function manageOther(obj) {

    var id = $(obj).attr('data-id');
    //var id = obj.parentElement.parentElement.parentElement.rowIndex;
    var select = $("#"+obj.id);
    var theSelection = select.find(':selected').text();
    var value = obj.value;
   
    $.ajax({
        type : 'get',
        url : 'get_freelance',
        data : { 'text' : theSelection},
        dataType: "json",
        success : function(results) {
            if (results.data) {
                var x = document.getElementById('divwintT' + id);
                var other = results.data.Other;
                if (other == "1") {
                    x.style.display = 'block';

                    $(obj).attr('data-other', other);
                } else {
                    if (x) {
                        x.style.display = 'none';
                    }
                }
            }
        },
        error : function(err) {
            console.log("In error ",err);
        }
    });
}

function showFreelance (obj) {
    if (obj.checked) {
        $("#freelance-area").show();
        $('#freelance-table').show();
    } else {
        $("#freelance-area").hide();
        $('#freelance-table').hide();
    }
}