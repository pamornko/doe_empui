

function addRow(table, datas) {

    for (var i = 0; i < datas.length; i++) { 
        
        let name = datas[i]['FirstName'];

        if (datas[i]['LastName']) {
            name = `${name} ${datas[i]['LastName']}`
        }

        let btEdit = "";
        let linkView = "";
        let id = datas[i]['TrackingID'];
        if (datas[i]['ReportingDate']) {
            
            let path = config.routes.search;
            path += "?id="+  id;
            btEdit = `<button class="btn btn-outline-success waves-effect waves-light" onclick="window.location='${path}'"><i class="fas fa-pencil-alt"></i> แก้ไข</button>`;
            
            let reportingDate = getDateString(datas[i]['ReportingDate']);
            let viewPath = config.routes.view;
            viewPath += "?id="+  id;
            linkView = `<a href="${viewPath}"> ${reportingDate} </a>`;
        }

       
        

        //console.log("btEdit = ",btEdit);

        table.row.add([
            i+1 , 
            datas[i]['RegisterNumber'],
            datas[i]['PersonalID'], 
            name, datas[i]['TrackingTime'] , 
            getDateString(datas[i]['ReportingDueDate']),
            linkView,
            btEdit]
        ).draw();
    }
}

