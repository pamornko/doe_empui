/*************************************************************************************/
// -->Template Name: Bootstrap Press Admin
// -->Author: Themedesigner
// -->Email: niravjoshi87@gmail.com
// -->File: datatable_basic_init
/*************************************************************************************/

/****************************************
 *         Table Responsive             *
 ****************************************/
$(function () {
    $('#config-table').DataTable({
        responsive: true
    });
})

var pageLength = 20;

/*var optionsLanguage = {
    "sLengthMenu": "Display _MENU_ records per page",
    "sZeroRecords": "ไม่พบข้อมูล",
    "sInfo": "แสดง _START_ ถึง _END_ ของทั้งหมด _TOTAL_ รายการ",
    "sInfoEmpty": "Showing 0 to 0 of 0 records",
    "sInfoFiltered": "(filtered from _MAX_ total records)",
    "sSearch": "ค้นหา :",
};*/

var optionsLanguage = {
    "sZeroRecords": "ไม่พบข้อมูล",
    "sInfo": "แสดง _START_ ถึง _END_ ของทั้งหมด _TOTAL_ รายการ",
    "sInfoEmpty": "",
    "sSearch": "ค้นหา :",
    "sInfoFiltered": "",
    "sPaginationType": "full_numbers",
    "oPaginate" : {
        sPrevious: 'ก่อนหน้า',
        sNext: 'ถัดไป'
    }
};

/****************************************
 *       Basic Table                   *
 ****************************************/
$('#zero_config').removeAttr('width').DataTable({
    
    "ordering": false,
    "autoWidth": false,
    "lengthChange": false,
    "pageLength": pageLength,
    "oLanguage": optionsLanguage,

    columnDefs: [
        { width: "10%", targets : 0 },
        { "width": "20%", targets: 1 },
        { "width": "30%", targets: 2 },
       
        { orderable: false, targets: '_all' }
    ],
    fixedColumns: true
});

$("#zero_config").css("width","100%");

/****************************************
 *       Basic Table                   *
 ****************************************/
$('#reporting_table').removeAttr('width').DataTable({
    "lengthChange": false,
    "ordering": false,
    "autoWidth": false,
    "paging":   false,
    "info":     false,
    "searching": false,
    "oLanguage": optionsLanguage,
    /*"columns": [
        { "width": "10%" },
        { "width": "20%" },
        { "width": "30%" },
        { "width": "40%" }
    ],*/
    columnDefs: [
        { width: "10%", targets : 0 },
        { width: "30%", targets : 1 },
        { "width": "30%", targets: 2 },
       
        { orderable: false, targets: '_all' }
    ],
    fixedColumns: true
});

$("#reporting_table").css("width","100%");


$('#report_benefit_table').removeAttr('width').DataTable({
    "lengthChange": false,
    "ordering": false,
    "autoWidth": false,
    "info":     false,
    "paging":   false,
    "searching": false,
    "oLanguage": optionsLanguage,
    columnDefs: [
        { width: "50px", targets : 0, "className" : "table-text-align-center" },
        { width: "35%", targets : 1 },
        { width: "35%", targets : 2 },
        { orderable: false, targets: '_all' }
    ],
    fixedColumns: true
});

$("#report_benefit_table").css("width","100%");


/****************************************
 *       Basic Table                   *
 ****************************************/
$('#apply_job_table').removeAttr('width').DataTable({
    "lengthChange": false,
    "ordering": false,
    "autoWidth": false,
    "paging":   false,
    "info":     false,
    "searching": false,
    'processing': true,
    "oLanguage": {

        //"sSearch": "ค้นหา : "
        'processing': '<div class="spinner-border"></div>'
    },
    /*"columns": [
        { "width": "10%" },
        { "width": "20%" },
        { "width": "30%" },
        { "width": "40%" }
    ],*/
    columnDefs: [
        { width: "10%", targets : 0 },
        { width: "30%", targets : 1 },
        { "width": "30%", targets: 2 },
       
        { orderable: false, targets: '_all' }
    ],
    fixedColumns: true
});

$("#apply_job_table").css("width","100%");
//$("#apply_job_table").ajax.reload();


/****************************************
 *       Basic Table                   *
 ****************************************/
$('#reporting_officer_table').removeAttr('width').DataTable({
    "lengthChange": false,
    "ordering": false,
    "autoWidth": false,
    "paging":   false,
    "info":     false,
    "oLanguage": optionsLanguage,
    /*"columns": [
        { "width": "10%" },
        { "width": "20%" },
        { "width": "30%" },
        { "width": "40%" }
    ],*/
    columnDefs: [
        { width: "5%", targets : 0 },
        { width: "15%", targets : 1 },
        { "width": "15%", targets: 2 },
        { "width": "20%", targets: 3 },
        { width: "10%", targets : 4 },
        { "width": "10%", targets: 5 },
        { "width": "10%", targets: 6 },
       
        { orderable: false, targets: '_all' }
    ],
    fixedColumns: true
});

$("#reporting_officer_table").css("width","100%");

/****************************************
 *       summary_insured_services Table                   *
 ****************************************/

$('#summary_personal_services').removeAttr('width').DataTable({
    "lengthChange": false,
    "ordering": false,
    "autoWidth": false,
    "paging":   false,
    "info":     false,
    "searching": false,
   
    columnDefs: [
        /*{
            "visible": false,
            "targets": -1
        },*/
        { width: "50px", targets : 0, "className" : "table-no-align-right" },
        { width: "7%", targets : 2 },
        { width: "7%", targets : 3 },
        { width: "7%", targets : 4 },
        { orderable: false, targets: '_all' }
    ],
    fixedColumns: true
});

$(".summary_personal_services").css("width","100%");


/****************************************
 *       summary_insured_services Table                   *
 ****************************************/
$('#summary_insured_services').removeAttr('width').DataTable({
    "lengthChange": false,
    "ordering": false,
    "autoWidth": false,
    "paging":   false,
    "info":     false,
    "searching": false,
   
    columnDefs: [
        /*{
            "visible": false,
            "targets": -1
        },*/
        { width: "15px;", targets : 0 },
        { width: "7%", targets : 2 },
        { width: "7%", targets : 3 },
        { width: "7%", targets : 4 },
        { width: "7%", targets : 5 },
        { width: "7%", targets : 6 },
       
        { width: "7%", targets : 7 },
        { orderable: false, targets: '_all' }
    ],
    fixedColumns: true
});

$(".summary_insured_services").css("width","100%");

/****************************************
 *       summary_insured_services Table                   *
 ****************************************/
$('#summary_insured_nation_wide').removeAttr('width').DataTable({
    "lengthChange": false,
    "ordering": false,
    "autoWidth": false,
    "paging":   false,
    "info":     false,
    "searching": false,
   
    /*"columns": [
        { "width": "10%" },
        { "width": "20%" },
        { "width": "30%" },
        { "width": "40%" }
    ],*/
   
    columnDefs: [
        /*{
            "visible": false,
            "targets": -1
        },*/
        { width: "5px", targets : 0 },
        { width: "100px", targets : 1 },
       
        { orderable: false, targets: '_all' }
    ],
    fixedColumns: true
});

$(".summary_insured_nation_wide").css("width","100%");




/****************************************
 *       Basic Table                   *
 ****************************************/
$('#summary_insured_sso').removeAttr('width').DataTable({
    "lengthChange": false,
    "ordering": false,
    "autoWidth": false,
    "searching": false,
    "info" : true,
    "oLanguage": optionsLanguage,
   
    columnDefs: [
       
        { orderable: false, targets: '_all' }
    ],
    fixedColumns: true
});

$("#summary_insured_sso").css("width","100%");


$('#notification_email').removeAttr('width').DataTable({
    
   
    "lengthChange": false,
    "ordering": false,
    "autoWidth": false,
    "searching": false,
    "info" : true,
    "oLanguage": optionsLanguage,
   
    columnDefs: [
        { width: "5%", targets : 0 },
        { "width": "20%", targets: 5 },
       
       
        { orderable: false, targets: '_all' }
    ],
    fixedColumns: true
});

$("#notification_email").css("width","100%");

/****************************************
 *       Default Order Table           *
 ****************************************/
/*$('#default_order').DataTable({
    "order": [
        [3, "desc"]
    ]
});*/

/****************************************
 *       Multi-column Order Table      *
 ****************************************/
/*$('#multi_col_order').DataTable({
    columnDefs: [{
        targets: [0],
        orderData: [0, 1]
    }, {
        targets: [1],
        orderData: [1, 0]
    }, {
        targets: [4],
        orderData: [4, 0]
    }]
});*/

/****************************************
 *       Complex header Table          *
 ****************************************/
$('#complex_header').DataTable();

/****************************************
 *       DOM positioning Table         *
 ****************************************/
$('#DOM_pos').DataTable({
    "dom": '<"top"i>rt<"bottom"flp><"clear">'
});

/****************************************
 *     alternative pagination Table    *
 ****************************************/
$('#alt_pagination').DataTable({
    "pagingType": "full_numbers"
});

/****************************************
 *     vertical scroll Table    *
 ****************************************/
$('#scroll_ver').DataTable({
    "scrollY": "300px",
    "scrollCollapse": true,
    "paging": false
});

/****************************************
 * vertical scroll,dynamic height Table *
 ****************************************/
$('#scroll_ver_dynamic_hei').DataTable({
    scrollY: '50vh',
    scrollCollapse: true,
    paging: false
});

/****************************************
 *     horizontal scroll Table    *
 ****************************************/
$('#scroll_hor').DataTable({
    "scrollX": true
});

/****************************************
 * vertical & horizontal scroll Table  *
 ****************************************/
$('#scroll_ver_hor').DataTable({
    "scrollY": 300,
    "scrollX": true
});

/****************************************
 * Language - Comma decimal place Table  *
 ****************************************/
$('#lang_comma_deci').DataTable({
    "language": {
        "decimal": ",",
        "thousands": "."
    }
});

/****************************************
 *         Language options Table      *
 ****************************************/
$('#lang_opt').DataTable({
    "language": {
        "lengthMenu": "Display _MENU_ records per page",
        "zeroRecords": "Nothing found - sorry",
        "info": "แสดง page _PAGE_ of _PAGES_",
        "infoEmpty": "No records available",
        "infoFiltered": "(filtered from _MAX_ total records)"
    }
});