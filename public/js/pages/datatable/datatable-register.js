

function addRow(table, datas) {
    for (var i = 0; i < datas.length; i++) { 
        
        let name = datas[i]['FirstName'];

        if (datas[i]['LastName']) {
            name = `${name} ${datas[i]['LastName']}`
        }

        let btEdit = "";
        let id = datas[i]['RegisterID'];
        if (datas[i]['TrackingTime'] == 0) {
            if (datas[i]['isActiveRegister'] == 1) {
                let path = config.routes.search;
                path += "?id="+  datas[i]['RegisterID'];
                //console.log("path = ",path);
                btEdit = `<button type="button" class="btn btn-outline-success waves-effect waves-light" onclick="window.location='${path}'"><i class="fas fa-pencil-alt"></i> แก้ไข</button>
                <button type="button" class="btn btn-outline-danger waves-effect waves-light" onclick="cancel(${id})"><i class="fas fa-eraser"></i> ยกเลิก</button>
                `;
            } else {
                btEdit = `<i class="text-danger mdi mdi-checkbox-blank-circle"></i> <code>ยกเลิก</code>`;
            }
        }

        let linkRegisterNumber = `<a href="/register-officer-view?id=${datas[i]['RegisterID']}">${datas[i]['RegisterNumber']}</a>`;
        //console.log("btEdit = ",btEdit);

        //table.row.add([i+1 ,datas[i]['PersonalID'],datas[i]['RegisterNumber'], name, getDateString(datas[i]['RegisterDate']), btEdit]).draw();
        
        table.row.add([i+1 ,datas[i]['PersonalID'], linkRegisterNumber, name, getDateString(datas[i]['RegisterDate']), btEdit]).draw().nodes().to$().find('td:eq(5)').each(function() {$(this).attr('id', 'td-' + id  );});
    }
}