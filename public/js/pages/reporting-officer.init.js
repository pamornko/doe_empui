$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var cYear = new Date().getFullYear();
    var bYear = cYear - 1
    var bYearBE = bYear + 543;
    $("#StartDate").val("01/10/"+bYearBE);
    
    var transactions = $('#reporting_search_table').removeAttr('width').DataTable({
        serverSide: true,
        ordering: false,
        processing: true,
        searching: false,
        oLanguage: optionsLanguage,
        bLengthChange : false, //thought this line could hide the LengthMenu
        pageLength: pageLength,
        "ajax": {
            "url": "/reporting-search",
            "type": "POST",
     
            "data": function ( d ) {
                d.StartDate = $("#StartDate").val();
                d.EndDate = $("#EndDate").val();
                d.Organization = $("#Organization").val();
                d.RegisterNumber = $("#RegisterNumber").val();
                d.Service = $("#Service").val();
                return $.extend( {}, d);
            }
        },
        "columns": [
            { "data": "id", render: getIndex},
            { "data": "register_number"},
            { "data": "national_id"},
            { "data": "name"},
            { "data": "tracking_time"},
            { "data": "reporting_duedate"},
            { render : getView},
            { render : getActions}
        ],
        columnDefs: [
            { width: "5%", targets : 0, "className" : "table-no-align-right" },
            { "width": "5%", targets: 4 , "className" : "table-text-align-center"},
            { "width": "10%", targets: 5  , "className" : "table-text-align-center"},
            { "width": "10%", targets: 6 , "className" : "table-text-align-center"},
            { orderable: false, targets: '_all' }
        ],
        fixedColumns: true
    });
    $("#reporting_search_table").css("width","100%");

    function getIndex(data, type, full, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
    }

    function getView(data, type, full, meta) {
        let viewPath = config.routes.view;
        viewPath += "?id="+  full.id;
        if (full.reporting_date) {
            return `<a href="${viewPath}"> ${full.reporting_date} </a>`;
        } else {
            return ``;
        }
    }

    function getActions(data, type, full, meta) {
        if (full.reporting_date) {
            let path = config.routes.search;
            path += "?id="+  full.id;
            return `<button class="btn btn-outline-success waves-effect waves-light" onclick="window.location='${path}'"><i class="fas fa-pencil-alt"></i> แก้ไข</button>`;
        } else {
            return ``;
        }
    }

    $("#reporting-search").submit(function(event){
        event.preventDefault();

        if ($("#RegisterNumber").val() === "") {
            if (($("#StartDate").val() == "") &&  ($("#EndDate").val() == "")) {
                //Noti
                toastr.error('กรุณา ใส่คำค้น หรือวันที่ <br>สำหรับค้นหาข้อมูล.', 'คำเตือน');
                $("#RegisterNumber").focus();
                return
            } else {
                if (($("#StartDate").val() == "")) {
                    //Noti
                    toastr.error('กรุณา เลือกวันที่ สำหรับค้นหาข้อมูล.', 'คำเตือน!');
                    return
                }
            }
        }

        transactions.ajax.reload();

        /*showLoadingModal();
        
        var dataString = $(this).serialize();
        $.ajax({
            type : 'post',
            url: 'reporting-search',
            data: dataString,
            //dataType: "html",
            success: function (datas) { 
                var dataTable = $('#reporting_search_table').DataTable();
                dataTable.clear().draw();
                addRow(dataTable, datas.ReportingList);

                dataTable.recordsTotal = 1000;
                hideLoadingModal();
            },
            error : function (err) { 
                hideLoadingModal();
            }
        }); */

    });

    $.datetimepicker.setLocale('th'); // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
    // กรณีใช้แบบ input
    $(".custom-date-picker").datetimepicker({
        timepicker:false,
        format:'d/m/Y',  // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000            
        lang:'th',  // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
        onSelectDate:function(dp,$input){
            var yearT=new Date(dp).getFullYear();  
            var yearTH=yearT+543;
            var fulldate=$input.val();
            var fulldateTH=fulldate.replace(yearT,yearTH);
            $input.val(fulldateTH);
        },
    }); 

});

function exportExcel() {
    var RegisterNumber = $("#RegisterNumber").val();
    var Organization = $("#wOrganization").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();
    var Service = $("#Service").val();

    const sDateArr = StartDate.split("/");
    if (sDateArr.length == 3) {
        let year = sDateArr[2] - 543;
        StartDate = year+"-"+sDateArr[1]+"-"+sDateArr[0];
    }
    const eDateArr = EndDate.split("/");
    if (eDateArr.length == 3) {
        let year = eDateArr[2] - 543;
        EndDate = year+"-"+eDateArr[1]+"-"+eDateArr[0];
    }

    var path = 'export_excel_reporting';
    //var path = 'http://164.115.45.124/doe_sjc_api/api/EmpuiReport/tracking_report';
    var url = path+'?RegisterNumber='+ RegisterNumber+"&Organization="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate+"&Service="+Service ;
    window.location=url;
}

function exportPDF() {
    var RegisterNumber = $("#RegisterNumber").val();
    var Organization = $("#wOrganization").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();
    var Service = $("#Service").val();

    const sDateArr = StartDate.split("/");
    if (sDateArr.length == 3) {
        let year = sDateArr[2] - 543;
        StartDate = year+"-"+sDateArr[1]+"-"+sDateArr[0];
    }
    const eDateArr = EndDate.split("/");
    if (eDateArr.length == 3) {
        let year = eDateArr[2] - 543;
        EndDate = year+"-"+eDateArr[1]+"-"+eDateArr[0];
    }

    var path = 'export_pdf_reporting';
    var url = path+'?RegisterNumber='+ RegisterNumber+"&Organization="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate+"&Service="+Service ;
    window.location=url;
}

function print() {
    var RegisterNumber = $("#RegisterNumber").val();
    var Organization = $("#wOrganization").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();
    var Service = $("#Service").val();

    const sDateArr = StartDate.split("/");
    if (sDateArr.length == 3) {
        let year = sDateArr[2] - 543;
        StartDate = year+"-"+sDateArr[1]+"-"+sDateArr[0];
    }
    const eDateArr = EndDate.split("/");
    if (eDateArr.length == 3) {
        let year = eDateArr[2] - 543;
        EndDate = year+"-"+eDateArr[1]+"-"+eDateArr[0];
    }

    var path = 'print_reporing';
    var url = path+'?RegisterNumber='+ RegisterNumber+"&Organization="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate+"&Service="+Service ;
    //window.location=url;

    window.open(url, '_blank');
}
