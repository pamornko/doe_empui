$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});


function downloadPDFFile(btn) {
    var id = btn.dataset["id"];
    $.ajax({

        type: 'GET',

        url: '/download_pdf_sso?id='+id,

        id: id,

        xhrFields: {

            responseType: 'blob'

        },

        success: function(response){
            var blob = new Blob([response]);
            var link = document.createElement('a');

            link.href = window.URL.createObjectURL(blob);
            link.target = '_blank';
            //link.download = "Sample.pdf";
            link.download = "สปส.2-01-7.pdf";
            link.click();

        },

        error: function(blob){

            console.log(blob);

        }

    });
}
