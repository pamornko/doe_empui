$(document).ready(function(){
          
    $("#report-search").submit(function(event){
        event.preventDefault();

        /*if ($("#RegisterNumber").val() === "") {
            toastr.error('กรุณา ใส่คำค้น สำหรับค้นหาข้อมูล.', 'คำเตือน');
            $("#RegisterNumber").focus();
            return
        }*/
        showLoadingModal();
        var dataString = $(this).serialize();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type : 'post',
            url: 'notification_email_logs_search',
            data: dataString,
            //dataType: "html",
            success: function (datas) { 
                var dataTable = $('#notification_email').DataTable();
                dataTable.clear().draw();

                addRow(dataTable, datas.reportList);
                hideLoadingModal();
            },
            error : function (err) { 
                hideLoadingModal();
            }
        }); 

    });
});

function exportExcel() {
    var RegisterNumber = $("#RegisterNumber").val();
    var TrackingTime = $("#TrackingTime").val();

    var path = 'export_excel_notification_email_logs';
    var url = path+"?RegisterNumber="+RegisterNumber+"&TrackingTime="+TrackingTime;
    window.location=url;
}