$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var cYear = new Date().getFullYear();
    var bYear = cYear - 1
    var bYearBE = bYear + 543;

    $("#StartDate").val("01/10/"+bYearBE);
    
    var transactions = $('#register_table').removeAttr('width').DataTable({
        serverSide: true,
        ordering: false,
        processing: true,
        searching: false,
        oLanguage: optionsLanguage,
        bLengthChange : false, //thought this line could hide the LengthMenu
        pageLength: pageLength,
        "ajax": {
            "url": "/register-search",
            "type": "POST",
     
            "data": function ( d ) {
                d.StartDate = $("#StartDate").val();
                d.EndDate = $("#EndDate").val();
                d.Organization = $("#wOrganization").val();
                d.RegisterNumber = $("#RegisterNumber").val();
                d.Service = $("#Service").val();
                return $.extend( {}, d);
            }
        },
        "columns": [
            { "data": "id", render: getIndex},
            { "data": "national_id"},
            { render : getView},
            { "data": "name"},
            { "data": "register_date"},
            { render : getActions}
        ],
        columnDefs: [
            { width: "5%", targets : 0, "className" : "table-no-align-right" },
            { "width": "20%", targets: 1 },
            { "width": "20%", targets: 2 },
            { "width": "20%", targets: 3 },
            { "width": "15%", targets: 4, "className" : "table-text-align-center" },
            { orderable: false, targets: '_all' }
        ],
        fixedColumns: true
    });
    $("#register_table").css("width","100%");

    function getIndex(data, type, full, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
    }

    function getView(data, type, full, meta) {
        return `<a href="/register-officer-view?id=${full.id}">${full.register_number}</a>`;
    }

    function getActions(data, type, full, meta) {
        let path = config.routes.search;
        path += "?id="+full.id;
        console.log(full);
        if (full.tracking_time == 0) {
            if (full.sso) {
                return `<button class="btn btn-outline-success waves-effect waves-light download-pdf-file" data-id="${full.id}" download="แบบฟอร์ม-สปส.2-01-7" onclick="downloadPDFFile(this)"><i class="fas fa-print"></i> พิมพ์แบบ สปส.2-01/7</button>`;
            } else {
                if (full.active == 1) {
                    return `
                    <button class="btn btn-outline-success waves-effect waves-light download-pdf-file" data-id="${full.id}" download="แบบฟอร์ม-สปส.2-01-7" onclick="downloadPDFFile(this)"><i class="fas fa-print"></i> พิมพ์แบบ สปส.2-01/7</button>
                    <button type="button" class="btn btn-outline-success waves-effect waves-light" onclick="window.location='${path}'"><i class="fas fa-pencil-alt"></i> แก้ไข</button>
                    <button type="button" class="btn btn-outline-danger waves-effect waves-light" onclick="cancel(${full.id})"><i class="fas fa-eraser"></i> ยกเลิก</button>`
                } else {
                    return `<i class="text-danger mdi mdi-checkbox-blank-circle"></i> <code>ยกเลิก</code>`;
                }
            }
        } else {
            return ``;
        }
    }
            
    $("#register-search").submit(function(event){
        event.preventDefault();

        if ($("#RegisterNumber").val() === "") {
            if (($("#StartDate").val() == "") &&  ($("#EndDate").val() == "")) {
                //Noti
                toastr.error('กรุณา ใส่คำค้น หรือวันที่ <br>สำหรับค้นหาข้อมูล. ', 'คำเตือน');
                $("#RegisterNumber").focus();
                return
            } else {
                if (($("#StartDate").val() == "")) {
                    //Noti
                    toastr.error('กรุณา เลือกวันที่ สำหรับค้นหาข้อมูล.', 'คำเตือน!');
                    return
                }
            }
        }
        //showLoadingModal();
        transactions.ajax.reload();

        /*$.ajax({
            type : 'post',
            url: 'register-search',
            data: dataString,
            //dataType: "html",
            success: function (datas) { 
                var dataTable = $('#register_table').DataTable();
                dataTable.clear().draw();
                addRow(dataTable, datas.registers)
                dataTable.recordsTotal = 1000;
                dataTable.recordsFiltered = 25;
                hideLoadingModal();
            },
            error : function (err) { 
                hideLoadingModal();
            }
        }); */

    });

    $.datetimepicker.setLocale('th'); // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
    // กรณีใช้แบบ input
    $(".custom-date-picker").datetimepicker({
        timepicker:false,
        format:'d/m/Y',  // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000            
        lang:'th',  // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
        onSelectDate:function(dp,$input){
            var yearT=new Date(dp).getFullYear();  
            var yearTH=yearT+543;
            var fulldate=$input.val();
            var fulldateTH=fulldate.replace(yearT,yearTH);
            $input.val(fulldateTH);
        },
    }); 

});

function cancel(registerId) {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'mr-2 btn btn-danger'
        },
        buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
        title: 'คุณต้องการยกเลิกข้อมูล ?',
        text: "ต้องการยกเลิกรายการขึ้นทะเบียนนี้หรือไม่ ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            var $container = $('.table-responsive');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type : 'post',
                url: 'cancel_register',
                data: 'RegisterID='+registerId,
                dataType: "json",
                success: function (data) { 
        
                    Swal.close();
                    if (data.success) {
                        Swal.fire({
                            title: "ยกเลิกรายการสำเร็จ",
                            text: "",
                            type: "success",
                            button: "ตกลง",
                        });

                        document.getElementById('td-' + registerId).innerHTML = '<i class="text-danger mdi mdi-checkbox-blank-circle"></i> <code>ยกเลิก</code>';
                        
                    } else {
                        Swal.fire({
                            title: "บันทึกข้อมูลไม่สำเร็จ",
                            text: "กรุณาลองใหม่อีกครั้ง",
                            type: "error",
                            button: "ตกลง",
                        });
                        
                    }
                }
            });
        }
    });

}

function exportExcel() {
    var RegisterNumber = $("#RegisterNumber").val();
    var Organization = $("#wOrganization").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();
    var Service = $("#Service").val();
    const sDateArr = StartDate.split("/");
    if (sDateArr.length == 3) {
        let year = sDateArr[2] - 543;
        StartDate = year+"-"+sDateArr[1]+"-"+sDateArr[0];
    }
    const eDateArr = EndDate.split("/");
    if (eDateArr.length == 3) {
        let year = eDateArr[2] - 543;
        EndDate = year+"-"+eDateArr[1]+"-"+eDateArr[0];
    }

    var path = 'export_excel_register';
   
    //var path = 'http://164.115.45.124/doe_sjc_api/api/EmpuiReport/register_report';
    var url = path+'?RegisterNumber='+RegisterNumber+"&OrganizationID="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate+"&Service="+Service ;
    window.location=url;
}

function exportPDF() {
    var RegisterNumber = $("#RegisterNumber").val();
    var Organization = $("#wOrganization").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();
    var Service = $("#Service").val();

    const sDateArr = StartDate.split("/");
    if (sDateArr.length == 3) {
        let year = sDateArr[2] - 543;
        StartDate = year+"-"+sDateArr[1]+"-"+sDateArr[0];
    }
    const eDateArr = EndDate.split("/");
    if (eDateArr.length == 3) {
        let year = eDateArr[2] - 543;
        EndDate = year+"-"+eDateArr[1]+"-"+eDateArr[0];
    }

    var path = 'export_pdf_register';
    var url = path+'?RegisterNumber='+ RegisterNumber+"&OrganizationID="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate+"&Service="+Service  ;
    window.location=url;
}

function print() {
    var RegisterNumber = $("#RegisterNumber").val();
    var Organization = $("#wOrganization").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();
    var Service = $("#Service").val();

    const sDateArr = StartDate.split("/");
    if (sDateArr.length == 3) {
        let year = sDateArr[2] - 543;
        StartDate = year+"-"+sDateArr[1]+"-"+sDateArr[0];
    }
    const eDateArr = EndDate.split("/");
    if (eDateArr.length == 3) {
        let year = eDateArr[2] - 543;
        EndDate = year+"-"+eDateArr[1]+"-"+eDateArr[0];
    }

    var path = 'print_register';
    var url = path+'?RegisterNumber='+ RegisterNumber+"&OrganizationID="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate+"&Service="+Service  ;
    //window.location=url;

    window.open(url, '_blank');
}