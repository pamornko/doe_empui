$(document).ready(function(){
    loadContent();

    $("#report-search").submit(function(event){
        event.preventDefault();
        if (($("#StartDate").val() == "") || ($("#EndDate").val() == "")) {
            //Noti
            toastr.error('กรุณา เลือกวันที่ สำหรับค้นหาข้อมูล.', 'คำเตือน!');
            return
        }
        
        var dataString = $(this).serialize();
        loadContent(dataString);

    });
});

function loadContent(dataString) {
    showLoadingModal();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type : 'post',
        url: 'report_summary_nationwide_search',
        data: dataString,
        //dataType: "html",
        success: function (data) { 
            $('.table-responsive').html(data.html);
            hideLoadingModal();
        },
        error : function (err) { 
            hideLoadingModal();
        }
    }); 
}

function exportPDF() {
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();

    var path = 'export_pdf_summary_insured_nation_wide';
    var url = path+"?StartDate="+StartDate+"&EndDate="+EndDate ;
    window.location=url;
}

function exportExcel() {
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();

    var path = 'export_excel_summary_insured_nation_wide';
    var url = path+"?StartDate="+StartDate+"&EndDate="+EndDate ;
    window.location=url;
}

function print() {
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();

    var path = 'print_summary_insured_nation_wide';
    var url = path+"?StartDate="+StartDate+"&EndDate="+EndDate ;

    window.open(url, '_blank');
}