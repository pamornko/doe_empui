$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).on('click', '.fav-button', function() {
    var id = $(this).attr('data-id');
    const swalWithBootstrapButtons = Swal.mixin({
        /*customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'mr-2 btn btn-danger'
        },
        buttonsStyling: false,*/
    });
    swalWithBootstrapButtons.fire({
        //title: 'คุณต้องการยกเลิกใช่หรือไม่ ?',
        text: "คุณยืนยันการเก็บงานนี้ ในประวัติงานนี้ หรือไม่ ?",
        type: 'info',
        showCancelButton: true,
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type : 'post',
                url : 'job_fav',
                data : { 'ID' : id},
                success : function(datas) {
                    console.log("Fav job success ",datas);
                    $('#fav-area').addClass("fav-icon");
                    $( "#fav-area" ).prop( "disabled", true );
                },
                error : function(err) {
                    console.log("Fav job error ",err);
                }
            });
        }
    });
});

$(document).on('click', '.apply-job-button', function(env) {
    env.stopPropagation();
    var parent = $(this).parents('.bd-highlight');
    var btn = parent.find('.applying-job-button');
    var id = $(this).attr('data-id');

    var kParent = '#body-area-'+id;
    var oParent = $(kParent);
    var aBtn = oParent.find('.applying-job-button');
    var bBtn = oParent.find('.apply-job-button');


    Swal.fire({
        //title: 'คุณต้องการยกเลิกใช่หรือไม่ ?',
        text: "คุณยืนยัน การสมัครงานนี้ หรือไม่ ?",
        type: 'info',
        showCancelButton: true,
        confirmButtonText: 'สมัครเลย !',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            var my =  $(this);
            $.ajax({
                type : 'post',
                url : 'job_apply',
                data : { 'ID' : id},
                success : function(datas) {

                    reloadApplyJobs();
                    my.addClass("e-hide");
                    btn.removeClass("e-hide");
                    
                    bBtn.addClass("e-hide");
                    aBtn.removeClass("e-hide");
                },
                error : function(err) {
                    console.log("Apply job error ",err);
                }
            });
        }
    });
});

function reloadApplyJobs() {
    $.ajax({
        type : 'get',
        url : 'reloadApplyJobs',
       
        success : function(datas) {
           
            $('.applying-jobs-table').html(datas.html);
            if (datas.jobs.length > 0) {
                $(".form-check-inline").hide();
            }
        },
        error : function(err) {
            console.log("Apply job error ",err);
        }
    });
}