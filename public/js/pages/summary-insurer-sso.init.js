$(document).ready(function(){
    var dp=$("#datepicker").datepicker( {
        format: "mm-yyyy",
        startView: "months", 
        minViewMode: "months"
    });

    /*dp.on('changeMonth', function (e) {    
        //do something here
        //alert("Month changed");
    });*/

    $("#report-search").submit(function(event){
        event.preventDefault();
        
        showLoadingModal();
        var dataString = $(this).serialize();

        console.log("dataString = ",dataString);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type : 'post',
            url: 'report_officer_summary_insured_sso_search',
            data: dataString,
            //dataType: "html",
            success: function (data) { 
                //console.log('data = ',data.html);
                $('.table-responsive').html(data.html);
                hideLoadingModal();
            },
            error : function (err) { 
                hideLoadingModal();
            }
        }); 

    });
});

function exportPDF() {
    var DisplayType = $("#DisplayType").val();
    var StartDate = $("#datepicker").val();

    var path = 'export_pdf_summary_insured_sso';
    var url = path+"?DisplayType="+DisplayType+"&StartDate="+StartDate;
    window.location=url;
}

function exportExcel() {
    var DisplayType = $("#DisplayType").val();
    var StartDate = $("#datepicker").val();

    var path = 'export_excel_summary_insured_sso';
    var url = path+"?DisplayType="+DisplayType+"&StartDate="+StartDate;
    window.location=url;
}

function print() {
    var DisplayType = $("#DisplayType").val();
    var StartDate = $("#datepicker").val();

    var path = 'print_summary_insured_sso';
    var url = path+"?DisplayType="+DisplayType+"&StartDate="+StartDate;
    window.open(url, '_blank');
}