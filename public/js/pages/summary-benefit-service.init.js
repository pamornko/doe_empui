$(document).ready(function(){

    loadContent();

    $("#report-service-search").submit(function(event){
        event.preventDefault();
        
        var dataString = $(this).serialize();

        console.log("dataString = ",dataString);
        loadContent(dataString);
    });

});

function loadContent(dataString){
    showLoadingModal();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type : 'post',
        url: 'report_sso_summary_insured_services_search',
        data: dataString,
        //dataType: "html",
        success: function (data) { 
            console.log('data = ',data.html);
            $('.table-responsive').html(data.html);
            hideLoadingModal();
        },
        error : function (err) { 
            hideLoadingModal();
        }
    }); 

}

function exportPDF() {
    var Organization = $("#OrganizationID").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();

    var path = 'export_pdf_summary_benefit_sso';
    var url = path+"?OrganizationID="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate ;
    window.location=url;
}

function exportExcel() {
    var Organization = $("#OrganizationID").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();

    var path = 'export_excel_summary_benefit_sso';
    var url = path+"?OrganizationID="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate ;
    window.location=url;
}

function print() {
    var Organization = $("#OrganizationID").val();
    var StartDate = $("#StartDate").val();
    var EndDate = $("#EndDate").val();

    var path = 'print_summary_benefit_sso';
    var url = path+"?OrganizationID="+Organization+"&StartDate="+StartDate+"&EndDate="+EndDate ;
    window.open(url, '_blank');
}