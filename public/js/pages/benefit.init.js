$(document).ready(function(){
    $("#info-block").hide();

    $("#report-search").submit(function(event){
        event.preventDefault();

        if ($("#criteria").val() === "") {
            toastr.error('กรุณา ใส่คำค้น สำหรับค้นหาข้อมูล.', 'คำเตือน');
            $("#criteria").focus();
            return
        }

        showLoadingModal();
        
        var dataString = $(this).serialize();

        //console.log("dataString = ",dataString);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type : 'post',
            url: 'benefit_report_search',
            data: dataString,
            //dataType: "html",
            success: function (data) { 
                var dataTable = $('#report_benefit_table').DataTable();
                dataTable.clear().draw();
                if (data) {
                    if (data.reportings) {
                        $("#info-block").show();
                        $('#fName').text(data.userInfo.FirstName);
                        $('#lName').text(data.userInfo.LastName);
                        $('#rNumber').text(data.userInfo.RegisterNumber);
                    } else {
                        $("#info-block").hide();
                    }
                    
                    if (data.registerId) {
                        $('#registerId').val(data.registerId);
                    }
                    
                    if (data.reportings) {
                        
                        for (var i = 0; i < data.reportings.length; i++) { 
                            let dueDate = getDateString(data.reportings[i]['ReportingDueDate']);
                            let reportingDate = "";
                            if (data.reportings[i]['ReportingDate']) {
                                reportingDate = getDateString(data.reportings[i]['ReportingDate']);
                            }

                            let lStatust = "";
                            if (data.reportings[i]['payNo']) {
                                lStatust = `<span>${data.reportings[i]['financeStatusDesc']}</span>`;
                            } else if (data.reportings[i]['ReportingDate']) {
                                lStatust = `<span>อยู่ระหว่างพิจารณาโดยสำนักงานประกันสังคม</span>`;
                            }
                            dataTable.row.add([data.reportings[i].TrackingTime, dueDate, reportingDate, lStatust]).draw();
                        }
                    }
                }
                hideLoadingModal();
            },
            error : function (err) { 
                hideLoadingModal();
            }
        }); 

    });
});

$(document).on('click', '.view-benefit', function() {
    var registerId = $("#registerId").val();
    console.log("registerId = ",registerId);
    $('.modal-body').html('<div class="d-flex justify-content-center"><div class="spinner-border" style="width: 3rem; height: 3rem;" role="status"><span class="sr-only">Loading...</span></div></div>');
    $('#benefit-detail-modal').modal('show');
    //window.open("/job_details?ID="+jobID, "_blank");
    $.ajax({
        type : 'get',
        url : 'register-details-view',
        data : { 'id' : registerId},
        success : function(datas) {
            $('.modal-body').html(datas.html);
        },
        error : function(err) {
            console.log("Job details error ",err);
        }
    });

});