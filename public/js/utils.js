function leftFillNum(num, targetLength) {
    return num.toString().padStart(targetLength, 0);
}

function getDateString(d) {
    let strDt = "";
    if (d) {
        var dt = new Date(d);
        let dd = dt.getDate();

        let mm = dt.getMonth()+1; 
        let yyyy = dt.getFullYear() + 543;
        mm = leftFillNum(mm, 2);
        strDt = `${dd}/${mm}/${yyyy}`;
    }
    return strDt;
}

function getDateTimeString(d) {
    let strDt = "";
    if (d) {
        var dt = new Date(d);
        let mi = ('0'+dt.getMinutes()).slice(-2);
        let hr = dt.getHours();
        let dd = dt.getDate();

        let mm = dt.getMonth()+1; 
        let yyyy = dt.getFullYear() + 543;
        strDt = `${dd}/${mm}/${yyyy} ${hr}:${mi}`;

        console.log("hr = ",hr);
        console.log("mi = ",mi);
    }
    console.log(moment(strDt, 'MM/DD/YYYY HH:mm'));
    return strDt;
}