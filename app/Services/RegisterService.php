<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class RegisterService extends Service {
    
    public function update($reqInput)
    {
        //dump($reqInput);
        $response = Http::asForm()->post($this->getApiDomain() . config('app.registerUpdate'), $reqInput)->json();
        //dump($response);
        return $response;
    }

    public function cancel($reqInput)
    {
        $response = Http::asForm()->put($this->getApiDomain() . "EmpuiRegister/", $reqInput)->json();
        return $response;
    }

    public function creatFreelance($reqInput)
    {
        //dump($reqInput);
        $response = Http::asForm()->post($this->getApiDomain() . config('app.registerFreelance'), $reqInput)->json();
        //dump($response);
        return $response;
    }

    public function getRegisterAll($reqInput)
    {
        //dump($reqInput);
        $response = Http::asForm()->post($this->getApiDomain() . config('app.registerList'), $reqInput)->json();
        //dump($response);
        return $response;
    }

    public function getDetails($reqInput)
    {
        //dump($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.registerDetail'), $reqInput)->json();
        //dump($response);
        return $response;
    }


    public function getRegisterLatest($reqInput)
    {
        $response = Http::get($this->getApiDomain() . config('app.GetLastactive'), $reqInput)->json();
        return $response;
    }
}