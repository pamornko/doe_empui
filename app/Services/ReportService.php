<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class ReportService extends Service {

    public function getOfficeServeReport($reqInput)
    {
        //dump(config('app.officeServeService') . '?' .$reqInput);
        $response = Http::get($this->getApiDomain() . config('app.officeServeService') . '?' .$reqInput)->json();
        //dump($response);
        return $response;
    }

    public function getOfficeNationWideReport($reqInput)
    {
        //dump(config('app.officeServeService') . '?' .$reqInput);
        $response = Http::get($this->getApiDomain() . config('app.officeReportNationWide') . '?' .$reqInput)->json();
        //dump($response);
        return $response;
    }

    public function getSsoServeReport($reqInput)
    {
        //dump(config('app.ssoServeService') . '?' .$reqInput);
        $response = Http::get($this->getApiDomain() . config('app.ssoServeService') . '?' .$reqInput)->json();
        //dump($response);
        return $response;
    }

    public function getOfficeRegisterReport($reqInput)
    {
        //dump($reqInput);
        $response = Http::asForm()->post($this->getApiDomain() . config('app.officeReportRegister'), $reqInput)->json();
        //dump($response);
        return $response ? $response['data'] : [] ;
    }

    public function getEmailLogs($reqInput)
    {
        $response = Http::asForm()->post($this->getApiDomain() . config('app.emailLogs'), $reqInput)->json();
        //dump($response);
        return $response ? $response['data'] : [] ;
    }

    public function getPersonalReport($reqInput)
    {
        $response = Http::get($this->getApiDomain() . config('app.personalReport')  ,$reqInput)->json();
        return $response ? $response : [];
    }

    public function getProvinceReport($reqInput)
    {
        //dump(config('app.officeServeService') . '?' .$reqInput);
        $response = Http::get($this->getApiDomain() . config('app.provinceReport') . '?' .$reqInput)->json();
        return $response ? $response : [];
    }

    public function getOfficeBenefitReport($reqInput)
    {
        //dump($reqInput);
        $response = Http::asForm()->post($this->getApiDomain() . config('app.BenefitList'), $reqInput)->json();
        //dump($response);
        return $response ? $response['data'] : [] ;
    }

}