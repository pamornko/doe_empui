<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class Service {
    protected $apiPrefix = "";
    protected $sjcPrefix = "";

    private $paramForMasterData = [
        'draw' => 1,
        'start' => 0,
        'length' => 100
    ];

    private $lengthDefault = 100;

    function __construct()
    {
        $this->apiPrefix = config('app.apiPrefix');
        $this->sjcPrefix = config('app.sjcrefix');
    }

    public function getApiDomain() {
        return $this->apiPrefix;
    }

    public function getSJCDomain() {
        return $this->sjcPrefix;
    }

    public function getRequestParams() {
        return $this->paramForMasterData;
    }

    public function getRequestDefaultParams($length) {
        $params = $this->paramForMasterData;
        $params['length'] = $length ? $length : $this->lengthDefault;
        return $params;
    }

    public function getRequestDefaultCriteria($length) {
        $params = $this->paramForMasterData;
        $params['length'] = $length ? $length : $this->lengthDefault;
        return $params;
    }


    public function getFontsDirPDF() {
        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        return $defaultConfig['fontDir'];
    }

    public function getFontsPDF() {
        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        return $defaultFontConfig['fontdata'] + ['sarabun_new' => ['R' => 'THSarabunNew.ttf','I' => 'THSarabunNew Italic.ttf','B' => 'THSarabunNew Bold.ttf',] ,];
    }

    public function getFormatFile($name , $extension) {
        return date("YmdHi").'-'.$name.'.'.$extension;
    }

    public function buildQuery($fields) {
        $separator = '';
        $fields_string = '';
        foreach($fields as $key=>$value) {
            $fields_string .= $separator . $key . '=' . $value; 
            $separator = '&'; 
        }
        return $fields_string;
    }

    public function curl($method,$url,$post = array()){
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		if($method=='POST'){
			curl_setopt($ch, CURLOPT_POST, 1);  
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		}
		$server_output = curl_exec($ch);
		//print_r($server_output);
		curl_close ($ch);
		return $server_output;
	}
}