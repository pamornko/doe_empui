<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class MenuService extends Service {

    private $UserMenus = [];
    private $Menus = array();
    protected $sjcPrefix = "";
    private $bStyle = "";

    function __construct(InitialService $iService)
    {
        $this->iniServvice = $iService;
        $this->apiPrefix = config('app.apiPrefix');
        $this->sjcPrefix = config('app.sjcrefix');
        $this->bStyle = "border: 1px solid #aaaaaa;";
    }

    private $MenusPath = [
        1310=> '/register/user-index',
        1311=> '/reporting',
        1312=> '/report-user-benefit',
        1314=> '/register-officer',
        1315=> '/reporting-officer',
        1316=> '/report-officer-benefit',
        0=>'',
        1317=> '/report-officer-summary-insured-services',
        1319=> '',
        1326=> '/report_officer_summary_insured_nation_wide',
        1327=> '/report_officer_summary_insured_province',
        1328=> '/report_officer_summary_insured_personal',
        1329=> '',
        1330=> '',
        1333=> '/report_officer_summary_insured_sso',
        1334=> '/notification_email_logs',
        1=>'',
        1324=>'/report-officer-summary-benefit-services',
        1322=>'http://128.199.231.137/doe_sjc_web/',
        1323=>'http://128.199.231.137/doe_sjc_web/ADM/Organization',
        2=>'',
        3=>'',
        5=>'',
        3343=> '',
        3342=> '',
        3344=> '',
        3345=> '',
        3346=> '',
        3347=> '',
        3348=> '',
        3349=> '',
        3338=> 'http://128.199.231.137/doe_sjc_web/ADM/UsersSSO',
        3373=> '/report-officer-benefit',
        3374=> '/register-officer'

     ];

    private $MenusIcon = [
        1310=> 'activity',
        1311=> 'user',
        1312=> 'book-open',
        1314=> 'activity',
        1315=> 'user',
        1316=> 'book-open',
        0=> 'file-text',
        1317=>'grid',
        1319=>'grid',
        1326=>'grid',
        1327=> 'grid',
        1328=> 'grid',
        1329=> 'grid',
        1330=> 'grid',
        1333=> '',
        1334=> 'grid',
        1=> 'grid',
        1324=>'book-open',
        2=>'grid',
        3=>'file-text',
        5=>'file-text',
        3343=> 'grid',
        3342=> 'grid',
        3344=> 'grid',
        3345=> 'grid',
        3346=> 'grid',
        3347=> 'grid',
        3348=> 'grid',
        3349=> 'grid',
        3373=> 'book-open',
        3374=> 'activity'
    ];

    function recursive($array, &$Menus)
    {
        $searchingValue = 'IsEmpui';
        if(is_array($array) && count($array) > 0)
        {
            foreach($array as $key => $value)
            {
                if (isset($value['childs'])) {
                    if (count($value['childs']) > 0) {
                        $this->recursive($value, $Menus);
                    } else {
                        
                        if (isset($value[$searchingValue]) && $value[$searchingValue] == 1) {
                            $Menus[] = $value;
                        }
                    }   
                } else {
                    $this->recursive($value, $Menus);
                }
            }
        }
        return ;
    }

    public function getMenus()
    {
        $UserTypeID = session('UserTypeID');
        if ($UserTypeID) {
            $inputParams= 'user_type_id='.$UserTypeID.'&lang_id=1&IsEmpui=1';
            $MenusMaster=$this->iniServvice->getMenus($inputParams);
        
            $this->recursive($MenusMaster, $this->Menus);

            /*$this->UserMenus[] = [
                'MenuName' => 'SJC',
                'MenuPath' => 'http://128.199.231.137/doe_sjc_web/',
                'MenuIcon' => 'cpu',
                'MenuColumn' => '',
                'MenuChilds[]' => array()
            ];*/

            $this->UserMenus[] = [
                'MenuName' => 'หน้าหลัก',
                'MenuPath' => '/',
                'MenuIcon' => 'home',
                'MenuColumn' => ''
            ];

            if ($UserTypeID == 4 || $UserTypeID == 1) {
                $this->UserMenus[2] = [
                    'MenuName' => "จัดการข้อมูลพื้นฐาน",
                    'MenuPath' => $this->MenusPath[2],
                    'MenuIcon' => $this->MenusIcon[2],
                    'MenuColumn' => 'mega-dropdown',
                    'MenuClass' => 'first-b-level',
                    'MenuChilds[]' => array()
                ];
            }
            /*
            - 15,16 type=SSO
            - 1, 2, 3, 4, 5, 6, 7 type=Admin
            - 8, 9, 10 type=Employer (อันนี้ Empui คงไม่ต้องใช้)
            - 12,13,14 type=Employee
            */
            $sjcURL =  $this->sjcPrefix;
            //dd($this->Menus);
            if ($this->Menus) {
                foreach ($this->Menus as $Menu) {
                    if ($UserTypeID >= 12 && $UserTypeID <= 14) {
                        if ($Menu['IsActive']) {
                            $menuID = $Menu['MenuID'];
                            $this->UserMenus[] = [
                                'MenuName' => $Menu['Name'],
                                'MenuPath' => $this->MenusPath[$menuID],
                                'MenuIcon' => $this->MenusIcon[$menuID]
                            ];
                        }
                    } else if ($UserTypeID >= 1 && $UserTypeID <= 7) {
                        
                            $ManageMenuArray = [0, 3342, 3346, 3347, 3348, 3349];
                            $ManageLogsMenuArray = [0, 1334, 3344, 3345];
                            $ReportMenuArray = [0, 1317, 1326, 1327, 1328, 1333];
                            $LogsMenuArray= [0, 1329, 1330];


                            if ($Menu['IsActive'] ) {
                                $menuID = $Menu['MenuID'];
                                $menuName = $Menu['Name'];
                                //dd($menuID);
                                $keyManages= array_search($menuID, $ManageMenuArray);
                                $keyManagesLogs= array_search($menuID, $ManageLogsMenuArray);
                                $keyReport= array_search($menuID, $ReportMenuArray);
                                $keyLogs= array_search($menuID, $LogsMenuArray);
                                //dump($keyLogs);
                                if ($keyReport > 0) {
                                    if(empty($this->UserMenus[3])){
                                        $this->UserMenus[3] = [
                                            'MenuName' => "รายงาน",
                                            'MenuPath' => $this->MenusPath[3],
                                            'MenuIcon' => $this->MenusIcon[3],
                                            'MenuColumn' => 'mega-dropdown',
                                            'MenuClass' => 'first-level',
                                            'MenuChilds[]' => array()
                                        ];
                                    }
                                    
                                    array_push($this->UserMenus[3]['MenuChilds[]'],
                                    array('MenuName' => $menuName, 
                                    'MenuPath' => $this->MenusPath[$menuID], 
                                    'MenuIcon' => $this->MenusIcon[$menuID]));
                                    
                                } else if ($keyLogs > 0) {   
                                    $key = 1;
                                    if(empty($this->UserMenus[$key])){
                                        
                                        $this->UserMenus[$key] = [
                                            'MenuName' => "ประวัติการใช้งาน",
                                            'MenuPath' => $this->MenusPath[$key],
                                            'MenuIcon' => $this->MenusIcon[$key],
                                            'MenuColumn' => '',
                                            'MenuClass' => 'first-a-level',
                                            'MenuChilds[]' => array()
                                        ];

                                    }
                                    //dump($this->UserMenus[$key]);
                                    
                                    array_push($this->UserMenus[$key]['MenuChilds[]'],
                                    array('MenuName' => $menuName, 
                                    'MenuPath' => $this->MenusPath[$menuID], 
                                    'MenuIcon' => $this->MenusIcon[$menuID]));
                                } else if ($keyManages > 0) {   
                                    $key = 2;
                                    if(empty($this->UserMenus[$key])){
                                        
                                        $this->UserMenus[$key] = [
                                            'MenuName' => "ข้อมูลพื้นฐาน",
                                            'MenuPath' => '',
                                            'MenuIcon' => $this->MenusIcon[$key],
                                            'MenuColumn' => '',
                                            'MenuChilds[]' => array()
                                        ];

                                    }

                                    array_push($this->UserMenus[$key]['MenuChilds[]'],
                                    array('MenuName' => $menuName, 
                                    'MenuPath' => $sjcURL.$Menu['URL'], 
                                    'MenuIcon' => $this->MenusIcon[$menuID]));
                                } else if ($keyManagesLogs > 0) {   
                                    $key = 5;
                                    if(empty($this->UserMenus[$key])){
                                        
                                        $this->UserMenus[$key] = [
                                            'MenuName' => "Activity Logs",
                                            'MenuPath' => $this->MenusPath[$key],
                                            'MenuIcon' => $this->MenusIcon[$key],
                                            'MenuColumn' => 'mega-dropdown',
                                            'MenuClass' => 'first-a-level',
                                            'MenuChilds[]' => array()
                                        ];

                                        if ($UserTypeID == 2) {
                                            $this->UserMenus[$key]['MenuClass'] = "first-c-level";
                                        }

                                    }
                                    $path = $sjcURL.$Menu['URL'];
                                    if ($menuID == 1334) {
                                        $path = $this->MenusPath[$menuID];
                                    }
                                    
                                    array_push($this->UserMenus[$key]['MenuChilds[]'],
                                    array('MenuName' => $menuName, 
                                    'MenuPath' => $path , 
                                    'MenuIcon' => $this->MenusIcon[$menuID]));
                                } else if (!$keyLogs){
                                    //dump($c);
                                    $this->UserMenus[$menuID] = [
                                        'MenuName' => $menuName,
                                        'MenuPath' => $this->MenusPath[$menuID],
                                        'MenuColumn' => '',
                                        'MenuIcon' => $this->MenusIcon[$menuID],
                                    ];
                                    //mdi mdi-notification-clear-all
                                }
                                
                            }
                        
                    } else if ($UserTypeID == 15 || $UserTypeID == 16) {
                        //dump($Menu);
                        if ($Menu['IsActive']) {
                            $menuID = $Menu['MenuID'];
                            $menuName = $Menu['Name'];
                            //$Childs = $Menu['childs']['MenuID'];
                            $MasterMenuArray = [0, 1322, 1323, 3338];
                            $keyMaster= array_search($menuID, $MasterMenuArray);
                            if ($keyMaster > 0) {
                                if(empty($this->UserMenus[2])){
                                    $this->UserMenus[2] = [
                                        'MenuName' => "จัดการข้อมูลพื้นฐาน",
                                        'MenuPath' => $this->MenusPath[2],
                                        'MenuIcon' => $this->MenusIcon[2],
                                        'MenuColumn' => '',
                                        'MenuClass' => 'first-a-level',
                                        'MenuChilds[]' => array()
                                    ];
                                }

                                array_push($this->UserMenus[2]['MenuChilds[]'],
                                array('MenuName' => $menuName, 
                                'MenuPath' => $sjcURL.$Menu['URL']));
                                
                            } else {
                                $this->UserMenus[] = [
                                    'MenuName' => $menuName,
                                    'MenuPath' => $this->MenusPath[$menuID],
                                    'MenuIcon' => $this->MenusIcon[$menuID]
                                ];
                            }   
                            
                            //dump($Childs['childs']);
                        }
                    } else if ($UserTypeID == 1) {

                    }
                    
                }

                return $this->UserMenus;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }
}