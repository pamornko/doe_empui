<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class ReportingService extends Service {

    public function update($reqInput)
    {
        //dump($reqInput);
        $response = Http::asForm()->put($this->getApiDomain() . config('app.trackingUpdate'), $reqInput)->json();
        //dump($response);
        return $response;
    }

    public function updateAndRegenerate($reqInput)
    {
        
        $response = Http::asForm()->put($this->getApiDomain() . config('app.trackingUpdateRegenerate'), $reqInput)->json();

        return $response;
    }
    
    public function creatFreelance($reqInput)
    {
        
        $response = Http::asForm()->post($this->getApiDomain() . config('app.trackingFreelance'), $reqInput)->json();
       
        return $response;
    }

    public function getWorkForReporting($reqInput)
    {
        //dump($reqInput);
        $response = Http::asForm()->post($this->getApiDomain() . config('app.trackingWorkDetail'), $reqInput)->json();
        //dump($response);
        return $response ? $response["data"] : [];
    }

    public function getReporting($reqInput)
    {
        $response = Http::asForm()->post($this->getApiDomain() . config('app.trackingSearch'), $reqInput)->json();
        return $response ? $response["data"] : [];
    }

    public function getReportings($reqInput)
    {
        $response = Http::asForm()->post($this->getApiDomain() . config('app.trackingSearch'), $reqInput)->json();
        return $response;
    }

    public function getTracking($reqInput)
    {
        $response = Http::get($this->getApiDomain() . config('app.getTrackingList') . '?RegisterID=' .$reqInput['RegisterID'])->json();
        //dump($response);
        return $response ? $response["data"] : [];
    }

}