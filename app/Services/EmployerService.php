<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class EmployerService extends Service {

    public function getDBDEmployerID($reqInput)
    {
        $response = Http::asForm()->post($this->getSJCDomain() . config('app.dbdEmployerID'). '?'.$reqInput)->json();
        return $response ;
    }
   
}