<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class FreelanceService extends Service {

    public function getMasterFreelances($reqInput)
    {
        //dump($reqInput);
        $response = Http::asForm()->post($this->getApiDomain() . config('app.freelance'), $reqInput)->json();
        //dump($response);
        return $response ? $response["data"] : [];
    }

    public function getFreelance($reqInput)
    {
        $response = Http::asForm()->post($this->getApiDomain() . config('app.freelance'), $reqInput)->json();
        return $response ? $response["data"] : [];
    }
    
    public function getFreelances($reqInput)
    {
        //dump($reqInput);
        $response = Http::asForm()->post($this->getApiDomain() . config('app.trackingFreelanceList'), $reqInput)->json();
        //dump($response);
        return $response ? $response["data"] : [];
    }
    
}