<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class NotificationService {
    protected $apiPrefix = "";

    private $paramForMasterData = [
        'draw' => 1,
        'start' => 0,
        'length' => 20
    ];

    function __construct()
    {
        $this->apiPrefix = config('app.apiPrefix');
    }
    
    function simulateSendEmail() {
       
        $datas['criteria']['ConfigTrackingID'] = 1;

        $searchData = array_merge(
            $datas, 
            $this->paramForMasterData
        );


        $response = Http::asForm()->post($this->apiPrefix . 'EmpuiConfigTracking/test_mail', $searchData)->json();
       
        return $response ;
    }
}