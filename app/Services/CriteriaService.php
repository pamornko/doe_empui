<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class CriteriaService extends Service {

    public function getMinAge()
    {
        return 15;
    }

    public function getMaxAge()
    {
        return 100;
    }

    public function getMinSalary()
    {
        $salary = [];
        $start = 0;
        for($i = 0 ; $i < 13 ; $i++) {
            $start += 10000;
            array_push($salary, [ 'id'=> $start, 'salary'=> sprintf('%s', number_format($start, 2)) ]);
        }
        return $salary;
    }

    public function getMaxSalary()
    {
        $salary = [];
        $start = 0;
        for($i = 0 ; $i < 13 ; $i++) {
            $start += 10000;
            array_push($salary, [ 'id'=> $start, 'salary'=> sprintf('%s', number_format($start, 2)) ]);
        }
        return $salary;
    }


    public function getJobField($reqInput)
    {
        //dump($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.jobCriteria') . '?lang_id=' .$reqInput['langID'])->json();
        //dump($response);
        return $response ;
    }

    public function getBusinessType($reqInput)
    {
        //dump($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.businessType') . '?lang_id=' .$reqInput['langID'])->json();
        //dump($response);
        return $response ;
    }

    public function getDegree($reqInput)
    {
        //dump($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.degree') . '?lang_id=' .$reqInput['langID'])->json();
        //dump($response);
        return $response ;
    }

    public function getWorkExperience($reqInput)
    {
        //dump($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.workExperience') . '?lang_id=' .$reqInput['langID'])->json();
        //dump($response);
        return $response ;
    }

    public function getResignCases($reqInput)
    {
        //dump($reqInput);
        $response = Http::asForm()->post($this->getApiDomain() . config('app.resingCase'), $reqInput)->json();
        //dump($response);
        return $response ? $response["data"] : [];
    }
    
    public function getLayoffCases($reqInput)
    {
        //dump($reqInput);
        $response = Http::asForm()->post($this->getApiDomain() . config('app.layoffCase'), $reqInput)->json();
        //dump($response);
        return $response ? $response["data"] : [];
    }

    public function getSsoList($reqInput)
    {
        //dump($reqInput);
        $response = Http::asForm()->post($this->getApiDomain() . config('app.ssoList'), $reqInput)->json();
        //dump($response);
        //$response = [];
        return $response ? $response["data"] : [];
    }

    public function getProvince($reqInput)
    {
        //dump($reqInput);
        if ($reqInput != "") {
            $reqInput =  '?' .$reqInput;
        }
        $response = Http::get($this->getApiDomain() . config('app.province') . $reqInput)->json();
        //dump($response);
        //$response = [];
        return $response ? $response : null;
    }

    public function getDistrict($reqInput)
    {
        //dump($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.district') . '?' .$reqInput)->json();
        //dump($response);
        //$response = [];
        return $response ? $response : null;
    }

    public function getSubDistrict($reqInput)
    {
        //dump($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.tambon') . '?' .$reqInput)->json();
        //dump($response);
        //$response = [];
        return $response ? $response : null;
    }

    public function getProvinceByName($reqInput)
    {
        $query = $this->buildQuery($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.ProvinceByName') .'?' .$query)->json();
        return $response ? $response : null;
    }

    public function getDistrictByName($reqInput)
    {
        //dump($reqInput);
        $query = $this->buildQuery($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.DistrictByName') . '?' .$query)->json();
        return $response ? $response : null;
    }

    public function getSubDistrictByName($reqInput)
    {
        $query = $this->buildQuery($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.TambonByName') . '?' .$query)->json();

        //$response = [];
        return $response ? $response : null;
    }
}