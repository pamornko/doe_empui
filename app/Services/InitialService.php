<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class InitialService extends Service {

    private $paramForMasterData = [
        'draw' => 1,
        'start' => 0,
        'length' => 50
    ];

    public function getAllOrganization()
    {   
        $criteria['criteria']['ShowHLW'] =  1;
        $criteria['criteria']['ReportSSO'] =  1;
        $search = array_merge(
            $criteria,
            $this->paramForMasterData
        );
        $response = Http::asForm()->timeout(30)->post($this->getApiDomain() . config('app.organization'), $search)->json();
        return $response ? $response["data"] : [];
    }

    public function getMenus($reqInput)
    {
        $response = Http::timeout(30)->get($this->getApiDomain() . config('app.menus') . '?'.$reqInput)->json();
        return $response ;
    }
}