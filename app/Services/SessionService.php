<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use SebastianBergmann\Environment\Console;

class SessionService extends Service {

    public function stampSession($input){
        //dump(session()->all());
        $newLogin = false;
        if ($input) {
            $typeUser = $this->getUserType($input['UserTypeID']);
            if ($input['UserID']) {
                $member = Http::get(config('app.apiPrefix') . config('app.memberProfile'), ['UserID' => $input['UserID'], 'type' => $typeUser])->json();
                $newLogin = $this->setSession( $member, $newLogin );
            }
        }
        return $newLogin;
    }

    private function getUserType($UserTypeID) {
        $type = null;
        switch(true) {
            case in_array($UserTypeID, range(1,7)) : //Admin
                $type = "Admin";
                break;
            case in_array($UserTypeID, range(8,10)) : //Employer
                $type = "Employer";
                break;
            case in_array($UserTypeID, range(12,14)) : //Employee
                $type = "Employee";
                break;
            case in_array($UserTypeID, range(15,16)) : //SSO
                $type = "SSO";
                break;
        }
        return $type;
    }

    public function setSession ( $member, $newLogin ) {
        if (!empty($member)) {
            if ($member["status"] == true) {
        
                if (session('SessionID') != session()->getId()) {
                    $newLogin = true;
                }
                
                $profile = null;
                if (!empty($member["data"]) && !empty($member["data"]["UserInfo"]) ) {
                    $profile = $member["data"]["UserInfo"] ? $member["data"]["UserInfo"] : null;
                    session(['EmployeeID' => $member["data"] ? $member["data"]["UserInfo"]["EmployeeID"] : null]);
                    session(['ProvinceID' => $profile["ProvinceID"]]);
                    session(['Email' => $profile["Email"]]);
                    session(['BirthDate' => $profile["BirthDate"]]);
                    session(['Age' => $profile["Age"]]);
                    session(['Address' => trim($profile["FullAddress"])]);
                    session(['PersonalID' => $profile["PersonalID"]]);
                } else {
                    $profile = $member ? $member["data"] : [];
                    session(['EmployeeID' =>  null]);
                    session(['ProvinceID' => null]);
                    session(['Email' => null]);
                }
            
                $oldUserID = session('UserID') ? session('UserID') : null;
                session(['SessionID' => session()->getId()]);
            
                if (!empty($member["data"])) {
                    $usserTypeID = $member["data"]["UserTypeID"];
                    session(['UserTypeID' => $usserTypeID]);
    
                    $UserCode = $member["data"]["UserCode"];
                    session(['UserCode' => $UserCode]);
    
                    $UserID = $member["data"]["UserID"];
                    session(['UserID' => $UserID]);
                }
                
                //session(['UserTypeID' => $member["data"] ? $member["data"]["UserTypeID"] : null]);
                //session(['UserCode' => $member["data"] ? $member["data"]["UserCode"] : null]);
                //session(['UserID' => $member["data"] ? $member["data"]["UserID"] : null]);
                
                if (!$newLogin && $oldUserID != session('UserID')) {
                    $newLogin = true;
                }
                
                $userID = null;
                if ($profile) {
                    session(['FirstName' => $profile["FirstName"]]);
                    session(['LastName' => $profile["LastName"]]);
                    $userID = $profile["UserID"];
                }
    
                $ssoProfile = Http::asForm()->timeout(60)->post(config('app.apiPrefix') . config('app.synSSOProfile'), [
                    'UserID'=> $userID
                ])->json();
                //dump($ssoProfile);
            
                if ($ssoProfile != null && !empty($ssoProfile)) {
                    session(['activeStatus' => $ssoProfile["activeStatus"] ?? null]);
                    session(['empStartDate' => $ssoProfile["empStartDate"] ?? null]);
                    session(['empResignDate' => $ssoProfile["empResignDate"] ?? null]);
    
                    /*if ($ssoProfile["empStartDate"] >= $ssoProfile["empResignDate"]) {
                    dump("ได้งาน");
                    } else {
                        dump("ว่างาน");
                    }*/
                    //session(['employerName' => $ssoProfile["name"]]);
                    //session(['SSOOfficeCode' => $ssoProfile["ssoBranchCode"] ? $ssoProfile["ssoBranchCode"] : '']);
                    //session(['SSOProvinceCode' => $ssoProfile["ssoProvinceCode"]]);
                    
                    //session(['compAddress' => $ssoProfile["compAddress"]]);
                    //session(['compProviceCode' => $ssoProfile["compProviceCode"]]);
                    //session(['compAmphurCode' => $ssoProfile["compAmphurCode"]]);
                    //session(['compTambonCode' => $ssoProfile["compTambonCode"]]);
                    
                } else {
                    session(['activeStatus' => null]);
                }
            }
        }
        
        return $newLogin;
    }

    public function create($reqInput){
        $response = Http::asForm()->post($this->getApiDomain() . config('app.sessionCreate'), $reqInput)->json();
        return $response;
    }

    public function getProfileDatacenter($token){
		return json_decode($this->curl('GET',config('app.apiDatacenter')."ssoService?cmd=getUserInfo&ssoToken=".$token));
	}

    //Refresh token with Datacetner
    public function refreshTokenDatacenter($token){
		$refreshToken = json_decode($this->curl('GET',config('app.apiDatacenter')."ssoService?cmd=reloadToken&ssoToken=".$token));

        if(empty($refreshToken->status)) redirect(config('app.webDatacenter'));
		if($refreshToken->status==0 && empty($refreshToken->code)) 
			return $refreshToken->newToken;
		return null;
	}

    //Auto Register Employee
    public function loginAutoRegis( $token , $personalID, $userMode ) {
        $params = array(
            "login" => $personalID,
            "token" => $token,
            "userMode" => $userMode,
        );
        return Http::get( config('app.apiPrefix')."Users/loginAutoRegis" , $params )->json();

    }

    //Authentication With SSO
    public function loginWithSSO( $request ) {

        $profile = null;
        
        if ($request->has('token')) {

            //Refresh Token && Get Profile with SSO
            $newToen = $this->refreshTokenDatacenter( $request->get('token') );
            $user_sso = $this->getProfileDatacenter( $newToen );
            
            //Check UserMode from SSO 
            if ( !empty($user_sso->userMode) && $user_sso->userMode==1 ) {

                $profile = $this->loginAutoRegis( $newToen, $user_sso->data->psnId, $user_sso->userMode );
                
                if( !empty($profile) ) {
                    $session = $this->setSession( $profile, false );
                } else {
                    redirect( config('app.webDatacenter') );
                }

            } else {
                redirect( config('app.webDatacenter') );
            }
        }
        return $profile;
    }

    
}