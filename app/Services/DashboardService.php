<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class DashboardService extends Service {

    public function getRegisterDashboard($reqInput)
    {
        $params = http_build_query($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.dashboardRegister') . '?' .$params)->json();
        return $response ? $response : [];
    }

    public function getReportingDashboard($reqInput)
    {
        $params = http_build_query($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.dashboardTraking') . '?' .$params)->json();
        return $response ? $response : [];
    }

    public function getJobDashboard($reqInput)
    {
        $params = http_build_query($reqInput);
        $response = Http::get($this->getApiDomain() . config('app.dashboardJob') . '?' .$params)->json();
        return $response ? $response : [];
    }

    public function getDashboardActive($reqInput)
    {
        $params = http_build_query($reqInput);
        //dump($params);
        $response = Http::get($this->getApiDomain() . config('app.dashboardActive') . '?' .$params)->json();
        //dump($response);
        return $response ? $response : [];
    }
}