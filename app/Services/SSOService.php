<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class SSOService extends Service {

    public function getSSOBenefit($reqInput)
    {
        //dd($reqInput);
        $response = Http::asForm()->post($this->getApiDomain() . config('app.SSOBenefit'), $reqInput)->json();
        return $response;
    }
   
}