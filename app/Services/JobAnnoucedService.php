<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class JobAnnoucedService extends Service{

    protected $apiPrefix = "";

    function __construct()
    {
        $this->apiPrefix = config('app.apiPrefix');
    }

    public function getQualifications()
    {
        $employeeID = session('EmployeeID');
        $provinceID = session('ProvinceID');

        $payLoad = [
            'EmployeeID'=> $employeeID,
            'ProvinceID'=> $provinceID,
            'draw'=> 1
        ];

        $Jobs = Http::asForm()->post($this->apiPrefix . config('app.jobQualifications'), $payLoad)->json();
       
        return $Jobs ? $Jobs['data'] : [];
    }

    public function getMatching($params)
    {
        //dump($params['category']);
        $searchCriteria = "";

        //PositionTypeSearch

        foreach( $params as $key=>$value) {

            
            if ($value) {
                //dump($key);
                //dump($value);
                if ($searchCriteria !== ""){
                    $searchCriteria .= "&";
                }

                switch($key) {
                    case "categoryValue" : 
                        if ($params['category'] == "1") {
                            $searchCriteria .= "JobPosition=".$params['categoryValue'];
                        } else {
                            $searchCriteria .= "Employer=".$params['categoryValue'];
                        }
                    break;
                    case "employmentType" :
                        $searchCriteria .= "EmploymentType=".$value;
                    break;
                    case "jobFieldID" :
                        $searchCriteria .= "JobFieldID=".$value;
                    break;
                    case "businessTypeID" :
                        $searchCriteria .= "ParentBusinessTypeID=".$value;
                    break;
                    case "degreeID" :
                        $searchCriteria .= "DegreeID=".$value;
                    break;
                    case "ageMin" :
                        $searchCriteria .= "Age_Min=".$value;
                    break;
                    case "ageMax" :
                        $searchCriteria .= "Age_Max=".$value;
                    break;
                    case "wageMin" :
                        $searchCriteria .= "Wage_Min=".$value;
                    break;
                    case "wageMax" :
                        $searchCriteria .= "Wage_Max=".$value;
                    break;
                    case "experience" :
                        $searchCriteria .= "Experience=".$value;
                    break;
                }

            }
        }
        
        $search = [
            'search_criteria'=> $searchCriteria
        ];
        
        $payLoad = array_merge(
            $search, 
            $this->getRequestParams()
        );

       // dump($payLoad);

        $Jobs = Http::asForm()->post($this->apiPrefix . config('app.jobMatching'), $payLoad)->json();
        return $Jobs ? $Jobs['data'] : [];
    }

    public function applyJob($reqInput)
    {
        $response = Http::asForm()->post($this->apiPrefix . config('app.applyJob'), $reqInput)->json();
        return $response;
    }

    public function getJobPrivateAIList($criteria, $start, $length)
    {

        $search = "";
        foreach ($criteria as $key => $val) {
            if ($key != "EmployeeID") {
                $search .= "&";
            }
            $search .= sprintf("criteria[%s]=%s", $key, $val);
        }

        if (empty($length)) {
            $payLoad = $this->getRequestParams();
        } else {
            $payLoad = [
                'draw' => 1,
                'start' => $start,
                'length' => $length
            ];
        }
        $payLoad['search_criteria'] = $search;
        $payLoad['length'] = 5;
        $response = Http::asForm()->post($this->apiPrefix . config('app.jobPrivateListAI'), $payLoad)->json();
       
        return $response ;
    }

    public function getJobDetailsAI($criteria)
    { 
        $response = Http::get($this->apiPrefix . config('app.jobDetailsAI'), $criteria)->json();
        return $response;
    }
    
    public function favJob($reqInput)
    {
        $response = Http::asForm()->post($this->apiPrefix . config('app.jobAIFAV'), $reqInput)->json();
        //dump($response);
        return $response;
    }
}