<?php


namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportInsuredSSO implements FromArray, WithMapping ,WithHeadings, ShouldAutoSize , WithEvents
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            'ลำดับ',
            'ชื่อ - นามสกุล ผู้ประกันตน',
            'ที่อยู่',
            'เบอร์โทรศัพท์',
            'เพศ',
            'วัน/เดือน/ปี เกิด',
            'อายุ',
            'วุฒิการศึกษาสูงสุด',
            'เลขขึ้นทะเบียน',
            'วันที่ขึ้นทะเบียน',
            'สถานะการมีงานทำ',
            'วันที่ได้งานทำ'
        ];
    }

    public function map($data, $index): array
    {
        //dd($data);
        return [
            $index + 1,
            $data['FirstName'].' '.$data['LastName'],
            $data['address1'].' '.$data['address2'],
            $data['contactTel'],
            $data['sexDesc'],
            $data['birthDate'],
            $data['birthDate'],
            '',
            $data['RegisterNumber'],
            $data['RegisterNumber'],
            $data['employStatus'] == 'A' ? 'มีงานทำ' : 'ว่างงาน',
            $data['empStartDate']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cnt = count($this->data) + 1;
                $colCnt = count($this->data[0]);
                $cellRange = 'A1:F1'; // All headers
                //$event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);

                $styleArray = [
                    'font' => [
                        'name'      =>  'Tahoma',
                        'size'=> 12,
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                   
                ];


                $event->sheet->getDelegate()->getParent()->getDefaultStyle()->getFont()->setName('Tahoma');
                //$event->sheet->getDelegate()->getStyle('A1:E1')->applyFromArray($styleArray);
                $event->sheet->getDelegate()->getStyle('A1:L1')->applyFromArray($styleArray);

                $event->sheet->getDelegate()->getStyle('B1'.''.($cnt))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('E1:L'.''.($cnt))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                //$event->sheet->getStyle('B1:C'.''.($cnt) )->getNumberFormat()->setFormatCode('#############');
            },
        ];
    }
}

