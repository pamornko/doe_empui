<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

class ExportInsuredPersonal implements  ShouldAutoSize , WithEvents, WithCustomStartCell
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function startCell(): string
    {
        return 'A3';
    }

    public static $alpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    function getColName($index){
       
        $index--;
        $nAlphabets = 26;
        $f = floor($index/pow($nAlphabets,0)) % $nAlphabets;
        $s = (floor($index/pow($nAlphabets,1)) % $nAlphabets)-1;

        $f = $f < 0 ? '' : self::$alpha[$f];
        $s = $s < 0 ? '' : self::$alpha[$s];
        return trim("{$s}{$f}");
    
    }

    public function registerEvents(): array
    {
        return [
            BeforeSheet::class    => function(AfterSheet $event) {
                
            },
            AfterSheet::class    => function(AfterSheet $event) {

                if (count($this->data) > 0) {
                    $cntR = count($this->data[0]['register']) + 2;
                    $col = $this->getColName($cntR);

                    $CaseRegisters = $this->data[0]['register'];
                    $CaseTrackings = $this->data[0]['tracking'];

                    $x = 2;
                    $cntCaseRegisters = count($CaseRegisters) + $x;
                    $endColCaseRegisters = $this->getColName($cntCaseRegisters);
                    $cntCaseTracking = count( $CaseTrackings);
                    $startColCaseTracking = $this->getColName($cntCaseRegisters + 1);
                    $endColCaseTracking = "L";
                    $colRemark = "M";
                    
                    $mergeArr = [
                        'A1:A2',
                        'B1:B2',
                        'C1:'.$endColCaseRegisters.'1', 
                        $startColCaseTracking.'1:'.$endColCaseTracking.'1',
                        $colRemark.'1:'.$colRemark.'2'
                    ];
                    
                    $event->sheet->getDelegate()->setMergeCells($mergeArr);

                    $event->sheet->setCellValue('A1', 'ลำดับ');
                    $event->sheet->setCellValue('B1', 'ชื่อ - นามสกุล');
                    $event->sheet->setCellValue('C1', 'ขึ้นทะเบียนผู้ประกันตน');
                    $event->sheet->setCellValue($startColCaseTracking.'1', 'การรายงานตัวผู้ประกันตน');
                    $event->sheet->setCellValue($colRemark.'1', 'หมายเหตุ');



                    
                    $row = 3;
                    $no = 0;
                  
                    foreach ($this->data as $report) {
                        
                        $col = 3;
                        if ($no == 0) {
                            foreach ($report['register'] as $k => $vo) {
                                $AlphaCol = $this->getColName($col);
                                $event->sheet->setCellValue($AlphaCol.'2', $k);
                                $col +=1;
                            }

                            $AlphaCol = $this->getColName($col);
                            $event->sheet->setCellValue($AlphaCol.'2', "ครั้งที่");
                            $col +=1;

                            $AlphaCol = $this->getColName($col);
                            $event->sheet->setCellValue($AlphaCol.'2', "กำหนดรายงาน");
                            $col +=1;

                            $AlphaCol = $this->getColName($col);
                            $event->sheet->setCellValue($AlphaCol.'2', "วันที่รายงาน");
                            $col +=1;

                            $AlphaCol = $this->getColName($col);
                            $event->sheet->setCellValue($AlphaCol.'2', "ขึ้นทะเบียนหางาน");
                            $col +=1;

                            $AlphaCol = $this->getColName($col);
                            $event->sheet->setCellValue($AlphaCol.'2', "ประกอบอาชีพอิสระ");
                            $col +=1;

                            $AlphaCol = $this->getColName($col);
                            $event->sheet->setCellValue($AlphaCol.'2', "ไม่ประสงค์สมัครงาน");
                            $col +=1;
 
                        }

                        $no +=1;
                        //$rCol = $this->getColName($x);
                        $event->sheet->setCellValue('A'.$row, $no);
                        $event->sheet->setCellValue('B'.$row, $report['FirstName'].' '.$report['LastName']);
                        $col = 3;

                        foreach ($report['register'] as $k => $v) {
                                $AlphaCol = $this->getColName($col);

                                if ($k != "ประเภทการออก") {
                                    if ($v) {
                                        $v = 1;
                                    } else {
                                        $v = "";
                                    }
                                }
                                $event->sheet->setCellValue($AlphaCol.$row, $v);
                                $col +=1;
                            

                            /*foreach ($report['tracking'][$key] as $k => $o) {
                                
                                $AlphaCol = $this->getColName($col);

                                if ($k != "ครั้งที่") {
                                    if ($k == "กำหนดรายงาน" || $k == "วันที่รายงาน") {
                                        $date = date_create($o);
                                        $o = date_format($date,"d/m/Y");
                                    } else {
                                        if ($o) {
                                            $o = 1;
                                        } else {
                                            $o = "";
                                        }
                                    }
                                }
                                

                                $event->sheet->setCellValue($AlphaCol.$row, $o);
                                $col +=1;
                                
                            }*/
                            
                        }

                        if (count($report['tracking']) > 0) {
                            foreach ($report['tracking'] as $k => $v) {
                                $AlphaCol = $this->getColName($col);
                                $event->sheet->setCellValue($AlphaCol.$row, $v);
                                $col +=1;
                                $row += 1;
                            }
                        } else {
                            $row += 1;
                        }
                    }
                    
                    $event->sheet->getDelegate()->setMergeCells($mergeArr);
                    $event->sheet->getDelegate()->getStyle('C1:'.$colRemark.''.($row))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(30);
                    $event->sheet->getDelegate()->getStyle('A2:'.$colRemark.''.($row))->getAlignment()->setWrapText(true);

                    $styleArray = [
                        'font' => [
                            'name'      =>  'Tahoma',
                            'size'=> 12,
                            'bold' => true,
                        ],
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                       
                    ];
    
                    $event->sheet->getDelegate()->getParent()->getDefaultStyle()->getFont()->setName('Tahoma');
                    $event->sheet->getDelegate()->getStyle('A1:'.$colRemark.'2')->applyFromArray($styleArray);
                }
                
            },
        ];
    }
}

