<?php


namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ExportBenefitSSO implements FromArray, WithMapping , ShouldAutoSize , WithEvents, WithCustomStartCell, WithColumnFormatting
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function startCell(): string
    {
        return 'A3';
    }

    public function columnFormats(): array
    {
        return [
           
            'C' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function map($data, $index): array
    {
        //dump($data);
        return [
            $index + 1,
            $data['SSOOfficeName'],
            ''.$data['register']['M'] + $data['register']['F'],
            ''.$data['register']['M'],
            ''.$data['register']['F'],
            ''.$data['tracking']['M'] + $data['tracking']['F'],
            ''.$data['tracking']['M'],
            ''.$data['tracking']['F']
        ];
    }

    public function registerEvents(): array
    {
        return [
            BeforeSheet::class    => function(AfterSheet $event) {
                
            },
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->getDelegate()->setMergeCells(['A1:A2', 'B1:B2', 'C1:E1', 'F1:H1']);

                $event->sheet->setCellValue('A1', 'ลำดับ');
                $event->sheet->setCellValue('B1', 'สำนักงาน/เขต');
                $event->sheet->setCellValue('C1', 'การขึ้นทะเบียนผู้ประกันตน');
                $event->sheet->setCellValue('C2', 'รวม');
                $event->sheet->setCellValue('D2', 'ชาย');
                $event->sheet->setCellValue('E2', 'หญิง');
                $event->sheet->setCellValue('F1', 'รายงานตัว');
                $event->sheet->setCellValue('F2', 'รวม');
                $event->sheet->setCellValue('G2', 'ชาย');
                $event->sheet->setCellValue('H2', 'หญิง');


                $cnt = count($this->data) + 1;
                $colCnt = count($this->data[0]);
                $cellRange = 'A1:E1'; // All headers
                //$event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $styleArray = [
                    'font' => [
                        'name'      =>  'Tahoma',
                        'size'=> 12,
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                   
                ];

                $event->sheet->getDelegate()->getParent()->getDefaultStyle()->getFont()->setName('Tahoma');
                //$event->sheet->getDelegate()->getStyle('A1:E1')->applyFromArray($styleArray);
                $event->sheet->getDelegate()->getStyle('A1:H2')->applyFromArray($styleArray);

                $event->sheet->getDelegate()->getStyle('C1:H'.''.($cnt))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getStyle('C3:H'.''.($cnt) )->getNumberFormat()->setFormatCode('#0');
            },
        ];
    }
}

