<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

class ExportInsuredNationWide implements  ShouldAutoSize , WithEvents, WithCustomStartCell
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function startCell(): string
    {
        return 'A3';
    }

    /*
    public static function getColName($index){
        $index--;
        $nAlphabets = 26;
        $f = floor($index/pow($nAlphabets,0)) % $nAlphabets;

        $f = $f < 0 ? '' : self::$alpha[$f];
        
        return self::$alpha[$f];

    }*/

    public static $alpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    function getColName($index){
       
        $index--;
        $nAlphabets = 26;
        $f = floor($index/pow($nAlphabets,0)) % $nAlphabets;
        $s = (floor($index/pow($nAlphabets,1)) % $nAlphabets)-1;

        $f = $f < 0 ? '' : self::$alpha[$f];
        $s = $s < 0 ? '' : self::$alpha[$s];
        return trim("{$s}{$f}");
    
    }

    public function registerEvents(): array
    {
        return [
            BeforeSheet::class    => function(AfterSheet $event) {
                
            },
            AfterSheet::class    => function(AfterSheet $event) {

                $cntR = count($this->data[0]['register']) + 2;
                $col = $this->getColName($cntR);

                $x = 2;
                foreach ($this->data[0]['register'] as $key => $vo) {
                    $x += 1;
                    $rCol = $this->getColName($x);
                    $event->sheet->setCellValue($rCol.'2', $key);
                }

                $sCntT = $cntR + 1;
                $eCntT = count($this->data[0]['tracking']) + $cntR;
                $sTCol = $this->getColName($sCntT);
                $eTCol = $this->getColName($eCntT);

                $x = $sCntT;
                foreach ($this->data[0]['tracking'] as $key => $vo) {
                    $rCol = $this->getColName($x);
                    $event->sheet->setCellValue($rCol.'2', $key);
                    $x += 1;
                }

                $sCntR = $eCntT + 1;
                $eCntR = count($this->data[0]['job']) + $sCntR;
                $sRCol = $this->getColName($sCntR);
                $eRCol = $this->getColName($eCntR -1);

                $x = $sCntR;
                foreach ($this->data[0]['job'] as $key => $vo) {
                    $rCol = $this->getColName($x);
                    $event->sheet->setCellValue($rCol.'2', $key);
                    $x += 1;
                }
                $eCCol = $this->getColName($x);

                $mergeArr = [
                    'A1:A2',
                    'B1:B2',
                    'C1:'.$col.'1', 
                    $sTCol.'1:'.$eTCol.'1', 
                    $sRCol.'1:'.$eRCol.'1', 
                    $eCCol.'1:'.$eCCol.'2'
                ];

                $event->sheet->setCellValue('A1', 'ลำดับ');
                $event->sheet->setCellValue('B1', 'สำนักงาน/เขต');
                $event->sheet->setCellValue('C1', 'การขึ้นทะเบียนผู้ประกันตน (คน)');
                $event->sheet->setCellValue($sTCol.'1', 'รายงานตัวผู้ประกันตน');
                $event->sheet->setCellValue($sRCol.'1', 'รายงานตัวบรรจุงาน');
                $event->sheet->setCellValue($eCCol.'1', 'หมายเหตุ');

                $cnt = count($this->data) + 1;
                $colCnt = count($this->data[0]);
                $cellRange = 'A1:E1'; // All headers
                //$event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $styleArray = [
                    'font' => [
                        'name'      =>  'Tahoma',
                        'size'=> 12,
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                   
                ];

                $event->sheet->getDelegate()->getParent()->getDefaultStyle()->getFont()->setName('Tahoma');
                //$event->sheet->getDelegate()->getStyle('A1:E1')->applyFromArray($styleArray);
                $event->sheet->getDelegate()->getStyle('A1:'.$eCCol.'2')->applyFromArray($styleArray);

                                //$event->sheet->getStyle('C3:H'.''.($cnt) )->getNumberFormat()->setFormatCode('#0');
                $row = 3;
                for ($x = 0; $x < sizeof($this->data); $x++) {

                    array_push($mergeArr, 'A'.$row.':B'.$row);
                    $event->sheet->setCellValue('A'.$row, $this->data[$x]['RegionName']);

                    $sCol = 3;
                    foreach ($this->data[$x]['register'] as $key => $vo) {
                        
                        $event->sheet->setCellValue($this->getColName($sCol).$row, $vo);
                        $sCol += 1;
                    }

                    foreach ($this->data[$x]['tracking'] as $key => $vo) {
                        
                        $event->sheet->setCellValue($this->getColName($sCol).$row, $vo);
                        $sCol += 1;
                    }

                    foreach ($this->data[$x]['job'] as $key => $vo) {
                        
                        $event->sheet->setCellValue($this->getColName($sCol).$row, $vo);
                        $sCol += 1;
                    }

                    $event->sheet->setCellValue($this->getColName($sCol + 1).$row, $this->data[$x]['remark']);
                    
                    $row += 1;
                }

                for ($x = 0; $x < sizeof($this->data); $x++) {
                    //
                    array_push($mergeArr, 'A'.$row.':'.$eCCol.$row);
                    $event->sheet->setCellValue('A'.$row, $this->data[$x]['RegionName']);
                    $row += 1;

                    foreach ($this->data[$x]['Organization'] as $key => $obj) {
                        $event->sheet->setCellValue('A'.$row, $key + 1);
                        $event->sheet->setCellValue('B'.$row, $obj['OrganizationName']);
                        $event->sheet->setCellValue('C'.$row, $obj['register']);

                        $subCol = 3;
                        foreach ($obj['register'] as $key => $register) {
                            
                            $event->sheet->setCellValue($this->getColName($subCol).$row, $register );
                            $subCol += 1;
                        }

                        foreach ($obj['tracking'] as $key => $tracking) {
                            
                            $event->sheet->setCellValue($this->getColName($subCol).$row, $tracking );
                            $subCol += 1;
                        }

                        foreach ($obj['job'] as $key => $tracking) {
                            
                            $event->sheet->setCellValue($this->getColName($subCol).$row, $tracking );
                            $subCol += 1;
                        }
                        $row += 1;
                    }
                }
                
                $event->sheet->getDelegate()->setMergeCells($mergeArr);
                $event->sheet->getDelegate()->getStyle('C1:'.$eRCol.''.($row))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(50);
                $event->sheet->getDelegate()->getStyle('A2:'.$eRCol.''.($row))->getAlignment()->setWrapText(true);
            
            },
        ];
    }
}

