<?php


namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportNotificationLogs implements FromArray, WithMapping ,WithHeadings, ShouldAutoSize , WithEvents
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            'ลำดับ',
           // 'ชื่อ - นามสกุล ผู้ประกันตน',
            'เลขขึ้นทะเบียน',
            'ครั้งที่',
            'กำหนดการรายงานตัว',
            'วันที่ส่งอีเมล์',
            'ที่อยู่อีเมล์',
            'สถานะการส่ง'
        ];
    }

    public function map($data, $index): array
    {
        //dump($data);
        return [
            $index + 1,
            //$data['FirstName'].' '.$data['LastName'],
            $data['RegisterNumber'],
            $data['TrackingTime'],
            '',
            $data['LogDate'],
            '',
            $data['Status'] ? 'สำเร็จ' : 'ไม่สำเร็จ'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cnt = count($this->data) + 1;
               

                $styleArray = [
                    'font' => [
                        'name'      =>  'Tahoma',
                        'size'=> 12,
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                   
                ];


                $event->sheet->getDelegate()->getParent()->getDefaultStyle()->getFont()->setName('Tahoma');
                //$event->sheet->getDelegate()->getStyle('A1:E1')->applyFromArray($styleArray);
                $event->sheet->getDelegate()->getStyle('A1:G1')->applyFromArray($styleArray);

                $event->sheet->getDelegate()->getStyle('B1:C'.''.($cnt))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('E1:G'.''.($cnt))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getStyle('B1:B'.''.($cnt) )->getNumberFormat()->setFormatCode('#############');
            },
        ];
    }
}

