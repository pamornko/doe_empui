<?php 
//Show Date with user timezone
if (! function_exists('show_thai_date')) {
	function show_thai_date($date, $format="d/m/Y")
	{
		if ($date) {
			if(!($date instanceof \Carbon\Carbon)) {
				try {
					if(is_numeric($date)) {
						// Assume Timestamp
						$date = \Carbon\Carbon::createFromTimestamp($date);
					} else {
						$date = \Carbon\Carbon::parse($date);
					}
				} catch (exception $e) {
					$date = Carbon\Carbon::createFromFormat('d/m/Y', $date);
				}
				
				$year = $date->format("Y") + 543;
				$month = $date->format("m");
				$day = $date->format("d");
				$tz = $date->toArray()["timezone"];
				
				$date = \Carbon\Carbon::createFromDate($year , $month, $day, $tz);
			}
			return $date->format($format);
		} else {
			return $date;
		}
	}
}

if (! function_exists('show_thai_long_date')) {
	function show_thai_long_date($date)
	{
       
		if(!($date instanceof \Carbon\Carbon)) {
			if(is_numeric($date)) {
				 // Assume Timestamp
				$date = \Carbon\Carbon::createFromTimestamp($date);
			} else {
				$date = \Carbon\Carbon::parse($date);
			}

            $year = $date->format("Y") + 543;
            $month = $date->format("m");
            $day = $date->format("d");

			$d = $year."-".$month."-".$day;

            //$date = \Carbon\Carbon::createFromDate($year , $month, $day, $tz);
			$date = \Carbon\Carbon::parse($d)->locale('th_TH')->isoFormat('LL');
		}
		return $date;
	}
}

if (! function_exists('get_sjc_domain')) {
	function get_sjc_domain()
	{
		return config('app.sjcrefix');
	}
}	

if (! function_exists('get_datacenter_domain')) {
	function get_datacenter_domain()
	{
		return config('app.webDatacenter');
	}
}	
