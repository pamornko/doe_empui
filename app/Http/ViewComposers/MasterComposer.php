<?php

namespace App\Http\ViewComposers;

use App\Services\InitialService;
use App\Services\FreelanceService;
use App\Services\CriteriaService;

use Illuminate\View\View;
use SebastianBergmann\Environment\Console;

//table SuMenu
class MasterComposer
{
    private $UserMenus = [];
    private $Menus = array();
    protected $sjcPrefix = "";
    private $bStyle = "";
    
    public function __construct(InitialService $iService, FreelanceService $fService, CriteriaService $cService)
    {
       $this->iniServvice = $iService;
       $this->freelanceService = $fService;
       $this->criteriaService = $cService;

        //dump(session()->all());
        $this->sjcPrefix = config('app.sjcrefix');
        $this->bStyle = "border: 1px solid #aaaaaa;";
    }

    //UserTypeID = 1 Admin
    //UserTypeID = 3 เจ้าหน้าที่กรมจัด
    //UserTypeID = 4 เจ้าหน้าที่กรมจัด
    
    //UserTypeID = 12 ผู้หางาน
    //UserTypeID = 15 เจ้าหน้าที่ สปส.
    //UserTypeID = 16 Admin เจ้าหน้าที่ สปส.

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $isEmployee = true;
        $Employee = [12, 13, 14];
        $UserTypeID = session('UserTypeID');
        /*
        - 15,16 type=SSO
        - 1, 2, 3, 4, 5, 6, 7 type=Admin
        - 8, 9, 10 type=Employer (อันนี้ Empui คงไม่ต้องใช้)
        - 12,13,14 type=Employee
        */
        if (!is_null($UserTypeID)) {
            $indexEmployee = array_search($UserTypeID, $Employee);
            if ($indexEmployee >= 0) {
                $isEmployee = true;
            } else {
                $isEmployee = false;
            }
        }
        

        return $view->with('Menus',  $this->UserMenus)
       
        ->with('Name', session('FirstName').' '. session('LastName'))
        ->with('Email', session('Email') )
        ->with('BirthDate', session('BirthDate') )
        ->with('Age', session('Age') )
        ->with('Address', session('Address') )
        ->with('UserTypeID', $UserTypeID )
        ->with('UserType', 12 )
        ->with('isEmployee', $isEmployee )
        ->with('EmployerName', session('employerName'))
        ->with('bStyle',  $this->bStyle) ;
    }
}

?>