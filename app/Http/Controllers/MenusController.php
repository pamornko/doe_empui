<?php

namespace App\Http\Controllers;

use App\Services\MenuService;
use Illuminate\View\View;
use Illuminate\Http\Request;

class MenusController extends Controller
{
    function __construct(MenuService $menuService)
    {
        $this->menuService = $menuService;
    }

    public function getMenu(Request $request)
    { 
        $this->menuService->getMenus();
        
        //return redirect("http://128.199.231.137/doe_sjc_web/JSK/Employee/profile?system=empui");
        return $this->menuService->getMenus();
    }
}
