<?php

namespace App\Http\Controllers;

use App\Exports\ExportRegister;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

use App\Services\JobAnnoucedService;
use App\Services\RegisterService;
use App\Services\CriteriaService;
use App\Services\FreelanceService;
use App\Services\ReportingService;
use App\Services\SessionService;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class RegisterController extends Controller
{
    private $paramForMasterData = [
        'draw' => 1,
        'start' => 0,
        'length' => 200
    ];

    function __construct(
        JobAnnoucedService $service,
        RegisterService $rService,
        CriteriaService $cService,
        SessionService $sService,
        FreelanceService $fService,
        ReportingService $rpService
        )
    {
        $this->jobService = $service;
        $this->registerService = $rService;
        $this->criteriaService = $cService;
        $this->sessionService = $sService;
        $this->freelanceService = $fService;
        $this->reportingService  = $rpService;
    }


    public function OfficerIndex()
    {
        //dump(session()->all());
        $API_PREFIX = config('app.apiPrefix');
       

        $StartDate = date("Y-m-d", strtotime("-30 day"));
        if ($StartDate) {
            $datas['criteria']['StartDate'] =  $StartDate;
            $arrStartDate = explode("-",$StartDate);
            if (count($arrStartDate) ==  3) {
                $adYear = $arrStartDate[0] + 543;
                $StartDate = sprintf('%s/%s/%s', $arrStartDate[2], $arrStartDate[1], $adYear);
            } 
        }

        $EndDate = date("Y-m-d");
        if ($EndDate) {
            $datas['criteria']['EndDate'] =  $EndDate;
            $arrEndDate = explode("-",$EndDate);
            if (count($arrEndDate) ==  3) {
                $adYear = $arrEndDate[0] + 543;
                $EndDate = sprintf('%s/%s/%s', $arrEndDate[2], $arrEndDate[1], $adYear);
            } 
        }

        $datas['criteria']['Service'] = 0;
        $datas['criteria']['SessionUserID'] = session('UserID');

        $datas = array_merge(
            $datas, 
            $this->paramForMasterData
        );
        //$RegisterList = Http::asForm()->post($API_PREFIX . config('app.registerList'), $datas)->json();
        $RegisterList = [];

        $UserTypeID = session('UserTypeID');
        $Offices = [];
        $SSOList = [];
        $SSO = false;
        if ($UserTypeID == 15 || $UserTypeID == 16) {
            //สปส
            $SSO = true;
            $SSOList = $this->criteriaService->getSsoList($this->reportingService->getRequestDefaultParams(0));
        } else {
            $Offices = Http::asForm()->post($API_PREFIX . config('app.organization'), $this->paramForMasterData)->json();
        }

        //$ReportList = $this->reportService->getSsoServeReport($inputParameter);
       // $SsoList = $this->criteriaService->getSsoList( $this->reportingService->getRequestDefaultParams($this->length));
        //dd($SSOList);
        return view('registers.officer.index', [
            'Offices'=> $Offices ? $Offices["data"] : [],
            'SSO' => $SSO,
            'SSOList' => $SSOList,
            'RegisterList' => $RegisterList,
            'StartDate' => $StartDate,
            'EndDate' => $EndDate
        ]);
    }

    public function OfficerEditPage(Request $request)
    {
        $EmployerName = session('employerName');
       
        return view('registers.officer.form-register', [
            'Register' =>[],
            'ApplyJobs' =>  [],
            'RegisterFreelances' => [],
            'Action' => null,
            'EmployerName' => $EmployerName
        ]);
    }

    public function OfficerEdit(Request $request)
    {
        $input = $request->all();
        $registerID = $input["id"];
        $path = $request->path();
        $action = "view";
        if (Str::endsWith($path, 'edit')) {
            $action = "edit";
        }
        
        $Register = $this->registerService->getDetails( [
            'RegisterID'=> $registerID
        ]);
      
        $criteria['criteria']['RegisterID'] =  $registerID;
        if (!empty($Register['ResignDate'])) {
            //ทำไมต้องเช็ค ResignDate ถึงค่อย set EmployeeID
            //$criteria['criteria']['EmployeeID'] =  $Register['EmployeeID'];
            $Register['ResignDate'] = date("Y-m-d", strtotime($Register['ResignDate']));
        }
        //$criteria = [ 'criteria'=> []];
        $datas = array_merge(
            $criteria, 
            $this->paramForMasterData
        );
        //dump($Register);
        $ApplyJobs = [];
        $criteria['criteria']['EmployeeID'] =  $Register['EmployeeID'];
        if (!empty($criteria['criteria']['EmployeeID'])) {
            $ApplyJobs = Http::asForm()->post(config('app.apiPrefix') . config('app.applyJobList'), $datas)->json();
        }
        //dd($ApplyJobs);
       
        $Freelances = Http::asForm()->post(config('app.apiPrefix') . config('app.registerFreelanceList'), $datas)->json();
       
        $Province = null;
        if (!empty($Register['ProvinceID'])) {
            $Province =  $this->criteriaService->getProvince("ProvinceID=".$Register['ProvinceID']);
        }
        $District = null;
        if (!empty($Register['DistrictID'])) {
            $District =  $this->criteriaService->getDistrict("DistrictID=".$Register['DistrictID']);
        }
        $SubDistrict = null;
        if (!empty($Register['TambonID'])) {
            $SubDistrict = $this->criteriaService->getSubDistrict("TambonID=".$Register['TambonID']);
        }
        $BankAccountImageName = "";
        if (!empty($Register['BankAccountImage'])) {
            $BArray = explode("/", $Register["BankAccountImage"]);
            $BankAccountImageName = $BArray[count($BArray) - 1];
        }
        //dump($Register['BankAccountImage']);
        return view('registers.officer.form-register', [
            'RegisterID' => $registerID,
            'Register' => $Register,
            'ApplyJobs' => $ApplyJobs ? $ApplyJobs['data'] : [],
            'RegisterFreelances' => $Freelances ? $Freelances['data'] : [],
            'Action' => $action,
            'ProvinceName' => $Province ? $Province['ProvinceName'] : '',
            'DistrictName' => $District  ? $District['DistrictName'] : '',
            'TambonName' => $SubDistrict  ? $SubDistrict['TambonName'] : '',
            'BankAccountImageName' => $BankAccountImageName
        ]);
        
    }

    public function registerDetails(Request $request){
       
        $input = $request->all();
        $RegisterID = $input["id"];
        $path = $request->path();
        $Action = "view";
        if (Str::endsWith($path, 'edit')) {
            $Action = "edit";
        }
        
        $Register = $this->registerService->getDetails( [
            'RegisterID'=> $RegisterID
        ]);
      
        $criteria['criteria']['RegisterID'] =  $RegisterID;
        if (!empty($Register['ResignDate'])) {
            $criteria['criteria']['EmployeeID'] =  $Register['EmployeeID'];
            $Register['ResignDate'] = date("Y-m-d", strtotime($Register['ResignDate']));
        }
        //$criteria = [ 'criteria'=> []];
       
        $datas = array_merge(
            $criteria, 
            $this->paramForMasterData
        );
       
        $ApplyJobs = [];
        if (!empty($criteria['criteria']['EmployeeID'])) {
            $ApplyJobs = Http::asForm()->post(config('app.apiPrefix') . config('app.applyJobList'), $datas)->json();
            if ($ApplyJobs['data']) {
                $ApplyJobs = $ApplyJobs['data'];
            } else {
                $ApplyJobs = [];
            }
        }
       
        $Freelances = Http::asForm()->post(config('app.apiPrefix') . config('app.registerFreelanceList'), $datas)->json();
        $RegisterFreelances = [];
        if ($Freelances['data']) {
            $RegisterFreelances = $Freelances['data'];
        }
       
        $Province = null;
        if (!empty($Register['ProvinceID'])) {
            $Province =  $this->criteriaService->getProvince("ProvinceID=".$Register['ProvinceID']);
        }
        $District = null;
        if (!empty($Register['DistrictID'])) {
            $District =  $this->criteriaService->getDistrict("DistrictID=".$Register['DistrictID']);
        }
        $SubDistrict = null;
        if (!empty($Register['TambonID'])) {
            $SubDistrict = $this->criteriaService->getSubDistrict("TambonID=".$Register['TambonID']);
        }

        $ProvinceName = '';
        if ($Province && $Province['ProvinceName']) {
            $ProvinceName = $Province['ProvinceName'];
        }

        $DistrictName = '';
        if ($District && $District['DistrictName']) {
            $DistrictName = $District['DistrictName'];
        }

        $TambonName = '';
        if (!empty($SubDistrict['DistrictName'])) {
            $TambonName = $SubDistrict['DistrictName'];
        }

        $html = view('components.register-details', 
        compact('RegisterID', 'Register', 'ApplyJobs', 'RegisterFreelances', 'Action', 'ProvinceName', 'DistrictName', 'TambonName'))->render();
        return response()->json(compact('html'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        //dump($input);
    
        $validator = Validator::make($request->all(), [
            'bFile' => 'required|mimes:png,jpg,jpeg,pdf|max:2048'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' =>  'กรุณาแนบไฟล์ หน้าบัญชีธนาคาร'
            ]);
        } else {

            $dt = now();
           
            $userID = session('UserID');
           
            $EmployeeID =  session('EmployeeID');
           
            $registerDate = $dt->format('Y-m-d H:i:s');

            $arrResignDate = explode("/",$input["ResignDate"]);
            $resignDate = "";
            if (count($arrResignDate) ==  3) {
                $adYear = $arrResignDate[2] - 543;
                $resignDate = sprintf('%s-%s-%s %s',$adYear , $arrResignDate[1], $arrResignDate[0], $dt->format('H:i:s'));
            } 
            
            
            $isApply = 1;
            if ($request->has("ignoreJob")) {
                $isApply = 0;
            }
           
            $PersonalID = session('PersonalID');
            $fileName = $PersonalID .'_'.$dt->format('Ymd_His').'.'.$request->file('bFile')->extension();
            //$filePath = $request->file('bFile')->storeAs('uploads', $fileName, 'public');
            //$filePath = 'storage/'.$filePath;

            $filePath = Storage::putFileAs('uploads', $request->file('bFile'), $fileName);

            $resignObj = json_decode($input["ResignCaseID"]);
            $registerDatas = [
                'UserID'=>$userID,
                //"RegisterNumber" => "25631020000000001",
                "ResignID" => $resignObj->id,
                "ResignCaseID" => $input["LayoffCaseID"],
                "RegisterDate" => $registerDate,
                "ResignPosition" => $input["ResignPosition"],
                "TypeOfBusiness" => $input["TypeOfBusiness"],
                "EmployerName" => $input["EmployerName"],
                "SalaryID" => $input["SalaryID"],
                "OrganizationID" => session('OrganizationID'),
                "isApply" => $isApply,
                "BankAccountImage" => $filePath
            ];
        
            if (session('EmployerID')) {
                $registerDatas["EmployerID"] = session('EmployerID');
            }

            if ($resignDate) {
                $registerDatas["ResignDate"] = $resignDate;
            }
        
            /*if ($input["ResignID"]) {
                $registerDatas["ResignID"] = $input["ResignID"];
            }*/
            $registerDatas["ResignID"] = $resignObj->id;

            if ($input["ResignOther"]) {
                $registerDatas["ResignOther"] = $input["ResignOther"];
            }

            if ($input["BankID"]) {
                $registerDatas["BankID"] = $input["BankID"];
            }

            if ($input["BankAccountNumber"]) {
                $registerDatas["BankAccountNumber"] = $input["BankAccountNumber"];
            }

            //พร้อมเพย์
            if ($input["channelId"] == "1") {
                $registerDatas["Payment"] = 1;
                if ($input["PromtpayNumber"]) {
                    $registerDatas["PromtPay"] = $input["PromtpayNumber"];
                }
            }
            $API_PREFIX = config('app.apiPrefix');
            $REGISTER = config('app.register');
            $Register = Http::asForm()->post($API_PREFIX . $REGISTER, $registerDatas)->json();
        // if $input["ignoreJob"] == "on"
            if ($Register) {
                $RegisterID = $Register['RegisterID'];
                if ($RegisterID) {
                    if ($request->has('wintType')) {
                        for ($x = 0; $x < sizeof($input["wintType"]); $x++) {
                                
                            $freelanceID = $input['wintType'][$x];
                            
                            if ($freelanceID) {
        
                                $registerFreelanceDatas = [
                                    'RegisterID' => $RegisterID,
                                    'FreelanceID[]'=> $freelanceID,
                                    'Other[]'=>$input['wint'][$x] ? 1 : 0,
                                    'FreelanceOther[]'=> $input['wint'][$x] ? $input['wint'][$x] : ""
                                ];
                
                                $this->registerService->creatFreelance($registerFreelanceDatas);
                            }
                            
                        }
                    }

                    $workDatas = [
                        'RegisterID'=>  $RegisterID,
                        'EmployerCode'=> $input["EmployerCode"],
                        'EmployerName'=>  $input["EmployerName"],
                        'EmployerAddress'=> $input["employerAddress"],
                        'Postcode'=> $input["postcode"],
                        'PhoneNumber'=> $input["phoneNumber"],
                        'ContactPerson'=> $input["contactPerson"],
                        'ProvinceID'=>  $input["province"],
                        'DistrictID'=>  $input["district"],
                        'TambonID'=> $input["subDistrict"]
                    ];
                
                    Http::asForm()->post($API_PREFIX . config('app.registerWork'), $workDatas)->json();
                
                    $ApplyJobs = session('ApplyJobs');
                    if ($ApplyJobs) {
                        foreach ($ApplyJobs as $applyjob) {
                    
                            $APPLY_JOB = config('app.applyJob');
                            $response = Http::asForm()->post($API_PREFIX . $APPLY_JOB, [
                                'EmployeeID' => $EmployeeID,
                                'JobAnnounceID' => $applyjob['id'],
                                'RegisterID'  => $RegisterID
                            ])->json();
                        
                            /*dump([
                                'EmployeeID' => $EmployeeID,
                                'JobAnnounceID' => $applyjob['id'],
                                'RegisterID'  => $RegisterID
                            ]);
                            dump($response);*/
                        }
                    }
        
                    session()->forget('ApplyJobs');
        
                    return response()->json([
                        'success' => true,
                        'auto' => $Register['AutoFirst']
                    ]);
                }
            } else {
                return response()->json([
                    'success' => false
                ]);
            }
        }
    }

    public function registerForm()
    {
        //dump(session()->all());
        $paramForMasterData = [
            'draw' => 1,
            'start' => 0,
            'length' => 30
        ];
        $EmployeeID = session('EmployeeID');

        $API_PREFIX = config('app.apiPrefix');

        $SALARY = config('app.salary');
        $BANK = config('app.bank');

        $Salarys = Http::asForm()->post($API_PREFIX . $SALARY, $paramForMasterData)->json();
        $Banks = Http::asForm()->post($API_PREFIX . $BANK, $paramForMasterData)->json();
        
        $reqInput = ['langID'=>1];
        $JobFieldIDs = $this->criteriaService->getJobField($reqInput);
        $BusinessTypes = $this->criteriaService->getBusinessType($reqInput);
        $Degrees = $this->criteriaService->getDegree($reqInput);
        $WorkExperiences = $this->criteriaService->getWorkExperience($reqInput);
        $ResignCases = $this->criteriaService->getResignCases($paramForMasterData);

        $ApplyJobs = session("ApplyJobs", []);

        $ResignDate = "";
        /*if (session('UserID') == "14957486") {
            $ResignDate = "2020-10-12";
        }*/
        if (session('empResignDate')){
            
            $pieces = explode("/", session('empResignDate'));
            $ResignDate = session('empResignDate');
            /*$time = strtotime(session('empStartDate'));
           
            $WorkDay = date('d',$time);
            $WorkMonth = date('m',$time);
            $WorkYear = date('Y', $time) - 543;*/

            if (count($pieces) >= 3) {
                $WorkDay = $pieces[0];
                $WorkMonth = $pieces[1];
                $WorkYear = $pieces[2] - 543; 
                //$ResignDate = sprintf("%s-%s-%s", $WorkYear, $WorkMonth, $WorkDay);
                //$ResignDate = sprintf("%s-%s-%s", $WorkDay, $WorkMonth, $pieces[2]);
                //$ResignDate = session('empResignDate');
            }
        }

        $Provinces = Http::get($API_PREFIX.config('app.province'))->json();
        $Freelances = $this->freelanceService->getMasterFreelances($this->paramForMasterData);
        return view('registers.form-wizard', [
            'Freelances' => $Freelances ? $Freelances : [],
            'Provinces'=> $Provinces ? $Provinces : [],
            'Salarys'=> $Salarys ? $Salarys["data"] : [],
            'Banks'=> $Banks ? $Banks["data"] : [],
            //'Employer'=> $Employers ? $Employers["data"][0] : [],
            'PersonalID' => session('PersonalID'),
            'JobsSuggestions'=>  [],
            'JobQualifications'=> [],
            'JobMatchings'=> [],
            //'Freelances'=> $Freelances,
            'ApplyJobs'=> $ApplyJobs,
            'JobFieldID'=> $JobFieldIDs,
            'BusinessTypes'=> $BusinessTypes,
            'Degrees'=> $Degrees,
            'WorkExperiences'=> $WorkExperiences,
            'ResignCases'=> $ResignCases,
            'LayoffCases'=> [],
            'MinAge' => $this->criteriaService->getMinAge(),
            'MaxAge' => $this->criteriaService->getMaxAge(),
            'MixSalarys' => $this->criteriaService->getMinSalary(),
            'MaxSalarys' => $this->criteriaService->getMaxSalary(),
            'ResignDate' => $ResignDate,
            'CntFl'=> 1
        ]);
    }

    public function Search(Request $request){
        $inputs = $request->all();
        $draw = 1;
        $start = 0;
        $pageLength = 20;
        $datas = [ 'criteria'=> []];
        
        if ($request->has('draw')) {
            $draw = $inputs['draw'];
        }
        if ($request->has('start')) {
            $start = $inputs['start'];
        }
        $RegisterNumber = "";
        if ($request->has('RegisterNumber')) {
            $RegisterNumber = $inputs["RegisterNumber"];
        }

        if ($request->has('StartDate')) {
            $StartDateArr = explode("/",$request->get('StartDate'));
            if (count($StartDateArr) == 3) {
                $datas['criteria']['StartDate'] =  sprintf("%s-%s-%s", ($StartDateArr[2] - 543), $StartDateArr[1], $StartDateArr[0]);
            }
        }

        if ($request->has('EndDate') && trim($request->get('EndDate') != "")) {
            $EndDateArr = explode("/",$request->get('EndDate'));
            $datas['criteria']['EndDate'] =  sprintf("%s-%s-%s", ($EndDateArr[2] - 543), $EndDateArr[1], $EndDateArr[0]);
        }
        $datas['criteria']['Service'] = 0;
        if ($request->has('Service')) {
            $datas['criteria']['Service'] =  $request->get('Service');
        }
       
        $API_PREFIX = config('app.apiPrefix');
        $REGISTER_LIST = config('app.registerList');
    
        if ($RegisterNumber) {
            //$datas['criteria']['RegisterNumber'] =  $RegisterNumber;
            $datas['criteria']['keyword'] =  $RegisterNumber;
        }

        $OrganizationID = "";
        if ($request->has('Organization')) {
            $OrganizationID = $request->get('Organization');
            //$datas['criteria']['OrganizationSupportID'] =  $request->get('Organization');
        }
        $UserTypeID = session('UserTypeID');
        $SSO = false;
        if ($UserTypeID == 15 || $UserTypeID == 16) {
            $SSO = true;
            $datas['criteria']['SSOOfficeID'] =  $OrganizationID;
        } else {
            $datas['criteria']['OrganizationSupportID'] =  $OrganizationID;
        }

       
        $datas['criteria']['SessionUserID'] = session('UserID');
        $datas['columns'][0]['name'] =  'RegisterDate';
        $datas['order'][0]['column'] =  0;
        $datas['order'][0]['dir'] =  'desc';

        //dd($datas['criteria']);

        $this->paramForMasterData["draw"] = $draw;
        $this->paramForMasterData["length"] = $pageLength;
        $this->paramForMasterData["start"] = $start;
        $searchData = array_merge(
            $datas, 
            $this->paramForMasterData
        );

        $datas = array();
        $total = 0;
        $fills = 0;
        if ($request->has('StartDate') && $draw > 1) {
            $RegisterList = Http::asForm()->post($API_PREFIX . $REGISTER_LIST, $searchData)->json();
            
            $total = $RegisterList["recordsTotal"];
            $fills = $RegisterList["recordsFiltered"];
            if ($RegisterList['data']) {
                foreach($RegisterList['data'] as $Register) {
                    //dump($Register);
                    $datas[] = [
                        'id' => $Register["RegisterID"],
                        'national_id' => $Register["PersonalID"],
                        'register_number' => $Register["RegisterNumber"], 
                        'name' => $Register["Firstname"]." ".$Register["Lastname"],
                        'register_date' => show_thai_date($Register["RegisterDate"]),
                        'tracking_time' => $Register["TrackingTime"],
                        'active' => $Register["isActiveRegister"],
                        'sso' => $SSO
                    ];
                }    
            }
        }
        
        /*return response()->json([
            'registers' => $RegisterList ? $RegisterList['data'] : []
        ]);*/
        /*$html = view('components.register-table', compact('RegisterList'))->render();
        return response()->json(compact('html'));*/
        $response = [
            "draw" => intval($draw),
            "recordsTotal" => $total,
            "recordsFiltered" =>  $fills,
            "data"            => $datas
        ];

        //dump($response);
        return response()->json( $response
        , 200);
    }

    public function Edit(Request $request)
    {
        $input = $request->all();
        $input['RegisterDate'] = sprintf('%s %s', $input["RegisterDate"], now()->format('H:i:s'));
        $response = $this->registerService->update($input);
 
        if ($response) {
            return response()->json([
                'success' => true,
                'registerId' => $response
            ]);
        } else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    public function Cancel(Request $request)
    {
        $input = $request->all();

        $input['isActive'] = 0;
        
        $response = $this->registerService->cancel($input);
        
        if ($response) {
            return response()->json([
                'success' => true,
                'registerId' => $response
            ]);
        } else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    public function exportExcel(Request $request)
    {
        $input = $request->all();
        //dump($input);
        $RegisterNumber = $input["RegisterNumber"];
        $StartDate = $input["StartDate"];
        $EndDate = $input["EndDate"];
        $Service = $input["Service"];
        $OrganizationID = $input["OrganizationID"];

       
        $datas = [ 'criteria'=> []];
        if ($RegisterNumber) {
            $datas['criteria']['keyword'] =  $RegisterNumber;
        }

        if ($StartDate) {
            $datas['criteria']['StartDate'] =  $StartDate;
        }
        
        if ($EndDate) {
            $datas['criteria']['EndDate'] =  $EndDate;
        }

        if ($OrganizationID) {
            $datas['criteria']['OrganizationSupportID'] =  $OrganizationID;
        }

        $datas['criteria']['Service'] = 0;
        if ($Service) {
            $datas['criteria']['Service'] =  $Service;
        }
        $datas['criteria']['SessionUserID'] = session('UserID');

        $searchData = array_merge(
            $datas, 
            $this->paramForMasterData
        );

        //dump($searchData);
        $RegisterList = $this->registerService->getRegisterAll($searchData);

        $data = [];
        if ($RegisterList) {
            $data = $RegisterList['data'];
        }
        //dump($RegisterList['data']);
        /*$rec_arr = array();
        $headings = ['ลำดับ','รหัสบัตรประจำตัวประชาชน'];
        array_push($rec_arr, $headings);
        $no = 0;
        foreach ($data as $row)
        {
            $no +=1;
            //$rec_arr[] = array_values($row);
            //$rec_arr[] = array_values($row['PersonalID']);
            $obj_arr = array();
            $obj_arr[] = $no;
            $obj_arr[] = $row['PersonalID'];
            array_push($rec_arr, $obj_arr);
        }*/

        //dd($rec_arr);

        //$export = new ExportFormatted($data);
        $export = new ExportRegister($data);
        $filename = $this->sessionService->getFormatFile('register', 'xlsx');
        return Excel::download($export, $filename);
    }

    public function exportPDF(Request $request)
    {
        $RegisterList = [];

        $input = $request->all();
       
        $RegisterNumber = $input["RegisterNumber"];
        $StartDate = $input["StartDate"];
        $EndDate = $input["EndDate"];
        $Service = $input["Service"];
        $OrganizationID = $input["OrganizationID"];

       
        $datas = [ 'criteria'=> []];
        if ($RegisterNumber) {
            $datas['criteria']['RegisterNumber'] =  $RegisterNumber;
        }

        if ($StartDate) {
            $datas['criteria']['StartDate'] =  $StartDate;
        }
        
        if ($EndDate) {
            $datas['criteria']['EndDate'] =  $EndDate;
        }

        $searchData = array_merge(
            $datas, 
            $this->paramForMasterData
        );

        //dump($searchData);
        $data = $this->registerService->getRegisterAll($searchData);
        if ($data) {
            $RegisterList = $data['data'];
        }

        $pdf = PDF::loadView('exports.register-table-pdf',  ['RegisterList' => $RegisterList]);
        $filename = $this->sessionService->getFormatFile('register', 'pdf');
        //return $pdf->stream();    
        return $pdf->download($filename);
        //return $pdf->stream( 'register.pdf' );
        //->header('Content-Type','application/pdf');
    }

    public function printRegister(Request $request)
    {
        $input = $request->all();
        $RegisterNumber = $input["RegisterNumber"];
        $StartDate = $input["StartDate"];
        $EndDate = $input["EndDate"];
        $Service = $input["Service"];
        $OrganizationID = $input["OrganizationID"];
       
        $datas = [ 'criteria'=> []];
        if ($RegisterNumber) {
            $datas['criteria']['keyword'] =  $RegisterNumber;
        }

        if ($StartDate) {
            $datas['criteria']['StartDate'] =  $StartDate;
        }
        
        if ($EndDate) {
            $datas['criteria']['EndDate'] =  $EndDate;
        }

        if ($OrganizationID) {
            $datas['criteria']['OrganizationSupportID'] =  $OrganizationID;
        }

        $datas['criteria']['Service'] = 0;
        if ($Service) {
            $datas['criteria']['Service'] =  $Service;
        }
        $datas['criteria']['SessionUserID'] = session('UserID');

        $searchData = array_merge(
            $datas, 
            $this->paramForMasterData
        );

        /*if ($Organization) {
            $criteria['criteria']['Organization'] =  $Organization;
        }*/

        $RegisterList = $this->registerService->getRegisterAll( $searchData );
        //dump($RegisterList);
        return view('print.register', [
            'RegisterList'=> $RegisterList ? $RegisterList['data'] : []
        ]);
    }

    public function exportPDFTest(Request $request)
    {
        /*$data = [
            'title' => 'Inspirational Quotes',
            'content' => 
                'แรงบันดาลใจเป็นส่วนสำคัญที่จะกระตุ้นให้เราเริ่มต้นลงมือทำ มุ่งไปยังเป้าหมายที่วางไว้ รักษาโมเมนตัม มีความสม่ำเสมอและอดทน แล้วเราจะพบความก้าวหน้าที่เร็วขึ้นแรงบันดาลใจที่จะกระตุ้นตัวเอง เราหาได้จากบุคคลที่ชื่นชม บุคคลที่ประสบความสำเร็จ คนที่มีประสบการณ์ชีวิตมากมาย เราได้รับแรงบันดาลใจจากคำพูดของคนเหล่านั้น คำพูดที่สร้างแรงบันดาลใจ ให้แง่คิด คำพูดที่เปี่ยมด้วยพลังที่ขับดันให้เราลงมือทำ ช่วยให้เราเกิดความชัดเจน ทำให้เรารู้ตัว เข้าใจอย่างชัดเจน ถึงเส้นทางที่เรากำลังเดินไป
                คำคม ข้อคิด เป็นบทเรียนชีวิตที่สื่อผ่านคำพูดสั้นๆ ของบุคคลที่ประสบความสำเร็จ
                ประโยชน์ของคำคมหรือข้อคิดดีๆ มันจะช่วยให้เรารู้ตัวอยู่เสมอ มันจะคอยสะกิดเรา ทำให้เรารู้ ว่าเราทำได้ สร้างแรงกระตุ้น จุดประกายให้เราตื่นในทุกๆ วัน ในแต่ละวันที่เราต้องเจอปัญหา ความคิดแง่ลบที่เกิดขึ้น มันจะทำให้อารมณ์ไม่ดี มันรบกวนจิตใจ และการใช้คำคมหรือข้อคิดเพื่อจุดประกาย มันจะเปลี่ยนอารมณ์ให้เป็นบวก ทำให้เราคิดบวกได้'
        ];
        
        $pdf = PDF::loadView('exports.test',$data);
        return $pdf->stream();  */ 
        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];
        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];
        $html = view('exports.test')->render();
        $mpdf = new \Mpdf\Mpdf(['fontDir' => array_merge($fontDirs, [storage_path('fonts/'),]),'fontdata' => $fontData + ['sarabun_new' => ['R' => 'THSarabunNew.ttf','I' => 'THSarabunNew Italic.ttf','B' => 'THSarabunNew Bold.ttf',],],'default_font' => 'sarabun_new','tempDir'=>storage_path('tempdir')]);
        $mpdf->WriteHTML($html);
        return $mpdf->Output('MyPDF.pdf', 'D');
    }
    

}
