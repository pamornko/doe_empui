<?php

namespace App\Http\Controllers;

use App\Services\FreelanceService;
use Illuminate\Http\Request;

use App\Services\JobAnnoucedService;
use Illuminate\Support\Arr;

class JobAnnoucedController extends Controller
{
    function __construct(
        JobAnnoucedService $service,
        FreelanceService $fService
        )
    {
        $this->jobService = $service;
        $this->freelanceService = $fService;
    }

    public function Search(Request $request){
        $input = $request->all();
       
        $JobMatchings =  $this->jobService->getMatching($input);

        //dump($JobMatchings);
        $html = view('components.jobs-annouced', compact('JobMatchings'))->render();
        return response()->json(compact('html'));
    }

    public function removeApplyJobs(Request $request)
    {
        $input = $request->all();
        $id = $input["id"];

        $ApplyJobs = session("ApplyJobs");
        $jobs = array();
        foreach ($ApplyJobs as $key => $Job) {
            if ($Job['id'] == $id) {
                //dump($Job);
                //dump($key);
                //array_splice($ApplyJobs, $key, 1);
                unset($ApplyJobs[$key]);
            } else {
                array_push($jobs, $Job['id']);
            }
        }
        $cnt = count($ApplyJobs);
        session(['ApplyJobs' => $ApplyJobs]);

        $html = view('components.apply-jobs-table', compact('ApplyJobs'))->render();
        return response()->json(compact('html', 'cnt', 'jobs'));
    }
    
    public function jobPrivateAIList(Request $request){
        $input = $request->all();
        //dump( $input);
        //$criteria['criteria'] = array();
        $criteria['EmployeeID'] = session('EmployeeID');

        if (!empty($input['position'])) {
            $criteria['JobPosition'] = $input['position'];
        }
        if (!empty($input['salary'])) {
            $criteria['Wage_Min'] = $input['salary'];
        }
        if (!empty($input['emp_type'])) {
            $criteria['EmploymentType'] = $input['emp_type'];
        }
        if (!empty($input['jobs_field'])) {
            $criteria['JobFieldID'] = $input['jobs_field'];
        }
        if (!empty($input['degree'])) {
            $criteria['DegreeID'] = $input['degree'];
        }
        if (!empty($input['province'])) {
            $criteria['ProvinceID'] = $input['province'];
        }
        if (!empty($input['amphoe'])) {
        }

        $PageNumber = 1;
        $PerPage = 5;
        $start = 0;
        $Total = 10;
        if (!empty($input['page'])) {
            if ($input['page'] >= 0) {
                $PageNumber = $input['page'];
                $start = ($PageNumber * $PerPage) - $PerPage;
            } else {
                $start = ($Total * $PerPage) - $PerPage;
            }
        }
        //dump($criteria);
        $JobPrivatesObject =  $this->jobService->getJobPrivateAIList($criteria,  $start, $PerPage);
        $Total = 0;
        if ($JobPrivatesObject['recordsFiltered']) {
            $Total =  ceil($JobPrivatesObject['recordsFiltered'] / $PerPage);
            $TotalRecords = $JobPrivatesObject['recordsFiltered'];
        }
       
        $applyingJobs = session('ApplyJobs');
        if (!empty($JobPrivatesObject["data"])) {
            foreach ($JobPrivatesObject["data"] as $key => $Job) {
            
                if ($applyingJobs != null && array_key_exists($Job['JobAnnounceID'], $applyingJobs)) {
                    $JobPrivatesObject["data"][$key]['ApplyDate'] = now()->format('Y-m-d');
                }
            }
        }
        $JobPrivates = $JobPrivatesObject["data"];
        $SeqStart = $start;
        
        $html = view('components.jobs-list', 
        compact('JobPrivates', 'Total', 'PageNumber', 'SeqStart', 'TotalRecords'))->render();
        return response()->json(compact('html'));
    }

    public function jobDetails(Request $request){
       
        $JobDetails = null;
        if (!empty($request->has('ID'))) {
            $criteria['EmployeeID'] = session('EmployeeID');
            $criteria['JobAnnounceID'] = $request->get('ID');
            $JobDetails =  $this->jobService->getJobDetailsAI($criteria);
        }
        /*return view('jobs.job_details', [
            'JobDetails' => $JobDetails
        ]);*/

        $html = view('jobs.job_details', 
        compact('JobDetails'))->render();
        return response()->json(compact('html'));
    }
    

    public function jobFav(Request $request){
        $success = false;
        if (!empty($request->has('ID'))) {
            $criteria['EmployeeID'] = session('EmployeeID');
            $criteria['JobAnnounceID'] = $request->get('ID');
            $success =  $this->jobService->favJob($criteria);
        }
        return response()->json([
            'success' => $success
        ]);
    }

    public function jobApply(Request $request){
        $success = true;
        
        if (!empty($request->has('ID'))) {
            $ApplyJobs = session("ApplyJobs", []);
           
            $criteria['EmployeeID'] = session('EmployeeID');
            $criteria['JobAnnounceID'] = $request->get('ID');
            $JobDetails =  $this->jobService->getJobDetailsAI($criteria);
            $JobPosition = $JobDetails["JobAnnounce"] ? $JobDetails["JobAnnounce"]["JobPosition"] : "";
            $EmployerName = $JobDetails["Employer"] ? $JobDetails["Employer"]["EmployerName"] : "";
            $ProvinceName = $JobDetails["Employer"] ? $JobDetails["Employer"]["ProvinceName"] : "";
            
            $ApplyJobs[$request->get('ID')] = array("id" => $request->get('ID'), "ApplyDate" => now()->format('Y-m-d'), "JobPosition" => $JobPosition, "EmployerName" => $EmployerName, "ProvinceName" => $ProvinceName);
            session(['ApplyJobs' => $ApplyJobs]);
        }
        //dump(session('ApplyJobs'));
        return response()->json([
            'success' => $success
        ]);
    }

    public function chkJobApply(Request $request){
        $cnt = 0;
        if (!empty(session('ApplyJobs'))) {
            $cnt = count(session('ApplyJobs'));
        }
        return response()->json([
            'cnt' => $cnt
        ]);
    }

    public function reloadApplyJobs(Request $request)
    {
        $input = $request->all();
        
        //dump(session('ApplyJobs'));
        $ApplyJobs = session("ApplyJobs", []);
       
        //array_push($ApplyJobs, array("id" => $id, "ApplyDate" => $applyDate, "JobPosition" => $position, "EmployerName" => $employer, "ProvinceName" => $area));
        
        $jobs = array();
        foreach ($ApplyJobs as $key => $Job) {
            array_push($jobs, $Job);
        }
        $Action = "";

        $html = view('components.apply-jobs-table', compact('ApplyJobs', 'Action'))->render();
        return response()->json(compact('html', 'jobs'));
    }

    public function getFreelances(Request $request){
        $Freelances = $this->freelanceService->getMasterFreelances($this->freelanceService->getRequestParams());
        return response()->json([
            'freelances' => $Freelances
        ]);
    }

    public function freelanceList(Request $request)
    { 
        $Freelances = $this->freelanceService->getMasterFreelances($this->freelanceService->getRequestDefaultParams(200));
        $Datas[] = [
            "id" => "",
            "text" => "ประเภทงาน"
        ];
        foreach ($Freelances as $Freelance) { 
            $Datas[] = [
                "id" => $Freelance['FreelanceID'],
                "text" => $Freelance['FreelanceName']
            ];
        }
        return response()->json(['success'=> true, 'datas'=> $Datas]);
    }

    public function getFreelance(Request $request)
    { 
        $filter = $this->freelanceService->getRequestDefaultCriteria(1);
        $filter['criteria']['FreelanceName'] =  $request->get("text");
        $Freelance = $this->freelanceService->getFreelance($filter);
        
        return response()->json(['success'=> true, 'data'=> $Freelance ? $Freelance[0] : []]);
    }

}
