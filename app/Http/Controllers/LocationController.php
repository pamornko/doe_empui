<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\SessionService;

class LocationController extends Controller
{
    function __construct(
        SessionService $sService
    ) {
        $this->sessionService = $sService;
    }

    public function store(Request $request)
    {
        view()->share('mShow', 'hide');
        $input = $request->all();
    
        $userID = session('UserID');
        $sessionID = session('SessionID');

        
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }

        if (!$OrganizationID || $OrganizationID == "") {
            $OrganizationID = 24;//Internet
        }
        
        session(['OrganizationID' => $OrganizationID]);
        $request->session()->put('OrganizationID',$OrganizationID);
        $datas = [
            'Session'=>$sessionID,
            "OrganizationID" => $OrganizationID,
            "UserID" => $userID
        ];

        $response = $this->sessionService->create($datas);

        return $response;
    }
}
