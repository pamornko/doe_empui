<?php

namespace App\Http\Controllers;

use App\Exports\ExportInsuredNationWide;
use App\Exports\ExportInsuredPersonal;
use App\Exports\ExportInsuredProvince;
use App\Exports\ExportInsuredService;
use App\Exports\ExportInsuredSSO;
use App\Exports\ExportNotificationLogs;
use App\Exports\ExportBenefitSSO;
use App\Services\CriteriaService;
use Illuminate\Http\Request;

use App\Services\RegisterService;
use App\Services\ReportingService;
use App\Services\ReportService;
use App\Services\SessionService;
use App\Services\SSOService;
use Illuminate\Support\Facades\Http;

use Maatwebsite\Excel\Facades\Excel;

class ReportDataController extends Controller
{
    private $paramForMasterData = [
        'draw' => 1,
        'start' => 0,
        'length' => 100
    ];

    function __construct(
        ReportingService $service,
        RegisterService $rService,
        ReportService $rrService,
        SessionService $sService,
        CriteriaService $cService,
        SSOService $ssoService
        )
    {
        $this->reportingService = $service;
        $this->registerService = $rService;
        $this->reportService = $rrService;
        $this->sessionService = $sService;
        $this->criteriaService = $cService;
        $this->ssoService = $ssoService;
    }

    public function userBenefitReport()
    {   

        $Registers = array();
        $Register = $this->registerService->getRegisterLatest([
            'UserID'=> session('UserID')
        ]);
        if ($Register) {
            $datas['RegisterID'] = $Register['RegisterID'];
            $Reportings  = $this->reportingService->getTracking($datas);
            $Register['Reportings'] = $Reportings;

            $Registers[0] = $Register;
        }
        
        //dump($Register);
        /*if ($Register) {
            $params['RegisterID'] =  $Register['RegisterID'];
            $params['UserID'] =   session('UserID');
            $SSOBenefit = $this->ssoService->getSSOBenefit($params);
        }*/
        /*$criteria['criteria']['UserID'] =  session('UserID');
        $criteria['draw'] = 1;
        $criteria['start'] = 0;
        $criteria['length'] = 100;
        $criteria['columns'][0]['name'] =  'RegisterNumber';
        $criteria['order'][0]['column'] =  0;
        $criteria['order'][0]['dir'] =  'desc';
        $criteria['criteria']['isActive'] =  1;
        $criteria['StartDate'] = '2021-01-01';
        
        $Registers  = $this->registerService->getRegisterAll($criteria);
        //dump($Registers);
        if ($Registers) {
            for ($x = 0; $x < sizeof($Registers["data"]); $x++) {
                $datas['RegisterID'] = $Registers['data'][$x]['RegisterID'];
                $Reportings  = $this->reportingService->getTracking($datas);
                //dump($Reportings);
                $Registers['data'][$x]['Reportings'] = $Reportings;
            }
        }*/
        //dump($Registers);
        return view('reports.users.benefit', [
            'Registers'=> $Registers ? $Registers : []
            //'Registers'=> $Registers ? $Registers["data"] : []
        ]);
    }

    public function officerBenefitReport()
    {

        return view('reports.officer.benefit', [
            'Reportings' => [],
            'UserInfo' => []
        ]);
    }

    public function officerSummaryInsuredServicesReport(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }

        /*$OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }

        if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }*/

        //$ReportList = $this->reportService->getOfficeServeReport($inputParameter);
        
        //dump($ReportList);

        return view('reports.officer.summary-service',[
            'StartDate'=> $StartDate,
            'EndDate'=> $EndDate,
            'ReportList'=> []
        ]);
    }

    
    public function officerSummaryBenefitServicesReport(Request $request)
    {
        $input = $request->all();

        $userID = null;
        if ($request->has('UserID')) {
            $userID =$input["UserID"];
        }
        
        if ($userID) {
            $this->sessionService->stampSession(['UserID' => $request->get('UserID'), 'UserTypeID' => $request->get('UserTypeID')]);
        }

        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }

        if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }
      
        $ReportList = $this->reportService->getSsoServeReport($inputParameter);

        
        //$SsoList = $this->criteriaService->getSsoList($this->paramForMasterData);

        return view('reports.officer.summary-benefit-service', [
            'StartDate'=> $StartDate,
            'EndDate'=> $EndDate,
            'ReportList'=> [],
            'SsoList'=> $ReportList
        ]);
    }

    public function BenefitSearch(Request $request){
        $input = $request->all();
       
        $RegisterNumber = "";
        if ($request->has('criteria')) {
            $RegisterNumber = $input["criteria"];
        }

        $Reportings = [];
       
        if ($RegisterNumber) {
            $datas['criteria']['keyword'] = $RegisterNumber;
            $datas['criteria']['isActive'] =  1;
            $datas['draw'] = 1;
            $datas['start'] = 0;
            $datas['length'] = 1;

            //dump($datas);
           
            $Reportings  = $this->reportService->getOfficeBenefitReport($datas);
        }
        
        $UserInfo = [];
        $RegisterID = null;

        if ($Reportings) {
            $UserInfo = [
                'FirstName'=> $Reportings[0]['Firstname'] ?? '',
                'LastName'=> $Reportings[0]['Lastname'] ?? '',
                'RegisterNumber'=> $Reportings[0]['RegisterNumber']
            ];
            $RegisterID = $Reportings[0]["RegisterID"];
        }
        /*$html = view('components.benefit-table', compact('Reportings', 'UserInfo'))->render();
        return response()->json(compact('html'));*/

        return response()->json([
            'reportings' => $Reportings,
            'userInfo' => $UserInfo,
            'registerId' => $RegisterID
        ]);
    }

    public function officerSummaryInsuredNationWideReport(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }

        /*$ReportList = $this->reportService->getOfficeNationWideReport($inputParameter);

        $Case = [];
        $Tracking = [];
        $Job = [];
        if ($ReportList) {
            $Case = $ReportList[0]['register'];
            $Tracking = $ReportList[0]['tracking'];
            $Job = $ReportList[0]['job'];
           // dd($ReportList[0]['register']);
        }*/

        return view('reports.officer.summary-insurer-nationwide',[
            'StartDate'=> $StartDate,
            'EndDate'=> $EndDate
        ]);
    }

    public function officerSummaryInsuredProvinceReport(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }

        //$ReportList = $this->reportService->getOfficeServeReport($inputParameter);

        return view('reports.officer.summary-insurer-province',[
            'StartDate'=> $StartDate,
            'EndDate'=> $EndDate,
            'ReportList'=> [],
            'Case'=>  [],
            'Tracking'=>  [],
            'Job'=>  []
        ]);
    }

    public function officerSummaryInsuredNationWideSearchReport(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "&EndDate=".$EndDate;


        $ReportList = $this->reportService->getOfficeNationWideReport($inputParameter);

        $Case = [];
        $Tracking = [];
        $Job = [];
        if ($ReportList) {
            $Case = $ReportList[0]['register'];
            $Tracking = $ReportList[0]['tracking'];
            $Job = $ReportList[0]['job'];
        }

        $html = view('components.summary-insurer-nationwide-table', compact('ReportList', 'Case', 'Tracking', 'Job'))->render();
       
        return response()->json(compact('html'));
    }

    public function officerSummaryInsuredProvinceSearchReport(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
       
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
       
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }
        //$OrganizationID = 14;
        if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }
        //dump($inputParameter);
        $ReportList = $this->reportService->getProvinceReport($inputParameter);
        //dump($ReportList);
        
        $Case = [];
        $Tracking = [];
        $Job = [];
        if (count($ReportList) > 0 ) {
            $Case = $ReportList[0]['register'];
            $Tracking = $ReportList[0]['tracking'];
            $Job = $ReportList[0]['job'];
        }
        $html = view('components.summary-insurer-province-table', compact('ReportList', 'Case', 'Tracking', 'Job'))->render();
       
        return response()->json(compact('html'));
    }

    public function officerSummaryInsuredPersonalReport(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "&EndDate=".$EndDate;

        //dd($inputParameter);
        /*if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }

       */
       //$ReportList = $this->reportService->getPersonalReport($inputParameter);

        return view('reports.officer.summary-insurer-personal',[
            'StartDate'=> $StartDate,
            'EndDate'=> $EndDate,
            'ReportList'=> [],
            'Case'=> [],
            'Tracking'=> []
        ]);
    }

    public function officerSummaryInsuredPersonalSearchReport(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
       
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
       
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }

        if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }
        
       
        $ReportList = $this->reportService->getPersonalReport($input);

        $html = view('components.summary-insurer-personal-table', compact('ReportList'))->render();
       
        return response()->json(compact('html'));
    }

    public function ssoSummaryInsuredServicesReportSearch(Request $request){
        $input = $request->all();
        //$input['ReportingDate'] = sprintf('%s %s', $input["ReportingDate"], now()->format('H:i:s'));
        $dt = now();
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }
        $inputParameter = "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "&EndDate=".$EndDate;

        if ($request->has('OrganizationID')) {
            $inputParameter .= "&SSOOfficeID=".$input["OrganizationID"];
        }
        
        //dd($inputParameter);
        $ReportList = $this->reportService->getSsoServeReport($inputParameter);
        //dd($ReportList);
        $html = view('components.summary-service-table', compact('ReportList'))->render();
       
        return response()->json(compact('html'));
    }

    public function officerSummaryInsuredServicesReportSearch(Request $request){
        $input = $request->all();
        //$input['ReportingDate'] = sprintf('%s %s', $input["ReportingDate"], now()->format('H:i:s'));
        $dt = now();
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }
        $inputParameter = "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "&EndDate=".$EndDate;

        if ($request->has('OrganizationID')){
            $inputParameter .= "&OrganizationID=".$input["OrganizationID"];
        }
       
        $inputParameter =  $inputParameter;
        $ReportList = $this->reportService->getOfficeServeReport($inputParameter);
        
        $html = view('components.summary-service-table', compact('ReportList'))->render();
       
        return response()->json(compact('html'));
    }


    public function officerSummaryInsuredSsoReport(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = [];

        $DisplayType = "0";
        if ($request->has('DisplayType')) {
            $DisplayType = $input["DisplayType"];
        }
        $inputParameter['WorkStatus'] = $DisplayType;

        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        } else {
            $StartDate = $dt->format('m-Y');
        }

        $arr = explode("-",$StartDate);

        if (count($arr) == 2) {
            $StartYear = $arr[1];
            $StartMonth = (int)$arr[0];
            $inputParameter['month'] = $StartMonth;
            $inputParameter['year'] = $StartYear;
        }

        $searchData = array_merge(
            $inputParameter, 
            $this->paramForMasterData
        );

        $ReportList = $this->reportService->getOfficeRegisterReport($searchData);
        //dump($ReportList);
        return view('reports.officer.summary-insurer-sso',[
            'StartDate'=> $StartDate,
            'DisplayType'=> $DisplayType,
            'ReportList'=>  $ReportList
        ]);
    }

    public function officerSummaryInsuredSsoReportSearch(Request $request)
    {
        $input = $request->all();
        $inputParameter = [];
        $DisplayType = "0";
        if ($request->has('DisplayType')) {
            $DisplayType = $input["DisplayType"];
        }
        $inputParameter['WorkStatus'] = $DisplayType;

        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
            $arr = explode("-",$input["StartDate"]);

            if (count($arr) == 2) {
                $StartYear = $arr[1];
                $StartMonth = (int)$arr[0];
            }

            $inputParameter['month'] = $StartMonth;
            $inputParameter['year'] = $StartYear;
        }

        $searchData = array_merge(
            $inputParameter, 
            $this->paramForMasterData
        );

        $ReportList = $this->reportService->getOfficeRegisterReport($searchData);
       
        $html = view('components.summary-insurer-sso-table', compact('ReportList'))->render();
       
        return response()->json(compact('html'));
    }


    public function notificationEmailLogs(Request $request)
    {
        //$input = $request->all();

        $ReportList = $this->reportService->getEmailLogs($this->paramForMasterData);
       
        return view('reports.officer.notification-email-logs', [
            'ReportList'=> $ReportList
        ]);
    }

    public function notificationEmailLogsSearch(Request $request)
    {
        $input = $request->all();
        //dump( $input);
        $datas = [ 'criteria'=> []];
        $RegisterNumber = $input["RegisterNumber"];
        if ($RegisterNumber) {
            //$datas['criteria']['RegisterNumber'] =  $RegisterNumber;
            $datas['criteria']['keyword'] =  $RegisterNumber;
        }

        if ($request->has('TrackingTime')) {
            //$datas['TrackingTime'] =  $input["TrackingTime"];
            if ($input["TrackingTime"]) {
                $this->paramForMasterData['TrackingTime']  =  $input["TrackingTime"];
            }
        }
       
        $searchData = array_merge(
            $datas, 
            $this->paramForMasterData
        );

        //dump( $searchData);
        $ReportList = $this->reportService->getEmailLogs($searchData);
       
        /*$html = view('components.notification-email-logs-table', compact('ReportList'))->render();
       
        return response()->json(compact('html'));*/

        return response()->json([
            "reportList" => $ReportList
        ]);
    }

    public function exportSummaryInsuredServiceExcel(Request $request)
    {
        $input = $request->all();

        $inputParameter = "StartDate=".$input['StartDate']."&EndDate=".$input["EndDate"]."&OrganizationID=".$input["OrganizationID"];
        $ReportList = $this->reportService->getOfficeServeReport($inputParameter);

        $export = new ExportInsuredService($ReportList);

        $filename = $this->reportService->getFormatFile('สรุปผลการให้บริการผู้ประกันตนกรณีว่างงาน', 'xlsx');
        return Excel::download($export, $filename);
    }

    public function exportSummaryInsuredNationWideExcel(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "&EndDate=".$EndDate;


        $ReportList = $this->reportService->getOfficeNationWideReport($inputParameter);

        $Case = [];
        $Tracking = [];
        $Job = [];
        if ($ReportList) {
            $Case = $ReportList[0]['register'];
            $Tracking = $ReportList[0]['tracking'];
            $Job = $ReportList[0]['job'];
           // dd($ReportList[0]['register']);
        }
        //dd($ReportList);
        $export = new ExportInsuredNationWide($ReportList);
        $filename = $this->reportService->getFormatFile('แบบรายงานผู้ประกันตนกรณีว่างงาน (ทั่วประเทศ)', 'xlsx');
        return Excel::download($export,$filename);
    }

    public function exportSummaryInsuredProvinceExcel(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
       
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
       
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }
        //$OrganizationID = 14;
        if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }

        $criteria = $this->reportService->getRequestParams();
        $p['OrganizationID'] = $OrganizationID;
        $criteria['criteria'] = $p;
       
        $Offices = Http::asForm()->post($this->reportService->getApiDomain() . config('app.organization'), $criteria)->json();
        $Province = "";
        if (!empty($Offices)) {
            $Province = $Offices['data'] ? $Offices['data'][0]['OrganizationName'] : "";
        }
        $ReportList = $this->reportService->getProvinceReport($inputParameter);

        $export = new ExportInsuredProvince($ReportList);

        return Excel::download($export, now()->format('Y-m-d').'.xlsx');
    }

    public function exportSummaryInsuredPersonalExcel(Request $request)
    {
        $input = $request->all();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }

        if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }

        $ReportList = $this->reportService->getPersonalReport($inputParameter);
        //dd($ReportList);
        $export = new ExportInsuredPersonal($ReportList);
        $filename = $this->reportService->getFormatFile('แบบรายงานผู้ประกันตนกรณีว่างงาน (บุคคล)', 'xlsx');
        return Excel::download($export, $filename);
    }

    public function exportSummaryInsuredSsoReport(Request $request)
    {
        $input = $request->all();
        $inputParameter = [];
        $DisplayType = "0";
        if ($request->has('DisplayType')) {
            $DisplayType = $input["DisplayType"];
        }
        $inputParameter['WorkStatus'] = $DisplayType;

        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
            $arr = explode("-",$input["StartDate"]);

            if (count($arr) == 2) {
                $StartYear = $arr[1];
                $StartMonth = (int)$arr[0];
            }

            $inputParameter['month'] = $StartMonth;
            $inputParameter['year'] = $StartYear;
        }

        $searchData = array_merge(
            $inputParameter, 
            $this->paramForMasterData
        );

        $ReportList = $this->reportService->getOfficeRegisterReport($searchData);
        //dd($ReportList[0]);

        if ($ReportList[0]) {
            //$ReportList[0]['WorkStart'] =date("d/m/Y", strtotime($ReportList[0]['WorkStart']));
            $ReportList[0]['empStartDate'] =date("d/m/Y", strtotime($ReportList[0]['empStartDate']));
            $ReportList[0]['WorkStatus'] =$ReportList[0]['employStatus'];
        }
        
        $export = new ExportInsuredSSO($ReportList);

        return Excel::download($export, now()->format('Y-m-d').'.xlsx');
    }

    public function exportNotificationEmailLogs(Request $request)
    {
        $input = $request->all();

        $searchData = array_merge(
            $input, 
            $this->paramForMasterData
        );
        $ReportList = $this->reportService->getEmailLogs($searchData);
        //dd($ReportList);
        $export = new ExportNotificationLogs($ReportList);

        $filename = $this->reportService->getFormatFile('ประวัติการส่งอีเมล์แจ้งเตือนรายงานตัว', 'xlsx');
        return Excel::download($export, $filename);
    }

    public function exportsummaryBenefitServicesExcel(Request $request)
    {
        $input = $request->all();

        $StartDate = "";
        $inputParameter = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        $inputParameter = "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        $inputParameter .= "&EndDate=".$EndDate;

        if ($request->has('OrganizationID')) {
            $inputParameter .= "&SSOOfficeID=".$input["OrganizationID"];
        }

        $ReportList = $this->reportService->getSsoServeReport($inputParameter);
        //dd($ReportList);
        $export = new ExportBenefitSSO($ReportList);

        $filename = $this->reportService->getFormatFile('สรุปผลการให้บริการผู้ประกันตนขอรับประโยชน์ทดแทนกรณีว่างงาน', 'xlsx');
        return Excel::download($export, $filename);
    }

}
