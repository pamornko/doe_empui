<?php

namespace App\Http\Controllers;

use App\Exports\ExportReporting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

use App\Services\JobAnnoucedService;
use App\Services\ReportingService;
use App\Services\CriteriaService;
use App\Services\FreelanceService;
use App\Services\RegisterService;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class ReportingController extends Controller
{
    private $paramForMasterData = [
        'draw' => 1,
        'start' => 0,
        'length' => 10
    ];

    function __construct(
        JobAnnoucedService $jservice,
        ReportingService $rService,
        FreelanceService $fService,
        CriteriaService $cService,
        RegisterService $service
        )
    {
        $this->jobService = $jservice;
        $this->reportingService = $rService;
        $this->freelanceService = $fService;
        $this->criteriaService = $cService;
        $this->registerService = $service;
    }

    public function index(Request $request)
    {
        
        $input = $request->all();
        //dump($input);
        $id = "";
        if ($request->has('id')) {
            $id =$input["id"];
        }

        $Register = null;
        if ($id) {
            $Register = $this->registerService->getDetails( [
                'RegisterID'=> $id
            ]);
            //dump($Register);
        } else {
            $userID = session('UserID');
            if (!empty($userID) && !is_null($userID)) {
                $Register = $this->registerService->getRegisterLatest([
                    'UserID'=> $userID
                ]);
                //dd($userID);
                if ($Register) {
                    $id = $Register['RegisterID'];
                }
            }
        }
        $trackings = null;
        if ($id) {
            $trackings = Http::get(config('app.apiPrefix') . config('app.trackingList'), [
                'RegisterID'=> $id
            ])->json();
            //dump($trackings);
    
            $dtTimestamp = strtotime(now()->format('Y-m-d H:i:s'));//now()->format('Y-m-d H:i:s')
            //$duedate = date("Y-m-d H:i:s", $dtTimestamp);
            $isActive = true;
    
            if ($trackings) {
                for ($x = 0; $x < sizeof($trackings['data']); $x++) {
                
                    $afterTimestamp = strtotime($trackings['data'][$x]['ReportingDueDate']. '+7 day');
                    $beforeTimestamp = strtotime($trackings['data'][$x]['ReportingDueDate']. '-7 day');
        
                    $afterDuedate = date("Y-m-d H:i:s", $afterTimestamp);
                    $beforeDuedate = date("Y-m-d H:i:s", $beforeTimestamp);
        
                    //เดี๋ยวจะเอา code นี้ออก
                    /*if (($dtTimestamp >= $beforeTimestamp) && ($dtTimestamp <= $afterTimestamp)) {
                        $trackings['data'][$x]['Active'] = true;
                    } else {
                        $trackings['data'][$x]['Active'] = false;
                        //$trackings['data'][$x]['Active'] = true;
                    }
        
                    if ($trackings['data'][$x]['Active'] && $trackings['data'][$x]['ReportingDate']) {
                        $trackings['data'][$x]['Active'] = false;
                    }*/
    
                    //dump( $trackings['data'][$x]);
                    $trackings['data'][$x]['Active'] = $trackings['data'][$x]['isActive'];
        
                    /** For Dev **/
                    /*if ($trackings['data'][$x]['ReportingDate'] == null && $isActive) {
                        if ($trackings['data'][$x]['TrackingTime'] == 1) {
                            $trackings['data'][$x]['Active'] = true;
                        }
                        $isActive = false;
                        //$trackings['data'][$x]['ReportingDate'] = '27/11/2020';
                        //$trackings['data'][$x]['ReportingDate'] = now()->format('d\m\Y');
                        //dump(now()->format('d\m\Y'));
                    }*/
        
                    if ($trackings['data'][$x]['ReportingDate']) {
                        $trackings['data'][$x]['ReportingDate'] = date("d/m/Y", strtotime($trackings['data'][$x]['ReportingDate']));
                    }
        
                    //$trackings['data'][$x]['Active'] = true;
                    //dump($trackings['data'][$x]);
                }
            }
        }
        
        //dump($trackings["data"]);
        
        return view('reporting.index',[
            'trackings'=> $trackings ? $trackings["data"] : [],
            'RegisterID' => $id,
            'Register' => $Register
        ]);
    }

    public function reportingForm(Request $request)
    {
        session()->forget('ApplyJobs');
        $API_PREFIX = config('app.apiPrefix');

        $Provinces = Http::get($API_PREFIX.config('app.province'))->json();
        
        $ApplyJobs = [];
        
        $id = "";
        $input = $request->all();
        if ($request->has('id')) {
            $id =$input["id"];
        }
          
        $trackingID ="";
        if ($request->has('trackingID')) {
            $trackingID =$input["trackingID"];
        }

        $trackingTime = "";
        if ($request->has('trackingTime')) {
            $trackingTime =$input["trackingTime"];
        }

        $Tracking = [];
        $Tracking= Http::get(config('app.apiPrefix') . config('app.trackingDetail'), [
            'TrackingID'=> $trackingID
        ])->json();
      
        $JobQualifications = [];
        //$JobQualifications =  $this->jobService->getQualifications();
        
        $reqInput = ['langID'=>1];
        $JobFieldIDs = $this->criteriaService->getJobField($reqInput);
        $BusinessTypes = $this->criteriaService->getBusinessType($reqInput);
        $Degrees = $this->criteriaService->getDegree($reqInput);
        $WorkExperiences = $this->criteriaService->getWorkExperience($reqInput);

        $dt = now();
        $CurrentDate = $dt->format('Y-m-d');

        //dump($Tracking['ReportingDueDate']);
        /*$strDuedate = strtotime($Tracking['ReportingDueDate']. '+7 day');
        $strCurrentDate = strtotime($CurrentDate);
        $requireComment = false;
        if ($strCurrentDate > $strDuedate) {
            $requireComment = true;
        }*/
        $requireComment = false;
        if (isset($Tracking['IsLate'])) {
            $requireComment = true;
        }

        //$requireComment = true;
        $action = "edit";
        /*if (Str::endsWith($path, 'edit')) {
            $action = "edit";
        }*/

        $WorkDate = "";
        //วันที่ได้งาน
        $isGetJob = false;
        if (session('empStartDate') > session('empResignDate')) {
            $isGetJob = true;
            if (session('empStartDate')) {
                $WorkDate = session('empStartDate');
                /*$pieces = explode("/", session('empStartDate'));
    
                if (count($pieces) >= 3) {
                    $WorkDay = $pieces[0];
                    $WorkMonth = $pieces[1];
                    $WorkYear = $pieces[2] - 543; 
                    $WorkDate = sprintf("%s-%s-%s", $WorkYear, $WorkMonth, $WorkDay);
                }*/
               
            }    
        }

        $district = null;
        if (session('compAmphurCode')) {
            $district = $this->criteriaService->getDistrict("DistrictID=".session('compAmphurCode'));
        }

        $subDistrict = null;
        if (session('compTambonCode')) {
            $subDistrict = $this->criteriaService->getSubDistrict("TambonID=".session('compTambonCode'));
        }
        $paramForMasterData = [
            'draw' => 1,
            'start' => 0,
            'length' => 200
        ];
        $Freelances = $this->freelanceService->getMasterFreelances($paramForMasterData);
        return view('reporting.form-reporting-wizard', [
            'Freelances' => $Freelances ? $Freelances : [],
            'Provinces'=> $Provinces ? $Provinces : [],
            'ApplyJobs'=> $ApplyJobs,
            'JobsSuggestions'=> [],
            'RegisterID' => $id,
            'Tracking'=> $Tracking ? $Tracking : [],
            'TrackingID' => $trackingID,
            'TrackingTime' => $trackingTime,
            'isGetJob' => $isGetJob,
            'JobQualifications'=> [],
            'JobMatchings'=> [],
            'JobFieldID'=> $JobFieldIDs,
            'BusinessTypes'=> $BusinessTypes,
            'Degrees'=> $Degrees,
            'WorkExperiences'=> $WorkExperiences,
            'MinAge' => $this->criteriaService->getMinAge(),
            'MaxAge' => $this->criteriaService->getMaxAge(),
            'MixSalarys' => $this->criteriaService->getMinSalary(),
            'MaxSalarys' => $this->criteriaService->getMaxSalary(),
            'CntFl'=> 1,
            'CurrentDate' => $CurrentDate,
            'RequireComment' => $requireComment,
            'Action' => $action,
            'WorkDate' => $WorkDate,
            'organization' => session('employerName'),
            'contactAddress' => session('compAddress'),
            'provinceCode' => session('compProviceCode'),
            'district' => $district,
            'subDistrict' => $subDistrict,
            'Postcode' => $subDistrict ? $subDistrict['PostCode'] : ""
        ]);
    }

    public function store(Request $request)
    {

        $input = $request->all();
        $registerID = $input["RegisterID"];
        $trackingID = $input["TrackingID"];
        $trackingTime = "";
        if ($request->has('TrackingTime')) {
            $trackingTime =$input["TrackingTime"];
        }
        
        
        //dump($input);
        
        
        $workStatus = "0";
        if ($request->has('workStatus')) {
            $workStatus =$input["workStatus"];
        }

        $ReportingDueDate = "";
        if ($request->has('ReportingDueDate')) {
            $ReportingDueDate =$input["ReportingDueDate"];
        }

        $RemarkLate = "";
        if ($request->has('RemarkLate')) {
            $RemarkLate =$input["RemarkLate"];
        }
        $isApply = 1;
        if ($request->has("ignoreJob")) {
            $isApply = 0;
        }

        $arrReportingDate = explode("/",$input["ReportingDate"]);
        if (count($arrReportingDate) ==  3) {
            $adYear = $arrReportingDate[2] - 543;
            $reportingDate = sprintf('%s-%s-%s',$adYear , $arrReportingDate[1], $arrReportingDate[0]);
        } 

        $reportingDatas = [
            'WorkStatus'=> $workStatus,
            //'UserID'=> $userID,\\\
            'RegisterID'=> $registerID,
            'TrackingID'=> $trackingID,
            'ReportingDate'=> $reportingDate,
            'TrackingTime'=> $trackingTime,
            //'ReportingDueDate'=> $ReportingDueDate
            'RemarkLate' => $RemarkLate,
            "OrganizationID" => session('OrganizationID'),
            "isApply" => $isApply
            //"SSOOfficeCode" => session('SSOOfficeCode')
        ];

        //dump($reportingDatas);

        $API_PREFIX = config('app.apiPrefix');
        $response = $this->reportingService->update($reportingDatas);

        $province = session('compProviceCode');
        if ($request->has('province')) {
            $province = $input["province"];
        }

        $district = session('compAmphurCode');
        if ($request->has('district')) {
            $district = $input["district"];
        }

        $subDistrict = session('compTambonCode');
        if ($request->has('subDistrict')) {
            $subDistrict = $input["subDistrict"];
        }

        if ($response) {
            if ($workStatus == "1") {
                //update work
                //trackingWork
                $workDatas = [
                    'TrackingID'=> $trackingID,
                    'WorkDate'=> $input["workDate"],
                    'EmployerCode'=> $input["EmployerCode"],
                    'EmployerName'=> $input["organization"],
                    'EmployerAddress'=> $input["contactAddress"],
                    'Postcode'=> $input["postcode"],
                    'PhoneNumber'=> $input["phoneNumber"],
                    'ContactPerson'=> $input["contactPerson"],
                    'ProvinceID'=>  $province,
                    'DistrictID'=>  $district,
                    'TambonID'=> $subDistrict
                ];
                //dd($workDatas);
    
                $workResponse = Http::asForm()->post($API_PREFIX . config('app.trackingWork'), $workDatas)->json();
                //dump($workResponse);
            }
            
            if ($request->has('wintType')) {
                for ($x = 0; $x < sizeof($input["wintType"]); $x++) {


                    $freelanceID = $input['wintType'][$x];
                        
                    if ($freelanceID) {

                        $registerFreelanceDatas = [
                            'TrackingID' =>$trackingID,
                            'FreelanceID[]'=> $freelanceID,
                            'Other[]'=>$input['wint'][$x] ? 1 : 0,
                            'FreelanceOther[]'=> $input['wint'][$x] ? $input['wint'][$x] : ""
                        ];
                       
                        $this->reportingService->creatFreelance($registerFreelanceDatas);
                    }
        
                }
            }
    
            $ApplyJobs = session('ApplyJobs');
            $EmployeeID =  session('EmployeeID');
            //dump($ApplyJobs);
            //dump($EmployeeID);
            if (!empty($ApplyJobs)) {
                foreach ($ApplyJobs as $applyjob) {
                    //dump($applyjob);
                    $this->jobService->applyJob([
                        'EmployeeID' => $EmployeeID,
                        'JobAnnounceID' => $applyjob['id'],
                        'RegisterID'  => $registerID,
                        'TrackingID' => $trackingID
                    ]);
        
                }
                session()->forget('ApplyJobs');
            }

            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false
            ]);
        }
        
    }

    public function reportingOfficerIndex()
    {

        $API_PREFIX = config('app.apiPrefix');
        $Offices = Http::asForm()->post($API_PREFIX . config('app.organization'), $this->paramForMasterData)->json();

        $criteria['columns'][0]['name'] =  'RegisterNumber';
        $criteria['order'][0]['column'] =  0;
        $criteria['order'][0]['dir'] =  'desc';
        $criteria['criteria']['isActive'] =  1;
        $criteria['criteria']['Service'] =  0;

        //$StartDate = date("Y-m-d", strtotime("-30 day"));
        $startYear = date("Y") - 1;
        $StartDate = sprintf("01/10/%s", $startYear);
        if ($StartDate) {
            $criteria['criteria']['StartDate'] =  $StartDate;
        }

        $EndDate = date("Y-m-d");
        if ($EndDate) {
            $criteria['criteria']['EndDate'] =  $EndDate;
            $arrEndDate = explode("-",$EndDate);
            if (count($arrEndDate) ==  3) {
                $adYear = $arrEndDate[0] + 543;
                $EndDate = sprintf('%s/%s/%s', $arrEndDate[2], $arrEndDate[1], $adYear);
            } 
        }
        $criteria['criteria']['SessionUserID'] = session('UserID');

        $datas = array_merge(
            $criteria,
            $this->paramForMasterData
        );
        
        //$ReportingList = $this->reportingService->getReporting($datas);
        $ReportingList = [];
        return view('reporting.officer.index', [
            'Offices'=> $Offices ? $Offices["data"] : [],
            'ReportingList' => $ReportingList,
            'StartDate' => $StartDate,
            'EndDate' => $EndDate,
        ]);
    }

    public function reportingOfficerEdit(Request $request)
    {

        $input = $request->all();
        $trackingID = $input["id"];
        $path = $request->path();
        $action = "view";
        if (Str::endsWith($path, 'edit')) {
            $action = "edit";
        }

        $Reporting= Http::get(config('app.apiPrefix') . config('app.trackingDetail'), [
            'TrackingID'=> $trackingID
        ])->json();

        $criteria['criteria']['RegisterID'] =  $Reporting['RegisterID'];
        $criteria['criteria']['TrackingID'] =  $trackingID;
        $criteria['criteria']['EmployeeID'] = $Reporting['EmployeeID'];
        $datas = array_merge(
            $criteria, 
            $this->paramForMasterData
        );

        $Works = $this->reportingService->getWorkForReporting($datas);
        if (count($Works) > 0) {
            $Works[0]['WorkDate'] = date("d/m/Y", strtotime($Works[0]['WorkDate']));
        }
        //dd( $datas);
        $ApplyJobs= [];
        if ($criteria['criteria']['EmployeeID'] != null) {
            $ApplyJobs = Http::asForm()->post(config('app.apiPrefix') . config('app.applyJobList'), $datas)->json();
        }
        
        $Freelances = $this->freelanceService->getFreelances($datas);
        
        return view('reporting.officer.form-reporting',[
            'Reporting' => $Reporting ,
            'ApplyJobs' => $ApplyJobs ? $ApplyJobs["data"] : [],
            'Work' => $Works ? $Works[0] : null,
            'RegisterFreelances' => $Freelances,
            'Action' => $action,
            'TrackingID' => $trackingID
        ]);
    }

    public function Search(Request $request){
        $inputs = $request->all();
        $draw = 1;
        $start = 0;
        $pageLength = 20;
        $criteria = [ 'criteria'=> []];

        if ($request->has('draw')) {
            $draw = $inputs['draw'];
        }

        if ($request->has('start')) {
            $start = $inputs['start'];
        }

        $criteria['columns'][0]['name'] =  'RegisterNumber';
        $criteria['order'][0]['column'] =  0;
        $criteria['order'][0]['dir'] =  'desc';
        $criteria['criteria']['isActive'] =  1;
        $criteria['criteria']['Service'] =  0;

        if ($request->has('StartDate')) {
            $StartDateArr = explode("/", $inputs["StartDate"]);
            if (count($StartDateArr) == 3) {
                $criteria['criteria']['StartDate'] =  sprintf("%s-%s-%s", ($StartDateArr[2] - 543), $StartDateArr[1], $StartDateArr[0]);
            }
        }

        if ($request->has('EndDate') && trim($request->get('EndDate') != "")) {
            $EndDateArr = explode("/", $inputs["EndDate"]);
            if (count($EndDateArr) == 3) {
                $criteria['criteria']['EndDate'] =  sprintf("%s-%s-%s", ($EndDateArr[2] - 543), $EndDateArr[1], $EndDateArr[0]);
            }
        }

        if ($request->has('RegisterNumber')) {
            $criteria['criteria']['keyword'] =  trim($inputs["RegisterNumber"]);
        }

        $criteria['criteria']['Service'] = 0;
        if ($request->has('Service')) {
            $criteria['criteria']['Service'] =  $inputs["Service"];
        }
        $criteria['criteria']['SessionUserID'] = session('UserID');

        if ($request->has('typeBsiness')) {
            $criteria['criteria']['OrganizationSupportID'] =  $inputs["typeBsiness"];
        }

        $this->paramForMasterData["draw"] = $draw;
        $this->paramForMasterData["length"] = $pageLength;
        $this->paramForMasterData["start"] = $start;
        $searchData = array_merge(
            $criteria, 
            $this->paramForMasterData
        );

        //dump($searchData);
        //dd($searchData["criteria"]);
        //dd($searchData["columns"]);
        //dd($searchData["order"]);
        
        $datas = array();
        $total = 0;
        $fills = 0;
        if ($request->has('StartDate') && $draw > 1) {
            $ReportingList = $this->reportingService->getReportings($searchData);
            if ($ReportingList) {
                $total = $ReportingList["recordsTotal"];
                $fills = $ReportingList["recordsFiltered"];
                if ($ReportingList['data']) {
                    foreach($ReportingList['data'] as $Reporting) {
                        $datas[] = [
                            'id' => $Reporting["TrackingID"],
                            'register_number' => $Reporting["RegisterNumber"],
                            'national_id' => $Reporting["PersonalID"],
                            'name' => $Reporting["FirstName"]." ".$Reporting["LastName"],
                            'tracking_time' => $Reporting["TrackingTime"],
                            'reporting_duedate' => show_thai_date($Reporting["ReportingDueDate"]) ,
                            'reporting_date' => show_thai_date($Reporting["ReportingDate"]) 
                        ];
                    }
                }
            }
        }
        $response = [
            "draw" => intval($draw),
            "recordsTotal" => $total,
            "recordsFiltered" =>  $fills,
            "data"            => $datas
        ];
        return response()->json( $response
        , 200);
    }

    public function OfficerEdit(Request $request){
        $input = $request->all();
        //$input['ReportingDate'] = sprintf('%s %s', $input["ReportingDate"], now()->format('H:i:s'));
        $response = $this->reportingService->updateAndRegenerate([
            "Remark" => $input['RemarkLate'],
            "ReportingDate"  => $input['ReportingDate'],
            "TrackingID"  => $input['TrackingID']
        ]);
        //dump($response);
        //dump($response);
        if ($response) {
            return response()->json([
                'success' => true,
                'trackingId' => $response
            ]);
        } else {
            return response()->json([
                'success' => false
            ]);
        }
    }

    public function exportExcel(Request $request)
    {
        $input = $request->all();

        $datas = [ 'criteria'=> []];

        if ($request->has('RegisterNumber')) {
            $datas['criteria']['RegisterNumber'] =  $input["RegisterNumber"];
        }

        $StartDate = "";
        if ($StartDate) {
            $datas['criteria']['StartDate'] =  $input["StartDate"];
        }
        
        $EndDate = "";
        if ($EndDate) {
            $datas['criteria']['EndDate'] =  $input["EndDate"];
        }

        $datas['criteria']['Service'] = 0;
        if ($input["Service"]) {
            $datas['criteria']['Service'] =  $input["Service"];
        }

        if ($request->has('Organization')) {
            $datas['criteria']['OrganizationSupportID'] =  $input["Organization"];
        }
        $datas['criteria']['SessionUserID'] = session('UserID');

        /*$datas['columns'][0]['name'] =  'RegisterNumber';
        $datas['order'][0]['column'] =  0;
        $datas['order'][0]['dir'] =  'desc';*/
        $datas['criteria']['isActive'] =  1;

        $searchData = array_merge(
            $datas, 
            $this->paramForMasterData
        );

        $ReportingList = $this->reportingService->getReporting($searchData);

        $export = new ExportReporting($ReportingList);
        $filename = $this->registerService->getFormatFile('reporting', 'xlsx');
        //now()->format('Y-m-d')
        return Excel::download($export, $filename);
    }

    public function exportPDF(Request $request)
    {
        $input = $request->all();

        $datas = [ 'criteria'=> []];

        if ($request->has('RegisterNumber')) {
            $datas['criteria']['RegisterNumber'] =  $input["RegisterNumber"];
        }

        $StartDate = "";
        if ($StartDate) {
            $datas['criteria']['StartDate'] =  $input["StartDate"];
        }
        
        $EndDate = "";
        if ($EndDate) {
            $datas['criteria']['EndDate'] =  $input["EndDate"];
        }

        $datas['columns'][0]['name'] =  'RegisterNumber';
        $datas['order'][0]['column'] =  0;
        $datas['order'][0]['dir'] =  'desc';
        $datas['criteria']['isActive'] =  1;

        $searchData = array_merge(
            $datas, 
            $this->paramForMasterData
        );

        $ReportingList = [];
        $data = $this->reportingService->getReporting($searchData);
        if ($data) {
            $ReportingList = $data;
        }
        
        set_time_limit(300); 
        $pdf = PDF::loadView('exports.reporting-table-pdf',  ['ReportingList' => $ReportingList]);
        $filename = $this->registerService->getFormatFile('reporting', 'pdf');
        return $pdf->download($filename);
        //return $pdf->stream();    
    }
}
