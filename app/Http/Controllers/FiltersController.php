<?php

namespace App\Http\Controllers;

use App\Services\CriteriaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class FiltersController extends Controller
{
    private $paramForMasterData = [
        'draw' => 1,
        'start' => 0,
        'length' => 100
    ];

    function __construct(
       
        CriteriaService $cService
        )
    {
       
        $this->criteriaService = $cService;
    }

    public function jobsFieldList(Request $request)
    { 
        $criteria['langID'] = 1;
        $JobsFields = $this->criteriaService->getJobField($criteria);
        $Datas[] = [
            "id" => "",
            "text" => "ประเภทงาน"
        ];
        foreach ($JobsFields as $JobsField) { 
            $Datas[] = [
                "id" => $JobsField['JobFieldID'],
                "text" => $JobsField['JobFieldName']
            ];
        }
        return response()->json(['success'=> true, 'datas'=> $Datas]);
    }

    public function findDistricts(Request $request)
    { 
        $API_PREFIX = config('app.apiPrefix');
        $Districts = Http::get($API_PREFIX . config('app.district') . '?ProvinceID=' .$request->id)->json();
        //Log::debug('findDistrict ',$Districts);
        return response()->json(['success'=> true, 'districts'=> $Districts]);
    }

    public function findTambons(Request $request)
    {
        $API_PREFIX = config('app.apiPrefix');
        $Tambons = Http::get($API_PREFIX .config('app.tambon') . '?DistrictID=' . $request->id)->json();
        //Log::debug('findDistrict ',$Districts);
        return response()->json(['success'=> true, 'tambons'=> $Tambons]);
    }

    public function findResignCase(Request $request)
    {
      
        $input = $request->all();

        $API_PREFIX = config('app.apiPrefix');
        $data['ResignID'] = $input['id'];
        $this->paramForMasterData['criteria'] = $data;
        $LayoffCases = Http::asForm()->post($API_PREFIX .config('app.layoffCase'), $this->paramForMasterData)->json();

        $LayoffCases = $LayoffCases['data'];
        //dump($LayoffCases);

        $html = view('components.resign-case-dropdown', compact('LayoffCases'))->render();
        return response()->json(compact('html'));
    }

    public function findProvinceID(Request $request)
    {

        if ($request->has('name')) {
            $index = strpos($request->get('name'),"จังหวัด");
            $input['ProvinceName'] = $request->get('name');
            if ($index === 0) {
                $input['ProvinceName'] = str_replace("จังหวัด", "", $request->get('name'));
            }
        }
        
        $provinceID =  $this->criteriaService->getProvinceByName($input);
        $success = false;
        if ($provinceID) {
            $success = true;
        }
        return response()->json(['success'=> $success , 'id' => $provinceID]);
    }

    public function findDistrictID(Request $request)
    {

        if ($request->has('name')) {
            $index = strpos($request->get('name'),"เขต");
            $input['DistrictName'] = $request->get('name');
            if ($index === 0) {
                $input['DistrictName'] = str_replace("เขต", "", $request->get('name'));
            }
        }

        if ($request->has('province_id')) {
            $input['ProvinceID'] = $request->get('province_id');
        }
        
        $districtID =  $this->criteriaService->getDistrictByName($input);
        $success = false;
        if ($districtID) {
            $success = true;
        }
        return response()->json(['success'=> $success , 'id' => $districtID]);
    }

    public function findTumbonID(Request $request)
    {

        if ($request->has('name')) {
            $index = strpos($request->get('name'), "แขวง");
            $input['TambonName'] = $request->get('name');
            if ($index === 0) {
                $input['TambonName'] = str_replace("แขวง", "", $request->get('name'));
            }
        }

        if ($request->has('district_id')) {
            $input['DistrictID'] = $request->get('district_id');
        }
        
        $tumbonID =  $this->criteriaService->getSubDistrictByName($input);
        $success = false;
        if ($tumbonID) {
            $success = true;
        }
        return response()->json(['success'=> $success , 'id' => $tumbonID]);
    }
}
