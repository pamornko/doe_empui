<?php

namespace App\Http\Controllers;

use App\Services\EmployerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{
 
    function __construct()
    {
    }

    public function downloadBookbankFile(Request $request)
    {
        $input = $request->all();
        //dump($input);
        //return response()->download($input["page"]);
        return Storage::download($input["page"]);
        //return Storage::download('storage/app/public/uploads/3560100495711_20210929_220556.jpg');
    }
}