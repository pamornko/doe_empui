<?php 

namespace App\Http\Controllers;

use App\Services\DashboardService;

use Illuminate\Http\Request;

class APIController extends Controller
{
    function __construct(
        DashboardService $dService
        )
    {
        $this->dashService = $dService;  
    }

    public function registerDashboard(Request $request)
    {
        $input = $request->all();
        $lYear = date("Y") - 1;
        $d = date("d");
        $m = date("m");
        $y = date("Y");
        $sDate = sprintf("%s-%s-%s", $y, str_pad($m,2,"0", STR_PAD_LEFT) , $d);
        $eDate = sprintf("%s-%s-%s", $y, str_pad($m,2,"0", STR_PAD_LEFT) , $d);

        $input['StartDate'] = $sDate;
        $input['EndDate'] = $eDate;

        $results = $this->dashService->getRegisterDashboard($input);
        return response()->json([
            'results' => $results
        ]);
    }

    public function registerYearDashboard(Request $request)
    {
        $input = $request->all();
        $lYear = date("Y") - 1;
        $d = date("d");
        $m = date("m");
        $y = date("Y");
        $sDate = sprintf("%s-10-01", $lYear);
        $eDate = sprintf("%s-%s-%s", $y, str_pad($m,2,"0", STR_PAD_LEFT) , $d);

        $input['StartDate'] = $sDate;
        $input['EndDate'] = $eDate;

        $results = $this->dashService->getRegisterDashboard($input);
        return response()->json([
            'results' => $results
        ]);
    }

    public function reportingDashboard(Request $request)
    {
        $input = $request->all();
        $lYear = date("Y") - 1;
        $d = date("d");
        $m = date("m");
        $y = date("Y");
        $sDate = sprintf("%s-%s-%s", $y, str_pad($m,2,"0", STR_PAD_LEFT) , $d);
        $eDate = sprintf("%s-%s-%s", $y, str_pad($m,2,"0", STR_PAD_LEFT) , $d);

        $input['StartDate'] = $sDate;
        $input['EndDate'] = $eDate;

        $results = $this->dashService->getReportingDashboard($input);
        return response()->json([
            'results' => $results
        ]);
    }

    public function reportingYearDashboard(Request $request)
    {
        $input = $request->all();
        $lYear = date("Y") - 1;
        $d = date("d");
        $m = date("m");
        $y = date("Y");
        $sDate = sprintf("%s-10-01", $lYear);
        $eDate = sprintf("%s-%s-%s", $y, str_pad($m,2,"0", STR_PAD_LEFT) , $d);

        $input['StartDate'] = $sDate;
        $input['EndDate'] = $eDate;

        $results = $this->dashService->getReportingDashboard($input);
        return response()->json([
            'results' => $results
        ]);
    }

    public function jobYearDashboard(Request $request)
    {
        $input = $request->all();
        $lYear = date("Y") - 1;
        $d = date("d");
        $m = date("m");
        $y = date("Y");
        $sDate = sprintf("%s-10-01", $lYear);
        $eDate = sprintf("%s-%s-%s", $y, str_pad($m,2,"0", STR_PAD_LEFT) , $d);

        $input['StartDate'] = $sDate;
        $input['EndDate'] = $eDate;
        //dump($input);
        $results = $this->dashService->getJobDashboard($input);
        return response()->json([
            'results' => $results
        ]);
    }
}