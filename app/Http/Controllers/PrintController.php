<?php
namespace App\Http\Controllers;

use App\Services\ReportingService;
use App\Services\ReportService;
use App\Services\CriteriaService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PrintController extends Controller {

    function __construct(
        ReportingService $rService,
        ReportService $rrService,
        CriteriaService $cService
        )
    {
        $this->reportingService = $rService;
        $this->reportService = $rrService;
        $this->criteriaService = $cService;
    }

    public function exportReporting(Request $request)
    {
        $input = $request->all();

        $datas = [ 'criteria'=> []];

        if ($request->has('RegisterNumber')) {
            $datas['criteria']['RegisterNumber'] =  $input["RegisterNumber"];
        }

        $StartDate = "";
        if ($StartDate) {
            $datas['criteria']['StartDate'] =  $input["StartDate"];
        }
        
        $EndDate = "";
        if ($EndDate) {
            $datas['criteria']['EndDate'] =  $input["EndDate"];
        }

        $datas['criteria']['Service'] = 0;
        if ($request->has('Service')) {
            $datas['criteria']['Service'] =  $input["Service"];
        }

        if ($request->has('Organization')) {
            $datas['criteria']['OrganizationSupportID'] =  $input["Organization"];
        }
        $datas['criteria']['SessionUserID'] = session('UserID');

        /*$datas['columns'][0]['name'] =  'RegisterNumber';
        $datas['order'][0]['column'] =  0;
        $datas['order'][0]['dir'] =  'desc';*/
        $datas['criteria']['isActive'] =  1;

        $searchData = array_merge(
            $datas, 
            $this->reportingService->getRequestDefaultParams(0)
        );

        $ReportingList = [];
        $data = $this->reportingService->getReporting($searchData);
        if ($data) {
            $ReportingList = $data;
        }

        return view('print.reporting', [
            'ReportingList'=> $ReportingList
        ]);
    }

    public function exportSummaryInsuredServices(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }

        if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }

        $ReportList = $this->reportService->getOfficeServeReport($inputParameter);
        return view('print.summary-insured-service', [
            'ReportList'=> $ReportList
        ]);
    }

    public function exportSummaryInsuredNationWide(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "&EndDate=".$EndDate;


        $ReportList = $this->reportService->getOfficeNationWideReport($inputParameter);

        $Case = [];
        $Tracking = [];
        $Job = [];
        if ($ReportList) {
            $Case = $ReportList[0]['register'];
            $Tracking = $ReportList[0]['tracking'];
            $Job = $ReportList[0]['job'];
        }

        return view('print.summary-insurer-nationwide', [
            'Case' => $Case, 
            'Tracking' => $Tracking, 
            'Job' => $Job, 
            'ReportList'=> $ReportList
        ]);
    }

    public function exportSummaryInsuredProvince(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
       
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
       
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }
        //$OrganizationID = 14;
        if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }

        $criteria = $this->reportService->getRequestParams();
        $p['OrganizationID'] = $OrganizationID;
        $criteria['criteria'] = $p;
       
        $Offices = Http::asForm()->post($this->reportService->getApiDomain() . config('app.organization'), $criteria)->json();
        $Province = "";
        if (!empty($Offices)) {
            $Province = $Offices['data'] ? $Offices['data'][0]['OrganizationName'] : "";
        }
        $ReportList = $this->reportService->getProvinceReport($inputParameter);
        
        $Case = [];
        $Tracking = [];
        $Job = [];
        if (count($ReportList) > 0 ) {
            $Case = $ReportList[0]['register'];
            $Tracking = $ReportList[0]['tracking'];
            $Job = $ReportList[0]['job'];
        }

        return view('print.summary-insurer-province', [
            'Province' => $Province,
            'Case' => $Case, 
            'Tracking' => $Tracking, 
            'Job' => $Job, 
            'ReportList'=> $ReportList
        ]);
    }

    public function exportSummaryInsuredPersonal(Request $request)
    {
        $input = $request->all();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }

        if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }
        $ReportList = $this->reportService->getPersonalReport($inputParameter);

        $criteria = $this->reportService->getRequestParams();
        $p['OrganizationID'] = $OrganizationID;
        $criteria['criteria'] = $p;
       
        $Offices = Http::asForm()->post($this->reportService->getApiDomain() . config('app.organization'), $criteria)->json();
        $Province = "";
        if (!empty($Offices)) {
            $Province = $Offices['data'] ? $Offices['data'][0]['OrganizationName'] : "";
        }

        return view('print.summary-insurer-personal',  [
            'Province' => $Province, 
            'ReportList' => $ReportList
        ])->render();
        
    }


    public function exportSummaryInsuredSso(Request $request)
    {
        $input = $request->all();
        $inputParameter = [];
        $DisplayType = "0";
        if ($request->has('DisplayType')) {
            $DisplayType = $input["DisplayType"];
        }
        $inputParameter['WorkStatus'] = $DisplayType;

        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
            $arr = explode("-",$input["StartDate"]);

            if (count($arr) == 2) {
                $StartYear = $arr[1];
                $StartMonth = (int)$arr[0];
            }

            $inputParameter['month'] = $StartMonth;
            $inputParameter['year'] = $StartYear;
        }

        $searchData = array_merge(
            $inputParameter, 
            $this->reportingService->getRequestDefaultParams(0)
        );

        $ReportList = $this->reportService->getOfficeRegisterReport($searchData);
        

        if ($ReportList[0]) {
            //$ReportList[0]['WorkStart'] =date("d/m/Y", strtotime($ReportList[0]['WorkStart']));
            $ReportList[0]['empStartDate'] =date("d/m/Y", strtotime($ReportList[0]['empStartDate']));
            $ReportList[0]['WorkStatus'] =$ReportList[0]['employStatus'];
        }
       
        return view('print.summary-insurer-sso', [
            'ReportList'=> $ReportList
        ]);
    }

    public function summaryBenefitServices(Request $request)
    {
        $input = $request->all();

        $userID = null;
        if ($request->has('UserID')) {
            $userID =$input["UserID"];
        }
        
        if ($userID) {
            $this->sessionService->stampSession(['UserID' => $request->get('UserID'), 'UserTypeID' => $request->get('UserTypeID')]);
        }

        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }

        if ($OrganizationID) {
            $inputParameter .= "&SSOOfficeID=".$OrganizationID;
        }

        //dd($inputParameter);
        $ReportList = $this->reportService->getSsoServeReport($inputParameter);
        $SsoList = $this->criteriaService->getSsoList( $this->reportingService->getRequestDefaultParams(0));

        return view('print.summary-benefit-sso',  [
            'StartDate'=> $StartDate,
            'EndDate'=> $EndDate,
            'ReportList'=> $ReportList ? $ReportList : [],
            'SsoList'=> $SsoList
        ])->render();
    }
}