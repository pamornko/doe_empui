<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function goToProfile(Request $request)
    { 
        $API_PREFIX = config('app.sjcrefix');
        
        return redirect($API_PREFIX.'JSK/Employee/profile');
        //return redirect("http://128.199.231.137/doe_sjc_web/JSK/Employee/profile?system=empui");
    }
}
