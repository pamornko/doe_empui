<?php

namespace App\Http\Controllers;

use App\Services\EmployerService;
use Illuminate\Http\Request;

class DBDController extends Controller
{
 
    function __construct(EmployerService $service)
    {
        $this->eServic = $service;
    }

    public function getDBDEmployer(Request $request)
    {
        $input = $request->all();
        if ($request->has("EmployerCode")) {
            $params = "JusticeID=".$input['EmployerCode'];
        }
        $response = $this->eServic->getDBDEmployerID($params);
       
        return response()->json([
            'success' => true,
            'data' => $response
        ]);
    }
}