<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthenticationController extends Controller
{
    public function logout(Request $request)
    { 
        $UserTypeID = session('UserTypeID');
        session()->flush();
        if ($UserTypeID  ==  12){
            //return redirect('http://128.199.231.137/doe_sjc_web/Auth/logout');
            return redirect(config('app.webDatacenter').'login.do');
        } else  {
            //return redirect('http://128.199.231.137/doe_sjc_web/Auth/logout');
            return redirect(config('app.webDatacenter').'login.do');
        }
       
    }
}
