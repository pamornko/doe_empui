<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use App\Services\DashboardService;
use App\Services\InitialService;
use App\Services\MenuService;

class PageController extends Controller
{
   
    function __construct(
        MenuService $menuService,
        InitialService $iService,
        DashboardService $dService
        )
    {
        $this->menuService = $menuService;
        $this->iniServvice = $iService;
        $this->dashService = $dService;  
    }


    public function index(Request $request)
    {

        $Year = date("Y");
        $LastYear = $Year - 1 ;
        $startDate =  sprintf("%s-10-01", $LastYear);
        if ($request->has('StartDate')) {
            $startDate = $request->has('StartDate');
        }

        $endDate =  date("Y-m-d");
        if ($request->has('EndDate')) {
            $endDate = $request->has('EndDate');
        }

        $Year =  date("Y") + 543;
        $showStartDate =  sprintf("%s-10-01", $LastYear + 543);
        $showEndDate =  sprintf("%s-%s-%d", $Year ,  str_pad(date("m"),2,"0", STR_PAD_LEFT), date("d"));
        /*dump($startDate);
        dump($endDate);*/
        $input['UserID'] = session('UserID');
        $Summary = $this->dashService->getDashboardActive($input);

        if (!empty($Summary)) {
            if ($Summary['lastRegister']){
                $BEYear = date("Y", strtotime($Summary['lastRegister']['RegisterDate'])); 
                $BEYear = $BEYear + 543;
                $Summary['lastRegister']['RegisterDate'] = $BEYear ."-". date("m-d", strtotime($Summary['lastRegister']['RegisterDate']));
            }
    
            if ($Summary['lastTracking']){
                $BEYear = date("Y", strtotime($Summary['lastTracking']['ReportingDate'])); 
                $BEYear = $BEYear + 543;
                $Summary['lastTracking']['ReportingDate'] = $BEYear ."-". date("m-d", strtotime($Summary['lastTracking']['ReportingDate']));
            }
    
            if ($Summary['lastBenefit']){
                $BEYear = date("Y", strtotime($Summary['lastBenefit']['payBeginDate'])); 
                $BEYear = $BEYear;
                $Summary['lastBenefit']['payBeginDate'] = $BEYear ."-". date("m-d", strtotime($Summary['lastBenefit']['payBeginDate']));
            }
    
            if ($Summary['nextTracking']){
                $BEYear = date("Y", strtotime($Summary['nextTracking']['ReportingDueDate'])); 
                $BEYear = $BEYear + 543;
                $Summary['nextTracking']['ReportingDueDate'] = $BEYear ."-". date("m-d", strtotime($Summary['nextTracking']['ReportingDueDate']));
            }
        } 

        $Organizes = $this->iniServvice->getAllOrganization();
        $Menus = $this->menuService->getMenus();
        if ($Menus && count($Menus) > 0) {
            session(['Menus' =>$Menus]);
        }
        //dump(session('Menus'));
        return view('index',[
            'Menus' => $Menus ? $Menus : [],
            'Offices' => $Organizes,
            'Year' => $Year,
            'StartDate' => $startDate,
            'EndDate' => $endDate,
            'ShowStartDate' => $showStartDate,
            'ShowEndDate' => $showEndDate,
            'CurrentDate'  => $showEndDate,
            'Summary' => $Summary
        ]);
    }

    public function registerUnemployed(Request $request)
    {
        //dump(session()->all());
        //$userCode = $request->query('userCode', '3850400186702');
        //$userID = $request->query('userCode', '172986');
        //$userID = $request->query('UserID');
        //if (empty($userID)) {}
        $userID = (session('UserID'));
        //clear apply job
        session()->forget('ApplyJobs');

        $criteria['criteria']['StartDate'] =  "2021-01-01";
        //$criteria['criteria']['EndDate'] =  "2021-10-01";
        $criteria['criteria']['UserID'] =  $userID;
       /* $criteria['columns'][0]['name'] = 'RegisterNumber';
        $criteria['order'][0]['column'] = 0;
        $criteria['order'][0]['dir'] = 'desc';*/
        $criteria['criteria']['isActive'] = 1;
        $paramForMasterData = [
            'draw' => 1,
            'start' => 0,
            'length' => 10
        ];
        $Registers = null;
        if (!empty($userID) && !is_null($userID)) {
            $Registers = Http::asForm()->post(config('app.apiPrefix') . config('app.registerList'), 
                array_merge(
                    $criteria, 
                    $paramForMasterData
                )
            )->json();
        }

        //$Organizes = $this->iniServvice->getAllOrganization();
        return view('registers.index',[
            //'Offices'=> $Offices ? $Offices["data"] : [],
            'Registers'=> $Registers ? $Registers["data"] : [],
        ]);
    }

    public function welcome()
    {
        return view('welcome');
    }
}
