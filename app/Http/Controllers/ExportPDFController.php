<?php
namespace App\Http\Controllers;

use App\Services\RegisterService;
use App\Services\ReportingService;
use App\Services\ReportService;
use App\Services\CriteriaService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ExportPDFController extends Controller {

    private $length = 1000;
    private $extension = "pdf";

    function __construct(
        RegisterService $rgService,
        ReportingService $rService,
        ReportService $rrService,
        CriteriaService $cService
        )
    {
        $this->registerService = $rgService;
        $this->reportingService = $rService;
        $this->reportService = $rrService;
        $this->criteriaService = $cService;
    }

    public function exportRegister(Request $request)
    {
        $RegisterList = [];

        $input = $request->all();
        
        $RegisterNumber = $input["RegisterNumber"];
        $StartDate = $input["StartDate"];
        $EndDate = $input["EndDate"];
        $Service = $input["Service"];
        $OrganizationID = $input["OrganizationID"];
       
        $datas = [ 'criteria'=> []];
        if ($RegisterNumber) {
            $datas['criteria']['keyword'] =  $RegisterNumber;
        }

        if ($StartDate) {
            $datas['criteria']['StartDate'] =  $StartDate;
        }
        
        if ($EndDate) {
            $datas['criteria']['EndDate'] =  $EndDate;
        }

        $datas['criteria']['Service'] = 0;
        if ($Service) {
            $datas['criteria']['Service'] =  $Service;
        }

        if ($OrganizationID) {
            $datas['criteria']['OrganizationSupportID'] =  $OrganizationID;
        }
        $datas['criteria']['SessionUserID'] = session('UserID');

        $searchData = array_merge(
            $datas, 
            $this->registerService->getRequestDefaultParams($this->length)
        );

        $RegisterList = [];
        $data = $this->registerService->getRegisterAll($searchData);
        if ($data) {
            $RegisterList = $data['data'];
        }
       
        $filename = $this->registerService->getFormatFile('ข้อมูลขึ้นทะเบียนว่างงาน', $this->extension);
        $html = view('exports.register',  ['RegisterList' => $RegisterList])->render();
        $mpdf = new \Mpdf\Mpdf(['fontDir' => array_merge($this->registerService->getFontsDirPDF(), [storage_path('fonts/'),]),'fontdata' => $this->registerService->getFontsPDF(), 'default_font' => 'sarabun_new', 'tempDir'=>storage_path('tempdir')]);
        $mpdf->WriteHTML($html);
        return $mpdf->Output($filename, 'D');
    }

    public function exportReporting(Request $request)
    {
        $input = $request->all();

        $datas = [ 'criteria'=> []];

        if ($request->has('RegisterNumber')) {
            $datas['criteria']['RegisterNumber'] =  $input["RegisterNumber"];
        }

        $StartDate = "";
        if ($StartDate) {
            $datas['criteria']['StartDate'] =  $input["StartDate"];
        }
        
        $EndDate = "";
        if ($EndDate) {
            $datas['criteria']['EndDate'] =  $input["EndDate"];
        }

        $datas['criteria']['Service'] = 0;
        if ($request->has('Service')) {
            $datas['criteria']['Service'] =  $input["Service"];
        }

        if ($request->has('Organization')) {
            $datas['criteria']['OrganizationSupportID'] =  $input["Organization"];
        }
        $datas['criteria']['SessionUserID'] = session('UserID');

        /*$datas['columns'][0]['name'] =  'RegisterNumber';
        $datas['order'][0]['column'] =  0;
        $datas['order'][0]['dir'] =  'desc';*/
        $datas['criteria']['isActive'] =  1;

        $searchData = array_merge(
            $datas, 
            $this->reportingService->getRequestDefaultParams($this->length)
        );

        $ReportingList = [];
        $data = $this->reportingService->getReporting($searchData);
        if ($data) {
            $ReportingList = $data;
        }
        $filename = $this->reportingService->getFormatFile('reporting', $this->extension);
        $html = view('exports.reporting-table-pdf',  ['ReportingList' => $ReportingList])->render();
        $mpdf = new \Mpdf\Mpdf(['fontDir' => array_merge($this->reportingService->getFontsDirPDF(), [storage_path('fonts/'),]),'fontdata' => $this->reportingService->getFontsPDF(), 'default_font' => 'sarabun_new', 'tempDir'=>storage_path('tempdir')]);
        $mpdf->WriteHTML($html);
        return $mpdf->Output($filename, 'D');
    }

    public function exportSummaryInsuredServices(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }

        if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }

        $ReportList = $this->reportService->getOfficeServeReport($inputParameter);
        $filename = $this->reportService->getFormatFile('สรุปผลการให้บริการผู้ประกันตนกรณีว่างงาน', $this->extension);
        $html = view('exports.report-officer-summary-insured-services',  ['ReportList' => $ReportList])->render();
        $mpdf = new \Mpdf\Mpdf(['fontDir' => array_merge($this->reportingService->getFontsDirPDF(), [storage_path('fonts/'),]),'fontdata' => $this->reportingService->getFontsPDF(), 'default_font' => 'sarabun_new', 'tempDir'=>storage_path('tempdir')]);
        $mpdf->WriteHTML($html);
        return $mpdf->Output($filename, 'D');
    }

    public function exportSummaryInsuredNationWide(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "&EndDate=".$EndDate;


        $ReportList = $this->reportService->getOfficeNationWideReport($inputParameter);

        $Case = [];
        $Tracking = [];
        $Job = [];
        if ($ReportList) {
            $Case = $ReportList[0]['register'];
            $Tracking = $ReportList[0]['tracking'];
            $Job = $ReportList[0]['job'];
           // dd($ReportList[0]['register']);
        }
        $filename = $this->reportService->getFormatFile('แบบรายงานผู้ประกันตนกรณีว่างงาน (ทั่วประเทศ)', $this->extension);
        $html = view('exports.summary-insurer-nationwide',  [
            'Case' => $Case, 
            'Tracking' => $Tracking, 
            'Job' => $Job, 
            'ReportList' => $ReportList
        ])->render();
        $mpdf = new \Mpdf\Mpdf([ 'format' => 'A4-L', 'fontDir' => array_merge($this->reportingService->getFontsDirPDF(), [storage_path('fonts/'),]),'fontdata' => $this->reportingService->getFontsPDF(), 'default_font' => 'sarabun_new', 'tempDir'=>storage_path('tempdir')]);
        $mpdf->WriteHTML($html);
        return $mpdf->Output($filename, 'D');
    }

    public function exportSummaryInsuredProvince(Request $request)
    {
        $input = $request->all();
        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
       
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
       
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }
        //$OrganizationID = 14;
        if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }

        $criteria = $this->reportService->getRequestParams();
        $p['OrganizationID'] = $OrganizationID;
        $criteria['criteria'] = $p;
       
        $Offices = Http::asForm()->post($this->reportService->getApiDomain() . config('app.organization'), $criteria)->json();
        $Province = "";
        if (!empty($Offices)) {
            $Province = $Offices['data'] ? $Offices['data'][0]['OrganizationName'] : "";
        }
        $ReportList = $this->reportService->getProvinceReport($inputParameter);
        
        $Case = [];
        $Tracking = [];
        $Job = [];
        if (count($ReportList) > 0 ) {
            $Case = $ReportList[0]['register'];
            $Tracking = $ReportList[0]['tracking'];
            $Job = $ReportList[0]['job'];
        }

        $filename = $this->reportService->getFormatFile('แบบรายงานผู้ประกันตนกรณีว่างงาน (จังหวัด)', $this->extension);
        $html = view('exports.summary-insurer-province',  [
            'Province' => $Province,
            'Case' => $Case, 
            'Tracking' => $Tracking, 
            'Job' => $Job, 
            'ReportList' => $ReportList
        ])->render();
        $mpdf = new \Mpdf\Mpdf([ 'format' => 'A4-L', 'fontDir' => array_merge($this->reportingService->getFontsDirPDF(), [storage_path('fonts/'),]),'fontdata' => $this->reportingService->getFontsPDF(), 'default_font' => 'sarabun_new', 'tempDir'=>storage_path('tempdir')]);
        $mpdf->WriteHTML($html);
        return $mpdf->Output($filename, 'D');
    }

    public function exportSummaryInsuredPersonal(Request $request)
    {
        $input = $request->all();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }

        if ($OrganizationID) {
            $inputParameter .= "&OrganizationID=".$OrganizationID;
        }
        $ReportList = $this->reportService->getPersonalReport($inputParameter);
       
        $criteria = $this->reportService->getRequestParams();
        $p['OrganizationID'] = $OrganizationID;
        $criteria['criteria'] = $p;
       
        $Offices = Http::asForm()->post($this->reportService->getApiDomain() . config('app.organization'), $criteria)->json();
        $Province = "";
        if (!empty($Offices)) {
            $Province = $Offices['data'] ? $Offices['data'][0]['OrganizationName'] : "";
        }

        $filename = $this->reportService->getFormatFile('แบบรายงานผู้ประกันตนกรณีว่างงาน (บุคคล)', $this->extension);
        $html = view('exports.summary-insurer-personal',  [
            'Province' => $Province, 
            'ReportList' => $ReportList
        ])->render();
        $mpdf = new \Mpdf\Mpdf([ 'format' => 'A4-L', 'fontDir' => array_merge($this->reportingService->getFontsDirPDF(), [storage_path('fonts/'),]),'fontdata' => $this->reportingService->getFontsPDF(), 'default_font' => 'sarabun_new', 'tempDir'=>storage_path('tempdir')]);
        $mpdf->WriteHTML($html);
        return $mpdf->Output($filename, 'D');
    }

    public function exportSummaryInsuredSso(Request $request)
    {
        $input = $request->all();
        $inputParameter = [];
        $DisplayType = "0";
        if ($request->has('DisplayType')) {
            $DisplayType = $input["DisplayType"];
        }
        $inputParameter['WorkStatus'] = $DisplayType;

        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
            $arr = explode("-",$input["StartDate"]);

            if (count($arr) == 2) {
                $StartYear = $arr[1];
                $StartMonth = (int)$arr[0];
            }

            $inputParameter['month'] = $StartMonth;
            $inputParameter['year'] = $StartYear;
        }

        $searchData = array_merge(
            $inputParameter, 
            $this->reportingService->getRequestDefaultParams($this->length)
        );

        $ReportList = $this->reportService->getOfficeRegisterReport($searchData);
        

        if ($ReportList[0]) {
            //$ReportList[0]['WorkStart'] =date("d/m/Y", strtotime($ReportList[0]['WorkStart']));
            $ReportList[0]['empStartDate'] =date("d/m/Y", strtotime($ReportList[0]['empStartDate']));
            $ReportList[0]['WorkStatus'] =$ReportList[0]['employStatus'];
        }
        //dd($ReportList[0]);
        $filename = $this->reportService->getFormatFile('ตรวจสอบการขึ้นทะเบียนเป็นผู้ประกันตนกับสำนักงานประกันสังคม', $this->extension);
        $html = view('exports.summary-insurer-sso',  [
            
            'ReportList' => $ReportList
        ])->render();
        $mpdf = new \Mpdf\Mpdf([ 'fontDir' => array_merge($this->reportingService->getFontsDirPDF(), [storage_path('fonts/'),]),'fontdata' => $this->reportingService->getFontsPDF(), 'default_font' => 'sarabun_new', 'tempDir'=>storage_path('tempdir')]);
        $mpdf->WriteHTML($html);
        return $mpdf->Output($filename, 'D');
    }

    public function summaryBenefitServices(Request $request)
    {
        $input = $request->all();

        $userID = null;
        if ($request->has('UserID')) {
            $userID =$input["UserID"];
        }
        
        if ($userID) {
            $this->sessionService->stampSession(['UserID' => $request->get('UserID'), 'UserTypeID' => $request->get('UserTypeID')]);
        }

        $dt = now();

        $inputParameter = "";
        $StartDate = "";
        if ($request->has('StartDate')) {
            $StartDate = $input["StartDate"];
        }
        if (!$StartDate) {
            $StartDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "StartDate=".$StartDate;

        $EndDate = "";
        if ($request->has('EndDate')) {
            $EndDate = $input["EndDate"];
        }
        if (!$EndDate){
            $EndDate = $dt->format('Y-m-d');
        }
        $inputParameter .= "&EndDate=".$EndDate;

        $OrganizationID = "";
        if ($request->has('OrganizationID')) {
            $OrganizationID = $input["OrganizationID"];
        }

        if ($OrganizationID) {
            $inputParameter .= "&SSOOfficeID=".$OrganizationID;
        }

        //dd($inputParameter);
        $ReportList = $this->reportService->getSsoServeReport($inputParameter);
        $SsoList = $this->criteriaService->getSsoList( $this->reportingService->getRequestDefaultParams($this->length));

        $filename = $this->reportService->getFormatFile('สรุปผลการให้บริการผู้ประกันตนขอรับประโยชน์ทดแทนกรณีว่างงาน', $this->extension);
        $html = view('exports.summary-benefit-sso',  [
            'StartDate'=> $StartDate,
            'EndDate'=> $EndDate,
            'ReportList'=> $ReportList ? $ReportList : [],
            'SsoList'=> $SsoList
        ])->render();
        $mpdf = new \Mpdf\Mpdf([ 'fontDir' => array_merge($this->reportService->getFontsDirPDF(), [storage_path('fonts/'),]),'fontdata' => $this->reportService->getFontsPDF(), 'default_font' => 'sarabun_new', 'tempDir'=>storage_path('tempdir')]);
        $mpdf->WriteHTML($html);
        return $mpdf->Output($filename, 'D');
    }

    public function exportPDFSSO(Request $request)
    {
        $id = "";
        if ($request->has('id')) {
            $id =$request->get('id');
        }
    
        $Register = $this->registerService->getDetails( [
            'RegisterID'=> $id
        ]);

        //dd($Register);

        $pdf = new \setasign\Fpdi\Fpdi();
        $file = storage_path(). "/Form.2-01-7.pdf";
       
        $pdf->AddFont('THSarabunNew','','THSarabunNew.php');
        $pdf->AddFont('THSarabunNew','B','THSarabunNew_b.php');
        //asset("storage/Form.2-01-7.pdf")
        $pdf->AddPage();
        // set the source file
        $pdf->setSourceFile($file);
        $tplIdx = $pdf->importPage(1);
        $pdf->useTemplate($tplIdx);

        if (!empty($Register)) {
            $pdf->SetFont('THSarabunNew','',12);
            //$pdf->SetFontSize(10);
            $pdf->SetTextColor(46, 46, 48);
            //$pdf->SetXY(60, 46);
            $prefix =  $Register["PrefixID"];

            if ($prefix == 1) {
                $pdf->Rect(30.5, 41.5, 8, 5);
            } else if ($prefix == 2) {
                $pdf->Rect(38, 41.5, 8, 5);
            } else if ($prefix == 3) {
                $pdf->Rect(45.5, 41.5, 11.5, 5);
            }
            
            //ชื่อ-นามสกุล
            $pdf->Text(60, 44.5, iconv('UTF-8', 'cp874', $Register["FirstName"].' '.$Register["LastName"]));
            //$pdf->Write(0, 'This is just a simple text');
            //อายุ
            $pdf->Text(110, 44.5, $Register["Age"] ?? "");
            $PersonalIDArray = str_split($Register["PersonalID"]);
            //IDCard 1
            $pdf->Text(151, 45.5, $PersonalIDArray[0]);
            //IDCard 2
            $pdf->Text(156, 45.5, $PersonalIDArray[1]);
            //IDCard 3
            $pdf->Text(159, 45.5, $PersonalIDArray[2]);
            //IDCard 4
            $pdf->Text(162, 45.5, $PersonalIDArray[3]);
            //IDCard 5
            $pdf->Text(165, 45.5, $PersonalIDArray[4]);
            //IDCard 6
            $pdf->Text(170, 45.5, $PersonalIDArray[5]);
            //IDCard 7
            $pdf->Text(173.5, 45.5, $PersonalIDArray[6]);
            //IDCard 8
            $pdf->Text(176.5, 45.5, $PersonalIDArray[7]);
            //IDCard 9
            $pdf->Text(179.5, 45.5, $PersonalIDArray[8]);
            //IDCard 10
            $pdf->Text(182.5, 45.5, $PersonalIDArray[9]);

            //IDCard 11
            $pdf->Text(187.5, 45.5, $PersonalIDArray[10]);
            //IDCard 12
            $pdf->Text(190.5, 45.5, $PersonalIDArray[11]);
            //IDCard 13
            $pdf->Text(195, 45.5, $PersonalIDArray[12]);

            //$addressArray = explode('หมู่ที่',$Register["EmployeeAddress"]);
           
            //$Register["Address"] = "555/2 อาคารศูนย์เอนเนอร์ยี่คอมเพล็กซ์ อาคารบี ชั้นที่ 5 ถนนวิภาวดีรังสิต";
            $addressArray = $this->seperateDatas($Register["Address"]);
            //dump($addressArray);
            //บ้านเลขที่
            if (!empty($addressArray)) {
                $pdf->Text(70, 50, iconv('UTF-8', 'cp874', $addressArray["no"] ?? "-"));
                //หมู่ที่
                $pdf->Text(94, 50, iconv('UTF-8', 'cp874', $addressArray["moo"] ?? "-"));
                //หมู่บ้าน

                if (!empty($addressArray["building"])) {
                    $building = iconv('UTF-8', 'cp874', $addressArray["building"]);
                    /*if (strlen($building) > 28) {
                        $building = substr($building, 0, 28)."...";
                    }*/
                }
                
                $pdf->Text(125, 50,  $building ?? "-");
                //ซอย
                $pdf->Text(163.5, 50, iconv('UTF-8', 'cp874',  $addressArray["soi"] ?? "-"));
                //ถนน
                $pdf->Text(28, 55.5, iconv('UTF-8', 'cp874',  $addressArray["road"] ?? "-"));
            }
           
            //ตำบล
            $pdf->Text(76.5, 55.5, iconv('UTF-8', 'cp874', $Register["TambonName"] ?? ""));
            //อำเภอ
            $pdf->Text(120, 55.5, iconv('UTF-8', 'cp874', $Register["DistrictName"] ?? ""));
            //จังหวัด
            $pdf->Text(167.5, 55.5, iconv('UTF-8', 'cp874', $Register["ProvinceName"] ?? ""));
            //รหัสไปรษณีย์
            $pdf->Text(42, 61,  $Register["PostCode"] ?? "");
            //โทรศัพท์บ้าน
            $pdf->Text(81, 61,  "-");
            //มือถือ
            //$mobileNumber =  $Register["PhoneNumber"];
            $mobileNumber =  $Register["Telephone"];
            if (empty($mobileNumber)) {
                $mobileNumber = "-";
            } else {
                $mobileNumber = substr_replace($mobileNumber, "-", 3, 0);
            }
            $pdf->Text(118, 61,  $mobileNumber);
            //อีเมล
            $pdf->Text(167.5, 61,  $Register["Email"] ?? "");
            //สถานประกอบการสุดท้าย
            $pdf->Text(102, 95.5, iconv('UTF-8', 'cp874', $Register["EmployerName"] ?? ""));
            //สาขา
            $employerBranch = "-";
            if (!empty($Register["EmployerBranch"]) && $Register["EmployerBranch"] != null) {
                $employerBranch = $Register["EmployerBranch"];
            }
            $pdf->Text(165, 95.5, iconv('UTF-8', 'cp874', $employerBranch));
            //วัน เดือน ปีทอ่ีอกจากงาน
            
            $resignDate = "-";
            if (!empty($Register["ResignDate"])) {
                $resignDate = show_thai_long_date($Register["ResignDate"]);
            }
            $pdf->Text(60, 103, iconv('UTF-8', 'cp874', $resignDate));
            //เขตพืน้ที่/จังหวัด
            $province = $Register["OrganizationName"] ?? "";
            if ($province == "Internet") {
                $province = "-";
            }
            $pdf->Text(98, 135, iconv('UTF-8', 'cp874', $province ?? ""));
            //ขึน้ทะเบียน วันที่

            $registerDate = "";
            $registerMonth = "";
            $registerYear = "";
            if (!empty($Register["RegisterDate"])){
                $registerDate = show_thai_long_date($Register["RegisterDate"]);
                $registerDateArray = explode(' ',$registerDate);
                $registerDate = $registerDateArray[0];
                $registerMonth = $registerDateArray[1];
                $registerYear = $registerDateArray[2];
                
                $pdf->Text(39, 140.5, $registerDate ?? "");
                //ขึน้ทะเบียน เดือน
                $pdf->Text(63, 140.5, iconv('UTF-8', 'cp874', $registerMonth ?? ""));
                //ขึน้ทะเบียน เดือน
                $pdf->Text(108, 140.5, $registerYear ?? "");
            }

            $pdf->SetFont('THSarabunNew','B',14);
            //ยืน่คาขอในฐานะ
            $pdf->Text(47, 68.5,  "X");
            //สาเหตุการออกจากงาน
            //dd($Register["ResignID"]);
            if ($Register["ResignID"] == 1) {
                //ลาออก
                $pdf->Text(55.5, 110,  "X");
            } else if ($Register["ResignID"] == 2) {
                //ถูกเลิกจ้าง
                $pdf->Text(55.5, 117,  "X");
                //สาเหตุการถูกเลิกจ้าง
                $pdf->SetFont('THSarabunNew','',12);
                $pdf->Text(95, 116, iconv('UTF-8', 'cp874',  $Register["ResignCaseName"] ?? "-"));
            } else {
                $pdf->Text(55.5, 129,  "X");

                
            }
            $pdf->SetFont('THSarabunNew','B',18);
            
            $pdf->SetFont('ZapfDingbats','', 12);
            $pdf->Text(55.5, 74.5, "3");

            //$pdf->Text(57,47,"นำ้ทิพย์ อ่อนเวลาการ");  //2nd argument moves from top to bottom
            
            //$pdf->Text(50,40,"Mubeen Company");    // first argument moves from let to right 
            $pdf->SetFont('THSarabunNew','',12);
            $pdf->SetTextColor(46, 46, 48);
            $prefixName = "";
            if ($prefix == 1) {
                $prefixName = "นาย";
            } else if ($prefix == 2) {
                $prefixName = "นาง";
            } else if ($prefix == 3) {
                $prefixName = "นางสาว";
            }
            $pdf->Text(135, 171.5, iconv('UTF-8', 'cp874', $prefixName.$Register["FirstName"].' '.$Register["LastName"]));
            $pdf->Text(135, 177.5, iconv('UTF-8', 'cp874', $prefixName.$Register["FirstName"].' '.$Register["LastName"]));
            $pdf->Text(135, 182.5, $registerDate ?? "");
            $pdf->Text(149, 182.5, iconv('UTF-8', 'cp874', $registerMonth ?? ""));
            $pdf->Text(172, 182.5, iconv('UTF-8', 'cp874',  $registerYear ?? ""));
        }

        $pdf->AddPage();
        $tplIdx = $pdf->importPage(2);
        $pdf->useTemplate($tplIdx);
        
       
        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return response()->download($pdf->Output(), 'filename.pdf', $headers);
    }

    function seperateDatas($str) {
        $results = array();
        $conds = array();

        $allWord = array();
        $key = "";
        for ($i=0; $i <= 3; $i++) { 
            switch($i) {
                case 0 : //หาหมู่
                    $allWord = array("หมู่ที่", "ม.", "ม .");
                    $key = "moo";
                    break;
                case 1 : //หาอาคาร
                    $allWord = array("อาคาร", "ตึก", "หมู่บ้าน" , "มบ.", "มบ .");
                    $key = "building";
                    break;
                case 2 : //หาซอย
                    $allWord = array("ซอย", "ซ.", "ซ .");
                    $key = "soi";
                    break;
                case 3 : //หาถนน
                    $allWord = array("ถนน", "ถ.", "ถ .");
                    $key = "road";
                    break;
            }
           
            if (!empty($allWord)) {
                foreach ($allWord as $value) {
                    $index = stripos($str, $value);
                    if ($index !== false) {
                        $conds[$index] = array(
                            "start" => $index, 
                            "type" => $i,
                            "keyword" => $value,
                            "keyresult" => $key
                        );
                    }
                }
            }

            $allWord = array();
        }
        krsort($conds);
       
        $stop = 0;
        foreach ($conds as $key => $cond) {
            if ($stop === 0) {
                $stop = strlen($str);
            }

            $start = $cond["start"];
            $subStr = substr($str, $cond["start"], ($stop - $start));
            $subStr = $this->str_replace_first($cond["keyword"], "", $subStr);
            $subStr = trim($subStr);
            $results[$cond["keyresult"]] = $subStr;

            $stop = $key;
        }

        if ($stop > 0) {
            $results["no"] =  trim(substr($str, 0, $stop));
        } else if (empty($results)){
            $results["no"] = trim($str);
        }

        return $results;
    }

    function str_replace_first($from, $to, $content)
    {
        $from = '/'.preg_quote($from, '/').'/';

        return preg_replace($from, $to, $content, 1);
    }
}