<?php

namespace App\Http\Controllers;

use App\Services\NotificationService;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    function __construct(
        NotificationService $nService
    ) {
        $this->notiService = $nService;
    }
    //
    public function sendEmail(Request $request)
    {
        $response = $this->notiService->simulateSendEmail(); 
        dump($response);
        return $response;
    }
}
