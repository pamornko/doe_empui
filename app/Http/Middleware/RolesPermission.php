<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RolesPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //เช็คสิทธิ์การขึ้นทะเบียน
        //dd(session()->all());
        if (session('activeStatus') == null) {
            //error code 5 = ไม่่ใช่ผู้ประกันตน
            return redirect()->back()->with('error_code', 5);
        }
        
        return $next($request);
    }
}
