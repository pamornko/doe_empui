<?php

namespace App\Http\Middleware;
use App\Http\Middleware\Redirect;
use App\Services\SessionService;
use Closure;
use Illuminate\Http\Request;

class UserPermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function __construct(SessionService $sService) {
        $this->sessionService = $sService;
    }

    public function handle_old(Request $request, Closure $next)
    {
        view()->share('mShow', 'hide');
        if ($request->has('token')) {
            $Token = $request->token;
            //$UserID = $request->query('userCode', '14957486'); //ผู้หางาน //14960599 (พี่เอ๋)
            //$UserID = $request->query('userCode', '14958592'); //เจ้าหน้าที่กรมจัด
            //$UserID = $request->query('userCode', 's'); //สปส.
            
            if ($Token) {
                //invoke refresh token
                $newToken = $this->sessionService->refreshTokenDatacenter($Token);
                if(!empty($newToken)){
                    //Get Profile
                    $user_sso = $this->sessionService->getProfileDatacenter($newToken);//dd($user_sso);
                    //dd($user_sso);
                    if($user_sso->status==0){
                        $newLogin = $this->sessionService->stampSession(['UserID' => $request->get('UserID'), 'UserTypeID' => $request->get('UserTypeID')]);
                        //$newLogin = $this->sessionService->stampSession(['UserCode' => $user_sso->data->psnId]);
                        $UserTypeID = session('UserTypeID');
                      
                        if ($newLogin) {
                            if ($UserTypeID == 12) {
                                //เฉพาะผู้หางาน
                                view()->share('mShow', 'show');
                            }
                            return $next($request);
                        } else {
                            if ( empty(session('OrganizationID'))) {
                                if ($UserTypeID == 12) {
                                    view()->share('mShow', 'show');
                                }
                            }
                            return $next($request);
                        }
                    } else {
                        return $next($request);
                    }
                } else {
                    //return redirect(config('app.apiDatacenter').'login.do');
                    return redirect("/welcome");
                }
            } else {
                if (!session('UserID')) {
                    //dump("Redirect");
                    //return redirect(config('app.apiDatacenter').'login.do');
                    return redirect("/welcome");
                }
            }
        } else {
            if (session('UserID')) {
                return $next($request);
            } else {

                if ($request->has('UserID')) {
                    $this->sessionService->stampSession(['UserID' => $request->get('UserID'), 'UserTypeID' => $request->get('UserTypeID')]);
                    return $next($request);
                } else {
                    //return redirect(config('app.apiDatacenter').'login.do');
                    return redirect("/welcome");
                }
                
            }
        }
        //return Redirect::back()->with('error_code', 5);
        //return redirect()->back()->with('success', 'your message,here');   
        //return back()->with('error', 'Incorrect username or password.');
        
    }

    //Handle with SSO direct
    public function handle(Request $request, Closure $next)
    {
        
        view()->share('mShow', 'hide');
        if ($request->has('token')) {
            if(empty($request->get('audit')) || !$this->passwordDecrypt($request->get('audit'))=="captcha"){
                return redirect(config('app.webDatacenter'));
            } 
            $newLogin = $this->sessionService->loginWithSSO( $request );
            $UserTypeID = session('UserTypeID');
            
            if ($newLogin) {
                if ($UserTypeID == 12) {
                    //เฉพาะผู้หางาน
                    view()->share('mShow', 'show');
                }
                return $next($request);
            } else {
                if ( empty(session('OrganizationID'))) {
                    if ($UserTypeID == 12) {
                        view()->share('mShow', 'show');
                    }
                }
                return $next($request);
            }
        } else {
            if (session('UserID')) {
                return $next($request);
            } else {

                if ($request->has('UserID')) {
                    $this->sessionService->stampSession(['UserID' => $request->get('UserID'), 'UserTypeID' => $request->get('UserTypeID')]);
                    return $next($request);
                } else {
                    //return redirect(config('app.apiDatacenter').'login.do');
                    return redirect("/welcome");
                }
                
            }
        }
        
    }
    function passwordEncrypt($str) {
		$sKey = rand(0, 8888) + 555;
		$sNew = '';

		foreach (str_split($str) as $key => $value) {
			$sNew .= substr(((ord($value) + intval($sKey)) + 10000), 1);
		}

		$sNew = str_replace('3', 'E', $sNew);
		$sNew = str_replace('4', 'A', $sNew);
		$sNew = str_replace('0', 'O', $sNew);

		return $sNew . substr((intval($sKey) - 555) + 10000, 1);
	}

	function passwordDecrypt($str) {
		$sKey = intval(substr($str, strlen($str) - 5)) + 555;

		$sNew = '';
		$str = substr($str, 0, strlen($str) - 5);
		$str = str_replace('E', '3', $str);
		$str = str_replace('A', '4', $str);
		$str = str_replace('O', '0', $str);

		while ($str != "") {
			$sNew .= chr(intval(substr($str, 0, 4)) - $sKey);
			$str = substr($str, 4);
		}
		return $sNew;
	}
}
