<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use SebastianBergmann\Environment\Console;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    
    Artisan::call('view:clear');

    Artisan::call('route:clear');

    Artisan::call('config:clear');

    //php artisan clear-compiled
    echo "Cache cleared. \r\n";
});

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/test', function () {
    return view('tests.scroll');
});


//Route::get('/register', 'App\Http\Controllers\PageController@registerUnemployed');
/*Route::get('/{lang}', 'PageController@registerUnemployed');
Route::get('/register/{lang}', 'PageController@registerUnemployed');

Route::get('locales/{lang}', 'Locale@index');*/

/*Route::group([
    'prefix' => '{locale}',
    'where' => ['locale' => '[a-zA-Z]{2}'],
    'middleware' => 'setlocale',
], function() {
    Route::get('/', function () {
        return redirect(app()->getLocale());
    });

    Route::get('/{lang}', 'PageController@registerUnemployed');
});*/

Route::group(['middleware' => ['web', 'permission']], function () {
    Route::get('/lang/{locale}', 'LocalizationController@index');
    Route::get('/', 'PageController@index');
    Route::get('/register/user-index', 'PageController@registerUnemployed')->name('/register/user-index');
    /**Route::get('/register/user-index', 'PageController@registerUnemployed')->middleware('roles'); */
    Route::get('/unemployed-register', 'RegisterController@registerForm')->name('unemployed-register')->middleware('roles');

    Route::get('/reporting', 'ReportingController@index')->name('reporting');
    Route::get('/reporting-form', 'ReportingController@reportingForm')->name('reporting-form');

    Route::get('/report-user-benefit', 'ReportDataController@userBenefitReport')->name('report-user-benefit');

    Route::post('/register', 'RegisterController@store')->name('register');

    /*** Officer ***/

    Route::get('/register-officer', 'RegisterController@OfficerIndex')->name('register-officer');
    Route::get('/register-officer-edit', 'RegisterController@OfficerEdit')->name('register-officer-edit');
    Route::get('/register-officer-edit-page', 'RegisterController@OfficerEditPage')->name('register-officer-edit-page');
    Route::get('/register-officer-view', 'RegisterController@OfficerEdit')->name('register-officer-view');
    Route::get('/register-details-view', 'RegisterController@registerDetails')->name('register-details-view');

    Route::post('/edit_register', 'RegisterController@Edit');
    Route::post('/cancel_register', 'RegisterController@Cancel');

    Route::get('/report-officer-benefit', 'ReportDataController@officerBenefitReport')->name('report-officer-benefit');
    Route::get('/report-officer-summary-insured-services', 'ReportDataController@officerSummaryInsuredServicesReport')->name('report-officer-summary-insured-services');
    Route::get('/report-officer-summary-benefit-services', 'ReportDataController@officerSummaryBenefitServicesReport')->name('report-officer-summary-benefit-services');

    Route::get('/reporting-officer', 'ReportingController@reportingOfficerIndex')->name('reporting-officer');
    Route::get('/reporting-officer-edit', 'ReportingController@reportingOfficerEdit')->name('reporting-officer-edit');
    Route::get('/reporting-officer-view', 'ReportingController@reportingOfficerEdit')->name('reporting-officer-view');

    Route::post('/edit_reporting', 'ReportingController@OfficerEdit');

    Route::get('/report_officer_summary_insured_nation_wide', 'ReportDataController@officerSummaryInsuredNationWideReport');
    Route::get('/report_officer_summary_insured_province', 'ReportDataController@officerSummaryInsuredProvinceReport');
    Route::get('/report_officer_summary_insured_personal', 'ReportDataController@officerSummaryInsuredPersonalReport');
    Route::get('/report_officer_summary_insured_sso', 'ReportDataController@officerSummaryInsuredSsoReport');
    Route::get('/notification_email_logs', 'ReportDataController@notificationEmailLogs');

    Route::get('/export_excel_reporting', 'ReportingController@exportExcel')->name('export_excel_reporting');
    Route::get('/print_register', 'RegisterController@printRegister')->name('print_register');
    Route::get('/export_pdf_reporting', 'ExportPDFController@exportReporting')->name('export_pdf_reporting');
    Route::get('/print_reporing', 'PrintController@exportReporting')->name('print_reporing');
    Route::get('/export_pdf_test', 'RegisterController@exportPDFTest')->name('export_pdf_test');

    Route::get('/export_pdf_register', 'ExportPDFController@exportRegister')->name('export_pdf_register');
    Route::get('/export_excel_register', 'RegisterController@exportExcel')->name('export_excel_register');

    Route::get('/export_pdf_summary_insured_services', 'ExportPDFController@exportSummaryInsuredServices')->name('export_pdf_summary_insured_services');
    Route::get('/print_summary_insured_services', 'PrintController@exportSummaryInsuredServices')->name('print_summary_insured_services');

    Route::get('/export_pdf_summary_insured_nation_wide', 'ExportPDFController@exportSummaryInsuredNationWide')->name('export_pdf_summary_insured_nation_wide');
    Route::get('/print_summary_insured_nation_wide', 'PrintController@exportSummaryInsuredNationWide')->name('print_summary_insured_nation_wide');

    Route::get('/export_pdf_summary_insured_province', 'ExportPDFController@exportSummaryInsuredProvince')->name('export_pdf_summary_insured_province');
    Route::get('/print_summary_insured_province', 'PrintController@exportSummaryInsuredProvince')->name('print_summary_insured_province');

    Route::get('/export_pdf_summary_insured_personal', 'ExportPDFController@exportSummaryInsuredPersonal')->name('export_pdf_summary_insured_personal');
    Route::get('/print_summary_insured_personal', 'PrintController@exportSummaryInsuredPersonal')->name('print_summary_insured_personal');

    Route::get('/export_pdf_summary_insured_sso', 'ExportPDFController@exportSummaryInsuredSso')->name('export_pdf_summary_insured_sso');
    Route::get('/print_summary_insured_sso', 'PrintController@exportSummaryInsuredSso')->name('print_summary_insured_sso');

    Route::get('/export_pdf_summary_benefit_sso', 'ExportPDFController@summaryBenefitServices')->name('export_pdf_summary_benefit_sso');
    Route::get('/print_summary_benefit_sso', 'PrintController@summaryBenefitServices')->name('print_summary_benefit_sso');
    Route::get('/export_excel_summary_benefit_sso', 'ReportDataController@exportsummaryBenefitServicesExcel')->name('export_excel_summary_benefit_sso');

    Route::get('/export_excel_summary_insured_service', 'ReportDataController@exportSummaryInsuredServiceExcel')->name('export_excel_summary_insured_service');
    Route::get('/export_excel_summary_insured_nation_wide', 'ReportDataController@exportSummaryInsuredNationWideExcel')->name('export_excel_summary_insured_nation_wide');
    Route::get('/export_excel_summary_insured_province', 'ReportDataController@exportSummaryInsuredProvinceExcel')->name('export_excel_summary_insured_province');
    Route::get('/export_excel_summary_insured_personal', 'ReportDataController@exportSummaryInsuredPersonalExcel')->name('export_excel_summary_insured_personal');
    Route::get('/export_excel_summary_insured_sso', 'ReportDataController@exportSummaryInsuredSsoReport')->name('export_excel_summary_insured_sso');
    Route::get('/export_excel_notification_email_logs', 'ReportDataController@exportNotificationEmailLogs')->name('export_excel_notification_email_logs');

    Route::get('/jobsFields', 'FiltersController@jobsFieldList')->name('jobsFields');
    Route::get('/job_private_list_ai', 'JobAnnoucedController@jobPrivateAIList')->name('job_private_list_ai');
    Route::get('/job_details', 'JobAnnoucedController@jobDetails')->name('job_details');
    Route::post('/job_fav', 'JobAnnoucedController@jobFav')->name('job_fav');
    Route::post('/job_apply', 'JobAnnoucedController@jobApply')->name('job_apply');
    Route::post('/cnt_job_apply', 'JobAnnoucedController@chkJobApply')->name('cnt_job_apply');
    Route::post('/dbd_employer', 'DBDController@getDBDEmployer')->name('dbd_employer');
    Route::get('/get_freelances', 'JobAnnoucedController@getFreelances')->name('get_freelances');
    Route::get('/get_freelance', 'JobAnnoucedController@getFreelance')->name('get_freelance');
    Route::get('/freelances', 'JobAnnoucedController@freelanceList')->name('freelances');
    Route::get('/find_province_id', 'FiltersController@findProvinceID')->name('find_province_id');
    Route::get('/find_district_id', 'FiltersController@findDistrictID')->name('find_district_id');
    Route::get('/find_tumbon_id', 'FiltersController@findTumbonID')->name('find_tumbon_id');
    Route::get('/download_pdf_sso', 'ExportPDFController@exportPDFSSO')->name('download_pdf_sso');
    Route::get('/download_bookbank', 'DownloadController@downloadBookbankFile')->name('download_bookbank');
});


Route::post('/register-search', 'RegisterController@Search')->name('register-search');
Route::post('/reporting_update', 'ReportingController@store')->name('reporting_update');
Route::post('/reporting-search', 'ReportingController@Search')->name('reporting-search');

/*** Call Report ***/
Route::post('/benefit_report_search', 'ReportDataController@BenefitSearch')->name('benefit-report-search');

Route::post('/report_officer_summary_insured_services_search', 'ReportDataController@officerSummaryInsuredServicesReportSearch');
Route::post('/report_sso_summary_insured_services_search', 'ReportDataController@ssoSummaryInsuredServicesReportSearch');
Route::post('/report_officer_summary_insured_sso_search', 'ReportDataController@officerSummaryInsuredSsoReportSearch');
Route::post('/notification_email_logs_search', 'ReportDataController@notificationEmailLogsSearch');
Route::post('/report_summary_personal_search', 'ReportDataController@officerSummaryInsuredPersonalSearchReport');
Route::post('/report_summary_province_search', 'ReportDataController@officerSummaryInsuredProvinceSearchReport');
Route::post('/report_summary_nationwide_search', 'ReportDataController@officerSummaryInsuredNationWideSearchReport');

/*** Call Function ***/
Route::get('/findDistrict', 'FiltersController@findDistricts')->name('findDistrict');
Route::get('/findTambon', 'FiltersController@findTambons')->name('findTambon');
Route::get('/reloadApplyJobs', 'JobAnnoucedController@reloadApplyJobs')->name('reloadApplyJobs');
Route::get('/searchJobs', 'JobAnnoucedController@Search')->name('searchJobs');

Route::post('/set_location', 'LocationController@store');
Route::post('/removeApplyJob', 'JobAnnoucedController@removeApplyJobs');

Route::get('/logout', 'AuthenticationController@logout')->name('logout');
Route::get('/notification', 'NotificationController@sendEmail')->name('notification');

Route::get('/profile', 'MemberController@goToProfile')->name('goToProfile');
Route::get('/get_resign_case', 'FiltersController@findResignCase')->name('get_resign_case');

Route::get('/get_register_dash', 'APIController@registerDashboard')->name('get_register_dash');
Route::get('/get_register_year_dash', 'APIController@registerYearDashboard')->name('get_register_year_dash');

Route::get('/get_reporting_dash', 'APIController@reportingDashboard')->name('get_reporting_dash');
Route::get('/get_reporting_year_dash', 'APIController@reportingYearDashboard')->name('get_reporting_year_dash');

Route::get('/get_job_year_dash', 'APIController@jobYearDashboard')->name('get_job_year_dash');

/*** Call View ***/
Route::get('/freelance', function(Request $request){
    //return view($path_to_blade);'CntFl'=> 1
    return view('components.freelance', ['CntFl' => $request->id]);
});


Route::get('/testpdf', function(Request $request){
    //return view($path_to_blade);
    return view('exports.test1');
});
