<!DOCTYPE HTML>
<html>
<head>
    <title>
        ระบบขึ้นทะเบียนและรายงานตัวผู้ประกันตนกรณีว่างงาน
    </title>
</head>
<body>
</body>
</html>

<script type="text/javascript" src="{{ URL::asset('js/libs/jquery/dist/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var endpoint = "<?=config('app.webDatacenter')?>";
        Swal.fire({
            html:'<a href="'+endpoint+'login.do"><img src="/images/welcome/up_banner.jpg" width="100%"></a> ' ,
            width: '80%',
            confirmButtonText : 'กรณีสมัคร Digital ID แล้ว คลิ๊กที่นี่ เพื่อขึ้นทะเบียนกรณีว่างงาน / รายงานตัว',
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                window.location.href = endpoint+'login.do?cmd=goLoginOption&data_index=1' ;
            }
        });
    });
</script>