<div class="block-list-job"></div>
                            
<div class="fillter-list-action">
    <div class="block-fillter-list-action">
        <div class="row col-md-12 ml-2 pl-2 pr-4s" style="display: none;">
            <button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info w-100">
                <span class="btn-label"><i class="mdi mdi-account-star-variant"></i></span> กดที่นี่เพื่อเพิ่มทักษะ
            </button>
        </div>
        <div class="body-fillter-list">
            <div class="item-body-fillter-list">
                <!--<h3>เรียงลำดับจาก</h3>

                <div class="item-fillter-select icon mb-4">   
                    <select class="fm-control custom-fillter-select" nane="orderBy" id="orderBy">
                        <option value="latest" selected>วันที่ประกาศล่าสุด</option>
                      
                        <option value="rate">เงินเดือน</option>
                    </select>
                </div>-->

                <h3>กรองข้อมูลเพิ่มเติม</h3>
                <div class="item-fm-icon prompt-search mt-2" style="width:100%">
                    <input type="text" class="prompt position" name="position" id="position" placeholder="ตำแหน่งงาน" style="padding-left: 30px"> 
                </div>

                <div class="item-fillter-select icon mt-2">
                    <select class="fm-control custom-fillter-select" name="salary" id="salary">
                        <option selected="selected" value="">จำนวนเงินขั้นต่ำที่คาดหวัง</option>
                        <option value="0">ไม่กำหนด</option>
                        <option value="8000">8,000</option>
                        <option value="9000">9,000</option>
                        <option value="10000">10,000</option>
                        <option value="20000">20,000</option>
                        <option value="30000">30,000</option>
                        <option value="40000">40,000</option>
                        <option value="50000">50,000</option>
                        <option value="60000">60,000</option>
                        <option value="70000">70,000</option>
                        <option value="80000">80,000</option>
                        <option value="90000">90,000</option>
                        <option value="100000">100,000</option>
                    </select>
                </div>

                <div class="item-fillter-select icon mt-2">
                    <select class="fm-control custom-fillter-select" name="emp-type" id="emp-type">
                        <option value="" selected>สัญญาการจ้าง</option>
                        <option value="">ทั้งหมด</option>
                        <option value="R">งานประจำ</option>
                        <option value="T">งานชั่วคราว</option>
                        <option value="P">งานพาร์ทไทม์</option>
                    </select>
                </div>

                <div class="item-fillter-select icon mt-2">
                    <select class="fm-control custom-fillter-select" id="jobs-field" name="jobs-field" style="width: 100%;height: 36px;" placeholder="Select State"></select>
                </div>
                
                <div class="item-fillter-select icon mt-2">
                    <select class="fm-control custom-fillter-select" name="degree" id="degree">
                        <option value="" selected>วุฒิการศึกษา</option>
                        @foreach ($Degrees as $Degree)
                        <option value=" {{ $Degree['DegreeID'] }}">{{ $Degree['DegreeName'] }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="item-fillter-select icon mt-2">
                    <select class="fm-control custom-fillter-select" name="province-filter" id="province-filter">
                        <option value="" selected>ทุกจังหวัด</option>
                        @foreach ($Provinces as $Province)
                        <option value=" {{ $Province['ProvinceID'] }}">{{ $Province['ProvinceName'] }}</option>
                        @endforeach
                    </select>
                </div>

                <!--<div class="item-fillter-select icon mt-2">
                    <select class="fm-control custom-fillter-select" name="amphoe-filter" id="amphoe-filter">
                        <option value="" selected disabled>เลือกอำเภอ/เขต</option>
                    </select>
                </div> -->

                <button type="button" class="fillter-button w-100">ค้นหา</button>
                <button type="button" class="reset-fillter-button w-100">ล้างค่าใหม่</button>
                
            </div>
        </div>
    </div>
</div>


<div id="job-detail-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="tooltipmodel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" style="max-width: 1300px">
        <div class="modal-content">
            <div class="modal-header d-flex align-items-center">
                 รายละเอียดงาน 
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
            </div>
            <div class="modal-body"> 
                
                
                <!--<span class="fa fa-spinner fa-spin fa-3x w-100"></span>-->
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>-->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>