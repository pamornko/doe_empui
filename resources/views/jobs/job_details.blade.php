@extends('layout.view-master')

@push('styles')
    <style>
        .preview img {
            max-width: 100%;
            min-height: 85px;
            max-height: 145px;
        }

        .fav-button {
            background: #6c757d;
            color: #fff;
            display: block;
            padding: 7px 12px;
            border-radius: 4px;
            border: 1px solid transparent;
            margin-top: 10px;
        }

        .apply-job-button {
            background: #2962FF;
            color: #fff;
            display: block;
            padding: 7px 12px;
            border-radius: 4px;
            border: 1px solid transparent;
            margin-top: 10px;
        }

        .applying-job-button {
            background: transparent;
            color: #36bea6;
            display: block;
            padding: 7px 12px;
            border-radius: 4px;
            border: 1px solid #36bea6;
            margin-top: 10px;
        }

        .description-body-detail {
            padding-top: 20px;
        }

        .title1  {
            font-size: 16px;
            color: #327a05;
            margin: 0 0 5px 0;
            font-weight: bold;
        }

        .txt-detail {
            font-size: 16px;
            line-height: 1.2;
            color: #003d6b;
        }

        .txt-detail1 {
            font-size: 14px;
            color: #1f1f1f;
            margin: 10px 10px 5px 20px;
        } 

        .fav-icon {
            /*font-size: 1.5em; */
            color: Tomato;
        }

        .job-content-loader {
            position: relative;
            background-color: #9e9a9a;
            width: 100%;
            height: 7px;
            overflow: hidden;
            border-radius: 1000px;
        }

        @media  (min-width: 290px) and (max-width: 850px) {
            .preview img {
                max-width: 100%;
                min-height: 85px;
                max-height: 100px;
            }
        }
    </style>
@endpush


@section('content')

<div class="row">
    <div class="col-12">
                <div class="row col-12">
                    <div class="col-md-8 col-sm-12">
                        <div class="card" style="border-radius: 8px;background-color: #eef5f9;">
                            <div class="card-body">
                                <div class="row col-md-12">
                                    <div class="col-md-2 col-sm-3 preview">
                                        <img src="{{ asset('images/jobs/logo_thai_jobs.png') }}">
                                    </div>
                                    <div class="col-md-9 col-sm-9">
                                        <div class="d-flex flex-column">
                                            <div class=""><h3 style="color: #348bee;">{{ $JobDetails['JobAnnounce']['JobPosition'] ?? ''}}</h3></div>
                                            <div class="">{{ $JobDetails['JobAnnounce']['JobFieldName'] ?? '' }}</div>
                                            <div class="pt-1"><h5 style="color: black;"> {{ $JobDetails['Employer']['OrganizationName'] ?? '' }}</h5></div>
                                            <div class="pt-1">วันที่ประกาศ : {{ $JobDetails['JobAnnounce']['AnnounceDate'] ?? '' }}</div>
                                            <div class="row">
                                               
                                                <!--<button id="fav-area" type="button" class="fav-button @if(!empty($JobDetails['JobAnnounce']['FavouriteDate'])) fav-icon @endif" @if(!empty($JobDetails['JobAnnounce']['FavouriteDate'])) disabled @endif data-id="{{ $JobDetails['JobAnnounce']['JobAnnounceID'] }}"><span class="btn-label"><i class="fa fa-heart"></i></span></button>-->
                                                
                                                <div class="d-flex flex-row bd-highlight" id="btn-area-{{ $JobDetails['JobAnnounce']['JobAnnounceID'] }}">
                                                    <button type="button" class="ml-2 apply-job-button @if(!empty($JobDetails['JobAnnounce']['ApplyDate'])) e-hide @endif" data-id="{{ $JobDetails['JobAnnounce']['JobAnnounceID'] }}">สมัครงานนี้</button>
                                                
                                                    <button type="button" class="ml-2 applying-job-button @if(empty($JobDetails['JobAnnounce']['ApplyDate'])) e-hide @endif"  data-id="{{ $JobDetails['JobAnnounce']['JobAnnounceID'] }}" disabled>สมัครงานนี้แล้ว</button>
                                                
                                                </div>
                                                <!--<button type="button" class="ml-2 fav-button">แจ้งปัญหาเกี่ยวกับงานนี้</button>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="description-body-detail">
                                    <div class="d-flex flex-column">
                                        <div><h4>รายละเอียดงาน</h4></div>
                                        <div><span class="txt-detail">{{ $JobDetails['JobAnnounce']['JobDescription'] ?? '' }} </span></div>

                                        <div class="pt-4"><h4>วิธีการคัดเลือก</h4></div>
                                        <div><span class="txt-detail">{{ $JobDetails['JobAnnounce_Condition']['TypeName'] ?? '' }}  </span></div>

                                        <div class="pt-4"><h6 style="color: #003d6b;">ติดต่อสอบถามเพิ่มเติมได้ที่</h6></div>
                                        <div>
                                            <span class=""> {{ $JobDetails['Employer']['OrganizationName'] ?? '' }} ({{ $JobDetails['Employer']['BranchName'] ?? '' }})
                                           </span>
                                        </div>
                                        <div>
                                            <span class="">
                                                {{ $JobDetails['Employer']['Address'] ?? '' }} {{ $JobDetails['Employer']['TambonName'] ?? '' }} {{ $JobDetails['Employer']['DistrictName'] ?? '' }} {{ $JobDetails['Employer']['ProvinceName'] ?? '' }} {{ $JobDetails['Employer']['PostCode'] ?? '' }}
                                            </span>
                                        </div>
                                        <div>
                                            <span class="mt-1">
                                                    หมายเลขติดต่อ : <a href="tel:{{ $JobDetails['Employer']['Telephone'] ?? '' }}">{{ $JobDetails['Employer']['Telephone'] ?? '' }}</a> อีเมล์ : <a href="mailto:{{ $JobDetails['Employer']['Email'] ?? '' }} ">{{ $JobDetails['Employer']['Email'] ?? '' }} </a>
                                            </span>
                                        </div>
                                        <div class="row">
                                           
                                            @if (empty($JobDetails['JobAnnounce']['ApplyDate']))
                                                    <!--<button type="button" class="ml-2 apply-job-button" data-id="{{ $JobDetails['JobAnnounce']['JobAnnounceID'] }}">สมัครงานนี้</button>-->
                                                @else
                                                    <!--<button type="button" class="ml-2 applying-job-button"  data-id="{{ $JobDetails['JobAnnounce']['JobAnnounceID'] }}" disabled>สมัครงานนี้แล้ว</button>-->
                                            @endif
                                            <!--<button type="button" class="ml-2 fav-button">แจ้งปัญหาเกี่ยวกับงานนี้</button>-->
                                        </div>
                                        @if(!empty($JobDetails['Employer']['CompanyInformation']))
                                        <div class="pt-4"><h4>เกี่ยวกับ {{ $JobDetails['Employer']['OrganizationName'] ?? '' }}</h4></div>
                                        <div><span class="txt-detail">{{ $JobDetails['Employer']['CompanyInformation'] ?? '' }} </span></div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="col-lg-12">
                            <div class="card" style="border-radius: 8px;background-color: #eef5f9;">
                                <div class="card-body">
                                    <div class="d-flex flex-column">
                                        <div><h4>ข้อมูลตำแหน่งงาน</h4></div>

                                        <div>
                                            <i class="ti-location-pin text-success fa-lg"></i> <span class="title1">สถานที่</span>
                                            <p class="txt-detail1 mt-0">
                                                ({{ $JobDetails['Employer']['BranchName'] ?? '' }})
                                            </p>
                                            <p class="txt-detail1 mt-0">
                                                {{ $JobDetails['Employer']['Address'] ?? '' }} {{ $JobDetails['Employer']['TambonName'] ?? '' }} {{ $JobDetails['Employer']['DistrictName'] ?? '' }} {{ $JobDetails['Employer']['ProvinceName'] ?? '' }} {{ $JobDetails['Employer']['PostCode'] ?? '' }}
                                            </p>
                                            <p class="txt-detail1 mt-0">
                                                หมายเลขติดต่อ : <a href="tel:{{ $JobDetails['Employer']['Telephone'] ?? '' }}">{{ $JobDetails['Employer']['Telephone'] ?? '' }}</a>
                                            </p>
                                            <p class="txt-detail1 mt-0">
                                                อีเมล์ : <a href="mailto:{{ $JobDetails['Employer']['Email'] ?? '' }} ">{{ $JobDetails['Employer']['Email'] ?? '' }} </a>
                                            </p>
                                        </div>
                                        <div class="mt-2">
                                            <i class="ti-time text-success fa-lg"></i> <span class="title1">ประเภทงาน</span>
                                            <p class="txt-detail1 mt-0">
                                                งาน{{ $JobDetails['JobAnnounce']['TypeName'] ?? '' }}
                                            </p>
                                        </div>
                                        <div class="mt-2">
                                            <i class="fas fa-dollar-sign text-success fa-lg mr-2"></i> <span class="title1"> อัตราเงินค่าจ้าง / วัน</span>
                                            <p class="txt-detail1 mt-0">
                                                @if (empty($JobDetails['JobAnnounce']['Wage_Min']) || $JobDetails['JobAnnounce']['Wage_Min'] == "0.00")
                                                    ตามโครงสร้างบริษัท
                                                @else
                                                    {{ $JobDetails['JobAnnounce']['Wage_Min'] ?? '' }} บาท
                                                @endif
                                            </p>
                                        </div>
                                        <div class="mt-2">
                                            <i class="icon-briefcase text-success fa-lg"></i> <span class="title1">อัตราที่รับสมัคร</span>
                                            <p class="txt-detail1 mt-0">
                                                - อัตรา
                                            </p>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="card" style="border-radius: 8px;background-color: #eef5f9;">
                                <div class="card-body">
                                    <div class="d-flex flex-column">
                                        <div><h4>คุณสมบัติเฉพาะตำแหน่งงาน</h4></div>

                                        <div>
                                            <span class="title1">เพศ</span>
                                            <p class="txt-detail1 mt-0">
                                                @if (!empty($JobDetails['JobAnnounce_Qualification']['Sex']))
                                                    @if ($JobDetails['JobAnnounce_Qualification']['Sex'] == 1)
                                                        ชาย
                                                    @elseif ($JobDetails['JobAnnounce_Qualification']['Sex'] == 3)
                                                        ไม่ระบุเพศ
                                                    @endif
                                                @endif
                                                
                                            </p>
                                            
                                        </div>

                                        <div class="mt-2">
                                            <span class="title1">อายุ (ปี)</span>
                                            <p class="txt-detail1 mt-0">
                                                {{ $JobDetails['JobAnnounce_Qualification']['Age_Min'] }} -  {{ $JobDetails['JobAnnounce_Qualification']['Age_Max'] }} ปี
                                            </p>
                                        </div>
                                        
                                        <div class="mt-2">
                                            <span class="title1">ประเภทผู้สมัครงาน</span>
                                            <p class="txt-detail1 mt-0">
                                                งานนี้เหมาะกับ{{ $JobDetails['JobAnnounce']['ApplicantTypeName'] ?? '' }}
                                            </p>
                                        </div>

                                        <div class="mt-2">
                                            <span class="title1"> ระดับการศึกษา</span>
                                            <p class="txt-detail1 mt-0">
                                                @if ($JobDetails['JobAnnounce_Qualification']['DegreeID_Min'] != $JobDetails['JobAnnounce_Qualification']['DegreeID_Max'])
                                                    @if (!empty($JobDetails['JobAnnounce_Qualification']['DegreeName']))
                                                        {{ $JobDetails['JobAnnounce_Qualification']['DegreeName'] ?? '' }} -
                                                    @endif
                                                    @if (!empty($JobDetails['JobAnnounce_Qualification']['DegreeName_Max']))
                                                        {{ $JobDetails['JobAnnounce_Qualification']['DegreeName_Max'] ?? '' }}
                                                    @endif
                                                @else 
                                                    {{ $JobDetails['JobAnnounce_Qualification']['DegreeName_Max'] ?? '' }}
                                                @endif

                                            </p>
                                        </div>

                                        <div class="mt-2">
                                            <span class="title1"> ประสบการณ์</span>
                                            <p class="txt-detail1 mt-0">
                                                {{ $JobDetails['JobAnnounce_Qualification']['WorkExperience'] ?? '' }} @if(!empty($JobDetails['JobAnnounce_Qualification']['WorkExperience'])) ปี@endif
                                            </p>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="card" style="border-radius: 8px;background-color: #eef5f9;">
                                <div class="card-body">
                                    <div class="d-flex flex-column">
                                       
                                        <span class="title1"> งานนี้เหมาะกับความสามารถของคุณ %</span>
                                        <div class="job-content-loader"></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
       
</div>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/pages/jobs.init.js') }}"></script>
@endpush