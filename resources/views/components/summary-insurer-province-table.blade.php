

<table id="summary_insured_services" class="table table-striped table-bordered head_table" style="table-layout: fixed; border-collapse: collapse;width: 1800px;">
    <thead>
        <tr style="background-color: #cbd9f7">
            <th rowspan="2" style="<?=$bStyle?>">{{ __('No.') }}</th>
            <th rowspan="2" style="width: 200px; <?=$bStyle?>">{{ __('Office/Zone') }}</th>
            <th colspan="{{ count($Case) ?? 0 }}" style="<?=$bStyle?>">{{ __('Insured registration') }} (คน)</th>
            <th colspan="{{ count($Tracking) ?? 0 }}" style="<?=$bStyle?>">{{ __('Reporting') }}ผู้ประกันตน</th>
            <th colspan="{{ count($Job) ?? 0 }}" style="<?=$bStyle?>">{{ __('Reporting') }}บรรจุงาน</th>
            <th rowspan="2" style="<?=$bStyle?>">หมายเหตุ</th>
        </tr>

        <tr style="background-color: #cbd9f7">
            @foreach ($Case as $key => $val)
            <th style="<?=$bStyle?>">{{ $key }}</th>
            @endforeach
            @foreach ($Tracking as $key => $val)
            <th style="<?=$bStyle?>">{{ $key }}</th>
            @endforeach
            @foreach ($Job as $key => $val)
            <th style="<?=$bStyle?>">{{ $key }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>

        @if (count($ReportList) > 0)  
            @foreach ($ReportList as $Report)
                <tr>
                    <td class="text-center" style="<?=$bStyle?>">{{$loop->iteration}}</td>
                    <td style="width: 200px; <?=$bStyle?>">{{$Report['OrganizationName']}}</td>
                    @foreach ($Report['register'] as $value)
                        <td class="text-center" style="<?=$bStyle?>"> {{ $value }} </td>
                    @endforeach
                    @foreach ($Report['tracking'] as $value)
                        <td class="text-center" style="<?=$bStyle?>"> {{ $value }} </td>
                    @endforeach
                    @foreach ($Report['job'] as $value)
                        <td class="text-center" style="<?=$bStyle?>"> {{ $value }} </td>
                    @endforeach
                    <td style="<?=$bStyle?>">{{ $Report['remark'] }}</td>
                </tr>
            @endforeach
        @else
            <tr>  
                <td colspan="6" class="text-center" style="<?=$bStyle?>">ไม่พบข้อมูล</td>
            </tr>
        @endif
       
    </tbody>
    
</table>
