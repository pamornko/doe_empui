
<table id="summary_insured_sso" class="table table-striped table-bordered head_table">
    <thead>
      
        <tr style="background-color: #cbd9f7">
            <th style="<?=$bStyle?>">{{ __('No.') }}</th>
            <th style="<?=$bStyle?>">{{ __('Name - Surname') }}</th>
            <th style="<?=$bStyle?>">{{ __('Registration number') }}</th>
            <th style="<?=$bStyle?>">{{ __('Registration Date') }}</th>
            <th style="<?=$bStyle?>">สถานะการมีงานทำ</th>
            <th style="<?=$bStyle?>">บรรจุงาน</th>
            <th style="<?=$bStyle?>">วันที่ได้งานทำ</th>
            <th style="<?=$bStyle?>">ข้อมูล<br>ผู้ประกันตนล่าสุดเมื่อ</th>
        </tr>

    </thead>
    <tbody>
        @foreach ($ReportList as $Report)
        <tr style="<?=$bStyle?>">
            <td style="<?=$bStyle?>" class="text-right">{{$loop->iteration}}</td>
            <td style="<?=$bStyle?>">{{ $Report['FirstName'] ?? '' }} {{ $Report['LastName'] ?? '' }}</td>
            <td style="<?=$bStyle?>">{{ $Report['RegisterNumber'] ?? '' }}</td>
            <td class="text-center" style="<?=$bStyle?>">{{ show_thai_date($Report['RegisterDate']) ?? '' }}</td>
            <td class="text-center" style="<?=$bStyle?>">
                @if (!empty($Report['WorkStatus']))
                    มีงานทำ
                @else 
                    ว่างงาน
                @endif
            </td>
            <td style="<?=$bStyle?>"></td>
            <td class="text-center" style="<?=$bStyle?>"> @if (!empty($Report['empStartDate'])) {{ $Report['empStartDate'] }} @endif</td>
            <td class="text-center" style="<?=$bStyle?>"> @if (!empty($Report['updated_at'])) {{ show_thai_date($Report['updated_at']) }} @endif</td>
        </tr>
        @endforeach
    </tbody>
    
</table>