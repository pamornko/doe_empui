<div class="col-md-6">
        
    <div class="form-group">
        <label>อาชีพอิสระ : </label>
        <select class="custom-select form-control" data-placeholder="Type to search cities" id="wintType{{ $CntFl }}" name="wintType[]" onchange="manageOther({{ $CntFl }}, this.value)">
            <option value='{ "id" : null }'>กรุณาเลือก</option>
            <!--<option value='{"name":"rajiv","age":"40"}'>Normal</option>
            <option value='{"name":"mithun","age":"22"}'>Difficult</option>--> 
            @foreach ($Freelances as $Freelance)
                <option value='{"id":"{{ $Freelance['FreelanceID'] ?? '' }}","other":"{{ $Freelance['Other'] ?? ''}}"}'>{{ $Freelance['FreelanceName'] ?? ''}}</option>
            @endforeach

        </select>
    </div>
    
</div>

<div class="col-md-6">
    <div class="form-group" style="display:none" id="divwintT{{ $CntFl }}">
        <label for="wint1">อื่นๆ ระบุ :</label>
        <input type="text" class="form-control required" name="wint[]"> </div>
</div>  
