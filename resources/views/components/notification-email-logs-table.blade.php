
        @foreach ($ReportList as $Report)
        <tr>
            <td class="text-center" >{{$loop->iteration}}</td>
            <td>{{ $Report['RegisterNumber'] ?? '' }}</td>
            <td class="text-center" >{{ $Report['TrackingTime'] ?? '' }}</td>
            <td>{{ show_thai_date($Report['ReportingDueDate'], 'd/m/Y') ?? '' }}</td>

            <td> {{ show_thai_date($Report['LogDate'], 'd/m/Y H:i') }} </td>
            <td> {{ $Report['To'] ?? '' }} </td>
            <td>{{ $Report['Message'] ?? '' }}</td>
            <td> 
                @if ($Report['Status']) 
                    สำเร็จ
                @else 
                    ไม่สำเร็จ
                @endif
            </td>
        </tr>
        @endforeach
    