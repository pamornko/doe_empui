
<table id="summary_insured_services" class="table table-striped table-bordered head_table" style="table-layout: fixed; border: 1px solid black;border-collapse: collapse;width: 1800px;">
    <thead>
        <tr style="background-color: #cbd9f7">
            <th rowspan="2" style="<?=$bStyle?>">{{ __('No.') }}</th>
            <th rowspan="2" style="<?=$bStyle?> width:250px;">{{ __('Office/Zone') }}</th>
            <th colspan="{{ count($Case) ?? 0 }}" style="<?=$bStyle?>">{{ __('Insured registration') }} (คน)</th>
            <th colspan="{{ count($Tracking) ?? 0 }}" style="<?=$bStyle?>">{{ __('Reporting') }}ผู้ประกันตน</th>
            <th colspan="{{ count($Job) ?? 0 }}" style="<?=$bStyle?>">{{ __('Reporting') }}บรรจุงาน</th>
            <th rowspan="2" style="<?=$bStyle?> width: 100px;">หมายเหตุ</th>
        </tr>

        <tr style="background-color: #cbd9f7">
            @foreach ($Case as $key => $val)
            <th style="<?=$bStyle?>">{{ $key }}</th>
            @endforeach
            @foreach ($Tracking as $key => $val)
            <th style="<?=$bStyle?>">{{ $key }}</th>
            @endforeach
            @foreach ($Job as $key => $val)
            <th style="<?=$bStyle?>">{{ $key }}</th>
            @endforeach
        </tr>

    </thead>
    <tbody>
        @php
            $colspan = 2 + count($Case)  + count($Tracking) + count($Job) + 1;
        @endphp
        @foreach ($ReportList as $Report)
        <tr>
            <td colspan="2" style="<?=$bStyle?>">{{$Report['RegionName']}}</td>
            
            
            @foreach ($Report['register'] as $value)
            <td class="text-center" style="<?=$bStyle?>"> {{ $value }} </td>
            @endforeach
            @foreach ($Report['tracking'] as $value)
            <td class="text-center" style="<?=$bStyle?>"> {{ $value }} </td>
            @endforeach
            @foreach ($Report['job'] as $value)
            <td class="text-center" style="<?=$bStyle?>"> {{ $value }} </td>
            @endforeach
            <td style="<?=$bStyle?> width: 100px;">{{ $Report['remark'] }}</td>
        </tr>
        @endforeach
        @foreach ($ReportList as $Report)
        <tr>
            <td colspan="{{ $colspan }}" style="<?=$bStyle?> background-color: #dce5fa">{{$Report['RegionName']}}</td>
        </tr>
            @foreach ($Report['Organization'] as $Org)
            <tr>
                <td style="<?=$bStyle?>">{{$loop->iteration}}</td>
                <td  style="<?=$bStyle?> width:250px;">{{$Org['OrganizationName']}}</td>
                @foreach ($Org['register'] as $value)
                <td class="text-center" style="<?=$bStyle?>"> {{ $value }} </td>
                @endforeach
                @foreach ($Org['tracking'] as $value)
                <td class="text-center" style="<?=$bStyle?>"> {{ $value }} </td>
                @endforeach
                @foreach ($Org['job'] as $value)
                <td class="text-center" style="<?=$bStyle?>"> {{ $value }} </td>
                @endforeach
                <td style="<?=$bStyle?> width: 100px;">{{ $Org['remark'] }}</td>
            </tr>
            @endforeach
        @endforeach
    </tbody>
    
</table>