<table id="summary_insured_services" class="table table-striped table-bordered head_table">
    <thead>
        <tr>
            <th rowspan="2">{{ __('No.') }}</th>
            <th rowspan="2">{{ __('Office/Zone') }}</th>
            <th colspan="3">{{ __('Insured registration') }}</th>
            <th colspan="3">{{ __('Reporting') }}</th>
            
        </tr>

        <tr>
            <th>{{ __('Total') }}</th>
            <th>{{ __('Male') }}</th>
            <th>{{ __('Female') }}</th>
            <th>{{ __('Total') }}</th>
            <th>{{ __('Male') }}</th>
            <th>{{ __('Female') }}</th>
        </tr>

    </thead>
    <tbody>
        @foreach ($ReportList as $Report)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{ $Report['SSOOfficeName'] }}</td>
            <td class="text-center">{{ $Report['register']['F'] + $Report['register']['M'] }}</td>
            <td class="text-center">{{ $Report['register']['F']  }}</td>
            <td class="text-center">{{ $Report['register']['M']  }}</td>
            <td class="text-center">{{ $Report['tracking']['F'] + $Report['tracking']['M'] }}</td>
            <td class="text-center">{{ $Report['tracking']['F']  }}</td>
            <td class="text-center">{{ $Report['tracking']['M']  }}</td>
            
        </tr>
        @endforeach
    </tbody>
    
</table>