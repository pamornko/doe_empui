
@foreach ($JobMatchings as $JobMatching)

<div  class="note-has-grid row listing-job" id="{{ $JobMatching['JobAnnounceID'] }}"  data-job-apply="" data-job-desc="<?=$JobMatching['JobDescription'];?>" onclick="presentJob(this)">
    <div class="col-md-12 single-note-item note-important">
        <div class="card card-body job-short-area">
            
            <span class="side-stick"></span>
            
            <h5 class="note-title job-position mb-0" data-noteHeading="Go for lunch">{{ $JobMatching['JobPosition'] }} </h5>
            <p class="note-date font-10 text-muted job-annouce-date" data-exprie="{{ $JobMatching['ExpireDate'] }}"> {{ $JobMatching['AnnounceDate'] }} </p>
            <div class="note-content">
                        <p data-noteContent=""> <h4 class="text-truncate txt-job-content job-employer"><small>{{  $JobMatching['EmployerName'] }}</small></h4> </p>
                </div>
                    <div class="d-flex align-items-center">
                        <h5 class="text-muted"><small>{{ $JobMatching['TypeName'] }}</small></h5>
                        <div class="ml-auto">
                        <div class="category-selector btn-group">
                            
                            <div class="pull-right">
                                <h5 class="font-11 text-info job-salary"><small>อ้ตราจ้าง {{ $JobMatching['Wage_Min'] }}</small></h5>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="bottom-section">
                <div class="bottom-aligner"></div>
                <div class="bottom-content job-area">
                    {{ $JobMatching['ProvinceName']  }}
                <!--<br> at the bottom.-->
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
                                                