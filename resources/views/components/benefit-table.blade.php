
    <input type="hidden" id="registerId">
    @foreach ($Reportings as $Reporting)
        <tr style="<?=$bStyle?>">
            <td style="<?=$bStyle?>">{{$loop->iteration}}</td>
            <td style="<?=$bStyle?>"> {{ \Carbon\Carbon::parse($Reporting['ReportingDueDate'])->format('d/m/Y') }}</td>
            <td style="<?=$bStyle?>"> <?=$Reporting['ReportingDate'] ?  date('d/m/Y',strtotime($Reporting['ReportingDate']))  : '' ?> </td>
            
            <td style="<?=$bStyle?>">
                @if ($Reporting['payNo'])
                        <!--<span class="label label-table label-success">จ่ายแล้ว</span>-->
                    <span>{{ $Reporting['financeStatusDesc'] ?? "" }}</span>
                @else   
                    @if ($Reporting['ReportingDate']) 
                        <span>อยู่ระหว่างพิจารณาโดยสำนักงานประกันสังคม</span>
                    @endif
                @endif
            </td>
        </tr>
    @endforeach


