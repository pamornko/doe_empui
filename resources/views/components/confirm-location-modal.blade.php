<!-- /.modal- area -->
<div id="first-modals" class="modal" tabindex="-1" role="dialog" aria-labelledby="tooltipmodel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header d-flex align-items-center">
                <h4 class="modal-title" id="tooltipmodel">{{ __('Choose a location') }}</h4>
                <!--<button type="button" class="close ml-auto" data-dismiss="modal" aria-hidden="true">×</button>-->
            </div>
            <div class="modal-body">
                <form id="fm-location">
                <h5 class="card-title text-center">{{ __('Choose at home') }}</h5>
                <div class="d-flex justify-content-center mt-3 mb-4">
                    <button class="btn btn-major waves-effect waves-light" type="submit" ><span class="btn-label"><i class="fas fa-home"></i> {{  __('Continue') }} </span></button>

                </div>
                <p class="text-center">{{ __('OR') }}</p>
                <hr>
                <h5 class="card-title text-center">{{ __('Choose at Department of Employment') }}</h5>
                
                
                    <div class="d-flex justify-content-center">
                        <div class="form-group">
                            <select class="custom-select form-control" id="selectOffice" data-placeholder="Type to search cities" name="OrganizationID">
                                <option value="">กรุณาเลือกสำนักงานที่มาใช้บริการ</option>
                                @foreach ($Offices as $Office)
                                    <option value="{{  $Office['OrganizationID'] }}">{{ $Office['OrganizationName'] }}</option>
                                @endforeach
                            </select>
                        </div>
        
                    </div>
                    <div class="d-flex justify-content-center mt-2 mb-3 hide-element" id="btnOffice">
                        <button class="btn btn-major waves-effect waves-light" type="submit"><span class="btn-label"><i class="far fa-building"></i> {{  __('Choose an office') }} </span></button>

                    </div>
                </form>
                
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>-->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>