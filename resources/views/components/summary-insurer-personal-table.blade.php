
@php
    //dump($ReportList);
@endphp
<table id="summary_personal_services" class="table table-striped table-bordered head_table">
    <thead>
        <tr style="background-color: #cbd9f7">
            <th rowspan="2"  style="<?=$bStyle?>">{{ __('No.') }}</th>
            <th rowspan="2"  style="width:200px;<?=$bStyle?>">ชื่อ-นามสกุล</th>
            <th colspan="4"  style="<?=$bStyle?>">ขึ้นทะเบียนผู้ประกันตน</th>
            <th colspan="6"  style="<?=$bStyle?>">การรายงานตัวผู้ประกันตน</th>
            <th rowspan="2"  style="<?=$bStyle?>">หมายเหตุ</th>
        </tr>

        <tr style="background-color: #cbd9f7">
            <th  style="<?=$bStyle?>">ประเภทการออก</th>
            <th  style="<?=$bStyle?>">ขึ้นทะเบียนหางาน</th>
            <th  style="<?=$bStyle?>">ประกอบอาชีพอิสระ</th>
            <th  style="<?=$bStyle?>">ไม่ประสงค์สมัครงาน</th>
            <th  style="<?=$bStyle?>">ครั้งที่</th>
            <th  style="<?=$bStyle?>">กำหนดรายงาน</th>
            <th  style="<?=$bStyle?>">วันที่รายงาน</th>
            <th  style="<?=$bStyle?>">ขึ้นทะเบียนหางาน</th>
            <th  style="<?=$bStyle?>">ประกอบอาชีพอิสระ</th>
            <th  style="<?=$bStyle?>">ไม่ประสงค์สมัครงาน</th>
        </tr>

    </thead>
    <tbody>
        @if (!empty($ReportList) && count($ReportList) > 0)
           
            @foreach ($ReportList as $Report)
                <tr>
                    <td class="text-right" style="<?=$bStyle?>">{{$index = $loop->iteration}}</td>
                    <td style="<?=$bStyle?>">{{$Report["FirstName"] ?? ""}} {{$Report["LastName"] ?? ""}}</td>
                    <td class="text-center" style="<?=$bStyle?>">{{$Report["register"]["ประเภทการออก"] ?? ""}}</td>
                    <td class="text-center" style="<?=$bStyle?>">@if ($Report["register"]["ขึ้นทะเบียนผู้ประกันตน"]) &#10004; @endif</td>
                    <td class="text-center" style="<?=$bStyle?>">@if ($Report["register"]["ประกอบอาชีพอิสระ"]) &#10004; @endif</td>
                    <td class="text-center" style="<?=$bStyle?>">@if ($Report["register"]["ไม่ประสงค์สมัครงาน"]) &#10004; @endif</td>
                    @php
                        $isFirst = true;
                    @endphp
                    @if (count($Report["tracking"]) > 0)
                        @foreach ($Report["tracking"] as $tracking)
                            @if ($isFirst)
                                @php
                                    $isFirst = false;
                                @endphp
                                <td class="text-center" style="<?=$bStyle?>">{{$tracking["ครั้งที่"] ?? ""}}</td>
                                <td class="text-center" style="<?=$bStyle?>">{{ show_thai_date($tracking["กำหนดรายงาน"]) ?? ""}}</td>
                                <td class="text-center" style="<?=$bStyle?>">{{ show_thai_date($tracking["วันที่รายงาน"]) ?? ""}}</td>
                                <td class="text-center" style="<?=$bStyle?>">@if ($tracking["ขึ้นทะเบียนหางาน"]) &#10004; @endif</td>
                                <td class="text-center" style="<?=$bStyle?>">@if ($tracking["ประกอบอาชีพอิสระ"]) &#10004; @endif</td>
                                <td class="text-center" style="<?=$bStyle?>">@if ($tracking["ไม่ประสงค์สมัครงาน"]) &#10004; @endif</td>
                                <td style="<?=$bStyle?>">{{ $Report["remark"] ?? "" }}</td>
                            @else 
                                <tr>
                                    <td style="<?=$bStyle?>"></td>
                                    <td style="<?=$bStyle?>"></td>
                                    <td style="<?=$bStyle?>"></td>
                                    <td style="<?=$bStyle?>"></td>
                                    <td style="<?=$bStyle?>"></td>
                                    <td style="<?=$bStyle?>"></td>
                                    <td class="text-center" style="<?=$bStyle?>">{{$tracking["ครั้งที่"] ?? ""}}</td>
                                    <td class="text-center" style="<?=$bStyle?>">{{  show_thai_date($tracking["กำหนดรายงาน"])?? ""}}</td>
                                    <td class="text-center" style="<?=$bStyle?>">{{ show_thai_date($tracking["วันที่รายงาน"]) ?? ""}}</td>
                                    <td class="text-center" style="<?=$bStyle?>">@if ($tracking["ขึ้นทะเบียนหางาน"]) &#10004; @endif</td>
                                    <td class="text-center" style="<?=$bStyle?>">@if ($tracking["ประกอบอาชีพอิสระ"]) &#10004; @endif</td>
                                    <td class="text-center" style="<?=$bStyle?>">@if ($tracking["ไม่ประสงค์สมัครงาน"]) &#10004; @endif</td>
                                    <td style="<?=$bStyle?>">{{ $Report["remark"] ?? "" }}</td>
                                </tr>
                            @endif
                            
                        @endforeach
                    @else
                        <td style="<?=$bStyle?>"></td>
                        <td style="<?=$bStyle?>"></td>
                        <td style="<?=$bStyle?>"></td>
                        <td style="<?=$bStyle?>"></td>
                        <td style="<?=$bStyle?>"></td>
                        <td style="<?=$bStyle?>"></td>
                        <td style="<?=$bStyle?>">{{ $Report["remark"] ?? "" }}</td>
                    @endif
                    
                </tr>
                
               
            @endforeach
        @else
            <tr>  
                <td colspan="13" class="text-center" style="<?=$bStyle?>">ไม่พบข้อมูล</td>
            </tr>
        @endif
    </tbody>
    
</table>