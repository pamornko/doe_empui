@if (count($ApplyJobs) > 0 ) 
<div class="card-body pl-3 pr-3" id="apply-area">
    <!--<h5 class="card-title">ตำแหน่งงานที่สมัคร</h5>-->
    <div class="table-responsive ml-0 mr-0">
        <h4 class="card-title">งานที่คุณสมัครไว้</h4>
        <table id="apply_job_table" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>วันที่สมัคร</th>
                    <th>ตำแหน่งงานที่สมัคร</th>
                    <th>บริษัท</th>
                    <th>พื้นที่</th>
                    
                    @if (empty($Action) || $Action != "view" && (isset($ApplyJob['id'])))
                    <th></th>
                    @endif
                    
                </tr>
            </thead>
            <tbody>
                @foreach ($ApplyJobs as $ApplyJob)
                
                    <tr>
                        <td> @if(!empty($ApplyJob)) @if(empty($ApplyJob['ApplyDateSQL'])) {{ show_thai_date($ApplyJob['ApplyDate'], 'd/m/Y') }} @else {{ show_thai_date($ApplyJob['ApplyDateSQL'], 'd/m/Y') }} @endif  @endif </td>
                        <td> {{ $ApplyJob['JobPosition'] ?? ''}} </td>
                        <td> {{ $ApplyJob['EmployerName'] ?? ''}} </td>
                        <td>{{ $ApplyJob['ProvinceName'] ?? ''}} </td>
                        
                        @if (empty($Action) || $Action != "view" && (isset($ApplyJob['id'])))
                        <td class="text-center">
                            <div class="action-btn">
                                <a @isset($ApplyJob['id']) onclick="remove(<?=$ApplyJob['id']?>)" @endisset class="delete-multiple text-danger" style="cursor: pointer;"><i class="fas fa-trash font-16 font-medium"></i></a>
                            </div>
                        </td>
                        @endif
                        
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endif

@push('scripts')
<script>
    $(document).ready(function(){
    
    });


</script>
@endpush