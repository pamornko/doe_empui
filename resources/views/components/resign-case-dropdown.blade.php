@php
    //dump($LayoffCases);
@endphp
    <select class="custom-select form-control" id="wLayoffCaseID-option" name="LayoffCaseID" required>
        <option value="">กรุณาเลือก</option>
        @if (!empty($LayoffCases)) 
            @foreach ($LayoffCases as $LayoffCase)
                <option value="{{  $LayoffCase['ResignCaseID'] }}">{{ $LayoffCase['ResignCaseName'] }}</option>
            @endforeach
        @endif
    </select>
