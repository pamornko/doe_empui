
    @if (count($ReportingList) > 0)
        @foreach ($ReportingList as $Reporting)
            <tr style="<?=$bStyle?>">
                <td>{{$loop->iteration}}</td>
                <td>{{ $Reporting['RegisterNumber'] }}</td>
                <td>{{ $Reporting['PersonalID'] }}</td>
                <td>{{ $Reporting['FirstName'] }} {{ $Reporting['LastName'] }}</td>
                <td>{{ $Reporting['TrackingTime'] }}</td>
                <td> {{ show_thai_date($Reporting['ReportingDueDate']) }} </td>
                <td> 
                    <?php 
                
                    if ($Reporting['ReportingDate']) 
                        {
                    ?>   
                        <a href="{{ URL::route('reporting-officer-view' , ['id'=>$Reporting['TrackingID']]) }}">
                    
                        {{ show_thai_date($Reporting['ReportingDate']) }}
                      
                        </a>
                    <?php
                        }
                    ?>    
                    
                </td>
               
                <td>
                    @if (!empty($Reporting['ReportingDate']))
                    <button class="btn btn-outline-success waves-effect waves-light" onclick="window.location='{{ URL::route('reporting-officer-edit' , ['id'=> $Reporting['TrackingID']]) }}'"><i class="fas fa-pencil-alt"></i> {{ __('Edit') }}</button>
                    <!--<button class="btn btn-outline-danger waves-effect waves-light"><i class="fas fa-eraser"></i> {{ __('Cancel') }}</button>-->
                    @endif
                </td>
            
            </tr>
        @endforeach
    @endif
    