
@if (empty($JobPrivates))
    <div class="row col-12 mt-5" style="text-align: center;">
        <div class="row col-5" style="text-align: center;">
        </div>
        <div class="row col-4">
            <h3>
                ไม่พบข้อมูล
            </h3>
        </div>
        <div class="row col-3" style="text-align: center;">
        </div>
    </div>
@else
    @foreach ($JobPrivates as $JobPrivate)
        <div class="item-list-job list-page" id="body-area-{{$JobPrivate['JobAnnounceID']}}" data-job-id="{{$JobPrivate['JobAnnounceID']}}">
            <div class="list-job-img">
            <img src="{{ asset('images/jobs/logo_thai_jobs.png') }}">
            </div>
            <div class="list-job-content">
                <div class="list-job-content-title">
                    <h3 class="card-title mt-0" style="color: #000"> {{ $JobPrivate['JobPosition'] ?? ''}}</h3>
                    <h5 class="text-sky">{{ $JobPrivate['EmployerName'] ?? ''}}</h5>
                    <p class="card-text">{{ $JobPrivate['JobDescription'] ?? ''}}</p>
                </div>
                <div class="list-job-content-address">
                    <div class="item-content-address">
                        <i class="ti-location-pin"></i> {{ $JobPrivate['ProvinceName'] ?? '' }}
                    </div>
                    <div class="item-content-address">
                        <i class="ti-time"></i> {{ $JobPrivate['TypeName'] ?? '' }}
                    </div>
                    <div class="item-content-address">
                        <i class="fas fa-dollar-sign"></i> ค่าจ้าง {{ $JobPrivate['Wage_Min'] ?? '' }}/{{ $JobPrivate['SalaryRequireUnitName'] ?? '' }}
                    </div>
                    
                </div>
                <!--<div class="list-job-content-loader">
                    <h3>งานนี้เหมาะกับความสามารถของคุณ 0%</h3>
                    <div class="job-content-loader"></div>
                </div>-->
            </div>
            <div class="list-job-action">
                <div class="d-flex flex-row bd-highlight" id="btn-area-{{$JobPrivate['JobAnnounceID']}}">
                    <!--<div class="p-2 bd-highlight"><button id="fav-area" type="button" class="fav-button @if(!empty($JobPrivate['FavouriteDate'])) fav-tomato @endif" @if(!empty($JobPrivate['FavouriteDate'])) disabled @endif data-id="{{$JobPrivate['JobAnnounceID']}}"><span class="btn-label"><i class="fa fa-heart"></i></span></button></div>-->
                    
                        <div class="p-2 bd-highlight"><button type="button" data-id="{{$JobPrivate['JobAnnounceID']}}" class="apply-job-button @if(!empty($JobPrivate['ApplyDate'])) e-hide @endif">สมัครงานนี้</button><button type="button" class="applying-job-button @if(empty($JobPrivate['ApplyDate'])) e-hide @endif"  disabled>สมัครงานนี้แล้ว</button></div>

                </div>
            
            </div>
        </div>
    @endforeach
@endif

<div class="row col-12">
    <div class="col-md-6 col-sm12">
        แสดง {{ $SeqStart + 1 }}  ถึง  {{ ($SeqStart + 5 < $TotalRecords) ? ($SeqStart + 5) : $TotalRecords }}  จาก {{ $TotalRecords }} รายการ
    </div>
    <div class="col-md-6 col-sm12 block-pagination" style="margin:0px;">
        @if($Total > 0)
            @php
                $page = 5;
                $mPage = '<span style="padding-left:5px;padding-right:5px;padding-top:1px;font-size: medium;">...</span>';
            @endphp
            <input type="hidden" value="{{ $PageNumber }}" id="c-page">
            <ul class="pagination-list">
                @if($PageNumber > 1)
                <li class="">
                    <a class="first-page" href="javascript:void(0)">
                        ก่อนหน้า
                    </a>
                </li>
                @endif
    
                <li class="@if(1 == $PageNumber) active @endif">
                    <a class="a-page" href="javascript:void(0)">1</a>
                </li>
    
                @if($Total > 1 )
                    @if($PageNumber < $page)
                        @if ($Total < $page)
                            @php
                                $page =  $Total
                            @endphp
                            
                        @endif
                        @for ($i = 2; $i <= $page; $i++)
                            <li class="@if($i == $PageNumber) active @endif">
                                <a class="a-page" href="javascript:void(0)">{{$i}}</a>
                            </li>
                        @endfor
                        @if ($Total > $page) 
                            @php
                                echo $mPage;
                            @endphp
                        @endif
                    @elseif ($PageNumber == $Total)
                            @if (($PageNumber - $page) > 0)
                                @php
                                    echo $mPage;
                                    $lPage = $PageNumber - $page;
                                @endphp
                                @for ($i = $lPage; $i < $Total; $i++)
                                    <li class="@if($i == $PageNumber) active @endif">
                                        <a class="a-page" href="javascript:void(0)">{{$i}}</a>
                                    </li>
                                @endfor
                            @else 
                                @php
                                    $lPage = 2;  
                                @endphp
                                @for ($i = $lPage; $i <= $Total; $i++)
                                    <li class="@if($i == $PageNumber) active @endif">
                                        <a class="a-page" href="javascript:void(0)">{{$i}}</a>
                                    </li>
                                @endfor
                            @endif
                    @else 
                        @php
                            echo $mPage;
                        @endphp
                        @php
                            $lPage = $PageNumber - 2;
                            $rage = $PageNumber + 2;
                        @endphp
                        @for ($i = $lPage; $i <= $PageNumber; $i++)
                            <li class="@if($i == $PageNumber) active @endif">
                                <a class="a-page" href="javascript:void(0)">{{$i}}</a>
                            </li>
                        @endfor
                        @for ($i = $PageNumber + 1; $i <= $rage; $i++)
                            <li class="@if($i == $PageNumber) active @endif">
                                <a class="a-page" href="javascript:void(0)">{{$i}}</a>
                            </li>
                        @endfor
                        @php
                            echo $mPage;
                        @endphp
                    @endif
                
                
                    @if ($Total > $page)
                    <li class="@if($Total == $PageNumber) active @endif">
                        <a class="a-page" href="javascript:void(0)">{{ $Total }}</a>
                    </li>
                    @endif
    
                    @if ($PageNumber != $Total)
                        <li class="">
                            <a class="last-page" href="javascript:void(0)">
                                ถัดไป
                            </a>
                        </li>
                    @endif
                
                @endif
            </ul>
        @endif
    </div>
    
</div>
