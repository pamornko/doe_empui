
        @if ($RegisterList)
        @foreach ($RegisterList['data'] as $Register)
            <tr style="<?=$bStyle?>">
                <td style="text-align: right;padding-right:20px;">{{$loop->iteration}}</td>
                <td>{{ $Register['PersonalID'] }}</td>
                <td> <a href="{{ URL::route('register-officer-view' , ['id'=>$Register['RegisterID']]) }}"> {{ $Register['RegisterNumber'] }} </a></td>
                <td>{{ $Register['Firstname'] }} {{ $Register['Lastname'] }}</td>
                <td style="text-align: center">{{ show_thai_date($Register['RegisterDate']) }}</td>
                <td id="td-{{  $Register['RegisterID'] }}">
                    @if ($Register['TrackingTime'] == 0)
                        @if ($Register['isActiveRegister'] == 1)
                            <button type="button" class="btn btn-outline-success waves-effect waves-light" onclick="window.location='{{ URL::route('register-officer-edit' , ['id'=>$Register['RegisterID']]) }}'"><i class="fas fa-pencil-alt"></i> {{ __('Edit') }}</button>
                            <button type="button" class="btn btn-outline-danger waves-effect waves-light" onclick="cancel({{ $Register['RegisterID'] }})"><i class="fas fa-eraser"></i> {{ __('Cancel') }}</button>
                        @else
                            <i class="text-danger mdi mdi-checkbox-blank-circle"></i> <code>ยกเลิก</code>
                        @endif
                    @else
                    <!--<button type="button" class="btn btn-outline-info waves-effect waves-light" onclick="" ><i class="fas fa-search"></i> ดูรายการ</button>-->
                    @endif
                </td>
            
            </tr>
        @endforeach
        @endif
