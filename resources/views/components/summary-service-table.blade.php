
<table id="summary_insured_services" class="table table-striped table-bordered head_table table-report-border">
    <thead>
        <tr style="background-color: #b9ccf5">
            <th rowspan="2" style="<?=$bStyle?>">{{ __('No.') }}</th>
            <th rowspan="2" style="<?=$bStyle?>">{{ __('Office/Zone') }}</th>
            <th colspan="3" style="<?=$bStyle?>">{{ __('Insured registration') }}</th>
            <th colspan="3" style="<?=$bStyle?>">{{ __('Reporting') }}</th>
            
        </tr>

        <tr style="background-color: #b9ccf5">
            <th style="<?=$bStyle?>">{{ __('Total') }}</th>
            <th style="<?=$bStyle?>">{{ __('Male') }}</th>
            <th style="<?=$bStyle?>">{{ __('Female') }}</th>
            <th style="<?=$bStyle?>">{{ __('Total') }}</th>
            <th style="<?=$bStyle?>">{{ __('Male') }}</th>
            <th style="<?=$bStyle?>">{{ __('Female') }}</th>
        </tr>

    </thead>
    <tbody>
        @if (!empty($ReportList))
            @foreach ($ReportList as $Report)
            <tr>
                <td style="<?=$bStyle?>">{{$loop->iteration}}</td>
                <td style="<?=$bStyle?>">{{ $Report['OrganizationName'] ?? $Report['SSOOfficeName']  }}</td>
                <td class="text-center" style="<?=$bStyle?>">
                    @php
                        $totalRegister = $Report['register']['F'] + $Report['register']['M'];
                    @endphp
                    {{ $totalRegister > 0 ? $totalRegister : '' }}
                </td>
                <td class="text-center" style="<?=$bStyle?>">{{ $Report['register']['M'] > 0 ? $Report['register']['M'] : '' }}</td>
                <td class="text-center" style="<?=$bStyle?>">{{ $Report['register']['F'] > 0 ? $Report['register']['F'] : '' }}</td>
                <td class="text-center" style="<?=$bStyle?>">
                    @php
                        $totalTracking = $Report['tracking']['F'] + $Report['tracking']['M'];
                    @endphp
                    {{ $totalTracking > 0 ? $totalTracking : '' }}
                </td>
                <td class="text-center" style="<?=$bStyle?>">{{ $Report['tracking']['M'] > 0 ? $Report['tracking']['M'] : '' }}</td>
                <td class="text-center" style="<?=$bStyle?>">{{ $Report['tracking']['F'] > 0 ? $Report['tracking']['F'] : '' }}</td>
                
            </tr>
            @endforeach
        @endif
    </tbody>
    
</table>