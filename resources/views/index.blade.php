@extends('layout.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('js/libs/chartist/dist/chartist.min.css') }}">    
    <link rel="stylesheet" href="{{ asset('js/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/c3/c3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/css-chart/css-chart.css') }}">
    
    <style>
        .text-purple-dark {
            color: #2F2FA2;
        }

        .text-purple {
            color: #A239CA;
        }

        .text-pink-soft {
            color: #E7717D;
        }

        .text-pink-dark {
            color: #F64C72;
        }
        
        .text-green-soft {
            color: #AFD275;
        }

        .text-green {
            color: #8EE4AF;
        }

        .text-orange-soft {
            color: #D79922;
        }

        .text-blue-soft {
            color: #40c4ff;
        }

        .text-blue {
            color: #2961ff;
        }

        .card {
           
            margin-bottom: 24px;
            -webkit-box-shadow: 0 .75rem 1.5rem rgba(18,38,63,.03);
            box-shadow: 0 .75rem 1.5rem rgba(18,38,63,.03);
            border-radius: 8px;
        }

        .bg-primary.bg-soft {
            background-color: rgba(85,110,230,.25) !important;
        }
        
        .text-primary {
            color: #556ee6 !important;
        }

        .preview-dashbroad img {
            width: 100%;
        }

        @media  (min-width: 290px) and (max-width: 850px) {
            .preview-dashbroad img {
                width: 100%;
            }

            body    {overflow-y:scroll;}
        }
    </style>
@endpush

@section('topic-menu')
@endsection


@section('content')

<!-- ============================================================== -->
<!-- Email Campaign -->
<!-- ============================================================== -->

@if(!$isEmployee)
<div class="row">
    <!-- column -->

    <div class="col-sm-12 col-lg-8">
        <div class="card e-campaign">

            <!-- <div class="card-body border-bottom">
                <div class="row col-sm-12 col-lg-12">
                   <div class="col-lg-4 col-sm-12 pb-3">
                        <label for="wRayOffDate"> {{  __('Since') }} : </label>
                        <input type="date" class="form-control" name="StartDate" id="StartDate" value="{{ $StartDate }}">
                    </div>
                    <div class="col-lg-4 col-sm-12 pb-3">
                        <label for="wRayOffDate"> {{  __('To') }} : </label>
                        <input type="date" class="form-control" name="EndDate" id="EndDate" value="{{ $EndDate }}">
                    </div>
                </div>
            </div>-->


            <div class="card-body" style="background-color: #F7ECEE">
                <div class="d-flex align-items-center">
                    <div class="col-md-12 mb-4">
                        <h5 class="card-title" style="text-align: center;">ขึ้นทะเบียนและรายงานตัวผู้ประกันตนกรณีว่างงาน ประจำงบประมาณ พ.ศ. {{ $Year ?? '' }}</h5>
                        <h6 class="card-title text-center" >ข้อมูลตั้งแต่วันที่ {{ \Carbon\Carbon::parse($ShowStartDate)->locale('th_TH')->isoFormat('LL') }} - วันที่ {{ \Carbon\Carbon::parse($ShowEndDate)->locale('th_TH')->isoFormat('LL') }}</h6>
                    </div>
                </div>

                <div class="row col-md-12 col-sm-12 align-items-center mt-4">
                    <div class="row col-md-6 col-sm-12">
                        
                        <div class="col-md-6 col-sm-6">
                            <h6 class="card-title">ขึ้นทะเบียน จำนวน <span id="register-year"></span> คน</h6>
                            <div id="c-register-year" class="mt-3" style="height:180px; width:100%;"></div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <ul id="ul-c-register-year" class="list-style-none">
                                
                                <!--<li class="mt-3"><i class="fa fa-circle mr-1 text-cyan font-12"></i> ลาออก</li>
                                <li class="mt-3"><i class="fa fa-circle mr-1 text-info font-12"></i> เลิกจ้าง </li>
                                <li class="mt-3"><i class="fa fa-circle mr-1 text-orange font-12"></i> อื่นๆ </li>-->
                            
                            </ul>
                        </div>

                        <div class="row col-md-12 col-sm-12">
                            <span class="text-black op-5 mr-1"><i class="ti-angle-right"></i> ผ่านระบบอินเตอร์เน็ตด้วยตนเอง จำนวน </span> <span id="c-register-year-internet"></span><span class="text-black op-5 ml-1">คน</span>
                        </div>
                        <div class="row col-md-12 col-sm-12">
                            <span class="text-black op-5 mr-1"><i class="ti-angle-right"></i> ณ สำนักงาน จำนวน </span> <span id="c-register-year-office"></span><span class="text-black op-5 ml-1">คน</span>
                        </div>
                    </div>

                    <div class="row col-md-6 col-sm-12 ml-2">

                        <div class="col-md-6 col-sm-6">
                            <h6 class="card-title">รายงานตัว จำนวน <span id="reporting-year"></span> คน</h6>
                            <div id="c-reporting-year" class="mt-3" style="height:180px; width:100%;"></div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <ul id="ul-c-reporting-year" class="list-style-none"></ul>
                        </div>

                        <div class="row col-md-12 col-sm-12">
                            <span class="text-black op-5 mr-1"><i class="ti-angle-right"></i> ผ่านระบบอินเตอร์เน็ตด้วยตนเอง จำนวน </span> <span id="c-reporting-year-internet"></span><span class="text-black op-5 ml-1">คน</span>
                        </div>
                        <div class="row col-md-12 col-sm-12">
                            <span class="text-black op-5 mr-1"><i class="ti-angle-right"></i> ณ สำนักงาน จำนวน </span> <span id="c-reporting-year-office"></span><span class="text-black op-5 ml-1">คน</span>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>


        <div class="card e-campaign" style="background-color: #FBFACB">
            <div class="row col-md-12 col-sm-12">
                    <div class="card-body row col-md-6 col-sm-12 ml-2">
                        <div class="d-flex align-items-center">
                            <div>
                                <h6 class="card-title">ผู้ประกันตนขึ้นทะเบียนฯ มีความประสงค์</h6>
                            </div>
                        </div>

                        <div class="row col-md-12 col-sm-12 align-items-center mt-4">
                            <div class="row col-md-12 col-sm-12">
                            
                                <div class="col-md-6 col-sm-6">
                                    <div id="c-wish" class="mt-3" style="height:180px; width:100%;"></div>
                                </div>
        
                                <div class="col-md-6 col-sm-6">
                                    <ul class="list-style-none" id="ul-c-wish">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    
                    </div>
        
                    <div class="card-body row col-md-6 col-sm-12 ml-1" style="height: 100%">
                        <div class="d-flex align-items-center">
                            <div>
                                <h6 class="card-title">ผู้ขึ้นทะเบียนและรายงานตัวผู้ประกันตนกรณีว่างงานกลับเข้าสู่ตลาดแรงงาน</h6>
                            </div>
                        </div>
        
                        <div class="d-flex align-items-center">
                            <div class="mr-2"><span class="text-cyan display-5"><i class="mdi mdi-star-circle"></i></span></div>
                            <div><span>(มีงานทำและเป็นผู้ประกันตน)</span>
                                <h3 class="font-medium mb-0">จำนวน <span id="have-work"></span> คน</h3>
                            </div>
                        </div>
                    </div>

                </div>

            
            
        </div>


    </div>
    <!-- column -->
    <div class="col-sm-12 col-lg-4">

        <div class="card">
            <div class="card-body" style="background-color: #E4E7F9">
                <div class="row justify-content-md-center col-md-12" style="text-align: center;">
                    <h6 class="card-title">ขึ้นทะเบียนและรายงานตัว ผู้ประกันตนคนว่างงาน</h6>
                    <h6 class="card-title text-center">ณ วันที่ {{ \Carbon\Carbon::parse($CurrentDate)->locale('th_TH')->isoFormat('LL') }}</h6>
                </div>
                
                <!--<div class="d-flex align-items-center mt-4">
                    <div class="" id="ravenue"></div>
                    <div class="ml-auto">
                        <h3 class="text-white mb-0"><i class="ti-arrow-up"></i>$351</h3>
                        <span class="text-white op-5">Jan 10  - Jan  20</span>
                    </div>
                </div>-->

                <div class="d-flex align-items-center mt-4">
                    <div class="col s6 l6">
                        <h6 class="card-title">ขึ้นทะเบียน จำนวน <span id="register"></span> คน</h6>
                        <div id="c-register" class="mt-3" style="height:180px; width:100%;"></div>
                    </div>

                    <div class="col s6 l6">
                        <ul id="ul-c-register" class="list-style-none">
                            
                        </ul>
                    </div>
                </div>

                <div class="d-flex align-items-center mt-4">
                    <span class="text-black op-5 mr-1"><i class="ti-angle-right"></i> ผ่านระบบอินเตอร์เน็ตด้วยตนเอง จำนวน </span> <span id="c-register-internet"></span><span class="text-black op-5 ml-1">คน</span>
                </div>
                <div class="d-flex align-items-center">
                    <span class="text-black op-5 mr-1"><i class="ti-angle-right"></i> ณ สำนักงาน จำนวน </span> <span id="c-register-office"></span><span class="text-black op-5 ml-1">คน</span>
                </div>
            </div>

            <div class="card-body" style="background-color: #E4E7F9">
                
                <div class="d-flex align-items-center mt-4">
                    <div class="col s6 l6">
                        <h6 class="card-title">รายงานตัว จำนวน <span id="reporting"></span> คน</h6>
                        <div id="c-reporting" class="mt-3" style="height:180px; width:100%;"></div>
                    </div>

                    <div class="col s6 l6">
                        <ul id="ul-c-reporting" class="list-style-none"></ul>
                    </div>
                </div>

                <div class="d-flex align-items-center mt-4">
                    <span class="text-black op-5 mr-1"><i class="ti-angle-right"></i> ผ่านระบบอินเตอร์เน็ตด้วยตนเอง จำนวน </span> <span id="c-reporting-internet"></span><span class="text-black op-5 ml-1">คน</span>
                </div>
                <div class="d-flex align-items-center">
                    <span class="text-black op-5 mr-1"><i class="ti-angle-right"></i> ณ สำนักงาน จำนวน </span> <span id="c-reporting-office"></span><span class="text-black op-5 ml-1">คน</span>
                </div>
            </div>
        </div>


        <div class="card">
            
        </div>


    </div>
</div>

@else 

<div class="row">
    <!-- column -->


    <div class="col-sm-12 col-lg-6">
        <div class="card e-campaign bg-primary bg-soft">
            <div class="card-body" style="padding-left: 20px;">
                <div class="row">
                    <div class="row col-12">
                        
                        <div class="col-12 text-center">
                            <h3 class="card-title text-primary">ข้อมูลการขึ้นทะเบียนกรณีว่างงานของ</h3>
                            <h3 class="card-title text-info">{{ $Name ?? '' }}</h3>
                        </div>
                        @if (!empty($Summary['lastRegister']))
                        <ul class="ps-3 mb-0">
                            <li class="mt-2" style="color: #556ee6;font-size: 18px;">เคยขึ้นทะเบียนครั้งล่าสุด เมื่อวันที่ {{  \Carbon\Carbon::parse($Summary['lastRegister']['RegisterDate'])->locale('th_TH')->isoFormat('LL') }}</li>
                            @if ($Summary['lastTracking']) <li class="mt-2" style="color: #556ee6;font-size: 18px;">รายงานตัวครั้งล่าสุด เมื่อวันที่ {{ \Carbon\Carbon::parse($Summary['lastTracking']['ReportingDate'])->locale('th_TH')->isoFormat('LL') }}</li> @endif
                            @if ($Summary['lastBenefit']) <li class="mt-2" style="color: #556ee6;font-size: 18px;">รับประโยชน์ทดแทนฯครั้งล่าสุด เมื่อวันที่  {{ \Carbon\Carbon::parse($Summary['lastBenefit']['payBeginDate'])->locale('th_TH')->isoFormat('LL') }} </li> @endif
                            <li class="mt-2" style="color: #556ee6;font-size: 18px;">ครบกำหนดรายงานตัว ครั้งถัดไป <span>ภายในวันที่ {{ \Carbon\Carbon::parse($Summary['nextTracking']['ReportingDueDate'])->locale('th_TH')->isoFormat('LL') }}</span> </li>
                        </ul>
                        @endif
                    </div>
                    <!--<ul class="list-style-none stats mt-2 ml-4">
                        <li class="mt-2"><i class="fa fa-circle mr-1 text-success font-12"></i> 25% Ads broke the target </li>
                        <li class="mt-2"><i class="fa fa-circle mr-1 text-info font-12"></i> 75% Ads are Successfull </li>
                        <li class="mt-2"><i class="fa fa-circle mr-1 text-danger font-12"></i> 75% Ads are Successfull </li>
                        <li class="mt-2"><i class="fa fa-circle mr-1 text-primary font-12"></i> 75% Ads are Successfull </li>
                    </ul>-->
                </div>
            </div>
        </div>
    </div>
    <!-- column -->
    <div class="col-sm-12 col-lg-6">
        <div class="preview-dashbroad">
            <img src="{{ asset('images/dashbroad/infographic.jpeg') }}">
        </div>
        <!--<div class="card bg-cyan">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <h5 class="card-title text-white">Page Views</h5>
                        <h3 class="text-white mb-0"><i class="ti-arrow-up"></i> 6548</h3>
                    </div>
                    <div class="ml-auto">
                        <ul class="list-style-none mb-0">
                            <li class="text-white"><i class="fa fa-circle mr-1 text-white font-12 op-3"></i> Visit </li>
                            <li class="text-white"><i class="fa fa-circle mr-1 text-white font-12"></i> Page Views </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="mt-3" id="views"></div>
        </div>-->
    </div>
</div>
@endif

@endsection

@include('components.confirm-location-modal')

@push('scripts')
<script type="text/javascript"> 
    var config = {
        isEmployee: "{{ $isEmployee }}"
    };
</script>
<!--This page JavaScript -->
<script type="text/javascript" src="{{ URL::asset('js/libs/chartist/dist/chartist.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/extra-libs/c3/d3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/extra-libs/c3/c3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/libs/chart.js/dist/Chart.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/pages/dashboards/dashboard6.js?v=[timestamp]') }}"></script>
<script>
    $(document).ready(function(){
        //active home menu
        $('#sidebarnav').children('li:eq(1)').children('a').addClass('active');

        var mShow = "<?=$mShow?>";
        $('#selectOffice').on('change', function() {
            if (this.value === "") {
                $('#btnOffice').addClass("hide-element");
            } else {
                $('#btnOffice').removeClass("hide-element");
            }
        });

        /* for dev */

        if (mShow === 'show') {
            $("#first-modals").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#first-modals').modal('show');
        }


        $('#fm-location').submit(function(event){
            event.preventDefault();
                
            var dataString = $(this).serialize();
            console.log("dataString = ",dataString);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type : 'post',
                url: '{!! URL::to('set_location')!!}',
                data: dataString,
                //dataType: "html",
                success: function (data) { 
                    
                    $('#first-modals').modal('hide');
                }
            }); 
        });

        function closeModal() {
            $('#first-modals').modal('hide');
        }
    });
</script>
@endpush