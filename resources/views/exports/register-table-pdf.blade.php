<!DOCTYPE html>
<html lang="th">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <style>
        @font-face{
            font-family:  'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face{
            font-family:  'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ asset('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }
        @font-face{
            font-family:  'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ asset('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }
        @font-face{
            font-family:  'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ asset('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        body{
            font-family: "THSarabunNew"; font-size: 16px;
        }
        @page {
            size: A4;
            margin: 5px;
        }
        @media print {
            html, body {

                font-family: "THSarabunNew"; 
                font-size: 16px;
                margin:0px;
            }
        }

        .topic {
            font-size: 20px;
        }

        table th {
            text-align: center;
        }
    </style>
    
</head>

<body>
    <div class="container mt-5">
        <div class="text-center mb-3">
            <span class="topic">{{  __('Unemployment registration information') }}</span>
        </div>
        

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>{{ __('No.') }}</th>
                    <th>{{ __('ID Card Number') }}</th>
                    <th>{{ __('Registration number') }}</th>
                    <th>{{ __('Insured Person Name') }}</th>
                    <th>{{ __('Registration Date') }}</th>
                </tr>
            </thead>
            <tbody>
                @if ($RegisterList)
                    @foreach ($RegisterList as $Register)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{ $Register['PersonalID'] }}</td>
                            <td> {{ $Register['RegisterNumber'] }} </td>
                            <td>{{ $Register['FirstName'] }} {{ $Register['LastName'] }}</td>
                            <td>{{ \Carbon\Carbon::parse($Register['RegisterDate'])->format('d/m/Y') }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>

    </div>

</body>

</html>