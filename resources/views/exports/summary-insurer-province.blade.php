<!DOCTYPE html>
<html lang="th">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="stylesheet" href="{{ asset('css/report-style.css') }}">
</head>

<body>
   
        <div style="width:100%;text-align: center;">
            <span class="topic">แบบรายงานผู้ประกันตนกรณีว่างงาน (จังหวัด)</span>
        </div>
        <div style="width:100%;text-align: center;">
            <span class="topic"> {{ $Province ?? '' }}</span>
        </div>
        @php
           //dd($ReportList); 
        @endphp

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th rowspan="2" style="text-align: right;">{{ __('No.') }}</th>
                    <th rowspan="2">{{ __('Office/Zone') }}</th>
                    <th colspan="{{ count($Case) ?? 0 }}">{{ __('Insured registration') }} (คน)</th>
                    <th colspan="{{ count($Tracking) ?? 0 }}">{{ __('Reporting') }}ผู้ประกันตน</th>
                    <th colspan="{{ count($Job) ?? 0 }}">{{ __('Reporting') }}บรรจุงาน</th>
                    <th rowspan="2">หมายเหตุ</th>
                </tr>
        
                <tr>
                    @foreach ($Case as $key => $val)
                    <th style="text-align: center; width: 4px;">{{ $key }}</th>
                    @endforeach
                    @foreach ($Tracking as $key => $val)
                    <th style="text-align: center; width: 4px;">{{ $key }}</th>
                    @endforeach
                    @foreach ($Job as $key => $val)
                    <th style="text-align: center; width: 4px;">{{ $key }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($ReportList as $Report)
                    <tr>
                        <td style="text-align: right; padding-right:5px;">{{$loop->iteration}}.</td>
                        <td style="text-align: center;">{{$Report['OrganizationName']}}</td>
                        @foreach ($Report['register'] as $value)
                            <td style="text-align: center; width: 4px;" > {{ $value }} </td>
                        @endforeach
                        @foreach ($Report['tracking'] as $value)
                            <td  style="text-align: center; width: 4px;"> {{ $value }} </td>
                        @endforeach
                        @foreach ($Report['job'] as $value)
                            <td style="text-align: center; width: 4px;"> {{ $value }} </td>
                        @endforeach
                        <td>{{ $Report['remark'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

   

</body>

</html>