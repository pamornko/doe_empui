<!DOCTYPE html>
<html lang="th">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="stylesheet" href="{{ asset('css/report-style.css') }}">
</head>

<body>
   
        <div style="width:100%;text-align: center;">
            <span class="topic">ตรวจสอบการขึ้นทะเบียนเป็นผู้ประกันตนกับสำนักงานประกันสังคม</span>
        </div>
       
        @php
           //dd($ReportList); 
        @endphp

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>{{ __('No.') }}</th>
                    <th>{{ __('Name - Surname') }}</th>
                    <th>{{ __('Registration number') }}</th>
                    <th>{{ __('Registration Date') }}</th>
                    <th>สถานะการมีงานทำ</th>
                    <th>บรรจุงาน</th>
                    <th>วันที่ได้งานทำ</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($ReportList as $Report)
                <tr>
                    <td style="text-align: right;padding-right: 5px;">{{$loop->iteration}}.</td>
                    <td>{{ $Report['FirstName'] ?? '' }} {{ $Report['LastName'] ?? '' }}</td>
                    <td style="text-align: center;">{{ $Report['RegisterNumber'] ?? '' }}</td>
                    <td style="text-align: center;">{{ $Report['empRegisterDate'] ?? '' }}</td>
                    <td style="text-align: center;">
                        @if (!empty($Report['WorkStatus']))
                            มีงานทำ
                        @else 
                            ว่างงาน
                        @endif
                    </td>
                    <td></td>
                    <td style="text-align: center;"> @if (!empty($Report['empStartDate'])) {{ $Report['empStartDate'] }} @endif</td>
                </tr>
                @endforeach
            </tbody>
        </table>

   

</body>

</html>