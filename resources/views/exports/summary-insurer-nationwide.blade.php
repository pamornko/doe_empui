<!DOCTYPE html>
<html lang="th">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <style>
        body {
            font-family: 'sarabun_new', sans-serif;
            font-size: 16px;
        }

        .topic {
            text-align: center;
            font-size: 20px;
            font-weight: bold;
        }

        table {
            width: 100%;
            border-collapse:collapse;
            border: 1px solid black;
        }

        table th {
            text-align: center;
            border-collapse:collapse;
            border-bottom: 1px solid black;
            padding-left: 2px;
            padding-right: 2px;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        table td {
            border-collapse:collapse;
            border-bottom: 1px dotted black;
        }

    </style>
</head>

<body>
   
        <div style="width:100%;text-align: center;">
            <span class="topic">แบบรายงานผู้ประกันตนกรณีว่างงาน (ทั่วประเทศ)</span>
        </div>
        @php
           //dd($ReportList); 
        @endphp

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th rowspan="2">{{ __('No.') }}</th>
                    <th rowspan="2" style="border-right: 1px solid black;">{{ __('Office/Zone') }}</th>
                    <th colspan="{{ count($Case) ?? 0 }}">{{ __('Insured registration') }} (คน)</th>
                    <th colspan="{{ count($Tracking) ?? 0 }}">{{ __('Reporting') }}ผู้ประกันตน</th>
                    <th colspan="{{ count($Job) ?? 0 }}">{{ __('Reporting') }}บรรจุงาน</th>
                    <th rowspan="2" style="border-left: 1px solid black;">หมายเหตุ</th>
                </tr>
        
                <tr>
                    @foreach ($Case as $key => $val)
                    <th>{{ $key }}</th>
                    @endforeach
                    @foreach ($Tracking as $key => $val)
                    <th>{{ $key }}</th>
                    @endforeach
                    @foreach ($Job as $key => $val)
                    <th>{{ $key }}</th>
                    @endforeach
                </tr>
        
            </thead>
            <tbody>
                @foreach ($ReportList as $Report)
                    <tr>
                        <td colspan="2" style="border-right: 1px solid black;">{{$Report['RegionName']}}</td>
                        
                        
                        @foreach ($Report['register'] as $value)
                        <td style="text-align: center;"> {{ $value }} </td>
                        @endforeach
                        @foreach ($Report['tracking'] as $value)
                        <td style="text-align: center;"> {{ $value }} </td>
                        @endforeach
                        @foreach ($Report['job'] as $value)
                        <td style="text-align: center;"> {{ $value }} </td>
                        @endforeach
                        <td style="border-left: 1px solid black;">{{ $Report['remark'] }}</td>
                    </tr>
                @endforeach
                @foreach ($ReportList as $Report)
                    <tr>
                        <td colspan="2" style="border-right: 1px solid black;">{{$Report['RegionName']}}</td>
                      
                       
                    </tr>
                    @foreach ($Report['Organization'] as $Org)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td style="border-right: 1px solid black;">{{$Org['OrganizationName']}}</td>
                        @foreach ($Org['register'] as $value)
                        <td  style="text-align: center;"> {{ $value }} </td>
                        @endforeach
                        @foreach ($Org['tracking'] as $value)
                        <td  style="text-align: center;"> {{ $value }} </td>
                        @endforeach
                        @foreach ($Org['job'] as $value)
                        <td  style="text-align: center;"> {{ $value }} </td>
                        @endforeach
                        <td style="border-left: 1px solid black;">{{ $Org['remark'] }}</td>
                    </tr>
                    @endforeach
                @endforeach
            </tbody>
        </table>

   

</body>

</html>