<!DOCTYPE html>
<html lang="th">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="stylesheet" href="{{ asset('css/report-style.css') }}">
</head>

<body>
   
        <div style="width:100%;text-align: center;">
            <span class="topic">{{  __('Summary of insured services in the event of unemployment') }}</span>
        </div>
        @php
           //dd($ReportList); 
        @endphp

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th rowspan="2">{{ __('No.') }}</th>
                    <th rowspan="2">{{ __('Office/Zone') }}</th>
                    <th colspan="3">{{ __('Insured registration') }}</th>
                    <th colspan="3">{{ __('Reporting') }}</th>
                    
                </tr>
        
                <tr>
                    <th>{{ __('Total') }}</th>
                    <th>{{ __('Male') }}</th>
                    <th>{{ __('Female') }}</th>
                    <th>{{ __('Total') }}</th>
                    <th>{{ __('Male') }}</th>
                    <th>{{ __('Female') }}</th>
                </tr>
        
            </thead>
            <tbody>
                @foreach ($ReportList as $Report)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{ $Report['OrganizationName'] ?? $Report['SSOOfficeName']  }}</td>
                    <td style="text-align: center;">{{ $Report['register']['F'] + $Report['register']['M'] }}</td>
                    <td style="text-align: center;">{{ $Report['register']['M']  }}</td>
                    <td style="text-align: center;">{{ $Report['register']['F']  }}</td>
                    <td style="text-align: center;">{{ $Report['tracking']['F'] + $Report['tracking']['M'] }}</td>
                    <td style="text-align: center;">{{ $Report['tracking']['M']  }}</td>
                    <td style="text-align: center;">{{ $Report['tracking']['F']  }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

   

</body>

</html>