<!DOCTYPE html>
<html lang="th">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="stylesheet" href="{{ asset('css/report-style.css') }}">
</head>

<body>
   
        <div style="width:100%;text-align: center;">
            <span class="topic">แบบรายงานผู้ประกันตนกรณีว่างงาน (บุคคล)</span>
        </div>
        <div style="width:100%;text-align: center;">
            <span class="topic"> {{ $Province ?? '' }}</span>
        </div>
        @php
           //dd($ReportList); 
        @endphp

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th rowspan="2" >{{ __('No.') }}</th>
                    <th rowspan="2"  style="width:200px;">ชื่อ-นามสกุล</th>
                    <th colspan="4" >ขึ้นทะเบียนผู้ประกันตน</th>
                    <th colspan="6" >การรายงานตัวผู้ประกันตน</th>
                    <th rowspan="2" >หมายเหตุ</th>
                </tr>
        
                <tr>
                    <th >ประเภทการออก</th>
                    <th >ขึ้นทะเบียนหางาน</th>
                    <th >ประกอบอาชีพอิสระ</th>
                    <th >ไม่ประสงค์สมัครงาน</th>
                    <th >ครั้งที่</th>
                    <th >กำหนดรายงาน</th>
                    <th >วันที่รายงาน</th>
                    <th >ขึ้นทะเบียนหางาน</th>
                    <th >ประกอบอาชีพอิสระ</th>
                    <th >ไม่ประสงค์สมัครงาน</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($ReportList) && count($ReportList) > 0)
           
            @foreach ($ReportList as $Report)
                <tr>
                    <td class="text-center">{{$index = $loop->iteration}}</td>
                    <td>{{$Report["FirstName"] ?? ""}} {{$Report["LastName"] ?? ""}}</td>
                    <td class="text-center">{{$Report["register"]["ประเภทการออก"] ?? ""}}</td>
                    <td class="text-center">@if ($Report["register"]["ขึ้นทะเบียนผู้ประกันตน"]) / @endif</td>
                    <td class="text-center">@if ($Report["register"]["ประกอบอาชีพอิสระ"]) / @endif</td>
                    <td class="text-center">@if ($Report["register"]["ไม่ประสงค์สมัครงาน"]) / @endif</td>
                    @php
                        $isFirst = true;
                    @endphp
                    @if (count($Report["tracking"]) > 0)
                        @foreach ($Report["tracking"] as $tracking)
                            @if ($isFirst)
                                @php
                                    $isFirst = false;
                                @endphp
                                <td class="text-center">{{$tracking["ครั้งที่"] ?? ""}}</td>
                                <td class="text-center">{{$tracking["กำหนดรายงาน"] ?? ""}}</td>
                                <td class="text-center">{{$tracking["วันที่รายงาน"] ?? ""}}</td>
                                <td class="text-center">@if ($tracking["ขึ้นทะเบียนหางาน"]) / @endif</td>
                                <td class="text-center">@if ($tracking["ประกอบอาชีพอิสระ"]) / @endif</td>
                                <td class="text-center">@if ($tracking["ไม่ประสงค์สมัครงาน"]) / @endif</td>
                                <td>{{ $Report["remark"] ?? "" }}</td>
                            @else 
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center">{{$tracking["ครั้งที่"] ?? ""}}</td>
                                    <td class="text-center">{{  \Carbon\Carbon::parse($tracking["กำหนดรายงาน"])->format('d/m/Y') ?? ""}}</td>
                                    <td class="text-center">{{ \Carbon\Carbon::parse($tracking["วันที่รายงาน"])->format('d/m/Y') ?? ""}}</td>
                                    <td class="text-center">@if ($tracking["ขึ้นทะเบียนหางาน"]) / @endif</td>
                                    <td class="text-center">@if ($tracking["ประกอบอาชีพอิสระ"]) / @endif</td>
                                    <td class="text-center">@if ($tracking["ไม่ประสงค์สมัครงาน"]) / @endif</td>
                                    <td>{{ $Report["remark"] ?? "" }}</td>
                                </tr>
                            @endif
                            
                        @endforeach
                    @else
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{ $Report["remark"] ?? "" }}</td>
                    @endif
                    
                </tr>
                
               
            @endforeach
        @else
            <tr>  
                <td colspan="13" style="text-align: center;">ไม่พบข้อมูล</td>
            </tr>
        @endif
            </tbody>
        </table>

   

</body>

</html>