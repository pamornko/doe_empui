<!DOCTYPE html>
<html lang="th">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="stylesheet" href="{{ asset('css/report-style.css') }}">
</head>

<body>
   
        <div style="width:100%;text-align: center;">
            <span class="topic">{{  __('Unemployment registration information') }}</span>
        </div>
       

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>{{ __('No.') }}</th>
                    <th>{{ __('ID Card Number') }}</th>
                    <th>{{ __('Registration number') }}</th>
                    <th>{{ __('Insured Person Name') }}</th>
                    <th>{{ __('Registration Date') }}</th>
                </tr>
            </thead>
            <tbody>
                @if ($RegisterList)
                    @foreach ($RegisterList as $Register)
                        <tr>
                            <td style="text-align: right; padding-right:5px;">{{$loop->iteration}}.</td>
                            <td style="text-align: center;">{{ $Register['PersonalID'] }}</td>
                            <td style="text-align: center;"> {{ $Register['RegisterNumber'] }} </td>
                            <td>{{ $Register['FirstName'] }} {{ $Register['LastName'] }}</td>
                            <td style="text-align: center;">{{ \Carbon\Carbon::parse($Register['RegisterDate'])->format('d/m/Y') }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>

   

</body>

</html>