@extends('layout.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/accordion.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/toastr/dist/build/toastr.min.css') }}">
    <style scoped>
        .fldset-class{
            border: 1px rgb(233, 236, 239) solid;
        }
        .legend-class{

            margin-left: 1em; 
            padding: 0.2em 0.8em;

            font-size: 1rem;
            width: 65px;
        }

        .rightDiv
        {
      
            color: #000;
            height: 100%;
            width: calc(100%-200px);
            float: right;
            overflow: auto;
        }

        .table-report-border {
            border: 1px solid #aaaaaa !important;
        }
    </style>
@endpush

@section('topic-menu')
@endsection


@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{  __('Summary of insured services in the event of unemployment') }}</h4>
                <form id="report-service-search">
                    <fieldset class="fldset-class">
                        <legend class="legend-class">{{  __('Search') }}</legend>
                        <div class="row col-lg-12 pl-5">
                            <div class="col-lg-3 col-sm-6">
                                <label for="wRayOffDate"> {{  __('Since') }} : </label>
                                <input type="date" name="StartDate" id="StartDate" value="{{ $StartDate }}" class="form-control">
                            </div>

                            <div class="col-lg-3 col-sm-6">
                                <label for="wRayOffDate"> {{  __('To') }} : </label>
                                <input type="date" name="EndDate" id="EndDate" value="{{ $EndDate }}" class="form-control">
                            </div>

                            <div class="col-lg-4 col-sm-6">
                                <div class="form-group">
                                    <label for="OrganizationID">สำนักงานกรมการจัดหางาน : </label>
                                    <select class="custom-select form-control" id="OrganizationID" name="OrganizationID">
                                        <option value="">กรุณาเลือก</option>
                                        @foreach ($Offices as $Office)
                                            <option value="{{  $Office['OrganizationID'] }}">{{ $Office['OrganizationName'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            

                            <div class="col-lg-2 col-sm-6 align-self-end pb-3">
                                <button class="btn btn-major waves-effect waves-light" type="submit"><i class="fas fa-search"></i> {{  __('Search') }}</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                    
                <div class="col-lg-12 pt-2 pb-4 d-flex justify-content-end">
                    <div class="col-lg-1 col-md-1 pr-0">
                        <button type="button" class="btn btn-block btn-sm btn-secondary" onclick="exportPDF()"><i class="fas fa-file-pdf"></i> {{  __('PDF') }}</button>
                    </div>
                    <div class="col-lg-1 col-md-1 pr-0">
                        <button type="button" class="btn btn-block btn-sm btn-secondary" onclick="exportExcel()"><i class="fas fa-file-excel"></i> {{  __('Excel') }}</button>
                    </div>
                    <div class="col-lg-1 col-md-1 pr-0">
                        <button type="button" class="btn btn-block btn-sm btn-secondary" onclick="print()"><i class="fas fa-print"></i> {{  __('Web') }}</button>
                    </div>
                </div>
                    
                    
                    
                <!-- Area Report -->
                <div class="table-responsive"></div>


            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')
    <!--This page JavaScript -->
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/datatable/datatable-basic.init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/toastr/dist/build/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/toastr/toastr-init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/summary-service-report.init.js') }}"></script>
@endpush