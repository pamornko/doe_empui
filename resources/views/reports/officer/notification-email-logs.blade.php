@extends('layout.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/accordion.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/toastr/dist/build/toastr.min.css') }}">

    <style scoped>
        .fldset-class{
            border: 1px rgb(233, 236, 239) solid;
        }
        .legend-class{

            margin-left: 1em; 
            padding: 0.2em 0.8em;

            font-size: 1rem;
            width: 65px;
        }

        .rightDiv
        {
      
            color: #000;
            height: 100%;
            width: calc(100%-200px);
            float: right;
            overflow: auto;
        }
    </style>
@endpush

@section('topic-menu')
@endsection


@section('content')
<?php
//dd($ReportList);

?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">ประวัติการส่งอีเมล์แจ้งเตือนรายงานตัว</h4>
                <form id="report-search">
                    <fieldset class="fldset-class">
                        <legend class="legend-class">{{  __('Search') }}</legend>
                        <div class="row col-lg-12 pl-5 pb-2">
                            <div class="col-lg-3 col-sm-6" title="ค้นหาจาก เลขขึ้นทะเบียน/ หมายเลขบัตรประชาชน">
                                <label for=""> {{  __('Search Label') }} : </label>
                                <input type="text" name="RegisterNumber" id="RegisterNumber" value="" class="form-control">
                            </div>

                            <div class="col-lg-3 col-sm-6">
                                <label for=""> ครั้งที่รายงานตัว : </label>
                                <input type="text" name="TrackingTime" id="TrackingTime" value="" class="form-control">
                            </div>


                           
                            <div class="col-lg-2 col-sm-6 align-self-end">
                                <button class="btn btn-major waves-effect waves-light" type="submit"><i class="fas fa-search"></i> {{  __('Search') }}</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                    
                <div class="col-lg-12 pt-2 pb-4 d-flex justify-content-end">
                    
                    <div class="col-lg-1 col-md-1 pr-0">
                        <button type="button" class="btn btn-block btn-sm btn-secondary" onclick="exportExcel()"><i class="fas fa-file-excel"></i> {{  __('Excel') }}</button>
                    </div>
                   
                </div>
                    
                    
                    
                   
                <div class="table-responsive">
                    <table id="notification_email" class="table table-striped table-bordered">
                        <thead>
                          
                            <tr class="text-center"  style="background-color: #b9ccf5;">
                                <th >{{ __('No.') }}</th>
                                <th>{{ __('Registration number') }}</th>
                                <th>{{ __('Number of Time') }}</th>
                                <th>{{ __('Reporting Schedule') }}</th>
                                <th>วันที่ส่งอีเมล์</th>
                                <th>อีเมล์</th>
                                <th>หัวข้อ</th>
                                <th>สถานะการส่ง</th>
                            </tr>
                    
                        </thead>
                        <tbody style=" <?=$bStyle?>">
                            @include('components.notification-email-logs-table')
                        </tbody>
            
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')
    <!--This page JavaScript -->
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/datatable/datatable-basic.init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/utils.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/datatable/datatable-logs.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/toastr/dist/build/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/toastr/toastr-init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/notification-email-logs.init.js') }}"></script>

@endpush