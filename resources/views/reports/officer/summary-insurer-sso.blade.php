@extends('layout.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/accordion.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
    <style scoped>
        .fldset-class{
            border: 1px rgb(233, 236, 239) solid;
        }
        .legend-class{

            margin-left: 1em; 
            padding: 0.2em 0.8em;

            font-size: 1rem;
            width: 65px;
        }

        .rightDiv
        {
      
            color: #000;
            height: 100%;
            width: calc(100%-200px);
            float: right;
            overflow: auto;
        }
    </style>
@endpush

@section('topic-menu')
@endsection


@section('content')
<?php
//dd($ReportList);

?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">ตรวจสอบการขึ้นทะเบียนเป็นผู้ประกันตนกับสำนักงานประกันสังคม</h4>
                <form id="report-search">
                    <fieldset class="fldset-class">
                        <legend class="legend-class">{{  __('Search') }}</legend>
                        <div class="row col-lg-12 pl-5 pb-2">
                            <div class="col-lg-3 col-sm-6">
                                <label for="datepicker"> เดือน : </label>
                                <input type="text" class="form-control" name="StartDate" id="datepicker" value="{{ $StartDate ?? '' }}"/>
                            </div>

                            <div class="col-lg-3 col-sm-6">
                                <label for="wRayOffDate"> การแสดงข้อมูล : </label>
                                <select class="form-control" name="DisplayType" id="DisplayType">
                                    <option value="0">ทั้งหมด</option>
                                    <option value="1">ได้งาน</option>
                                    <option value="2">ว่างงาน</option>
                                </select>
                            </div>

                           
                            <div class="col-lg-2 col-sm-6 align-self-end">
                                <button class="btn btn-major waves-effect waves-light" type="submit"><i class="fas fa-search"></i> {{  __('Search') }}</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                    
                <div class="col-lg-12 pt-2 pb-4 d-flex justify-content-end">
                    <div class="col-lg-1 col-md-1 pr-0">
                        <button type="button" class="btn btn-block btn-sm btn-secondary" onclick="exportPDF()"><i class="fas fa-file-pdf"></i> {{  __('PDF') }}</button>
                    </div>
                    <div class="col-lg-1 col-md-1 pr-0">
                        <button type="button" class="btn btn-block btn-sm btn-secondary" onclick="exportExcel()"><i class="fas fa-file-excel"></i> {{  __('Excel') }}</button>
                    </div>
                    <div class="col-lg-1 col-md-1 pr-0">
                        <button type="button" class="btn btn-block btn-sm btn-secondary" onclick="print()"><i class="fas fa-print"></i> {{  __('Web') }}</button>
                    </div>
                </div>
                    
                    
                    
                   
                <div class="table-responsive">
                    @include('components.summary-insurer-sso-table')
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')
    <!--This page JavaScript -->
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/datatable/datatable-basic.init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/summary-insurer-sso.init.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

@endpush