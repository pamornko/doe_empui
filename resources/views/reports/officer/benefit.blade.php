@extends('layout.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/accordion.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/toastr/dist/build/toastr.min.css') }}">
    <style scoped>
        .fldset-class{
            border: 1px rgb(233, 236, 239) solid;
        }
        .legend-class{

            margin-left: 1em; 
            padding: 0.2em 0.8em;

            font-size: 1rem;
            width: 65px;
        }

        .rightDiv
        {
      
            color: #000;
            height: 100%;
            width: calc(100%-200px);
            float: right;
            overflow: auto;
        }

        .tooltip-inner {
            max-width: 350px;
            /* If max-width does not work, try using width instead */
            width: 350px; 
        }
    </style>
@endpush

@section('topic-menu')
@endsection


@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{  __('Information regarding unemployment benefits') }}</h4>
                <form id="report-search">
                    <fieldset class="fldset-class">
                        <legend class="legend-class">{{  __('Search') }}</legend>
                        <div class="row col-lg-12 pl-5">
                            <div class="col-lg-4 col-sm-12">
                                <div class="form-group" title="ค้นหาจาก เลขขึ้นทะเบียน/ หมายเลขบัตรประชาชน">
                                    <label for=""> {{  __('Search Label') }} : </label>
                                    <input type="text" class="form-control" name="criteria" id="criteria" data-toggle="tooltip" data-bs-placement="top" title="กรุณาระบุเลขที่ขึ้นทะเบียน หรือเลขบัตรประจำตัวประชาขน">
                                </div>
                            </div>
                            

                            <div class="col-lg-4 col-sm-12 align-self-end pb-3">
                                <button class="btn btn-major waves-effect waves-light" type="submit"><i class="fas fa-search"></i> {{  __('Search') }}</button>
                            </div>
                        </div>
                    </fieldset>
                </form>

                <div class="table-responsive">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body" id="info-block">
                                    <div class="row"><span style="padding-top: 10px;"><strong> ข้อมูลผู้ประกันตน </strong></span> </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            ชื่อ - นามสกุล : <span id="fName"></span> <span id="lName"></span>
                                            <br>เลขขึ้นทะเบียน : <span id="rNumber"></span>
                                        </div>
                                        <div class="col-6 text-right">
                                            <button class="btn btn-major waves-effect waves-light view-benefit" type="button">ดูข้อมูลการขึ้นทะเบียนกรณีว่างงาน</button>
                                        </div>
                                    </div>
                                   
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <table id="report_benefit_table" class="table table-striped table-bordered head_table">
                        <thead>
                            <tr class="footable-filtering" style="background-color: #cbd9f7">
                                <th>{{ __('Number of Time') }}</th>
                                <th> {{ __('Due Date') }} </th>
                                <th> {{ __('Reporting Date') }} </th>
                                <th> {{ __('Pay Status') }} </th>
                            </tr>
                        </thead>
                        <tbody style="<?=$bStyle?>">
                            <div class="tbody-table">
                                @include('components.benefit-table')
                            </div>
                        </tbody>
    
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="benefit-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-body">
            ...
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')
    <!--This page JavaScript -->
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/datatable/datatable-basic.init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/utils.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/toastr/dist/build/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/toastr/toastr-init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/benefit.init.js') }}"></script>
@endpush