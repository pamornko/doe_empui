@extends('layout.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/accordion.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
    <style scoped>
        .aspect-input {
            display: none !important;
        }

        .rightDiv
        {
      
            color: #000;
            height: 100%;
            
            float: right;
            overflow: auto;
        }
    </style>
@endpush

@section('topic-menu')
@endsection


@section('content')

<div>

    <div class="card">
        <div class="card-body">
            <h4 class="card-title">{{  __('Information regarding unemployment benefits') }}</h4>
            <h6 class="card-subtitle"></h6>
           
            @if (!empty($Registers))
            <div class="accordion" id="accordionTable">
                @foreach ($Registers as $Register)
                    <div class="card">
                        <div class="card-header" id="heading{{$loop->iteration}}">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#col{{$loop->iteration}}" aria-expanded="true" aria-controls="col{{$loop->iteration}}">
                                    <i class="far fa-bookmark"></i> {{  __('Registration number') }} : <code class="text-success"> {{ $Register['RegisterNumber'] }}</code> (เมื่อวันที่ {{ show_thai_date($Register['RegisterDate']) }})
                                </button>
                            </h5>
                        </div>
                        <div id="col{{$loop->iteration}}" class="collapse show" aria-labelledby="heading{{$loop->iteration}}"
                            data-parent="#accordionTable">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table 
                                       
                                        class="table table-bordered m-b-0 toggle-arrow-tiny head_table"
                                        data-filtering="true" data-paging="true" data-sorting="true"
                                        data-paging-size="10">
                                        <thead>
                                            <tr class="footable-filtering">
                                                <th style="<?=$bStyle?>">{{ __('Number of Time') }}</th>
                                                <th style="<?=$bStyle?>"> {{ __('Due Date') }} </th>
                                                <th style="<?=$bStyle?>"> {{ __('Reporting Date') }} </th>
                                                <th style="<?=$bStyle?>"> {{ __('Pay Status') }} </th>
                                                <th style="<?=$bStyle?>"> {{ __('Amount') }} </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($Register['Reportings'] as $Reporting)
                                            <tr style="<?=$bStyle?>">
                                                <td style="<?=$bStyle?>"> {{$loop->iteration}} </td>
                                                <td style="<?=$bStyle?>"> {{ show_thai_date($Reporting['ReportingDueDate']) }}</td>
                                                <td style="<?=$bStyle?>"> <?=$Reporting['ReportingDate'] ?  show_thai_date($Reporting['ReportingDate'])  : '' ?> </td>
                                                <td style="<?=$bStyle?>">
                                                    @if ($Reporting['payNo'])
                                                            <!--<span class="label label-table label-success">จ่ายแล้ว</span>-->
                                                        <span>{{ $Reporting['financeStatusDesc'] ?? "" }}</span>
                                                    @else  
                                                        @if ($Reporting['ReportingDate']) 
                                                            <span>อยู่ระหว่างพิจารณาโดยสำนักงานประกันสังคม</span>
                                                        @endif
                                                    @endif
                                                </td>
                                                <td style="<?=$bStyle?>">
                                                    @if (!empty($Reporting['Amount']))
                                                        {{ number_format( $Reporting['Amount'], 2) }}

                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                
            </div>
            @else
                <div class="mt-5 col-lg-12" style="text-align: center">
                    - ไม่พบข้อมูล -
                </div>
                
            @endif
        </div>
    </div>
</div>

@endsection


@push('scripts')
    <!--This page JavaScript -->
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/datatable/datatable-basic.init.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            
        });
    </script>
@endpush