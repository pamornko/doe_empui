
<!-- Custom CSS -->
<link rel="stylesheet" href="{{ asset('css/style.min.css?=[timestamp]') }}">
<link rel="stylesheet" href="{{ asset('css/custom.css?=[timestamp]') }}">
<link rel="stylesheet" href="{{ asset('js/libs/sweetalert2/dist/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/libs/jquery-loadingModal/jquery.loadingModal.css') }}">

<style scoped>
    table.table-bordered.dataTable th, table.table-bordered.dataTable td {
        border: 1px solid #aaaaaa;
    }

    .table-no-align-right {
        text-align: right;
        padding-right: 20px;
    }

    .table-text-align-center {
        text-align: center;
    }

    thead, th {text-align: center;}

    .custom-date-picker {
        width: 100%;
        border: 1px solid #e9ecef;
        height: calc(1.5em + .75rem + 2px);
        font-size: .875rem;
        font-weight: 400;
        line-height: 1.5;
        color: #4f5467;
        padding: .375rem .75rem;
    }
</style>

@stack('styles')