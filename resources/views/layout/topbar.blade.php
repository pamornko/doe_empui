<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-lg navbar-dark">
        <div class="navbar-header">
            <!-- This is for the sidebar toggle which is visible on mobile only -->
            <a class="nav-toggler waves-effect waves-light d-block d-lg-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <a class="navbar-brand" href="index.html">
                <!-- Logo icon -->
                <b class="logo-icon">
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <!--<img src="{{ asset('images/logo-icon.png') }}" alt="homepage" class="dark-logo" />-->
                    <!-- Light Logo icon -->
                    <img src="{{ asset('images/logo.png') }}" alt="homepage" class="light-logo" /> 
                    
                </b>
                ระบบขึ้นทะเบียนและรายงานตัวผู้ประกันตนกรณีว่างงาน
                <!--End Logo icon -->
                
            </a>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Toggle which is visible on mobile only -->
            <!-- ============================================================== -->
            <a class="topbartoggler d-block d-lg-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse collapse" id="navbarSupportedContent">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto">
                <!-- <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li> -->
                <!-- ============================================================== -->
               
            </ul>
            <!-- ============================================================== -->
            <!-- Right side toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav">
                <!-- ============================================================== -->
                <!-- create new -->
                <!-- ============================================================== -->
                <!--<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="flag-icon flag-icon-us"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right  animated bounceInDown" aria-labelledby="navbarDropdown2">
                        <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-us"></i> English</a>
                        <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a>
                        <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-es"></i> Spanish</a>
                        <a class="dropdown-item" href="#"><i class="flag-icon flag-icon-de"></i> German</a>
                    </div>
                </li>-->
                <!-- ============================================================== -->
                <!-- Comment -->
                <!-- ============================================================== -->
                
               
                <!-- ============================================================== -->
                <!-- End Comment -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('images/users/1.jpg') }}" alt="user" class="rounded-circle" width="31"></a>
                    <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                        <span class="with-arrow"><span class="bg-light"></span></span>
                        <div class="d-flex no-block align-items-center p-15 bg-light text-primary m-b-10">
                            
                                <div class=""><img src="{{ asset('images/users/1.jpg') }}" alt="user" class="img-circle" width="60"></div>
                                <div class="row m-l-10">
                                    <h5 class="m-b-0">{{ $Name ?? '' }}</h5>
                                    <p class=" m-b-0">{{ $Email ?? '' }}</p>
                                    @if ($UserTypeID == $UserType)
                                        @if($BirthDate)
                                        <h6 class="m-b-0 text-muted"><small>วัน/เดือน/ปี เกิด : </small>{{ $BirthDate ?? '' }}</h6>
                                        @endif
                                        @if($Age)
                                        <h6 class="m-b-0 text-muted"><small>อายุ : </small>{{ $Age ?? '' }} ปี</h6>
                                        @endif
                                    @endif
                                </div>
                            
                            
                        </div>
                       
                        @if ($UserTypeID == $UserType)
                        <div class="row m-l-10">
                           
                            <span><small>ที่อยู่ : {{ empty(trim($Address))  ? '- ไม่ระบุ -' : trim($Address)}}</small></span>
                        </div>
                        <div class="row m-l-10">
                            <span><small>สถานที่ทำงาน : {{ $EmployerName ?? '- ไม่ระบุ -'}}</small></span>
                        </div>
                        
                        <div class="dropdown-divider"></div>
                        <!--<a class="dropdown-item" href="/profile"><i class="far fa-user m-r-5 m-l-5"></i>{{  __('My Profile') }}</a>
                       
                        <a class="dropdown-item" href="{{ get_sjc_domain() }}INF/MultiMedia" target="_self"><i class="fas fa-download m-r-5 m-l-5"></i>ดาวน์โหลดเอกสาร</a>
                        <div class="dropdown-divider"></div>-->
                        @endif
                       
                        <a class="dropdown-item" href="{{ get_datacenter_domain() }}login.do?cmd=goHome"><i class="fas fa-share-square"></i> ระบบบริการประชาชน (e-Service)</a>

                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item btn-logout" href="javascript:void(0);"><i class="fa fa-power-off m-r-5 m-l-5"></i>{{  __('Logout') }} </a>
                        <div class="dropdown-divider"></div>
                       
                    </div>
                </li>
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
            </ul>
        </div>
    </nav>
</header>