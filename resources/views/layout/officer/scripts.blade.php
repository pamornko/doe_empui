<script type="text/javascript" src="{{ URL::asset('js/libs/jquery/dist/jquery.min.js') }}"></script>

<!-- Bootstrap tether Core JavaScript -->
<script type="text/javascript" src="{{ URL::asset('js/libs/popper.js/dist/umd/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- apps -->
<script type="text/javascript" src="{{ URL::asset('js/app.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/app.init.horizontal.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/app-style-switcher.js') }}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script type="text/javascript" src="{{ URL::asset('js/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/extra-libs/sparkline/sparkline.js') }}"></script>
<!--Wave Effects -->
<script type="text/javascript" src="{{ URL::asset('js/waves.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/sidebarmenu.js') }}"></script>
<!--Custom JavaScript -->
<script type="text/javascript" src="{{ URL::asset('js/feather.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/custom.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>

@stack('scripts')