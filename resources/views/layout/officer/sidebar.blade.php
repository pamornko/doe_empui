<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                
                @foreach ($Menus as $Menu)  
                        
                        @if (empty($Menu['MenuChilds[]']))
                            <li class="sidebar-item"> <a class="sidebar-link two-column waves-effect waves-dark" href="{{ $Menu['MenuPath'] ?? ''}}" aria-expanded="false"><i data-feather="{{ $Menu['MenuIcon'] ?? '' }}" class="feather-icon"></i><span class="hide-menu">{{ $Menu['MenuName'] ?? ''}} </span></a>

                        @else 
                            
                            <li class="sidebar-item mega-dropdown"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i data-feather="{{ $Menu['MenuIcon'] ?? '' }}" class="feather-icon"></i><span class="hide-menu">{{ $Menu['MenuName'] ?? '' }} </span></a>
                                <ul aria-expanded="false" class="collapse first-level">
                                    @foreach ($Menu['MenuChilds[]'] as $Child)  
                                    <li class="sidebar-item"><a href="{{ $Child['MenuPath'] ?? ''}}" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> {{ $Child['MenuName'] }}</span></a></li>
                                    
                                    @endforeach
                                    
                                    <!--<li class="sidebar-item"><a href="/report_officer_summary_insured_nation_wide" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> {{  __('Report insurer') }} ({{  __('Nationwide') }})</span></a></li>
                                    <li class="sidebar-item"><a href="/report_officer_summary_insured_province" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> {{  __('Report insurer') }} ({{  __('Province') }})</span></a></li>
                                    <li class="sidebar-item"><a href="/report_officer_summary_insured_personal" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> {{  __('Report insurer') }} ({{  __('Individual') }})</span></a></li>
                                    <li class="sidebar-item"><a href="#" class="sidebar-link"><span class="hide-menu"> . </span></a></li>
                                    <li class="sidebar-item"><a href="#" class="sidebar-link"><span class="hide-menu"> . </span></a></li>-->
                                </ul>
                            </li>
                        
                        @endif
                    </li>
                @endforeach
                
                <!--<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="/register-officer" aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span class="hide-menu">{{  __('Unemployed Register') }} </span></a>
                    
                </li>
                 
              
                
                <li class="sidebar-item"> <a class="sidebar-link two-column waves-effect waves-dark" href="/reporting-officer" aria-expanded="false"><i data-feather="user" class="feather-icon"></i><span class="hide-menu">{{  __('Reporting') }} </span></a>
                    
                </li>

                <li class="sidebar-item"> <a class="sidebar-link two-column waves-effect waves-dark" href="/report-officer-benefit" aria-expanded="false"><i data-feather="book-open" class="feather-icon"></i><span class="hide-menu">{{  __('Unemployment benefits') }} </span></a>
                    
                </li>

                <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">UI</span></li>
                <li class="sidebar-item mega-dropdown"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i data-feather="file-text" class="feather-icon"></i><span class="hide-menu">{{  __('Report') }} </span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        
                        <li class="sidebar-item"><a href="/report-officer-summary-insured-services" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> {{  __('Summary of insured services in the event of unemployment') }}</span></a></li>
                        
                        <li class="sidebar-item"><a href="/report_officer_summary_insured_nation_wide" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> {{  __('Report insurer') }} ({{  __('Nationwide') }})</span></a></li>
                        <li class="sidebar-item"><a href="/report_officer_summary_insured_province" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> {{  __('Report insurer') }} ({{  __('Province') }})</span></a></li>
                        <li class="sidebar-item"><a href="/report_officer_summary_insured_personal" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> {{  __('Report insurer') }} ({{  __('Individual') }})</span></a></li>
                        <li class="sidebar-item"><a href="#" class="sidebar-link"><span class="hide-menu"> . </span></a></li>
                        <li class="sidebar-item"><a href="#" class="sidebar-link"><span class="hide-menu"> . </span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i data-feather="grid" class="feather-icon"></i><span class="hide-menu">การบันทึกการใช้งาน </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item"><a href="index.html" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> การเข้าใช้ระบบ </span></a></li>
                        <li class="sidebar-item"><a href="index2.html" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> การทำรายการ </span></a></li>
                        
                    </ul>
                </li>-->
               
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>