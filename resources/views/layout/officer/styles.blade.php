
<!-- Custom CSS -->
<link rel="stylesheet" href="{{ asset('css/style.min.css?=[timestamp]') }}">
<link rel="stylesheet" href="{{ asset('css/custom.css?=[timestamp]') }}">

@stack('styles')