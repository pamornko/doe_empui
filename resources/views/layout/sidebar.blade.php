<?php

//dump($Menus);
?>
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li class="sidebar-item"> <span style="padding-left: 20px;"></span></li>
                @php
                    $Menus = session('Menus');
                @endphp

                @if (!is_null($Menus) && !empty($Menus) && count($Menus) > 0)
                    @foreach ($Menus as $Menu)  
                    
                            @if (empty($Menu['MenuChilds[]']))
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="{{ $Menu['MenuPath'] ?? ''}}" aria-expanded="false"><i data-feather="{{ $Menu['MenuIcon'] ?? '' }}" class="feather-icon"></i><span class="hide-menu">{{ $Menu['MenuName'] ?? ''}} </span></a></li>
                            @else 
                                
                                <li class="sidebar-item {{ $Menu['MenuColumn'] }}"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i data-feather="{{ $Menu['MenuIcon'] ?? '' }}" class="feather-icon"></i><span class="hide-menu">{{ $Menu['MenuName'] ?? '' }} </span></a>
                                    <ul aria-expanded="false" class="collapse {{$Menu['MenuClass'] ?? '' }}">
                                        @foreach ($Menu['MenuChilds[]'] as $Child)  
                                        <li class="sidebar-item"><a href="{{ $Child['MenuPath'] ?? ''}}" class="sidebar-link"><i class="mdi mdi-toggle-switch" style="font-size: 14px;"></i><span class="hide-menu"> {{ $Child['MenuName'] }}</span></a></li>
                                        
                                        @endforeach

                                    </ul>
                                </li>
                            
                            @endif
                        
                    @endforeach
                @endif
                
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>