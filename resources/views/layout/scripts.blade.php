<script type="text/javascript" src="{{ URL::asset('js/libs/jquery/dist/jquery.min.js') }}"></script>

<!-- Bootstrap tether Core JavaScript -->
<script type="text/javascript" src="{{ URL::asset('js/libs/popper.js/dist/umd/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- apps -->
<script type="text/javascript" src="{{ URL::asset('js/app.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/app.init.horizontal.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/app-style-switcher.js') }}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script type="text/javascript" src="{{ URL::asset('js/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/extra-libs/sparkline/sparkline.js') }}"></script>
<!--Wave Effects -->
<script type="text/javascript" src="{{ URL::asset('js/waves.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('js/sidebarmenu.js') }}"></script>
<!--Custom JavaScript -->
<script type="text/javascript" src="{{ URL::asset('js/feather.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/custom.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/libs/jquery-loadingModal/jquery.loadingModal.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/libs/moment/min/moment.min.js') }}"></script>


@if(!empty(Session::get('error_code')) && Session::get('error_code') == 5)
    <script>
        $(function() {
            Swal.fire({
                title: "ไม่สามารถขึ้นทะเบียน กรณีว่างงานได้",
                text: "กรุณาตรวจสอบสถานะการเป็นผู้ประกันตนมาตรา 33 กับสายด่วนสำนักงานประกันสังคม หมายเลข 1506 กด 1",
                type: "error",
                button: "ตกลง",
            });
                                    
        });
    </script>
@endif


<script type="text/javascript">
    $(function () {
        $('[data-bs-toggle="tooltip"]').tooltip()
    })

    function showLoadingModal() {
        $('body').loadingModal({
            position: 'center',
            text: 'กำลังโหลด ...',
            color: '#fff',
            opacity: '0.7',
            backgroundColor: 'rgb(0,0,0)',
            animation: 'rotatingPlane',
            hide : true
        });
        $('body').loadingModal('show');
    }

    function hideLoadingModal() {
        $('body').loadingModal('hide');
    }

    $(document).on("click", ".btn-logout", function () {
        const swalButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'mr-2 btn btn-danger'
            },
            buttonsStyling: false,
        });
        
        swalButtons.fire({
            title: 'คุณต้องการออกจากระบบ หรือไม่?',
            text: "",
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ยกเลิก',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                window.location = '/logout';
            }
        });
    });
    
</script>

@stack('scripts')