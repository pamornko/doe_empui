@extends('layout.master')

@push('styles')

    <link href="js/libs/jquery-steps/jquery.steps.css" rel="stylesheet">
    <link href="js/libs/jquery-steps/steps.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/accordion.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/prism/prism.css') }}">
    <link rel="stylesheet" href="{{ asset('js/libs/footable/css/footable.bootstrap.min.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('css/jobs.css') }}">

    <link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">

    <style scoped>
        .wizard > .content > .body ul {
            list-style: none !important;
        }

        .tab-pane {
            display: none;
            border: 1px solid transparent !important;
            border-color: #ffffff #dee2e6 #dee2e6 #dee2e6 !important;
            height: 100%;
        }

        .outerDiv
        {
            color: #fff;
            height: 500px;
            width: 100%;
            margin: 0px auto;
            /*border: 1px solid #e9ecef;*/
            padding-left: 15px;

        }
        .leftDiv
        {

            color: #000;
            height: 100%;
            width: 265px;
            float: left;
            overflow: auto;
            border-right: 1px solid #e9ecef;
            /*padding-left: 0px;*/
            overflow-x: hidden;
        }
        .rightDiv
        {
      
            color: #000;
            height: 100%;
            width: calc(100% - 265px);
            float: right;
            overflow: auto;
            overflow-x: hidden;
            padding-right: 10px;
        }

        .job-widget {
            width : 90%;
            padding: 1rem;
        }

        .job {
            position:relative; 
            /*padding: 1rem;*/
        }
        
        .job:hover,  .job.active {
            background: rgba(0, 0, 0, 0.05) !important;
        }

        /*.product-detail {
            max-width: 120px;
            min-width: 120px;
        }*/

        .card {
            margin-bottom: 0px;
        }

        .card-body {
            flex: 1 1 auto;
            min-height: 1px;
            /*padding: 0.5rem !important;*/
            
        }

        .job-short-area {
            border-top: 1px solid #e9ecef;
            border-bottom: 1px solid #e9ecef;
        }

        .job-short-area:hover,  .job-short-area.active {
            background: rgba(0, 0, 0, 0.05) !important;
        }

        .top-content {
            border-bottom: 1px solid #e9ecef;
        }

        .bottom-section {
            
            display: flex;                   /* defines flexbox */
            align-items: flex-end;   
        }

        .bottom-aligner {
            display: inline-block;
            height: 100%;
            vertical-align: bottom;
            width: 0px;
        }

        .bottom-content {
            font-size: 12px;
            padding-left: 1rem;
            position:absolute;                  
            bottom:0;                          
            left:0;  
            padding-bottom: 0.5rem;
        }

        .aspect-input {
            display: none !important;
        }

        .search-content {
            padding: 1rem;
            background-color: #f9d7e7;
            width: 100%;
            height: 220px;
        }

        .txt-job-content {
            color : #555555;
        }

        .freelance {
            border-collapse: collapse;
            width: 100%;
            margin: 25px 10px 25px 200px;
            font-size: 0.9em;
            min-width: 400px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
            
        }

        .freelance thead tr {
            /*background-color: #009879;*/
            /*color: #ffffff;*/
            text-align: left;
        }

        .e-hide {
            display:none !important;
        }

        .highlight-border {
            border: 1px solid #f62d51;
            border-color: #f62d51;
        }

        .wizard > .actions > ul > li:hover {
            border-radius: 4px;
            background-color: #0018f9;
            opacity: 0.8;
        }

        .tooltip-inner {
            max-width: 350px;
            /* If max-width does not work, try using width instead */
            width: 350px; 
        }

        .freelance-table table, th, td 
        {   
            border: 1px solid #D8D8D8;
            border-collapse: collapse;
            padding: 5px;
        }

        .is-invalid {
            border-color: #f62d51 !important;
        }

    </style>
@endpush

@section('topic-menu')
@endsection


@section('content')
<?php

    //dump($JobQualifications);
    /*dump($JobsSuggestions);
    echo "=======================================";
    dump($JobQualifications);*/
?>


<input type="hidden" value="{{ $CntFl }}" id="cntFreelance">
<input type="hidden" id="jobId">
<div class="col-12">
    <div class="card">
        <div class="card-body wizard-content">
          
            <h4 class="card-title pt-3 pl-3">{{  __('Unemployed Register') }}</h4>
            <h6 class="card-subtitle font-12 pl-3">กรุณากรอกข้อมูลให้ครบถ้วน</h6>
            <input type="hidden" id="isWarning" value="0" >
            <form class="validation-wizard wizard-circle mt-3" enctype='multipart/form-data' method="post" >

                <!-- Step 1 -->
                <h6>{{  __('Unemployed Register') }}</h6>
                <section>
                   
                    <div class="pl-3 pr-3">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    
                                    <label for="wResignCaseID"> {{  __('Reason For Leaving Work') }} : <span class="danger-required">*</span> </label>
                                    <select class="custom-select form-control required" id="wResignCaseID" name="ResignCaseID" onchange="manageResignOther(this)" required>
                                        <option value="">กรุณาเลือก</option>
                                        @foreach ($ResignCases as $ResignCase)
                                            <option value='{"id":"{{ $ResignCase['ResignID'] ?? '' }}","other":"{{ $ResignCase['isOther'] ?? ''}}"}'>{{ $ResignCase['ResignName'] ?? '' }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                
                                    <label for="wLayoffCaseID-option" id="label-resign-case" style="display: none"></label>
                                    <div id="resign-case-area" style="display: none">
                                        @include('components.resign-case-dropdown')
                                    </div>
                                    <div id="other-resign-case-area" style="display: none">
                                        <input type="text" class="form-control" id="ResignOther" name="ResignOther"  required> 
                                        <input type="hidden" id="ResignID" name="ResignID">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12"> 
                                <div class="form-group">
                                    <label for="wResignDate"> {{  __('Retire Date') }} : <span class="danger-required">*</span> </label>
                                   
                                    <input name="ResignDate" class="custom-date-picker" type="text" id="wResignDate" value="{{  $ResignDate ??'' }}" required readonly>
 
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 pr-0">
                                <div class="row col-md-12 pr-0">
                                    <div class="col-md-9 col-sm-6 pl-0 pr-0">
                                        <div class="form-group">
                                            <label for="wEstablishment">เลขนิติบุคคลของบริษัท : </label>
                                            <input type="text" class="form-control" data-bs-toggle="tooltip" data-toggle="tooltip" data-bs-placement="top" title="กรอกเลขนิติบุคคล แล้วคลิ๊กที่ปุ่มตรวจสอบ เพื่อให้ระบบดึงชื่อนายจ้างและที่อยู่สถานประกอบการ มาแสดงให้อัตโนมัติ" id="EmployerCode" name="EmployerCode" value="{{ $EmployerName ?? '' }}"> 
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 pr-0">
                                        <div class="form-group pt-4">
                                           
                                            <button class="btn btn-major waves-effect waves-light" id="btnDBD" type="button" style="width: 100%;margin-top:3px;margin-right:0px;"><span class="btn-label"><!--<i class="fa fa-plus"></i>--> ตรวจสอบนายจ้าง </span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wEstablishment">{{  __('Establishment Name') }} : <span class="danger-required">*</span></label>
                                    <input type="text" class="form-control" id="wEstablishment" name="EmployerName" value="{{ $EmployerName ?? '' }}" required data-toggle="tooltip" data-bs-placement="top" title="หากทราบเลขนิติบุคคลของบริษัท ให้ทำการกรอกแล้วคลิ๊กที่ปุ่มตรวจสอบ ระบบจะแสดงข้อมูลให้อัตโนมัติ"> 
                                    
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wTypeOfBusiness">{{  __('Type of Business') }} : <span class="danger-required">*</span></label>
                                    <input type="text" class="form-control required" id="wTypeOfBusiness" name="TypeOfBusiness" required data-toggle="tooltip" data-bs-placement="top" title="หากทราบเลขนิติบุคคลของบริษัท ให้ทำการกรอกแล้วคลิ๊กที่ปุ่มตรวจสอบ ระบบจะแสดงข้อมูลให้อัตโนมัติ">
                                   
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wResignPosition"> {{  __('Position For Lay Off') }} : <span class="danger-required">*</span> </label>
                                    <input type="text" class="form-control required" id="wResignPosition" name="ResignPosition" required> </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wSalaryID">{{  __('Latest Salary') }} : <span class="danger-required">*</span> </label>
                                    <select class="custom-select form-control required" id="wSalaryID" name="SalaryID" required>
                                        <option value="">กรุณาเลือก</option>
                                        @foreach ($Salarys as $Salary)
                                            <option value="{{  $Salary['SalaryID'] }}">{{ $Salary['SalaryDesc'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                       

                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="channelId">ข่องทางการรับเงิน : <span class="danger-required">*</span> </label>
                                    <select class="custom-select form-control required" id="channelId" name="channelId" onchange="manageChannelTransfer(this.value)" required>
                                        
                                            <option value="">กรุณาเลือกช่องทางการรับเงิน</option>
                                            <option value="0">ธนาคาร</option>
                                            {{--  <option value="1">พร้อมเพย์ (หมายเลขบัตรประชาชน)</option>  --}}
                                        
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6" id="promtpayId" style="display:none" title="กรุณาตรวจสอบความถูกต้องของหมายเลขพร้อมเพย์ หากข้อมูลผิดพลาด มีผลต่อการโอนเงินผลประโยชน์ทดแทนกรณีว่างงาน">
                                <label for="wBankID">บัญชีพร้อมเพย์ : <span class="danger-required">*</span></label>
                                <input type="text" class="form-control" id="wPromtpayNumber" name="PromtpayNumber" value="{{ $PersonalID ?? ''}}" required readonly>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-md-6" id="zoneBankId" style="display:none">
                                <div class="form-group">
                                    <label for="wBankID">{{  __('Bank Name') }} : <span class="danger-required">*</span></label>
                                    <select class="custom-select form-control" id="wBankID" name="BankID" required>
                                        <option value="">กรุณาเลือก</option>
                                        @foreach ($Banks as $Bank)
                                            <option value="{{  $Bank['BankID'] }}">{{ $Bank['BankName'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6" id="zoneBankNumberId" style="display:none">
                                <div class="form-group">
                                    <label for="wBankAccountNumber"> {{  __('Account Number') }} : <span class="danger-required">*</span></label>
                                    <input type="text" class="form-control numberonly" id="wBankAccountNumber" name="BankAccountNumber" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" id="zoneUploadId" style="display:none" >
                                <div class="form-group">
                                    <label for="wBankAccountNumber"> ไฟล์แนบ : (หน้าสมุดบัญชีธนาคาร) <span class="danger-required">*</span></label>
                                    <input type="file" class="form-control" name="bFile" required>
                                    <span style="font-size: 12px;">
                                        แนบได้เฉพาะไฟล์ PDF และรูปภาพ เท่านั้น
                                    </span>
                                </div>
                               
                                <div style="padding-top: -30px;font-size: 12px;">
                                    
                                </div>
                            </div>
                        </div>

                        <div class="row pt-5">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> {{  __('Establishment address') }}</label>
                                </div>
                            </div>

                            
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="employerAddress"> {{  __('Label Address') }} :</label>
                                    <input type="text" class="form-control" id="employerAddress" name="employerAddress"  data-toggle="tooltip" data-bs-placement="top" title="หากทราบเลขนิติบุคคลของบริษัท ให้ทำการกรอกแล้วคลิ๊กที่ปุ่มตรวจสอบ ระบบจะแสดงข้อมูลให้อัตโนมัติ">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wProvince"> {{  __('Province') }} : <span class="danger-required">*</span> </label>
                                    <select class="custom-select form-control" id="wProvince" name="province" required data-toggle="tooltip" data-bs-placement="top" title="หากทราบเลขนิติบุคคลของบริษัท ให้ทำการกรอกแล้วคลิ๊กที่ปุ่มตรวจสอบ ระบบจะแสดงข้อมูลให้อัตโนมัติ">
                                        <option value="">กรุณาเลือก</option>
                                        @foreach ($Provinces as $Province)
                                            <option value="{{  $Province['ProvinceID'] }}" >{{ $Province['ProvinceName'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wDistrict"> {{  __('Label District') }} : <span class="danger-required">*</span> </label>
                                    <select class="custom-select form-control" id="wDistrict" name="district" required data-toggle="tooltip" data-bs-placement="top" title="หากทราบเลขนิติบุคคลของบริษัท ให้ทำการกรอกแล้วคลิ๊กที่ปุ่มตรวจสอบ ระบบจะแสดงข้อมูลให้อัตโนมัติ">
                                        <option value="">กรุณาเลือก</option>
                                           
                                    </select>
                                </div>
                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wSubDistrict"> {{  __('Label Sub District') }} : </label>
                                    <select class="custom-select form-control" id="wSubDistrict" name="subDistrict" data-toggle="tooltip" data-bs-placement="top" title="หากทราบเลขนิติบุคคลของบริษัท ให้ทำการกรอกแล้วคลิ๊กที่ปุ่มตรวจสอบ ระบบจะแสดงข้อมูลให้อัตโนมัติ">
                                        <option value="">กรุณาเลือก</option>
                                      
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wPostcode"> {{  __('Postcode') }} :</label>
                                    <input type="text" class="form-control" id="wPostcode" name="postcode" data-toggle="tooltip" data-bs-placement="top" title="หากทราบเลขนิติบุคคลของบริษัท ให้ทำการกรอกแล้วคลิ๊กที่ปุ่มตรวจสอบ ระบบจะแสดงข้อมูลให้อัตโนมัติ">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wPhoneNumber"> {{  __('Label Phone Number') }} :</label>
                                    <input type="text" class="form-control" id="wPhoneNumber" name="phoneNumber">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wContactPerson"> {{  __('Label Contact Establishment') }} : </label>
                                    <input type="text" class="form-control" id="wContactPerson" name="contactPerson">
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- Step 2 -->
                <h6>{{  __('Apply for a job') }}</h6>
                <section>
                    <div class="card-body card-apply-job">
                        <h4 class="card-title">สมัครงาน</h4>

                        <div class="row col-lg-12">
                            @include('jobs.job_index')
                        </div> 
                                
                    </div>
                  

                    <div class="row col-12 ml-2">
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="ignoreJobId" name="ignoreJob" required>
                                <label class="custom-control-label" for="ignoreJobId" style="font-size: 18px;">ไม่ประสงค์จะสมัครงาน</label>
                            </div>
                        </div>
                    </div>

                    <div class="row col-12 ml-2 applying-jobs-table">
                    </div>
                    
                </section>
                <!-- Step 3 -->
                <h6>{{  __('Freelance') }}</h6>
                <section>
                    
                    <div class="col-lg-12">
                        
                        <!--<div class="d-flex flex-row bd-highlight">
                            <input type="checkbox" class="form-control" id="haveFreelance" name="haveFreelance" onclick="showFreelance(this)">
                            <label for="haveFreelance">ปัจจุบัน ท่านประกอบอาชีพอิสระอยู่หรือไม่ ?</label>
                            <p class="text-danger">หมายเหตุ : เป็นการกรอกอาชีพอิสระที่คุณทำอยู่</p>
                        </div>-->

                        <div class="form-check">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="haveFreelance" name="haveFreelance" onclick="showFreelance(this)">
                                <label class="custom-control-label" for="haveFreelance" style="font-size: 18px;">ปัจจุบัน ท่านประกอบอาชีพอิสระอยู่หรือไม่ ?</label>
                                <p class="text-danger">หมายเหตุ : เป็นการกรอกอาชีพอิสระที่คุณทำอยู่</p>
                            </div>
                        </div>

                        <div id="freelance-area" style="display: none">
                            <button class="btn btn-major mb-2 ml-6" 
                                id="addBtn" type="button"> 
                                เพิ่มอาชีพอิสระ
                            </button> 
                            <div> 
                                <table class="freelance-table" id="freelance-table" style="width: 100%" style="display: none"> 
                                    <thead> 
                                        <tr> 
                                        <th class="text-center">อาชีพอิสระ</th> 
                                        <th class="text-center">ระบุ (กรณีเลือก อื่นๆ)</th> 
                                        <th class="text-center"></th> 
                                        </tr> 
                                    </thead> 
                                    <tbody id="tbody"> 
                                        <tr id="R1">
                                            <td class="row-index" style="width: 45%"> 
                                                <div class="form-group">
                                                    <select class="form-control custom-fillter-select" data-id="1" id="wintType1" name="wintType[]" style="width: 100%;height: 36px;" onchange="manageOther(this)" required></select>
                                                    <!--<select class="form-control custom-select required" data-id="1" id="wintType1" name="wintType[]" onchange="manageOther(this)" required>
                                                        <option value="">กรุณาเลือกอาชีพอิสระ</option>
                                                        @foreach ($Freelances as $Freelance)
                                                            <option value="{{$Freelance['FreelanceID']}}" other="{{$Freelance['Other']}}">{{$Freelance['FreelanceName']}}</option>
                                                        @endforeach
                                                    </select>-->
                                                </div>
                                            </td> 
                                            <td class="text-center" style="width: 40%"> 
                                                <div class="form-group" style="display:none" id="divwintT1">
                                                    <input type="text" class="form-control required" name="wint[]"> 
                                                </div>
                                            </td>
                                            <td class="text-center" style="width: 5%"> 
                                                <a class="delete-multiple text-danger remove" style="cursor: pointer;"><i class="fas fa-trash font-16 font-medium"></i></a>
                                                
                                            </td> 
                                        </tr>
                                    </tbody> 
                                </table> 
                            </div> 
                        </div>
                    
                    </div>
                            
                </section>
                
            </form>
        </div>
    </div>
</div>

<input type="hidden" id="applyJobsTmp">
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/libs/jquery-steps/build/jquery.steps.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/libs/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/prism/prism.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/libs/moment/moment.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/libs/footable/js/footable.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/tables/footable-init.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/register.init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/jobs-filter.init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/number.init.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/jquery.datetimepicker.full.js') }}"></script>
 
    <script>

    function manageChannelTransfer(val) {
        //console.log('arguments = ',arguments);
        var zBank = document.getElementById('zoneBankId');  
        var zBankNumber = document.getElementById('zoneBankNumberId');  
        var zoneUpload = document.getElementById('zoneUploadId');  
        var promtpay = document.getElementById('promtpayId');
        //comment ไว้ก่อน
        zoneUpload.style.display = 'none';
        
        if (val == "0") {   
            //ธนาคาร
            zBank.style.display = 'block';
            zBankNumber.style.display = 'block';
            zoneUpload.style.display = 'block';

            promtpay.style.display = 'none';
            $('#isWarning').val("1");
        } else if (val == "1") {   
            //พร้อมเพย์
            zBank.style.display = 'none';
            zBankNumber.style.display = 'none';
            zoneUpload.style.display = 'none';
            //x.style.display = 'none';
            promtpay.style.display = 'block';
            $('#isWarning').val("0");
        } else {
            zBank.style.display = 'none';
            zBankNumber.style.display = 'none';
            zoneUpload.style.display = 'none';

            promtpay.style.display = 'none';
        }
    }

    </script>
@endpush
