@extends('layout.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
@endpush

@section('topic-menu')
@endsection


@section('content')

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<!-- basic table -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                
                    <button class="btn btn-major waves-effect waves-light" type="button"  onclick="window.location='{{ URL::route('unemployed-register') }}'"><span class="btn-label"><!--<i class="fa fa-plus"></i>--> {{  __('Unemployed Register') }} </span></button>
                <div class="table-responsive">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>{{ __('No.') }}</th>
                                <th>{{ __('Registration Date') }}</th>
                                <th>{{ __('Registration number') }}</th>
                                <th>{{ __('Table Head Action') }}</th>
                                
                            </tr>
                        </thead>
                        <tbody style="<?=$bStyle?>">
                            @foreach ($Registers as $Register)
                                <tr style="<?=$bStyle?>">
                                    <td>{{$loop->iteration}}</td>
                                    <td> {{ show_thai_date($Register['RegisterDate']) }}</td>
                                    <td>
                                        <div title="{{ __('Click for details') }}">
                                        <a data-toggle="tooltip" data-placement="top" title="คลิ๊กดูรายละเอียด" href="{{ URL::route('register-officer-view' , ['id'=>$Register['RegisterID']]) }}">{{ $Register['RegisterNumber'] }}</a>
                                        </div>
                                    </td>
                                    
                                    <td>
                                        @if ($Register['isActiveRegister'] == 1)
                                        <!--<button class="btn btn-outline-success waves-effect waves-light" download="แบบฟอร์มใบรับรอง" onclick="window.open('{{ Storage::url('Certificate-form.pdf') }}','_blank');"><i class="fas fa-print"></i> {{ __('Print Paper Credentials') }}</button>-->
                                        <button class="btn btn-outline-success waves-effect waves-light download-pdf-file" data-id="{{ $Register['RegisterID'] }}" download="แบบฟอร์ม-สปส.2-01-7" onclick="downloadPDFFile(this)"><i class="fas fa-print"></i> {{ __('Print Paper Action') }}</button>
                                        <button class="btn btn-outline-success waves-effect waves-light" onclick="window.location='{{ URL::route('reporting', ['id'=>$Register['RegisterID']] ) }}'"><i class="fas fa-user"></i> {{ __('Print Paper Report') }}</button>
                                        @else
                                            <i class="text-danger mdi mdi-checkbox-blank-circle"></i> <code>ยกเลิกข้อมูล</code>
                                        @endif
                                        <!--<button type="button" class="btn btn-success" data-toggle="button" aria-pressed="false">
                                            <i class="fa-print" aria-hidden="true"></i>
                                            <span class="text">{{ __('Print Paper Credentials') }}</span>
                                            <i class="ti-print text-active" aria-hidden="true"></i>
                                            <span class="text-active">Success</span>
                                        </button>
                                        <button type="button" class="btn btn-success" data-toggle="button" aria-pressed="false">
                                            <i class="ti-settings text" aria-hidden="true"></i>
                                            <span class="text">{{ __('Print Paper Action') }}</span>
                                            <i class="ti-check text-active" aria-hidden="true"></i>
                                            <span class="text-active">Success</span>
                                        </button>
                                        <button type="button" class="btn btn-success" data-toggle="button" aria-pressed="false">
                                            <i class="ti-settings text" aria-hidden="true"></i>
                                            <span class="text">{{ __('Print Paper Report') }}</span>
                                            <i class="ti-check text-active" aria-hidden="true"></i>
                                            <span class="text-active">Success</span>
                                        </button>-->
                                    </td>
                                
                                </tr>
                            @endforeach
                          
                        </tbody>
                        <!--<tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                            </tr>
                        </tfoot>-->
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')
    <!--This page JavaScript -->
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/datatable/datatable-basic.init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/index.init.js') }}"></script>
@endpush