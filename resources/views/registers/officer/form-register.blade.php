@extends('layout.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/accordion.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">

    <style scoped>
        .aspect-input {
            display: none !important;
        }

        .rightDiv
        {
      
            color: #000;
            height: 100%;
            
            float: right;
            overflow: auto;
        }

        .txt-medium-regular
        {
            font-weight: normal;
        }
    </style>
@endpush

@section('topic-menu')
@endsection

@section('content')
<?php
    //dump($Register);
?>

<form class="needs-validation" novalidate>
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">{{  __('Unemployment registration information') }}</h4>
            <h6 class="card-subtitle"></h6>
    
            <div class="accordion" id="accordionTable">
                <div class="card pt-3">
                    <div class="card-header" id="heading1">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                data-target="#col1" aria-expanded="true" aria-controls="col1">
                                @if ($Action == 'edit')
                                    <i class="fas fa-pencil-alt"></i> {{  __('Edit Data') }}
                                @else 
                                    <i class="far fa-folder"></i> ข้อมูล
                                @endif
                            </button>
                        </h5>
                    </div>
                    <div id="col1" class="collapse show" aria-labelledby="heading1"
                            data-parent="#accordionTable">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        
                                        <!--<div class="form-horizontal">
                                            <div class="card-body">
                                                <div class="row col-12">
                                                    <div class="form-group row col-lg-6">
                                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">First Name</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" id="fname" placeholder="First Name Here">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row col-lg-6">
                                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Last Name</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" id="lname" placeholder="Last Name Here">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="email1" class="col-sm-3 text-right control-label col-form-label">Email</label>
                                                    <div class="col-sm-9">
                                                        <input type="email" class="form-control" id="email1" placeholder="Email Here">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Contact No</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="cono1" placeholder="Contact No Here">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
    
                                        <div class="row col-12 pt-3">
                                            <div class="col-lg-6 col-sm-12">
                                                <h6>{{  __('Name - Surname') }} : <span class="txt-medium-regular pl-2">{{ $Register['Firstname'] ?? $Register['FirstName'] ?? ''}} {{ $Register['Lastname'] ??  $Register['LastName'] ?? ''}}</span></h6>
                                            </div>
                                            <div class="col-lg-6 col-sm-12">
                                                <h6>{{  __('Registration number') }} : <span class="txt-medium-regular pl-2"> {{ $Register['RegisterNumber'] ?? ''}} </span></h6>
                                            </div>
                                            <div class="col-lg-6 col-sm-12">
                                                <h6>{{  __('Registration Date') }} @if ($Action == 'edit') ({{  __('Old') }}) @endif: <span class="txt-medium-regular pl-2">  @if(!empty($Register)) {{ show_thai_date($Register['RegisterDate'], 'd/m/Y') }} @endif </span></h6>
                                            </div>

                                            @if ($Action == 'edit')
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="form-group">
                                                    <label for="RegisterDate" class="text-left control-label col-form-label pl-0">{{  __('Registration Date') }} ({{  __('New') }}) :</label>
                                                    <div>

                                                        <input type="hidden" value="{{ $Register['RegisterID'] ?? '' }}" name="RegisterID" id="RegisterID">
                                                        <input type="text" class="custom-date-picker" name="RegisterDate" id="RegisterDate" required readonly>
                                                        <div class="invalid-feedback">
                                                            กรุณาเลือกวันที่ขึ้นทะเบียน (ใหม่)
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-horizontal col-lg-12">
                                                
                                            </div>   
                                            @endif
                                            
                                            @if ($Action == 'edit' || ($Action == 'view' &&  $Register && $Register['Comment']))
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="Comment">{{  __('Notes to edit') }} : </label>
                                                    <textarea class="form-control" id="exampleTextarea" name="Comment" rows="3" id="Comment" required @if($Action == 'view') disabled @endif>{{  $Register['Comment'] ?? '' }}</textarea>
                                                    <div class="invalid-feedback">
                                                        กรุณาใส่หมายเหตุการแก้ไข
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            @endif
                                        </div>
                                            
                                    </div>   
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>  
    
                <div class="card">
                    <div class="card-header" id="heading2">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                data-target="#col2" aria-expanded="false" aria-controls="col2">
                                <i class="far fa-folder"></i> {{  __('Unemployed Register') }}
                            </button>
                        </h5>
                    </div>
                    <div id="col2" class="collapse" aria-labelledby="heading2"
                            data-parent="#accordionTable">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="row col-lg-12">
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="">{{  __('Reason For Leaving Work') }} : </label>
                                                    <!--<select class="custom-select form-control" disabled>
    
                                                        <option value="India">India</option>
                                                        <option value="USA">USA</option>
                                                        <option value="Dubai">Dubai</option>
                                                    </select>-->
                                                    <input type="text" class="form-control" value=" {{ $Register['ResignName'] ?? '' }} " disabled>
                                                </div>
                                            </div>
    
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="">สาเหตุการ{{ $Register['ResignName'] ?? ''}} : </label>
                                                    @if (!empty($Register['ResignCaseName'])) 
                                                    <input type="text" class="form-control" value=" {{ $Register['ResignCaseName'] ?? '' }} " disabled>
                                                    @endif
                                                    
                                                    @if (!empty($Register['ResignOther'])) 
                                                    <input type="text" class="form-control" value=" {{ $Register['ResignOther'] ?? '' }} " disabled>
                                                    @endif
                                                </div>
                                            </div>
    
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="">{{  __('Retire Date') }} : </label>
                                                    <input type="text" class="form-control" value="{{ show_thai_date($Register['ResignDate'], 'd/m/Y') ?? '' }}" disabled>
                                                </div>
                                               
                                            </div>

                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="wEstablishment">เลขนิติบุคคลของบริษัท : </label>
                                                    <input type="text" class="form-control" id="EmployerCode" name="EmployerCode" value=" {{  $Register['EmployerCode'] ?? '' }}" disabled> 
                                                    
                                                </div>
                                            </div>
    
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="">{{  __('Establishment Name') }} : </label>
                                                    <input type="text" class="form-control" value=" {{ $Register['EmployerName'] ?? '' }} " disabled>
                                                </div>
                                            </div>
    
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="">{{  __('Position For Lay Off') }} : </label>
                                                    <input type="text" class="form-control" value=" {{ $Register['ResignPosition'] ?? '' }} " disabled>
                                                </div>
                                            </div>
    
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="">{{  __('Type of Business') }} : </label>
                                                    <input type="text" class="form-control" value=" {{ $Register['TypeOfBusiness'] ?? '' }} "  disabled>
                                                </div>
                                            </div>
    
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="">{{  __('Latest Salary') }} : </label>
                                                    <input type="text" class="form-control" value=" {{ $Register['SalaryDesc'] ?? '' }} "  disabled>
                                                </div>
                                            </div>
                                           
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="">ข่องทางการรับเงิน :  </label>
                                                    @if (!empty($Register['BankName'])) 
                                                    <input type="text" class="form-control" value="ธนาคาร"  disabled>
                                                    @endif
                                                    @if (!empty($Register['PromtPay'])) 
                                                    <input type="text" class="form-control" value="พร้อมเพย์ (หมายเลขบัตรประชาชน)"  disabled>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm-12 col-md-6">
                                            </div>

                                            @if (!empty($Register['BankName'])) 
                                            <div class="col-sm-12 col-md-6">
                                                <label for="wBankID">{{  __('Bank Name') }} : </label>
                                                <input type="text" class="form-control"  value="{{ $Register['BankName'] ?? ''}}" readonly>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wBankAccountNumber"> {{  __('Account Number') }} :</label>
                                                    <input type="text" class="form-control" value="{{ $Register['BankAccountNumber'] ?? ''}}"  readonly>
                                                </div>
                                            </div>
                                            @else
                                            <div class="col-sm-12 col-md-6">
                                                <label for="wBankID">บัญชีพร้อมเพย์ : </label>
                                                <input type="text" class="form-control"  value="{{ $Register['PromtPay'] ?? ''}}" readonly>
                                            </div>
                                            @endif

                                            <div class="col-lg-12 col-sm-12">
                                                @if (!empty($Register['BankAccountImage'])) 
                                                    ไฟล์แนบหน้าบัญชีธนาคาร : 
                                                    <a href="/download_bookbank?page={{$Register['BankAccountImage']}}">{{ $BankAccountImageName ?? ""}}</a>
                                                @endif
                                            </div>


                                            <div class="col-lg-12 col-sm-12 mt-5">
                                                <div class="form-group">
                                                    <label for="employerAddressId"> {{  __('Establishment address') }} : </label>
                                                    <input type="text" class="form-control"  value="{{ $Register['EmployerAddress'] ?? ''}}" readonly >
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wProvince"> {{  __('Province') }} :</label>
                                                    <input type="text" class="form-control" value="{{ $ProvinceName?? ''}}" disabled>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wProvince"> {{  __('Label District') }} :</label>
                                                    <input type="text" class="form-control" value="{{ $DistrictName?? ''}}" disabled>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wProvince"> {{  __('Label Sub District') }} :</label>
                                                    <input type="text" class="form-control" value="{{ $TambonName?? ''}}" disabled>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wProvince"> {{  __('Postcode') }} :</label>
                                                    <input type="text" class="form-control" value="{{ $Register['Postcode'] ?? ''}}" disabled>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">

                                                    <label> {{  __('Label Phone Number') }} :</label>
                                                    <input type="text" class="form-control" value="{{ $Register['PhoneNumber'] ?? ''}}" disabled>
                                            
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">

                                                    <label> {{  __('Label Contact Establishment') }} :</label>
                                                    <input type="text" class="form-control" value="{{ $Register['ContactPerson'] ?? ''}}" disabled>
                                            
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
    
    
                <div class="card">
                    <div class="card-header" id="heading3">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                data-target="#col3" aria-expanded="false" aria-controls="col3">
                                <i class="far fa-folder"></i> {{  __('Apply for position') }}
                            </button>
                        </h5>
                    </div>
                    <div id="col3" class="collapse" aria-labelledby="heading3"
                            data-parent="#accordionTable">
                       
                        @if (count($ApplyJobs))
                        <div class="table-responsive">
                            @include('components.apply-jobs-table')
                        </div>
                        @else 
                        <div class="pl-5 pt-3">
                            
                            <h5 class="text-danger">- ไม่ประสงค์จะสมัครงาน -</h5>
                        </div>
                        @endif
                    </div>
                </div>
    
                <div class="card">
                    <div class="card-header" id="heading4">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                data-target="#col4" aria-expanded="false" aria-controls="col4">
                                <i class="far fa-folder"></i> {{  __('Freelance') }}
                            </button>
                        </h5>
                    </div>
                    <div id="col4" class="collapse" aria-labelledby="heading4"
                            data-parent="#accordionTable">

                        @if (count($RegisterFreelances))
                            <div class="card-body">
                                @foreach ($RegisterFreelances as $Freelance)
                                <div class="row col-lg-12">
                                    <div class="col-md-6">
                                        
                                        <div class="form-group">
                                            <label for="wintType1">{{  __('Freelance') }} :</label>
                                            <input type="text" class="form-control" value=" {{ $Freelance['FreelanceName'] }}" disabled>
                                        </div>
                                        
                                    </div>
            
                                    <div class="col-md-6">
                                        @if ($Freelance['Other'])
                                        <div class="form-group">
                                            <label for="wint1">อื่นๆ ระบุ :</label>
                                            <input type="text" class="form-control" value="{{ $Freelance['FreelanceOther'] ?? '' }}" disabled> 
                                        </div>
                                        @endif
                                    </div>
                                    
                                </div>
                                @endforeach
                            </div>
                        @else 
                            <div class="pl-5 pt-3">
                                
                                <h5 class="text-danger">- ไม่ระบุอาชีพอิสระ -</h5>
                            </div>
                        @endif
                    </div>
                </div>
    
    
            </div>
        </div>
    </div>
    
    <div class="card-body">
        
        <div class="form-group mb-0 text-right">
            @if ($Action == 'edit')
                <button type="submit" class="btn btn-info waves-effect waves-light">บันทึก</button>
                <button type="button" class="btn btn-dark waves-effect waves-light" onclick="goBack()">ยกเลิก</button>
            @else 
                <button type="button" class="btn btn-dark waves-effect waves-light" onclick="goBack()">กลับ</button>
            @endif
        </div>
       
    </div>
</form>

@endsection

@push('scripts')
    <!--This page JavaScript -->
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/datatable/datatable-basic.init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/validate-register-form.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/jquery.datetimepicker.full.js') }}"></script>

    <script type="text/javascript">

(function() {
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            
            form.addEventListener('submit', function(event) {
                event.preventDefault();
                event.stopPropagation();
                if (form.checkValidity() === true) {
                    var dataString = $(this).serialize();
                    console.log('dataString = ',dataString);
                    
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    
                    $.ajax({
                        type : 'post',
                        url: '{!! URL::to('edit_register')!!}',
                        data: dataString,
                        //dataType: "html",
                        success: function (data) { 
                           
                            console.log('data = ',data);
                            
                            if (data.success) {
                                $('#RegisterID').val(registerId);
                                        //window.history.back();
                                var path = '{!! URL::to('register-officer')!!}';
                                window.location.href = path;
                                var registerId = data.registerId;
                                Swal.fire({
                                    title: "บันทึกข้อมูลสำเร็จ",
                                    text: "ส่งข้อมูลไปยังสำนักงานประกันสังคมเรียบร้อยแล้ว",
                                    type: "success",
                                    button: "ตกลง",
                                    onClose: (data) => {
                                        //window.location.href = base+'?id='+data.id ;
                                        
                                        
                                    }
                                });
                                
                            } else {
                                Swal.fire({
                                    title: "บันทึกข้อมูลไม่สำเร็จ",
                                    text: "กรุณาลองใหม่อีกครั้ง",
                                    type: "error",
                                    button: "ตกลง",
                                });
                                
                            }
                           
                        }
                    });
                }

                form.classList.add('was-validated');

            }, false);
        });
    }, false);
})();
        
        $(document).ready(function(){
            $("#apply-area").show();

            $.datetimepicker.setLocale('th'); // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.

            $(".custom-date-picker").datetimepicker({
                timepicker:false,
                format:'d/m/Y',  // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000            
                lang:'th',  // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
                onSelectDate:function(dp,$input){
                    var yearT=new Date(dp).getFullYear();  
                    var yearTH=yearT+543;
                    var fulldate=$input.val();
                    var fulldateTH=fulldate.replace(yearT,yearTH);
                    $input.val(fulldateTH);
                },
            }); 
        });

        function goBack() {
            window.history.back()
        }
    </script>
@endpush