@extends('layout.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/toastr/dist/build/toastr.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">

    <style scoped>
        .fldset-class{
            border: 1px rgb(233, 236, 239) solid;
        }
        .legend-class{

            margin-left: 1em; 
            padding: 0.2em 0.8em;

            font-size: 1rem;
            width: 65px;
        }

        .rightDiv
        {
      
            color: #000;
            height: 100%;
            width: calc(100%-200px);
            float: right;
            overflow: auto;
        }
    </style>
@endpush

@section('topic-menu')
@endsection


@section('content')

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<!-- basic table -->


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{  __('Reporting Information') }}</h4>
                <form id="reporting-search">
                    <fieldset class="fldset-class">
                        <legend class="legend-class">{{  __('Search') }}</legend>
                        <div class="row col-lg-12 pl-5">
                            <div class="col-lg-4 col-sm-12">
                                <div class="form-group" title="ค้นหาจาก เลขขึ้นทะเบียน/ หมายเลขบัตรประชาชน / ชื่อ สกุล">
                                    <label for=""> {{  __('Search Label') }} : </label>
                                    <input type="text" class="form-control" name="RegisterNumber" id="RegisterNumber">
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12">
                                <div class="form-group">
                                    <label for="wOrganization">สำนักงานกรมการจัดหางาน : </label>
                                    <select class="custom-select form-control" id="wOrganization" name="typeBsiness">
                                        <option value="">กรุณาเลือก</option>
                                        @foreach ($Offices as $Office)
                                            <option value="{{  $Office['OrganizationID'] }}">{{ $Office['OrganizationName'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12">
                                <div class="form-group">
                                    <label for="">การแสดงข้อมูล : </label>
                                    <select class="custom-select form-control" name="Service" id="Service">
                                        <option value="0">กรุณาเลือก</option>
                                        <option value="1">Internet</option>
                                        <option value="2">สำนักงาน</option>
                                    </select>
                                </div>
                            </div>
                          
                            <div class="col-lg-4 col-sm-12 pb-3">
                                <label for="wRayOffDate"> {{  __('Since') }} : </label>
                                <input name="StartDate" class="custom-date-picker" type="text" id="StartDate">
                            </div>

                            <div class="col-lg-4 col-sm-12">
                                <label for="wRayOffDate"> {{  __('To') }} : </label>
                                <input name="EndDate" class="custom-date-picker" type="text" id="EndDate">
                            </div>

                            <div class="col-lg-4 col-sm-12 align-self-end pb-3">
                                <button class="btn btn-major waves-effect waves-light" type="submit"><i class="fas fa-search"></i> {{  __('Search') }}</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                        
                        <div class="col-lg-12 pt-2 pb-4 d-flex justify-content-end">
                            <div class="col-lg-1 col-md-1 pr-0">
                                <button type="button" class="btn btn-block btn-sm btn-secondary" onclick="exportPDF()"><i class="fas fa-file-pdf"></i> {{  __('PDF') }}</button>
                            </div>
                            <div class="col-lg-1 col-md-1 pr-0">
                                <button type="button" class="btn btn-block btn-sm btn-secondary" onclick="exportExcel()"><i class="fas fa-file-excel"></i> {{  __('Excel') }}</button>
                            </div>
                            <div class="col-lg-1 col-md-1 pr-0">
                                <button type="button" class="btn btn-block btn-sm btn-secondary" onclick="print()"><i class="fas fa-print"></i> {{  __('Web') }}</button>
                            </div>
                        </div>
                    
                    
                    
                   
                <div class="table-responsive">
                    <table id="reporting_search_table" class="table table-striped table-bordered">
                        <thead>
                            <tr style="background-color: #cbd9f7">
                                <th>{{ __('No.') }}</th>
                                <th>{{ __('Registration number') }}</th>
                                <th>{{ __('ID Card Number') }}</th>
                                <th>{{ __('Insured Person Name') }}</th>
                                <th> ครั้งที่รายงานตัว </th>
                                <th>กำหนดการรายงานตัว</th>
                                <th>วันที่รายงานตัว</th>
                                <th>{{ __('Table Head Action') }}</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <div class="tbody-table">
                            @include('components.reporting-table')
                            </div>
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection


@push('scripts')
<script type="text/javascript"> 
    //fix
    var config = {
        routes: {
            search: "{{ URL::route('reporting-officer-edit') }}",
            view : "{{ URL::route('reporting-officer-view') }}"
        }
    };
</script>
    <!--This page JavaScript -->
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/datatable/datatable-basic.init.js') }}"></script>
    
    <script type="text/javascript" src="{{ URL::asset('js/utils.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/datatable/datatable-reporting.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/toastr/dist/build/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/toastr/toastr-init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.datetimepicker.full.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/reporting-officer.init.js') }}"></script>

@endpush