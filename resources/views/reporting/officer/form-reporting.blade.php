@extends('layout.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/accordion.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/responsive.dataTables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">

    <style scoped>
        .aspect-input {
            display: none !important;
        }

        .rightDiv
        {
      
            color: #000;
            height: 100%;
            
            float: right;
            overflow: auto;
        }

        .txt-medium-regular
        {
            font-weight: normal;
        }
    </style>
@endpush

@section('topic-menu')
@endsection

@section('content')
<?php
    //dump($Reporting);
    //dump($Work);
?>

<form class="needs-validation" novalidate>
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">{{  __('Reporting Information') }}</h4>
            <h6 class="card-subtitle"></h6>

            <div class="accordion" id="accordionTable">
                <div class="card pt-3">
                    <div class="card-header" id="heading1">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                data-target="#col1" aria-expanded="true" aria-controls="col1">
                                <i class="fas @if ($Action == 'edit') fa-pencil-alt @else fa-folder @endif"></i> @if ($Action == 'edit') {{  __('Edit Data') }} @else ข้อมูลรายงานตัว @endif
                            </button>
                        </h5>
                    </div>

                    <div id="col1" class="collapse show" aria-labelledby="heading1"
                            data-parent="#accordionTable">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">

                                        <div class="row col-12 pt-3">
                                            <div class="col-lg-6 col-sm-12">
                                                <h6>{{  __('Name - Surname') }} : <span class="txt-medium-regular pl-2"> {{ $Reporting['FirstName'] }} {{ $Reporting['LastName'] }}</span></h6>
                                            </div>
                                            <div class="col-lg-6 col-sm-12">
                                                <h6>{{  __('Registration number') }} : <span class="txt-medium-regular pl-2">{{ $Reporting['RegisterNumber'] }}</span></h6>
                                            </div>
                                            <div class="col-lg-6 col-sm-12 pb-2 pt-2">
                                                <h6>{{  __('Number of Time') }} : <span class="txt-medium-regular pl-2">{{ $Reporting['TrackingTime'] }}</span></h6>
                                            </div>
                                            <div class="col-lg-6 col-sm-12">
                                            </div>
                                            <div class="col-lg-6 col-sm-12">
                                
                                                <h6>{{  __('Reporting Date') }} : <span class="txt-medium-regular pl-2">{{  show_thai_date($Reporting['ReportingDate'], 'd/m/Y')   ?? 'ยังไม่ได้รายงานตัว' }}</span></h6>
                                            </div>

                                            @if ($Action == 'edit')
                                                <div class="form-horizontal col-lg-6">
                                                    <div class="form-group row col-12">
                                                        <label for="fname" class="text-left control-label col-form-label pl-0">{{  __('Reporting Date') }} ({{  __('New') }}) :</label>
                                                        <div class="col-sm-6">
                                                            <input type="hidden" value="{{$TrackingID}}" name="TrackingID" id="TrackingID">
                
                                                            <input type="date" class="form-control" name="ReportingDate" id="ReportingDate" required>
                                                            <div class="invalid-feedback">
                                                                กรุณาเลือกวันที่รายงานตัว (ใหม่)
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                            @if ($Action == 'edit')
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="Comment">{{  __('Notes to edit') }} : </label>
                                                        <textarea class="form-control" name="RemarkLate" rows="3" id="RemarkLate" required></textarea>
                                                        <div class="invalid-feedback">
                                                            กรุณาใส่หมายเหตุการแก้ไข
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            @endif

                                            <div class="row col-lg-12 pt-3">
                                                <div class="col-md-6 col-sm-12">
                                                    
                                                    <div class="custom-control custom-radio pl-4">
                                                        <input type="radio" value="0" id="customRadio1" name="workStatus"  class="custom-control-input" {{ ( $Reporting['WorkStatus'] =="0" || $Reporting['WorkStatus'] == null )? "checked" : "" }} @if ($Reporting['WorkStatus'] == null || $Reporting['WorkStatus'] =="1") disabled  @endif>
                                                        <label class="custom-control-label" for="customRadio1"> {{  __('Label Not Working') }} </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                    
                                                    <div class="custom-control custom-radio pl-4">
                                                        <input type="radio" value="1" id="customRadio2" name="workStatus"  class="custom-control-input" {{ ( $Reporting['WorkStatus'] =="1")? "checked" : "" }} @if ($Reporting['WorkStatus'] == null || $Reporting['WorkStatus'] =="0") disabled  @endif>
                                                        <label class="custom-control-label" for="customRadio1"> {{  __('Label Working') }} </label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div id="empInfomationId">

                                            <div class="row col-lg-12 pt-3">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wWorkDate"> {{  __('Label Working Date') }} : </label>
                                                        
                                                        <input type="text" class="form-control" id="wWorkDate" value="{{ $Work['WorkDate'] ?? '' }}" disabled>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                            <div class="row col-lg-12 pt-3">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wOrganization"> เลขนิติบุคคลของบริษัท : <span class="danger-required">*</span> </label>
                                                        <input type="text" class="form-control required" id="EmployerCode" name="EmployerCode" value="{{ $Reporting['EmployerCode'] ?? '' }}" disabled> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wOrganization"> {{  __('Label Organization Name') }} : </label>
                                                        <input type="text" class="form-control required" id="wOrganization"  value="{{ $Reporting['EmployerName'] ?? ''}}" disabled> 
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row col-lg-12 pt-5">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wWorkDate"> {{  __('Establishment address') }}</label>
                                                    </div>
                                                </div> 
                                            </div>


                                            <div class="row col-lg-12">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="wWorkAddress"> {{  __('Label Address') }} :</label>
                                                        <input type="text" class="form-control" id="wWorkAddress" value="{{ $Reporting['EmployerAddress'] ?? ''}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                    
                                            <div class="row col-lg-12">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wProvince"> {{  __('Province') }} :</label>
                                                        <input type="text" class="form-control" id="wProvince" value="{{ $Work['ProvinceName'] ?? ''}}" disabled>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wDistrict"> {{  __('Label District') }} :</label>
                                                        <input type="text" class="form-control" id="wDistrict" value="{{ $Work['DistrictName'] ?? ''}}" disabled>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row col-lg-12">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wSubDistrict"> {{  __('Label Sub District') }} :</label>
                                                        <input type="text" class="form-control" id="wSubDistrict" value="{{ $Work['TambonName'] ?? ''}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wPostcode"> {{  __('Postcode') }} :</label>
                                                        <input type="text" class="form-control" id="wPostcode" value="{{ $Work['PostCode'] ?? ''}}" disabled>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row col-lg-12">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wSubDistrict"> {{  __('Label Phone Number') }} :</label>
                                                        <input type="text" class="form-control" value="{{ $Reporting['PhoneNumber'] ?? '' }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="wPostcode"> {{  __('Label Contact Establishment') }} :</label>
                                                        <input type="text" class="form-control" value="{{ $Reporting['ContactPerson'] ?? '' }}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="heading2">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                data-target="#col2" aria-expanded="false" aria-controls="col2">
                                <i class="far fa-folder"></i> {{  __('Apply for a job') }}
                            </button>
                        </h5>
                    </div>

                    <div id="col2" class="collapse show" aria-labelledby="heading2"
                            data-parent="#accordionTable">

                            @if (count($ApplyJobs))
                                <div class="table-responsive">
                                    @include('components.apply-jobs-table')
                                </div>
                            @else 
                                <div class="pl-5 pt-3">
                                    
                                    <h5 class="text-danger">- ไม่ประสงค์จะสมัครงาน -</h5>
                                </div>
                            @endif
                        
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="heading3">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                data-target="#col3" aria-expanded="true" aria-controls="col3">
                                <i class="far fa-folder"></i> {{  __('Freelance') }}
                            </button>
                        </h5>
                    </div>
                    <div id="col3" class="collapse show" aria-labelledby="heading3"
                            data-parent="#accordionTable">
                        <div class="card-body">
                            
                            @if (count($RegisterFreelances))
                                @foreach ($RegisterFreelances as $Freelance)
                                    <div class="row col-lg-12">
                                        <div class="col-md-6">
                                            
                                            <div class="form-group">
                                                <label for="wintType1">{{  __('Freelance') }} :</label>
                                                <input type="text" class="form-control" value=" {{ $Freelance['FreelanceName'] ?? '' }}" disabled>
                                            </div>
                                            
                                        </div>
                
                                        <div class="col-md-6">
                                            @if ($Freelance['Other'] ?? '')
                                            <div class="form-group">
                                                <label for="wint1">อื่นๆ ระบุ :</label>
                                                <input type="text" class="form-control" value="{{ $Freelance['FreelanceOther'] ?? ''}}" disabled> 
                                            </div>
                                            @endif
                                        </div>
                                        
                                    </div>
                                @endforeach
                            @else 
                                <div class="pl-5 pt-3">
                                
                                    <h5 class="text-danger">- ไม่ระบุอาชีพอิสระ -</h5>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


    <div class="card-body">
        <div class="form-group mb-0 text-right">
            @if ($Action == 'edit')
            <button type="submit" class="btn btn-info waves-effect waves-light">บันทึก</button>
            <button type="button" class="btn btn-dark waves-effect waves-light" onclick="goBack()">ยกเลิก</button>
            @else 
                <button type="button" class="btn btn-dark waves-effect waves-light" onclick="goBack()">กลับ</button>
            @endif
        </div>
    </div>
</form>
@endsection

@push('scripts')
    <!--This page JavaScript -->
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/datatable/datatable-basic.init.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/jquery.datetimepicker.full.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            
            $("#apply-area").show();

            var workStatus = $('input[name="workStatus"]:checked').val();
            if (workStatus == "0") {
                $("#empInfomationId").hide();
                
            }

            $.datetimepicker.setLocale('th'); // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.

            $(".custom-date-picker").datetimepicker({
                timepicker:false,
                format:'d/m/Y',  // กำหนดรูปแบบวันที่ ที่ใช้ เป็น 00-00-0000            
                lang:'th',  // ต้องกำหนดเสมอถ้าใช้ภาษาไทย และ เป็นปี พ.ศ.
                onSelectDate:function(dp,$input){
                    var yearT=new Date(dp).getFullYear();  
                    var yearTH=yearT+543;
                    var fulldate=$input.val();
                    var fulldateTH=fulldate.replace(yearT,yearTH);
                    $input.val(fulldateTH);
                },
            }); 
        });

        (function() {
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            
            form.addEventListener('submit', function(event) {
                event.preventDefault();
                event.stopPropagation();
                if (form.checkValidity() === true) {
                    var dataString = $(this).serialize();
                    console.log('dataString = ',dataString);
                    
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    
                    $.ajax({
                        type : 'post',
                        url: '{!! URL::to('edit_reporting')!!}',
                        data: dataString,
                        //dataType: "html",
                        success: function (data) { 
                           
                            console.log('data = ',data);
                            
                            if (data.success) {
                                var path = '{!! URL::to('reporting-officer')!!}';
                                window.location.href = path;
                                
                                var trackingId = data.trackingId;
                                Swal.fire({
                                    title: "บันทึกข้อมูลสำเร็จ",
                                    text: "ส่งข้อมูลไปยังสำนักงานประกันสังคมเรียบร้อยแล้ว",
                                    type: "success",
                                    button: "ตกลง",
                                    onClose: (data) => {
                                        //window.location.href = base+'?id='+data.id ;
                                        //window.history.back();
                                        $('#TrackingID').val(trackingId);
                                        
                                        //window.history.back();
                                       
                                        
                                    }
                                });
                                
                            } else {
                                Swal.fire({
                                    title: "บันทึกข้อมูลไม่สำเร็จ",
                                    text: "กรุณาลองใหม่อีกครั้ง",
                                    type: "error",
                                    button: "ตกลง",
                                });
                                
                            }
                           
                        }
                    });
                }

                form.classList.add('was-validated');

            }, false);
        });
    }, false);
})();

        function goBack() {
            window.history.back()
        }
    </script>
@endpush