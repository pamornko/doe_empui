@extends('layout.master')

@push('styles')
    <link href="js/libs/jquery-steps/jquery.steps.css" rel="stylesheet">
    <link href="js/libs/jquery-steps/steps.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/accordion.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/prism/prism.css') }}">
    <link rel="stylesheet" href="{{ asset('js/libs/footable/css/footable.bootstrap.min.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('css/jobs.css') }}">

    <link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">

    <style scoped>
        .wizard > .content > .body ul {
            list-style: none !important;
        }

        .tab-pane {
            display: none;
            border: 1px solid transparent !important;
            border-color: #ffffff #dee2e6 #dee2e6 #dee2e6 !important;
            height: 100%;
        }

        .outerDiv
        {
            color: #fff;
            height: 500px;
            width: 100%;
            margin: 0px auto;
            /*border: 1px solid #e9ecef;*/
            padding-left: 15px;

        }
        .leftDiv
        {

            color: #000;
            height: 100%;
            width: 265px;
            float: left;
            overflow: auto;
            border-right: 1px solid #e9ecef;
            padding-left: 5px;
            overflow-x: hidden;
        }
        .rightDiv
        {
      
            color: #000;
            height: 100%;
            width: calc(100% - 265px);
            float: right;
            overflow: auto;
            overflow-x: hidden;
            padding-right: 10px;
        }

        .job-widget {
            width : 90%;
            padding: 1rem;
        }

        .job {
            position:relative; 
            /*padding: 1rem;*/
        }
        
        .job:hover,  .job.active {
            background: rgba(0, 0, 0, 0.05) !important;
        }

        /*.product-detail {
            max-width: 120px;
            min-width: 120px;
        }*/

        .card {
            margin-bottom: 0px;
        }

        .card-body {
            flex: 1 1 auto;
            min-height: 1px;
            /*padding: 0.5rem !important;*/
            
        }

        .job-short-area {
            border-top: 1px solid #e9ecef;
            border-bottom: 1px solid #e9ecef;
        }

        .job-short-area:hover,  .job-short-area.active {
            background: rgba(0, 0, 0, 0.05) !important;
        }

        .top-content {
            border-bottom: 1px solid #e9ecef;
        }

        .bottom-section {
            
            display: flex;                   /* defines flexbox */
            align-items: flex-end;   
        }

        .bottom-aligner {
            display: inline-block;
            height: 100%;
            vertical-align: bottom;
            width: 0px;
        }

        .bottom-content {
            font-size: 12px;
            padding-left: 1rem;
            position:absolute;                  
            bottom:0;                          
            left:0;  
            padding-bottom: 0.5rem;
        }

        .aspect-input {
            display: none !important;
        }

        .search-content {
            padding: 1rem;
            background-color: #f9d7e7;
            width: 100%;
            height: 220px;
        }

        .txt-job-content {
            color : #555555;
        }

        .wizard > .actions > ul > li:hover {
            border-radius: 4px;
            background-color: #0018f9;
            opacity: 0.8;
        }

        .tooltip-inner {
            max-width: 350px;
            /* If max-width does not work, try using width instead */
            width: 350px; 
        }

    </style>
@endpush

@section('topic-menu')
@endsection


@section('content')


<input type="hidden" value="{{ $CntFl ?? '0'}}" id="cntFreelance">
<input type="hidden" id="jobId">
<div class="col-12">
    <div class="card">
        <div class="card-body wizard-content">
            
            <h4 class="card-title pt-3 pl-3">{{  __('Reporting') }}</h4>
            <h6 class="card-subtitle font-12 pl-3">กรุณากรอกข้อมูลให้ครบถ้วน</h6>
            <form class="validation-wizard wizard-circle mt-3">

                <input type="hidden" id="RegisterID" name="RegisterID" value=" {{ $RegisterID ?? '' }}">
                <input type="hidden" id="TrackingID" name="TrackingID" value=" {{ $TrackingID ?? '' }}">
                <input type="hidden" id="ReportingDueDate" name="ReportingDueDate" value=" {{ $Tracking['ReportingDueDate'] ?? '' }}">
                <!-- Step 1 -->
                <h6>{{  __('Reporting') }}</h6>
                <section>
                    <div class="pl-5 pr-5">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wLeavingWork"> {{  __('Registration number') }} : </label> {{ $Tracking['RegisterNumber'] ?? '' }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wLeavingWork"> {{  __('Registration Date') }} : </label> @if(!empty($Tracking)) {{ show_thai_date($Tracking['RegisterDate'], 'd/m/Y') }} @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wLeavingWork"> {{  __('Name - Surname') }} : </label> {{ $Tracking['FirstName'] ?? ''}} {{ $Tracking['LastName'] ?? ''}}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="hidden" name="TrackingTime" value="{{ $TrackingTime ?? ''}}">
                                    <label for="wLeavingWork"> {{  __('Number of Time') }} : </label> {{ $TrackingTime ?? ''}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wRayOffDate"> {{  __('Reporting Date') }} : <span class="danger-required">*</span> </label>
                                    <input type="text" class="form-control" id="wRayOffDate" name="ReportingDate" value="{{ show_thai_date($CurrentDate, 'd/m/Y') }}" required readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                
                            </div>
                        </div>
                        
                        @if ($RequireComment)
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="Comment">หมายเหตุการรายงานตัวล่าช้า : </label>
                                    <textarea class="form-control" name="Comment" rows="3" id="Comment" required ></textarea>
                                    <div class="invalid-feedback">
                                        กรุณาใส่หมายเหตุการรายงานตัวล่าช้ากว่ากำหนด
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        
                        <hr>

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <div class="controls">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" value="0" id="workStatusId1" name="workStatus"  class="custom-control-input" onclick="showOrHide('hide')" required @if($isGetJob == true) disabled @endif>
                                        <label class="custom-control-label" for="workStatusId1">{{  __('Label Not Working') }}</label>
                                        <span style="color: #0018f9;font-size: 12px;"> (หางานแล้ว แต่ยังไม่ได้งาน)</span>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" value="1" id="workStatusId2" name="workStatus" class="custom-control-input" onclick="showOrHide('show')" {{ ( $isGetJob == true)? "checked" : "" }} required>
                                        <label class="custom-control-label" for="workStatusId2">{{  __('Label Working') }} </label>
                                        <span style="color: #0018f9;font-size: 12px;"> (นายจ้างรับเข้าทำงานแล้ว)</span>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <!--<div class="row pb-2">
                            <div class="col-md-6">
                                
                                <div class="custom-control custom-radio pl-4">
                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" onclick="showOrHide('show')">
                                    <label class="custom-control-label" for="customRadio1"> {{  __('Label Not Working') }} </label>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="custom-control custom-radio pl-4">
                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" onclick="showOrHide('hide')">
                                    <label class="custom-control-label" for="customRadio1"> {{  __('Label Working') }} </label>
                                </div>
                            </div>
                        </div>-->

                        <div id="empInfomationId">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    @php
                                        //dump($WorkDate);
                                    @endphp
                                        <label for="wWorkDate"> {{  __('Label Working Date') }} : <span class="danger-required">*</span> </label>
                                        
                                        <input name="workDate" class="custom-date-picker" type="text" id="wWorkDate" value="{{  $WorkDate ??'' }}" required readonly>
 
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                               
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="wOrganization"> {{  __('Label Organization Name') }} : <span class="danger-required">*</span> </label>
                                        <input type="text" class="form-control required" id="wOrganization" name="organization" value="{{ $organization }}" required  @if($organization != null) readonly @endif> 
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12 pr-0">
                                    <div class="row col-md-12 pr-0">
                                        <div class="col-md-9 col-sm-6 pl-0 pr-0">
                                            <div class="form-group">
                                                <label for="wEstablishment">เลขนิติบุคคลของบริษัท : </label>
                                                <input type="text" class="form-control" id="EmployerCode" name="EmployerCode" value="{{ $EmployerName ?? '' }}" data-bs-toggle="tooltip" data-toggle="tooltip" data-bs-placement="top" title="กรอกเลขนิติบุคคล แล้วคลิ๊กที่ปุ่มตรวจสอบ เพื่อให้ระบบดึงชื่อนายจ้างและที่อยู่สถานประกอบการ มาแสดงให้อัตโนมัติ"> 
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 pr-0">
                                            <div class="form-group pt-4">
                                               
                                                <button class="btn btn-major waves-effect waves-light" id="btnDBD" type="button" style="width: 100%;margin-top:3px;margin-right:0px;"><span class="btn-label"><!--<i class="fa fa-plus"></i>--> ตรวจสอบนายจ้าง </span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            

                            <div class="row pt-5">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label> {{  __('Establishment address') }}</label>
                                    </div>
                                </div>

                                
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="contactAddressId"> {{  __('Label Address') }} :</label>
                                        <input type="text" class="form-control" data-toggle="tooltip" data-bs-placement="top" title="หากทราบเลขนิติบุคคลของบริษัท ให้ทำการกรอกแล้วคลิ๊กที่ปุ่มตรวจสอบ ระบบจะแสดงข้อมูลให้อัตโนมัติ" id="contactAddressId" name="contactAddress" value="{{ $contactAddress }}" @if($contactAddress != null) readonly @endif>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="wProvince"> {{  __('Province') }} : <span class="danger-required">*</span> </label>
                                        <select class="custom-select form-control" data-toggle="tooltip" data-bs-placement="top" title="หากทราบเลขนิติบุคคลของบริษัท ให้ทำการกรอกแล้วคลิ๊กที่ปุ่มตรวจสอบ ระบบจะแสดงข้อมูลให้อัตโนมัติ" id="wProvince" name="province" required @if($provinceCode != null) disabled @endif>
                                            <option value="">กรุณาเลือก</option>
                                            @foreach ($Provinces as $Province)
                                                <option value="{{  $Province['ProvinceID'] }}"  @if($provinceCode == $Province['ProvinceID']) {{'selected="selected"'}} @endif>{{ $Province['ProvinceName'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="wDistrict"> {{  __('Label District') }} : <span class="danger-required">*</span> </label>
                                        <select class="custom-select form-control" data-toggle="tooltip" data-bs-placement="top" title="หากทราบเลขนิติบุคคลของบริษัท ให้ทำการกรอกแล้วคลิ๊กที่ปุ่มตรวจสอบ ระบบจะแสดงข้อมูลให้อัตโนมัติ" id="wDistrict" name="district" required @if($district != null) disabled @endif>
                                            <option value="">กรุณาเลือก</option>
                                                @if($district != null)
                                                    <option value="{{  $district['DistrictID'] }}"  {{'selected="selected"'}}>{{ $district['DistrictName'] }}</option>
                                                @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="wSubDistrict"> {{  __('Label Sub District') }} : </label>
                                        <select class="custom-select form-control" data-toggle="tooltip" data-bs-placement="top" title="หากทราบเลขนิติบุคคลของบริษัท ให้ทำการกรอกแล้วคลิ๊กที่ปุ่มตรวจสอบ ระบบจะแสดงข้อมูลให้อัตโนมัติ" id="wSubDistrict" name="subDistrict" @if($subDistrict != null) disabled @endif>
                                            <option value="">กรุณาเลือก</option>
                                            @if($subDistrict != null)
                                                <option value="{{  $subDistrict['TambonID'] }}"  {{'selected="selected"'}}>{{ $subDistrict['TambonName'] }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="wPostcode"> {{  __('Postcode') }} : </label>
                                        <input type="text" class="form-control" id="wPostcode" name="postcode" value="{{ $Postcode ?? '' }}"  @if($subDistrict != null || $subDistrict != "") readonly @endif>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="wPhoneNumber"> {{  __('Label Phone Number') }} :</label>
                                        <input type="text" class="form-control" id="wPhoneNumber" name="phoneNumber">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="wContactPerson"> {{  __('Label Contact Establishment') }} : </label>
                                        <input type="text" class="form-control" id="wContactPerson" name="contactPerson">
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>  
                </section>
                <!-- Step 2 -->
                <h6>{{  __('Apply for a job') }}</h6>
                <section>
                    <div class="card-body card-apply-job">
                        <h4 class="card-title">สมัครงาน</h4>

                        <div class="row col-lg-12">
                            @include('jobs.job_index')
                        </div> 
                                
                    </div>
                  

                    <div class="row col-12 ml-2">
                        <div class="form-check form-check-inline">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="ignoreJobId" name="ignoreJob" required>
                                <label class="custom-control-label" for="ignoreJobId" style="font-size: 18px;">ไม่ประสงค์จะสมัครงาน</label>
                            </div>
                        </div>
                    </div>

                    <div class="row col-12 ml-2 applying-jobs-table">
                    </div>
                    
                </section>
                <!-- Step 3 -->
                <h6>{{  __('Freelance') }}</h6>
                <section>

                    <div class="col-lg-12">
                      
                        <div class="form-check">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="haveFreelance" name="haveFreelance" onclick="showFreelance(this)">
                                <label class="custom-control-label" for="haveFreelance" style="font-size: 18px;">ปัจจุบัน ท่านประกอบอาชีพอิสระอยู่หรือไม่ ?</label>
                                <p class="text-danger">หมายเหตุ : เป็นการกรอกอาชีพอิสระที่คุณทำอยู่ (หากไม่มีสามารถกดบันทึกข้อมูลเพื่อรายงานตัว)</p>
                            </div>
                        </div>

                        <div id="freelance-area" style="display: none">
                            <button class="btn btn-major mb-2 ml-4" 
                                id="addBtn" type="button"> 
                                เพิ่มอาชีพอิสระ
                            </button> 
                            <div> 
                                <table class="table table-bordered" id="freelance-table" style="display: none"> 
                                  <thead> 
                                    <tr> 
                                      <th class="text-center">อาชีพอิสระ</th> 
                                      <th class="text-center">ระบุ (กรณีเลือก อื่นๆ)</th> 
                                      <th class="text-center"></th> 
                                    </tr> 
                                  </thead> 
                                    <tbody id="tbody"> 
                                        <tr id="R1">
                                            <td class="row-index text-center"> 
                                                <div class="form-group">
                                                    <select class="form-control custom-select required" data-id="1" id="wintType1" name="wintType[]" onchange="manageOther(this)" required>
                                                        <option value="">กรุณาเลือกอาชีพอิสระ</option>
                                                        @foreach ($Freelances as $Freelance)
                                                            <option value="{{$Freelance['FreelanceID']}}" other="{{$Freelance['Other']}}">{{$Freelance['FreelanceName']}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </td> 
                                            <td class="text-center"> 
                                                <div class="form-group" style="display:none" id="divwintT1">
                                                    <input type="text" class="form-control required" name="wint[]"> 
                                                </div>
                                            </td>
                                            <td class="text-center"> 
                                                <a class="delete-multiple text-danger remove" style="cursor: pointer;"><i class="fas fa-trash font-16 font-medium"></i></a>
                                                
                                            </td> 
                                        </tr>
                                    </tbody> 
                                </table> 
                              </div> 
                        </div>
                    
                    </div>

                </section>
                
            </form>
        </div>
    </div>
</div>
<input type="hidden" id="applyJobsTmp">
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/libs/jquery-steps/build/jquery.steps.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/libs/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/prism/prism.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/libs/moment/moment.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/libs/footable/js/footable.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/tables/footable-init.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/reporting.init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/jobs-filter.init.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/jquery.datetimepicker.full.js') }}"></script>

    <script>
    $(document).ready(function(){

        var workStatus = $('input[name="workStatus"]:checked').val();

        if (!workStatus) {
            $("#empInfomationId").hide();
        }

        var applyJobs = {!! json_encode($ApplyJobs)  !!};
        console.log("applyJobs = ",applyJobs);
        if(jQuery.isEmptyObject(applyJobs)) {
            //Show not apply job
            
        }

    });
    
    </script>
@endpush
