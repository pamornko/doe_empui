@extends('layout.master')

@push('styles')
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/extra-libs/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
@endpush

@section('topic-menu')
@endsection


@section('content')

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<!-- basic table -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">{{  __('Reporting') }}</h5>
                @if (!empty($Register['RegisterNumber']))
                    <h5 class="card-subtitle"><small>{{  __('Registration number') }}</small> : <code class="text-info"> {{ $Register['RegisterNumber'] ?? 'ไม่มีข้อมูลการขึ้นทะเบียนว่างงาน' }}</code></h5> 
                @endif
               
                <div class="table-responsive">
                    
                    <table id="reporting_table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>{{ __('Number of Time') }}</th>
                                <th>{{ __('Reporting Schedule') }}</th>
                                <th>{{ __('Table Head Reporting Date') }}</th>
                                <th>{{ __('Table Head Action') }}</th>
                                
                            </tr>
                        </thead>
                        <tbody style="<?=$bStyle?>">
                            @if (!empty($trackings))
                            @foreach ($trackings as $tracking)
                           
                            <tr style="<?=$bStyle?>">
                                <td class="text-center">{{ $tracking['TrackingTime'] }}</td>
                                <td>{{show_thai_date($tracking['ReportingDueDate']) }}  </td>
                                <td>
                                    <div title="{{ __('Click for details') }}">
                                       
                                    <a href="{{ URL::route('reporting-officer-view' , ['id'=>$tracking['TrackingID']]) }}"> @if(!empty($tracking['ReportingDate'])){{ show_thai_date($tracking['ReportingDate']) }} @endif</a>
                                    </div>
                                    
                                </td>
                                
                                <td>

                                    @if ($tracking['Active'] == true)
                                        <button class="btn btn-outline-success waves-effect waves-light" onclick="window.location='{{ URL::route('reporting-form' , 
                                            [
                                                'id'=>$RegisterID, 
                                                'trackingID'=>$tracking['TrackingID'], 
                                                'trackingTime' =>$tracking['TrackingTime']
                                            ]) }}'">
                                                <i class="fas fa-user"></i> {{ __('Print Paper Report') }}
                                        </button>
                                    @elseif ($tracking['ReportingDate'] != null)
                                        <i class="text-success mdi mdi-checkbox-blank-circle"></i> รายงานตัวสำเร็จ
                                    @else
                                        <i class="text-warning mdi mdi-checkbox-blank-circle"></i> ยังไม่ถึงกำหนดรายงานตัว
                                    @endif
                                    
                                </td>
                               
                            </tr>
                            @endforeach
                            @endif
                            
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection


@push('scripts')
    <!--This page JavaScript -->
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/extra-libs/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/datatable/datatable-basic.init.js') }}"></script>

    <script type="text/javascript">
        
    </script>
@endpush