<!DOCTYPE html>
<head>
   
</head>
<html>
<body>
    <div class="table-container">
        <div class="header">
            <link rel="stylesheet" href="{{ asset('css/print-style.css') }}">
            <h4><p class="tex-center">แบบรายงานผู้ประกันตนกรณีว่างงาน (จังหวัด)</p></h4>
           
        </div>
        <div style="width:100%;text-align: center;">
            <span class="topic"> {{ $Province ?? '' }}</span>
        </div>
        <div>
            <table width="100%">
                <thead>
                    <tr>
                        <th rowspan="2" style="width: 20%;">{{ __('Office/Zone') }}</th>
                        <th colspan="{{ count($Case) ?? 0 }}" style="width: 40%;">{{ __('Insured registration') }} (คน)</th>
                        <th colspan="{{ count($Tracking) ?? 0 }}" style="width: 30%;">{{ __('Reporting') }}ผู้ประกันตน</th>
                        <th colspan="{{ count($Job) ?? 0 }}" style="width: 30%;">{{ __('Reporting') }}บรรจุงาน</th>
                        <th rowspan="2" style="width: 10%;">หมายเหตุ</th>
                    </tr>
            
                    <tr>
                        @foreach ($Case as $key => $val)
                        <th style="text-align: center; width: 4%;">{{ $key }}</th>
                        @endforeach
                        @foreach ($Tracking as $key => $val)
                        <th style="text-align: center; width: 4%;">{{ $key }}</th>
                        @endforeach
                        @foreach ($Job as $key => $val)
                        <th style="text-align: center; width: 4%;">{{ $key }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach ($ReportList as $Report)
                    <tr>
                        <td style="text-align: center;width: 20%;">{{$Report['OrganizationName']}}</td>
                        @foreach ($Report['register'] as $value)
                            <td style="text-align: center; width: 4%;" > {{ $value }} </td>
                        @endforeach
                        @foreach ($Report['tracking'] as $value)
                            <td  style="text-align: center; width: 4%;"> {{ $value }} </td>
                        @endforeach
                        @foreach ($Report['job'] as $value)
                            <td style="text-align: center; width: 4%;"> {{ $value }} </td>
                        @endforeach
                        <td>{{ $Report['remark'] }}</td>
                    </tr>
                @endforeach
                </tbody>
                
            </table>
        </div>
    </div>

</body>
</html> 