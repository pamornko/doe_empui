<!DOCTYPE html>
<head>
   
</head>
<html>
<body>
    <div class="table-container">
        <div class="header">
            <link rel="stylesheet" href="{{ asset('css/print-style.css') }}">
            <h4><p class="tex-center">ข้อมูลรายงานตัว</p></h4>
           
        </div>
        <div>
            <table width="100%">
                <thead>
                    <tr>
                        <th rowspan="2" style="width: 5%">{{ __('No.') }}</th>
                        <th rowspan="2" style="width: 35%">{{ __('Office/Zone') }}</th>
                        <th colspan="3" style="width: 30%">{{ __('Insured registration') }}</th>
                        <th colspan="3" style="width: 30%">{{ __('Reporting') }}</th>
                        
                    </tr>
            
                    <tr>
                        <th style="width: 10%">{{ __('Total') }}</th>
                        <th style="width: 10%">{{ __('Male') }}</th>
                        <th style="width: 10%">{{ __('Female') }}</th>
                        <th style="width: 10%">{{ __('Total') }}</th>
                        <th style="width: 10%">{{ __('Male') }}</th>
                        <th style="width: 10%">{{ __('Female') }}</th>
                    </tr>
            
                </thead>
                <tbody>
                    @foreach ($ReportList as $Report)
                    <tr>
                        <td style="width: 5%">{{$loop->iteration}}</td>
                        <td style="width: 35%"> {{ $Report['OrganizationName'] ?? $Report['SSOOfficeName']  }}</td>
                        <td style="text-align: center;width: 10%">{{ $Report['register']['F'] + $Report['register']['M'] }}</td>
                        <td style="text-align: center; width: 10%">{{ $Report['register']['M']  }}</td>
                        <td style="text-align: center; width: 10%">{{ $Report['register']['F']  }}</td>
                        <td style="text-align: center; width: 10%">{{ $Report['tracking']['F'] + $Report['tracking']['M'] }}</td>
                        <td style="text-align: center; width: 10%">{{ $Report['tracking']['M']  }}</td>
                        <td style="text-align: center; width: 10%">{{ $Report['tracking']['F']  }}</td>
                    </tr>
                    @endforeach
                </tbody>
                
            </table>
        </div>
    </div>

</body>
</html> 