<!DOCTYPE html>
<head>
   
</head>
<html>
<body>
    <div class="table-container">
        <div class="header">
            <link rel="stylesheet" href="{{ asset('css/print-style.css') }}">
            <h4><p class="tex-center">ข้อมูลรายงานตัว</p></h4>
           
        </div>
        <div>
            <table width="100%">
                <thead>
                    <tr>
                        <th rowspan="2" style="width: 4%">{{ __('No.') }}</th>
                        <th rowspan="2" style="border-right: 1px solid black;">{{ __('Office/Zone') }}</th>
                        <th colspan="{{ count($Case) ?? 0 }}">{{ __('Insured registration') }} (คน)</th>
                        <th colspan="{{ count($Tracking) ?? 0 }}">{{ __('Reporting') }}ผู้ประกันตน</th>
                        <th colspan="{{ count($Job) ?? 0 }}">{{ __('Reporting') }}บรรจุงาน</th>
                        <th rowspan="2" style="border-left: 1px solid black;width: 4%;">หมายเหตุ</th>
                    </tr>
            
                    <tr>
                        @foreach ($Case as $key => $val)
                        <th style="width: 4%">{{ $key }}</th>
                        @endforeach
                        @foreach ($Tracking as $key => $val)
                        <th style="width: 4%">{{ $key }}</th>
                        @endforeach
                        @foreach ($Job as $key => $val)
                        <th style="width: 4%">{{ $key }}</th>
                        @endforeach
                    </tr>
            
                </thead>
                <tbody>
                    @foreach ($ReportList as $Report)
                    <tr>
                        <td colspan="2" style="border-right: 1px solid black;width: 25%;">{{$Report['RegionName']}}</td>
                        
                        
                        @foreach ($Report['register'] as $value)
                        <td style="text-align: center;width: 4%"> {{ $value }} </td>
                        @endforeach
                        @foreach ($Report['tracking'] as $value)
                        <td style="text-align: center;width: 4%"> {{ $value }} </td>
                        @endforeach
                        @foreach ($Report['job'] as $value)
                        <td style="text-align: center;width: 4%"> {{ $value }} </td>
                        @endforeach
                        <td style="border-left: 1px solid black;">{{ $Report['remark'] }}</td>
                    </tr>
                @endforeach
                @foreach ($ReportList as $Report)
                    <tr>
                        <td colspan="2" style="border-right: 1px solid black;width: 25%;">{{$Report['RegionName']}}</td>
                      
                       
                    </tr>
                    @foreach ($Report['Organization'] as $Org)
                    <tr>
                        <td style="width: 4%">{{$loop->iteration}}</td>
                        <td style="border-right: 1px solid black;width: 25%">{{$Org['OrganizationName']}}</td>
                        @foreach ($Org['register'] as $value)
                        <td  style="text-align: center;width: 4%"> {{ $value }} </td>
                        @endforeach
                        @foreach ($Org['tracking'] as $value)
                        <td  style="text-align: center;width: 4%"> {{ $value }} </td>
                        @endforeach
                        @foreach ($Org['job'] as $value)
                        <td  style="text-align: center;width: 4%"> {{ $value }} </td>
                        @endforeach
                        <td style="border-left: 1px solid black;">{{ $Org['remark'] }}</td>
                    </tr>
                    @endforeach
                @endforeach
                </tbody>
                
            </table>
        </div>
    </div>

</body>
</html> 