<!DOCTYPE html>
<head>
   
</head>
<html>
<body>
    <div class="table-container">
        <div class="header">
            <link rel="stylesheet" href="{{ asset('css/print-style.css') }}">
            <h4><p class="tex-center">ข้อมูลรายงานตัว</p></h4>
           
        </div>
        @php
            //dd($ReportingList);
        @endphp
        <div>
            <table width="100%">
                <thead>
                    <tr>
                        <th>{{ __('No.') }}</th>
                        <th>{{ __('Registration number') }}</th>
                        <th>{{ __('ID Card Number') }}</th>
                        <th>{{ __('Insured Person Name') }}</th>
                        <th> ครั้งที่รายงานตัว </th>
                        <th>กำหนดการรายงานตัว</th>
                        <th>วันที่รายงานตัว</th>
                    </tr>
                </thead>
                <tbody>
                    @if ($ReportingList)
                    @foreach ($ReportingList as $Reporting)
                        <tr>
                            <td style="text-align: right; padding-right:5px;">{{$loop->iteration}}.</td>
                            <td style="text-align: center;">{{ $Reporting['RegisterNumber'] ?? ''}}</td>
                            <td style="text-align: center;"> {{ $Reporting['PersonalID'] ?? ''}} </td>
                            <td> {{ $Reporting['FirstName'] ?? ''}} {{ $Reporting['LastName'] ?? ''}}</td>
                            <td style="text-align: center;"> {{ $Reporting['TrackingTime'] ?? ''}} </td>
                            <td style="text-align: center;">{{ \Carbon\Carbon::parse($Reporting['ReportingDueDate'])->format('d/m/Y') }}</td>
                            <td style="text-align: center;">{{ \Carbon\Carbon::parse($Reporting['ReportingDate'])->format('d/m/Y') }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
                
            </table>
        </div>
    </div>

</body>
</html> 