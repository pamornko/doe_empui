<!DOCTYPE html>
<head>
   
</head>
<html>
<body>
    <div class="table-container">
        <div class="header">
            <link rel="stylesheet" href="{{ asset('css/print-style.css') }}">
            <h4><p class="tex-center">ข้อมูลขึ้นทะเบียนว่างงาน</p></h4>
           
        </div>
        @php
            //dd($ReportingList);
        @endphp
        <div>
            <table width="100%">
                <thead>
                    <tr>
                        <th style="width: 5%">{{ __('No.') }}</th>
                        <th style="width: 25%">{{ __('ID Card Number') }}</th>
                        <th style="width: 25%">{{ __('Registration number') }}</th>
                        <th style="width: 25%">{{ __('Insured Person Name') }}</th>
                        <th style="width: 20%">{{ __('Registration Date') }}</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @if ($RegisterList)
                    @foreach ($RegisterList as $Register)
                        <tr>
                            <td style="width: 5%">{{$loop->iteration}}</td>
                            <td style="width: 25%">{{ $Register['PersonalID'] }}</td>
                            <td style="width: 25%">{{ $Register['RegisterNumber'] }}</td>
                            <td style="width: 25%">{{ $Register['FirstName'] }} {{ $Register['LastName'] }}</td>
                            <td style="width: 20%">{{ \Carbon\Carbon::parse($Register['RegisterDate'])->format('d/m/Y') }}</td>
                            
                        </tr>
                    @endforeach
                    @endif
                </tbody>
                
            </table>
        </div>
    </div>

</body>
</html> 