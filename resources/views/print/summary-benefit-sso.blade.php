<!DOCTYPE html>
<head>
   
</head>
<html>
<body>
    <div class="table-container">
        <div class="header">
            <link rel="stylesheet" href="{{ asset('css/print-style.css') }}">
            <h4><p style="width:100%;text-align: center;">สรุปผลการให้บริการผู้ประกันตนขอรับประโยชน์ทดแทนกรณีว่างงาน</p></h4>
           
        </div>
       
        <div>
            <table width="100%">
                <thead>
                    <tr>
                        <th rowspan="2">{{ __('No.') }}</th>
                        <th rowspan="2">{{ __('Office/Zone') }}</th>
                        <th colspan="3">{{ __('Insured registration') }}</th>
                        <th colspan="3">{{ __('Reporting') }}</th>
                        
                    </tr>
            
                    <tr>
                        <th>{{ __('Total') }}</th>
                        <th>{{ __('Male') }}</th>
                        <th>{{ __('Female') }}</th>
                        <th>{{ __('Total') }}</th>
                        <th>{{ __('Male') }}</th>
                        <th>{{ __('Female') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($ReportList as $Report)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $Report['OrganizationName'] ?? $Report['SSOOfficeName']  }}</td>
                        <td class="text-center">{{ $Report['register']['F'] + $Report['register']['M'] }}</td>
                        <td class="text-center">{{ $Report['register']['F']  }}</td>
                        <td class="text-center">{{ $Report['register']['M']  }}</td>
                        <td class="text-center">{{ $Report['tracking']['F'] + $Report['tracking']['M'] }}</td>
                        <td class="text-center">{{ $Report['tracking']['F']  }}</td>
                        <td class="text-center">{{ $Report['tracking']['M']  }}</td>
                        
                    </tr>
                @endforeach
                </tbody>
                
            </table>
        </div>
    </div>

</body>
</html> 